#include <assert.h>
#include <x86intrin.h>
#include <stdio.h>

#ifdef AVX512

void microk_2_12_mask_store(float *const __restrict__ output,
                            float const *const __restrict__ input, float const *const __restrict__ params,
                            int Y,
                            int strF_o, int strY_o, int strX_o,
                            int strC_in, int strY_in, int strX_in,
                            int strF_par, int strC_par, int strH_par, int strW_par);

void microk_2_12_mask3_fmadd(float *const __restrict__ output,
                             float const *const __restrict__ input, float const *const __restrict__ params,
                             int Y,
                             int strC_in, int strY_in, int strX_in,
                             int strF_o, int strY_o, int strX_o,
                             int strF_par, int strC_par, int strH_par, int strW_par);

void microk_2_12_mask_store_cin_param(float *const __restrict__ output,
                                      float const *const __restrict__ input, float const *const __restrict__ params,
                                      int Y,
                                      int C,
                                      int strF_o, int strY_o, int strX_o,
                                      int strC_in, int strY_in, int strX_in,
                                      int strF_par, int strC_par, int strH_par, int strW_par);

void microk_2_12_mask3_fmadd_cin_param(float *const __restrict__ output,
                                       float const *const __restrict__ input, float const *const __restrict__ params,
                                       int Y,
                                       int C,
                                       int strC_in, int strY_in, int strX_in,
                                       int strF_o, int strY_o, int strX_o,
                                       int strF_par, int strC_par, int strH_par, int strW_par);

void microk_2_12_mask_store_cin_param_stride2(float *const __restrict__ output,
                                              float const *const __restrict__ input, float const *const __restrict__ params,
                                              int Y,
                                              int C,
                                              int strF_o, int strY_o, int strX_o,
                                              int strC_in, int strY_in, int strX_in,
                                              int strF_par, int strC_par, int strH_par, int strW_par);

void microk_2_12_mask_store(float *const __restrict__ output,
                            float const *const __restrict__ input, float const *const __restrict__ params,
                            int Y,
                            int strF_o, int strY_o, int strX_o,
                            int strC_in, int strY_in, int strX_in,
                            int strF_par, int strC_par, int strH_par, int strW_par)
{
  /*
     [V f; U (2, f); U (12, y); T (256, c); Hoist_vars [c]]
     */
  int c, cp_0;

  assert((Y <= 12));
  int y = 0;
  int x = 0;
  int h = 0;
  int w = 0;
  int c0 = 0;
  int f = 0;
  float scal_0, scal_1, scal_10, scal_11, scal_2, scal_3, scal_4, scal_5, scal_6, scal_7, scal_8, scal_9;
  __m512 mem_vec_0, mem_vec_1, mem_vec_10, mem_vec_11, mem_vec_12, mem_vec_13, mem_vec_14, mem_vec_15, mem_vec_16, mem_vec_17, mem_vec_18, mem_vec_19, mem_vec_2, mem_vec_20, mem_vec_21, mem_vec_22, mem_vec_23, mem_vec_3, mem_vec_4, mem_vec_5, mem_vec_6, mem_vec_7, mem_vec_8, mem_vec_9, vec_0, vec_1, vec_10, vec_11, vec_12, vec_13, vec_14, vec_15, vec_16, vec_17, vec_18, vec_19, vec_2, vec_20, vec_21, vec_22, vec_23, vec_24, vec_25, vec_26, vec_27, vec_28, vec_29, vec_3, vec_30, vec_31, vec_32, vec_33, vec_34, vec_35, vec_36, vec_37, vec_4, vec_5, vec_6, vec_7, vec_8, vec_9;

  // for ( int i = 0; i < Y % 12; i++) {
  // }
  //  mask_fma : __m512 _mm512_mask_fmadd_ps (__m512 a, __mmask16 k, __m512 b, __m512 c)
  //  FOR j := 0 to 15
  //	i := j*32
  //	IF k[j]
  //		dst[i+31:i] := (a[i+31:i] * b[i+31:i]) + c[i+31:i]
  //	ELSE
  //		dst[i+31:i] := a[i+31:i]
  //	FI
  // ENDFOR
  // dst[MAX:512] := 0

  __mmask16 mask_zeros = _cvtu32_mask16(0);
  __mmask16 mask_ones = _cvtu32_mask16((1 << 30) - 1);
  __mmask16 mask0 = (Y >= 1) ? mask_ones : mask_zeros;
  __mmask16 mask1 = (Y >= 2) ? mask_ones : mask_zeros;
  __mmask16 mask2 = (Y >= 3) ? mask_ones : mask_zeros;
  __mmask16 mask3 = (Y >= 4) ? mask_ones : mask_zeros;
  __mmask16 mask4 = (Y >= 5) ? mask_ones : mask_zeros;
  __mmask16 mask5 = (Y >= 6) ? mask_ones : mask_zeros;
  __mmask16 mask6 = (Y >= 7) ? mask_ones : mask_zeros;
  __mmask16 mask7 = (Y >= 8) ? mask_ones : mask_zeros;
  __mmask16 mask8 = (Y >= 9) ? mask_ones : mask_zeros;
  __mmask16 mask9 = (Y >= 10) ? mask_ones : mask_zeros;
  __mmask16 mask10 = (Y >= 11) ? mask_ones : mask_zeros;
  __mmask16 mask11 = (Y >= 12) ? mask_ones : mask_zeros;

  mem_vec_0 = _mm512_load_ps(&output[strX_o * x + strY_o * y + f]);
  mem_vec_1 = _mm512_load_ps(&output[strX_o * x + strY_o * y + f + 16]);
  mem_vec_2 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 1) + f]);
  mem_vec_3 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16]);
  mem_vec_4 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 2) + f]);
  mem_vec_5 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16]);
  mem_vec_6 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 3) + f]);
  mem_vec_7 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16]);
  mem_vec_8 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 4) + f]);
  mem_vec_9 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16]);
  mem_vec_10 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 5) + f]);
  mem_vec_11 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16]);
  mem_vec_12 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 6) + f]);
  mem_vec_13 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16]);
  mem_vec_14 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 7) + f]);
  mem_vec_15 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16]);
  mem_vec_16 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 8) + f]);
  mem_vec_17 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16]);
  mem_vec_18 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 9) + f]);
  mem_vec_19 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16]);
  mem_vec_20 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 10) + f]);
  mem_vec_21 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16]);
  mem_vec_22 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 11) + f]);
  mem_vec_23 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16]);
  // y = 12, x = 1, h = 1, w = 1, c = 256, f = 32
  // T (c, 256) (256 / 1)
  for (c = c0, cp_0 = 0;
       c < c0 + 256;
       c += 1, cp_0 += 1)
  {
    scal_0 = input[(strX_in * strY_in) * (x + w) + strY_in * (y + h) + c];
    vec_1 = _mm512_set1_ps(scal_0);
    vec_2 = _mm512_load_ps(&params[strW_par * w + strH_par * h + strC_par * c + f]);

    vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_0);
    mem_vec_0 = vec_0;

    vec_4 = _mm512_load_ps(&params[strW_par * w + strH_par * h + strC_par * c + f + 16]);

    // vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
    vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
    mem_vec_1 = vec_3;
    scal_1 = input[strX_in * (x + w) + strY_in * (y + 1 + h) + c];
    vec_6 = _mm512_set1_ps(scal_1);

    vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_2);
    mem_vec_2 = vec_5;

    // vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
    vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
    mem_vec_3 = vec_7;
    scal_2 = input[strX_in * (x + w) + strY_in * (y + 2 + h) + c];
    vec_9 = _mm512_set1_ps(scal_2);

    // vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
    vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
    mem_vec_4 = vec_8;

    // vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
    vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
    mem_vec_5 = vec_10;
    scal_3 = input[strX_in * (x + w) + strY_in * (y + 3 + h) + c];
    vec_12 = _mm512_set1_ps(scal_3);

    // vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
    vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
    mem_vec_6 = vec_11;

    // vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
    vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
    mem_vec_7 = vec_13;
    scal_4 = input[strX_in * (x + w) + strY_in * (y + 4 + h) + c];
    vec_15 = _mm512_set1_ps(scal_4);

    // vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
    vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
    mem_vec_8 = vec_14;

    // vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
    vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
    mem_vec_9 = vec_16;
    scal_5 = input[strX_in * (x + w) + strY_in * (y + 5 + h) + c];
    vec_18 = _mm512_set1_ps(scal_5);

    // vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
    vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
    mem_vec_10 = vec_17;

    // vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
    vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
    mem_vec_11 = vec_19;
    scal_6 = input[strX_in * (x + w) + strY_in * (y + 6 + h) + c];
    vec_21 = _mm512_set1_ps(scal_6);

    // vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_12);
    vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_12);
    mem_vec_12 = vec_20;

    // vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_13);
    vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_13);
    mem_vec_13 = vec_22;
    scal_7 = input[strX_in * (x + w) + strY_in * (y + 7 + h) + c];
    vec_24 = _mm512_set1_ps(scal_7);

    // vec_23 = _mm512_mask_fmadd_ps(vec_24, vec_2, mem_vec_14);
    vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_14);
    mem_vec_14 = vec_23;

    // vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_15);
    vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_15);
    mem_vec_15 = vec_25;
    scal_8 = input[strX_in * (x + w) + strY_in * (y + 8 + h) + c];
    vec_27 = _mm512_set1_ps(scal_8);

    // vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_16);
    vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_16);
    mem_vec_16 = vec_26;

    // vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_17);
    vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_17);
    mem_vec_17 = vec_28;
    scal_9 = input[strX_in * (x + w) + strY_in * (y + 9 + h) + c];
    vec_30 = _mm512_set1_ps(scal_9);

    // vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_18);
    vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_18);
    mem_vec_18 = vec_29;

    // vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_19);
    vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_19);
    mem_vec_19 = vec_31;
    scal_10 = input[strX_in * (x + w) + strY_in * (y + 10 + h) + c];
    vec_33 = _mm512_set1_ps(scal_10);

    // vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_20);
    vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_20);
    mem_vec_20 = vec_32;

    // vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_21);
    vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_21);
    mem_vec_21 = vec_34;
    scal_11 = input[strX_in * (x + w) + strY_in * (y + 11 + h) + c];
    vec_36 = _mm512_set1_ps(scal_11);

    // vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_22);
    vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_22);
    mem_vec_22 = vec_35;

    // vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_23);
    vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_23);
    mem_vec_23 = vec_37;
  }
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * y + f], mask0, mem_vec_0);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * y + f + 16], mask0, mem_vec_1);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 1) + f], mask1, mem_vec_2);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16], mask1, mem_vec_3);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 2) + f], mask2, mem_vec_4);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16], mask2, mem_vec_5);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 3) + f], mask3, mem_vec_6);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16], mask3, mem_vec_7);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 4) + f], mask4, mem_vec_8);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16], mask4, mem_vec_9);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 5) + f], mask5, mem_vec_10);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16], mask5, mem_vec_11);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 6) + f], mask6, mem_vec_12);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16], mask6, mem_vec_13);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 7) + f], mask7, mem_vec_14);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16], mask7, mem_vec_15);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 8) + f], mask8, mem_vec_16);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16], mask8, mem_vec_17);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 9) + f], mask9, mem_vec_18);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16], mask9, mem_vec_19);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 10) + f], mask10, mem_vec_20);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16], mask10, mem_vec_21);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 11) + f], mask11, mem_vec_22);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16], mask11, mem_vec_23);
}

void microk_2_12_mask3_fmadd(float *const __restrict__ output,
                             float const *const __restrict__ input, float const *const __restrict__ params,
                             int Y,
                             int strC_in, int strY_in, int strX_in,
                             int strF_o, int strY_o, int strX_o,
                             int strF_par, int strC_par, int strH_par, int strW_par)
{
  /*
     [V f; U (2, f); U (12, y); T (256, c); Hoist_vars [c]]
     */
  int c, cp_0;

  assert((Y <= 12));
  int y = 0;
  int x = 0;
  int h = 0;
  int w = 0;
  int c0 = 0;
  int f = 0;
  float scal_0, scal_1, scal_10, scal_11, scal_2, scal_3, scal_4, scal_5, scal_6, scal_7, scal_8, scal_9;
  __m512 mem_vec_0, mem_vec_1, mem_vec_10, mem_vec_11, mem_vec_12, mem_vec_13, mem_vec_14, mem_vec_15, mem_vec_16, mem_vec_17, mem_vec_18, mem_vec_19, mem_vec_2, mem_vec_20, mem_vec_21, mem_vec_22, mem_vec_23, mem_vec_3, mem_vec_4, mem_vec_5, mem_vec_6, mem_vec_7, mem_vec_8, mem_vec_9, vec_0, vec_1, vec_10, vec_11, vec_12, vec_13, vec_14, vec_15, vec_16, vec_17, vec_18, vec_19, vec_2, vec_20, vec_21, vec_22, vec_23, vec_24, vec_25, vec_26, vec_27, vec_28, vec_29, vec_3, vec_30, vec_31, vec_32, vec_33, vec_34, vec_35, vec_36, vec_37, vec_4, vec_5, vec_6, vec_7, vec_8, vec_9;

  // for ( int i = 0; i < Y % 12; i++) {
  // }
  //  mask_fma : __m512 _mm512_mask_fmadd_ps (__m512 a, __mmask16 k, __m512 b, __m512 c)
  //  FOR j := 0 to 15
  //	i := j*32
  //	IF k[j]
  //		dst[i+31:i] := (a[i+31:i] * b[i+31:i]) + c[i+31:i]
  //	ELSE
  //		dst[i+31:i] := a[i+31:i]
  //	FI
  // ENDFOR
  // dst[MAX:512] := 0

  __mmask16 mask_zeros = _cvtu32_mask16(0);
  __mmask16 mask_ones = _cvtu32_mask16((1 << 30) - 1);
  __mmask16 mask0 = (Y >= 1) ? mask_ones : mask_zeros;
  __mmask16 mask1 = (Y >= 2) ? mask_ones : mask_zeros;
  __mmask16 mask2 = (Y >= 3) ? mask_ones : mask_zeros;
  __mmask16 mask3 = (Y >= 4) ? mask_ones : mask_zeros;
  __mmask16 mask4 = (Y >= 5) ? mask_ones : mask_zeros;
  __mmask16 mask5 = (Y >= 6) ? mask_ones : mask_zeros;
  __mmask16 mask6 = (Y >= 7) ? mask_ones : mask_zeros;
  __mmask16 mask7 = (Y >= 8) ? mask_ones : mask_zeros;
  __mmask16 mask8 = (Y >= 9) ? mask_ones : mask_zeros;
  __mmask16 mask9 = (Y >= 10) ? mask_ones : mask_zeros;
  __mmask16 mask10 = (Y >= 11) ? mask_ones : mask_zeros;
  __mmask16 mask11 = (Y >= 12) ? mask_ones : mask_zeros;

  mem_vec_0 = _mm512_load_ps(&output[(strX_o)*x + strY_o * y + f]);
  mem_vec_1 = _mm512_load_ps(&output[(strX_o)*x + strY_o * y + f + 16]);
  mem_vec_2 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 1) + f]);
  mem_vec_3 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 1) + f + 16]);
  mem_vec_4 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 2) + f]);
  mem_vec_5 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 2) + f + 16]);
  mem_vec_6 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 3) + f]);
  mem_vec_7 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 3) + f + 16]);
  mem_vec_8 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 4) + f]);
  mem_vec_9 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 4) + f + 16]);
  mem_vec_10 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 5) + f]);
  mem_vec_11 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 5) + f + 16]);
  mem_vec_12 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 6) + f]);
  mem_vec_13 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 6) + f + 16]);
  mem_vec_14 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 7) + f]);
  mem_vec_15 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 7) + f + 16]);
  mem_vec_16 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 8) + f]);
  mem_vec_17 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 8) + f + 16]);
  mem_vec_18 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 9) + f]);
  mem_vec_19 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 9) + f + 16]);
  mem_vec_20 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 10) + f]);
  mem_vec_21 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 10) + f + 16]);
  mem_vec_22 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 11) + f]);
  mem_vec_23 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 11) + f + 16]);
  // y = 12, x = 1, h = 1, w = 1, c = 256, f = 32
  // T (c, 256) (256 / 1)
  for (c = c0, cp_0 = 0;
       c < c0 + 256;
       c += 1, cp_0 += 1)
  {
    scal_0 = input[strX_in * (x + w) + strY_in * (y + h) + c];
    vec_1 = _mm512_set1_ps(scal_0);
    vec_2 = _mm512_load_ps(&params[strW_par * w + strH_par * h + strC_par * c + f]);

    vec_0 = _mm512_mask3_fmadd_ps(vec_1, vec_2, mem_vec_0, mask0);
    mem_vec_0 = vec_0;

    vec_4 = _mm512_load_ps(&params[strW_par * w + strH_par * h + strC_par * c + f + 16]);

    // vec_3 = _mm512_mask3_fmadd_ps(vec_1, vec_4, mem_vec_1);
    vec_3 = _mm512_mask3_fmadd_ps(vec_1, vec_4, mem_vec_1, mask0);
    mem_vec_1 = vec_3;
    scal_1 = input[strX_in * (x + w) + strY_in * (y + 1 + h) + c];
    vec_6 = _mm512_set1_ps(scal_1);

    vec_5 = _mm512_mask3_fmadd_ps(vec_6, vec_2, mem_vec_2, mask1);
    mem_vec_2 = vec_5;

    // vec_7 = _mm512_mask3_fmadd_ps(vec_6, vec_4, mem_vec_3);
    vec_7 = _mm512_mask3_fmadd_ps(vec_6, vec_4, mem_vec_3, mask1);
    mem_vec_3 = vec_7;
    scal_2 = input[strX_in * (x + w) + strY_in * (y + 2 + h) + c];
    vec_9 = _mm512_set1_ps(scal_2);

    // vec_8 = _mm512_mask3_fmadd_ps(vec_9, vec_2, mem_vec_4);
    vec_8 = _mm512_mask3_fmadd_ps(vec_9, vec_2, mem_vec_4, mask2);
    mem_vec_4 = vec_8;

    // vec_10 = _mm512_mask3_fmadd_ps(vec_9, vec_4, mem_vec_5);
    vec_10 = _mm512_mask3_fmadd_ps(vec_9, vec_4, mem_vec_5, mask2);
    mem_vec_5 = vec_10;
    scal_3 = input[strX_in * (x + w) + strY_in * (y + 3 + h) + c];
    vec_12 = _mm512_set1_ps(scal_3);

    // vec_11 = _mm512_mask3_fmadd_ps(vec_12, vec_2, mem_vec_6);
    vec_11 = _mm512_mask3_fmadd_ps(vec_12, vec_2, mem_vec_6, mask3);
    mem_vec_6 = vec_11;

    // vec_13 = _mm512_mask3_fmadd_ps(vec_12, vec_4, mem_vec_7);
    vec_13 = _mm512_mask3_fmadd_ps(vec_12, vec_4, mem_vec_7, mask3);
    mem_vec_7 = vec_13;
    scal_4 = input[strX_in * (x + w) + strY_in * (y + 4 + h) + c];
    vec_15 = _mm512_set1_ps(scal_4);

    // vec_14 = _mm512_mask3_fmadd_ps(vec_15, vec_2, mem_vec_8);
    vec_14 = _mm512_mask3_fmadd_ps(vec_15, vec_2, mem_vec_8, mask4);
    mem_vec_8 = vec_14;

    // vec_16 = _mm512_mask3_fmadd_ps(vec_15, vec_4, mem_vec_9);
    vec_16 = _mm512_mask3_fmadd_ps(vec_15, vec_4, mem_vec_9, mask4);
    mem_vec_9 = vec_16;
    scal_5 = input[strX_in * (x + w) + strY_in * (y + 5 + h) + c];
    vec_18 = _mm512_set1_ps(scal_5);

    // vec_17 = _mm512_mask3_fmadd_ps(vec_18, vec_2, mem_vec_10);
    vec_17 = _mm512_mask3_fmadd_ps(vec_18, vec_2, mem_vec_10, mask5);
    mem_vec_10 = vec_17;

    // vec_19 = _mm512_mask3_fmadd_ps(vec_18, vec_4, mem_vec_11);
    vec_19 = _mm512_mask3_fmadd_ps(vec_18, vec_4, mem_vec_11, mask5);
    mem_vec_11 = vec_19;
    scal_6 = input[strX_in * (x + w) + strY_in * (y + 6 + h) + c];
    vec_21 = _mm512_set1_ps(scal_6);

    // vec_20 = _mm512_mask3_fmadd_ps(vec_21, vec_2, mem_vec_12);
    vec_20 = _mm512_mask3_fmadd_ps(vec_21, vec_2, mem_vec_12, mask6);
    mem_vec_12 = vec_20;

    // vec_22 = _mm512_mask3_fmadd_ps(vec_21, vec_4, mem_vec_13);
    vec_22 = _mm512_mask3_fmadd_ps(vec_21, vec_4, mem_vec_13, mask6);
    mem_vec_13 = vec_22;
    scal_7 = input[strX_in * (x + w) + strY_in * (y + 7 + h) + c];
    vec_24 = _mm512_set1_ps(scal_7);

    // vec_23 = _mm512_mask_mask3_fmadd_ps(vec_24, vec_2, mem_vec_14);
    vec_23 = _mm512_mask3_fmadd_ps(vec_24, vec_2, mem_vec_14, mask7);
    mem_vec_14 = vec_23;

    // vec_25 = _mm512_mask3_fmadd_ps(vec_24, vec_4, mem_vec_15);
    vec_25 = _mm512_mask3_fmadd_ps(vec_24, vec_4, mem_vec_15, mask7);
    mem_vec_15 = vec_25;
    scal_8 = input[strX_in * (x + w) + strY_in * (y + 8 + h) + c];
    vec_27 = _mm512_set1_ps(scal_8);

    // vec_26 = _mm512_mask3_fmadd_ps(vec_27, vec_2, mem_vec_16);
    vec_26 = _mm512_mask3_fmadd_ps(vec_27, vec_2, mem_vec_16, mask8);
    mem_vec_16 = vec_26;

    // vec_28 = _mm512_mask3_fmadd_ps(vec_27, vec_4, mem_vec_17);
    vec_28 = _mm512_mask3_fmadd_ps(vec_27, vec_4, mem_vec_17, mask8);
    mem_vec_17 = vec_28;
    scal_9 = input[strX_in * (x + w) + strY_in * (y + 9 + h) + c];
    vec_30 = _mm512_set1_ps(scal_9);

    // vec_29 = _mm512_mask3_fmadd_ps(vec_30, vec_2, mem_vec_18);
    vec_29 = _mm512_mask3_fmadd_ps(vec_30, vec_2, mem_vec_18, mask9);
    mem_vec_18 = vec_29;

    // vec_31 = _mm512_mask3_fmadd_ps(vec_30, vec_4, mem_vec_19);
    vec_31 = _mm512_mask3_fmadd_ps(vec_30, vec_4, mem_vec_19, mask9);
    mem_vec_19 = vec_31;
    scal_10 = input[strX_in * (x + w) + strY_in * (y + 10 + h) + c];
    vec_33 = _mm512_set1_ps(scal_10);

    // vec_32 = _mm512_mask3_fmadd_ps(vec_33, vec_2, mem_vec_20);
    vec_32 = _mm512_mask3_fmadd_ps(vec_33, vec_2, mem_vec_20, mask10);
    mem_vec_20 = vec_32;

    // vec_34 = _mm512_mask3_fmadd_ps(vec_33, vec_4, mem_vec_21);
    vec_34 = _mm512_mask3_fmadd_ps(vec_33, vec_4, mem_vec_21, mask10);
    mem_vec_21 = vec_34;
    scal_11 = input[strX_in * (x + w) + strY_in * (y + 11 + h) + c];
    vec_36 = _mm512_set1_ps(scal_11);

    // vec_35 = _mm512_mask3_fmadd_ps(vec_36, vec_2, mem_vec_22);
    vec_35 = _mm512_mask3_fmadd_ps(vec_36, vec_2, mem_vec_22, mask11);
    mem_vec_22 = vec_35;

    // vec_37 = _mm512_mask3_fmadd_ps(vec_36, vec_4, mem_vec_23);
    vec_37 = _mm512_mask3_fmadd_ps(vec_36, vec_4, mem_vec_23, mask11);
    mem_vec_23 = vec_37;
  }
  _mm512_store_ps(&output[strX_o * x + strY_o * y + f], mem_vec_0);
  _mm512_store_ps(&output[strX_o * x + strY_o * y + f + 16], mem_vec_1);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 1) + f], mem_vec_2);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16], mem_vec_3);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 2) + f], mem_vec_4);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16], mem_vec_5);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 3) + f], mem_vec_6);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16], mem_vec_7);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 4) + f], mem_vec_8);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16], mem_vec_9);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 5) + f], mem_vec_10);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16], mem_vec_11);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 6) + f], mem_vec_12);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16], mem_vec_13);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 7) + f], mem_vec_14);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16], mem_vec_15);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 8) + f], mem_vec_16);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16], mem_vec_17);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 9) + f], mem_vec_18);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16], mem_vec_19);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 10) + f], mem_vec_20);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16], mem_vec_21);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 11) + f], mem_vec_22);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16], mem_vec_23);
}

void microk_2_12_mask_store_cin_param(float *const __restrict__ output,
                                      float const *const __restrict__ input, float const *const __restrict__ params,
                                      int Y,
                                      int C,
                                      int strF_o, int strY_o, int strX_o,
                                      int strC_in, int strY_in, int strX_in,
                                      int strF_par, int strC_par, int strH_par, int strW_par)
{
  /*
     [V f; U (2, f); U (12, y); T (256, c); Hoist_vars [c]]
     */
  int c, cp_0;

  assert((Y <= 12));
  int y = 0;
  int x = 0;
  int h = 0;
  int w = 0;
  int c0 = 0;
  int f = 0;
  float scal_0, scal_1, scal_10, scal_11, scal_2, scal_3, scal_4, scal_5, scal_6, scal_7, scal_8, scal_9;
  __m512 mem_vec_0, mem_vec_1, mem_vec_10, mem_vec_11, mem_vec_12, mem_vec_13, mem_vec_14, mem_vec_15, mem_vec_16, mem_vec_17, mem_vec_18, mem_vec_19, mem_vec_2, mem_vec_20, mem_vec_21, mem_vec_22, mem_vec_23, mem_vec_3, mem_vec_4, mem_vec_5, mem_vec_6, mem_vec_7, mem_vec_8, mem_vec_9, vec_0, vec_1, vec_10, vec_11, vec_12, vec_13, vec_14, vec_15, vec_16, vec_17, vec_18, vec_19, vec_2, vec_20, vec_21, vec_22, vec_23, vec_24, vec_25, vec_26, vec_27, vec_28, vec_29, vec_3, vec_30, vec_31, vec_32, vec_33, vec_34, vec_35, vec_36, vec_37, vec_4, vec_5, vec_6, vec_7, vec_8, vec_9;

  // for ( int i = 0; i < Y % 12; i++) {
  // }
  //  mask_fma : __m512 _mm512_mask_fmadd_ps (__m512 a, __mmask16 k, __m512 b, __m512 c)
  //  FOR j := 0 to 15
  //	i := j*32
  //	IF k[j]
  //		dst[i+31:i] := (a[i+31:i] * b[i+31:i]) + c[i+31:i]
  //	ELSE
  //		dst[i+31:i] := a[i+31:i]
  //	FI
  // ENDFOR
  // dst[MAX:512] := 0

  __mmask16 mask_zeros = _cvtu32_mask16(0);
  __mmask16 mask_ones = _cvtu32_mask16((1 << 30) - 1);
  __mmask16 mask0 = (Y >= 1) ? mask_ones : mask_zeros;
  __mmask16 mask1 = (Y >= 2) ? mask_ones : mask_zeros;
  __mmask16 mask2 = (Y >= 3) ? mask_ones : mask_zeros;
  __mmask16 mask3 = (Y >= 4) ? mask_ones : mask_zeros;
  __mmask16 mask4 = (Y >= 5) ? mask_ones : mask_zeros;
  __mmask16 mask5 = (Y >= 6) ? mask_ones : mask_zeros;
  __mmask16 mask6 = (Y >= 7) ? mask_ones : mask_zeros;
  __mmask16 mask7 = (Y >= 8) ? mask_ones : mask_zeros;
  __mmask16 mask8 = (Y >= 9) ? mask_ones : mask_zeros;
  __mmask16 mask9 = (Y >= 10) ? mask_ones : mask_zeros;
  __mmask16 mask10 = (Y >= 11) ? mask_ones : mask_zeros;
  __mmask16 mask11 = (Y >= 12) ? mask_ones : mask_zeros;

  mem_vec_0 = _mm512_load_ps(&output[strX_o * x + strY_o * y + f]);
  mem_vec_1 = _mm512_load_ps(&output[strX_o * x + strY_o * y + f + 16]);
  mem_vec_2 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 1) + f]);
  mem_vec_3 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16]);
  mem_vec_4 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 2) + f]);
  mem_vec_5 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16]);
  mem_vec_6 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 3) + f]);
  mem_vec_7 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16]);
  mem_vec_8 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 4) + f]);
  mem_vec_9 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16]);
  mem_vec_10 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 5) + f]);
  mem_vec_11 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16]);
  mem_vec_12 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 6) + f]);
  mem_vec_13 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16]);
  mem_vec_14 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 7) + f]);
  mem_vec_15 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16]);
  mem_vec_16 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 8) + f]);
  mem_vec_17 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16]);
  mem_vec_18 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 9) + f]);
  mem_vec_19 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16]);
  mem_vec_20 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 10) + f]);
  mem_vec_21 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16]);
  mem_vec_22 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 11) + f]);
  mem_vec_23 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16]);
  // y = 12, x = 1, h = 1, w = 1, c = 256, f = 32
  // T (c, 256) (256 / 1)
  for (c = c0, cp_0 = 0;
       c < c0 + C;
       c += 1, cp_0 += 1)
  {
    scal_0 = input[(strX_in * strY_in) * (x + w) + strY_in * (y + h) + c];
    vec_1 = _mm512_set1_ps(scal_0);
    vec_2 = _mm512_loadu_ps(&params[strW_par * w + strH_par * h + strC_par * c + f]);

    vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_0);
    mem_vec_0 = vec_0;

    vec_4 = _mm512_loadu_ps(&params[strW_par * w + strH_par * h + strC_par * c + f + 16]);

    // vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
    vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
    mem_vec_1 = vec_3;
    scal_1 = input[strX_in * (x + w) + strY_in * (y + 1 + h) + c];
    vec_6 = _mm512_set1_ps(scal_1);

    vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_2);
    mem_vec_2 = vec_5;

    // vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
    vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
    mem_vec_3 = vec_7;
    scal_2 = input[strX_in * (x + w) + strY_in * (y + 2 + h) + c];
    vec_9 = _mm512_set1_ps(scal_2);

    // vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
    vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
    mem_vec_4 = vec_8;

    // vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
    vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
    mem_vec_5 = vec_10;
    scal_3 = input[strX_in * (x + w) + strY_in * (y + 3 + h) + c];
    vec_12 = _mm512_set1_ps(scal_3);

    // vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
    vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
    mem_vec_6 = vec_11;

    // vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
    vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
    mem_vec_7 = vec_13;
    scal_4 = input[strX_in * (x + w) + strY_in * (y + 4 + h) + c];
    vec_15 = _mm512_set1_ps(scal_4);

    // vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
    vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
    mem_vec_8 = vec_14;

    // vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
    vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
    mem_vec_9 = vec_16;
    scal_5 = input[strX_in * (x + w) + strY_in * (y + 5 + h) + c];
    vec_18 = _mm512_set1_ps(scal_5);

    // vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
    vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
    mem_vec_10 = vec_17;

    // vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
    vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
    mem_vec_11 = vec_19;
    scal_6 = input[strX_in * (x + w) + strY_in * (y + 6 + h) + c];
    vec_21 = _mm512_set1_ps(scal_6);

    // vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_12);
    vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_12);
    mem_vec_12 = vec_20;

    // vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_13);
    vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_13);
    mem_vec_13 = vec_22;
    scal_7 = input[strX_in * (x + w) + strY_in * (y + 7 + h) + c];
    vec_24 = _mm512_set1_ps(scal_7);

    // vec_23 = _mm512_mask_fmadd_ps(vec_24, vec_2, mem_vec_14);
    vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_14);
    mem_vec_14 = vec_23;

    // vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_15);
    vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_15);
    mem_vec_15 = vec_25;
    scal_8 = input[strX_in * (x + w) + strY_in * (y + 8 + h) + c];
    vec_27 = _mm512_set1_ps(scal_8);

    // vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_16);
    vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_16);
    mem_vec_16 = vec_26;

    // vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_17);
    vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_17);
    mem_vec_17 = vec_28;
    scal_9 = input[strX_in * (x + w) + strY_in * (y + 9 + h) + c];
    vec_30 = _mm512_set1_ps(scal_9);

    // vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_18);
    vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_18);
    mem_vec_18 = vec_29;

    // vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_19);
    vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_19);
    mem_vec_19 = vec_31;
    scal_10 = input[strX_in * (x + w) + strY_in * (y + 10 + h) + c];
    vec_33 = _mm512_set1_ps(scal_10);

    // vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_20);
    vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_20);
    mem_vec_20 = vec_32;

    // vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_21);
    vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_21);
    mem_vec_21 = vec_34;
    scal_11 = input[strX_in * (x + w) + strY_in * (y + 11 + h) + c];
    vec_36 = _mm512_set1_ps(scal_11);

    // vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_22);
    vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_22);
    mem_vec_22 = vec_35;

    // vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_23);
    vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_23);
    mem_vec_23 = vec_37;
  }
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * y + f], mask0, mem_vec_0);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * y + f + 16], mask0, mem_vec_1);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 1) + f], mask1, mem_vec_2);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16], mask1, mem_vec_3);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 2) + f], mask2, mem_vec_4);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16], mask2, mem_vec_5);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 3) + f], mask3, mem_vec_6);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16], mask3, mem_vec_7);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 4) + f], mask4, mem_vec_8);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16], mask4, mem_vec_9);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 5) + f], mask5, mem_vec_10);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16], mask5, mem_vec_11);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 6) + f], mask6, mem_vec_12);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16], mask6, mem_vec_13);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 7) + f], mask7, mem_vec_14);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16], mask7, mem_vec_15);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 8) + f], mask8, mem_vec_16);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16], mask8, mem_vec_17);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 9) + f], mask9, mem_vec_18);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16], mask9, mem_vec_19);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 10) + f], mask10, mem_vec_20);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16], mask10, mem_vec_21);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 11) + f], mask11, mem_vec_22);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16], mask11, mem_vec_23);
}

void microk_2_12_mask3_fmadd_cin_param(float *const __restrict__ output,
                                       float const *const __restrict__ input, float const *const __restrict__ params,
                                       int Y,
                                       int C,
                                       int strC_in, int strY_in, int strX_in,
                                       int strF_o, int strY_o, int strX_o,
                                       int strF_par, int strC_par, int strH_par, int strW_par)
{
  /*
     [V f; U (2, f); U (12, y); T (256, c); Hoist_vars [c]]
     */
  int c, cp_0;

  assert((Y <= 12));
  int y = 0;
  int x = 0;
  int h = 0;
  int w = 0;
  int c0 = 0;
  int f = 0;
  float scal_0, scal_1, scal_10, scal_11, scal_2, scal_3, scal_4, scal_5, scal_6, scal_7, scal_8, scal_9;
  __m512 mem_vec_0, mem_vec_1, mem_vec_10, mem_vec_11, mem_vec_12, mem_vec_13, mem_vec_14, mem_vec_15, mem_vec_16, mem_vec_17, mem_vec_18, mem_vec_19, mem_vec_2, mem_vec_20, mem_vec_21, mem_vec_22, mem_vec_23, mem_vec_3, mem_vec_4, mem_vec_5, mem_vec_6, mem_vec_7, mem_vec_8, mem_vec_9, vec_0, vec_1, vec_10, vec_11, vec_12, vec_13, vec_14, vec_15, vec_16, vec_17, vec_18, vec_19, vec_2, vec_20, vec_21, vec_22, vec_23, vec_24, vec_25, vec_26, vec_27, vec_28, vec_29, vec_3, vec_30, vec_31, vec_32, vec_33, vec_34, vec_35, vec_36, vec_37, vec_4, vec_5, vec_6, vec_7, vec_8, vec_9;

  // for ( int i = 0; i < Y % 12; i++) {
  // }
  //  mask_fma : __m512 _mm512_mask_fmadd_ps (__m512 a, __mmask16 k, __m512 b, __m512 c)
  //  FOR j := 0 to 15
  //	i := j*32
  //	IF k[j]
  //		dst[i+31:i] := (a[i+31:i] * b[i+31:i]) + c[i+31:i]
  //	ELSE
  //		dst[i+31:i] := a[i+31:i]
  //	FI
  // ENDFOR
  // dst[MAX:512] := 0

  __mmask16 mask_zeros = _cvtu32_mask16(0);
  __mmask16 mask_ones = _cvtu32_mask16((1 << 30) - 1);
  __mmask16 mask0 = (Y >= 1) ? mask_ones : mask_zeros;
  __mmask16 mask1 = (Y >= 2) ? mask_ones : mask_zeros;
  __mmask16 mask2 = (Y >= 3) ? mask_ones : mask_zeros;
  __mmask16 mask3 = (Y >= 4) ? mask_ones : mask_zeros;
  __mmask16 mask4 = (Y >= 5) ? mask_ones : mask_zeros;
  __mmask16 mask5 = (Y >= 6) ? mask_ones : mask_zeros;
  __mmask16 mask6 = (Y >= 7) ? mask_ones : mask_zeros;
  __mmask16 mask7 = (Y >= 8) ? mask_ones : mask_zeros;
  __mmask16 mask8 = (Y >= 9) ? mask_ones : mask_zeros;
  __mmask16 mask9 = (Y >= 10) ? mask_ones : mask_zeros;
  __mmask16 mask10 = (Y >= 11) ? mask_ones : mask_zeros;
  __mmask16 mask11 = (Y >= 12) ? mask_ones : mask_zeros;

  mem_vec_0 = _mm512_load_ps(&output[(strX_o)*x + strY_o * y + f]);
  mem_vec_1 = _mm512_load_ps(&output[(strX_o)*x + strY_o * y + f + 16]);
  mem_vec_2 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 1) + f]);
  mem_vec_3 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 1) + f + 16]);
  mem_vec_4 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 2) + f]);
  mem_vec_5 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 2) + f + 16]);
  mem_vec_6 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 3) + f]);
  mem_vec_7 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 3) + f + 16]);
  mem_vec_8 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 4) + f]);
  mem_vec_9 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 4) + f + 16]);
  mem_vec_10 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 5) + f]);
  mem_vec_11 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 5) + f + 16]);
  mem_vec_12 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 6) + f]);
  mem_vec_13 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 6) + f + 16]);
  mem_vec_14 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 7) + f]);
  mem_vec_15 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 7) + f + 16]);
  mem_vec_16 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 8) + f]);
  mem_vec_17 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 8) + f + 16]);
  mem_vec_18 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 9) + f]);
  mem_vec_19 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 9) + f + 16]);
  mem_vec_20 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 10) + f]);
  mem_vec_21 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 10) + f + 16]);
  mem_vec_22 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 11) + f]);
  mem_vec_23 = _mm512_load_ps(&output[(strX_o)*x + strY_o * (y + 11) + f + 16]);
  // y = 12, x = 1, h = 1, w = 1, c = 256, f = 32
  // T (c, 256) (256 / 1)
  for (c = c0, cp_0 = 0;
       c < c0 + C;
       c += 1, cp_0 += 1)
  {
    scal_0 = input[strX_in * (x + w) + strY_in * (y + h) + c];
    vec_1 = _mm512_set1_ps(scal_0);
    vec_2 = _mm512_load_ps(&params[strW_par * w + strH_par * h + strC_par * c + f]);

    vec_0 = _mm512_mask3_fmadd_ps(vec_1, vec_2, mem_vec_0, mask0);
    mem_vec_0 = vec_0;

    vec_4 = _mm512_load_ps(&params[strW_par * w + strH_par * h + strC_par * c + f + 16]);

    // vec_3 = _mm512_mask3_fmadd_ps(vec_1, vec_4, mem_vec_1);
    vec_3 = _mm512_mask3_fmadd_ps(vec_1, vec_4, mem_vec_1, mask0);
    mem_vec_1 = vec_3;
    scal_1 = input[strX_in * (x + w) + strY_in * (y + 1 + h) + c];
    vec_6 = _mm512_set1_ps(scal_1);

    vec_5 = _mm512_mask3_fmadd_ps(vec_6, vec_2, mem_vec_2, mask1);
    mem_vec_2 = vec_5;

    // vec_7 = _mm512_mask3_fmadd_ps(vec_6, vec_4, mem_vec_3);
    vec_7 = _mm512_mask3_fmadd_ps(vec_6, vec_4, mem_vec_3, mask1);
    mem_vec_3 = vec_7;
    scal_2 = input[strX_in * (x + w) + strY_in * (y + 2 + h) + c];
    vec_9 = _mm512_set1_ps(scal_2);

    // vec_8 = _mm512_mask3_fmadd_ps(vec_9, vec_2, mem_vec_4);
    vec_8 = _mm512_mask3_fmadd_ps(vec_9, vec_2, mem_vec_4, mask2);
    mem_vec_4 = vec_8;

    // vec_10 = _mm512_mask3_fmadd_ps(vec_9, vec_4, mem_vec_5);
    vec_10 = _mm512_mask3_fmadd_ps(vec_9, vec_4, mem_vec_5, mask2);
    mem_vec_5 = vec_10;
    scal_3 = input[strX_in * (x + w) + strY_in * (y + 3 + h) + c];
    vec_12 = _mm512_set1_ps(scal_3);

    // vec_11 = _mm512_mask3_fmadd_ps(vec_12, vec_2, mem_vec_6);
    vec_11 = _mm512_mask3_fmadd_ps(vec_12, vec_2, mem_vec_6, mask3);
    mem_vec_6 = vec_11;

    // vec_13 = _mm512_mask3_fmadd_ps(vec_12, vec_4, mem_vec_7);
    vec_13 = _mm512_mask3_fmadd_ps(vec_12, vec_4, mem_vec_7, mask3);
    mem_vec_7 = vec_13;
    scal_4 = input[strX_in * (x + w) + strY_in * (y + 4 + h) + c];
    vec_15 = _mm512_set1_ps(scal_4);

    // vec_14 = _mm512_mask3_fmadd_ps(vec_15, vec_2, mem_vec_8);
    vec_14 = _mm512_mask3_fmadd_ps(vec_15, vec_2, mem_vec_8, mask4);
    mem_vec_8 = vec_14;

    // vec_16 = _mm512_mask3_fmadd_ps(vec_15, vec_4, mem_vec_9);
    vec_16 = _mm512_mask3_fmadd_ps(vec_15, vec_4, mem_vec_9, mask4);
    mem_vec_9 = vec_16;
    scal_5 = input[strX_in * (x + w) + strY_in * (y + 5 + h) + c];
    vec_18 = _mm512_set1_ps(scal_5);

    // vec_17 = _mm512_mask3_fmadd_ps(vec_18, vec_2, mem_vec_10);
    vec_17 = _mm512_mask3_fmadd_ps(vec_18, vec_2, mem_vec_10, mask5);
    mem_vec_10 = vec_17;

    // vec_19 = _mm512_mask3_fmadd_ps(vec_18, vec_4, mem_vec_11);
    vec_19 = _mm512_mask3_fmadd_ps(vec_18, vec_4, mem_vec_11, mask5);
    mem_vec_11 = vec_19;
    scal_6 = input[strX_in * (x + w) + strY_in * (y + 6 + h) + c];
    vec_21 = _mm512_set1_ps(scal_6);

    // vec_20 = _mm512_mask3_fmadd_ps(vec_21, vec_2, mem_vec_12);
    vec_20 = _mm512_mask3_fmadd_ps(vec_21, vec_2, mem_vec_12, mask6);
    mem_vec_12 = vec_20;

    // vec_22 = _mm512_mask3_fmadd_ps(vec_21, vec_4, mem_vec_13);
    vec_22 = _mm512_mask3_fmadd_ps(vec_21, vec_4, mem_vec_13, mask6);
    mem_vec_13 = vec_22;
    scal_7 = input[strX_in * (x + w) + strY_in * (y + 7 + h) + c];
    vec_24 = _mm512_set1_ps(scal_7);

    // vec_23 = _mm512_mask_mask3_fmadd_ps(vec_24, vec_2, mem_vec_14);
    vec_23 = _mm512_mask3_fmadd_ps(vec_24, vec_2, mem_vec_14, mask7);
    mem_vec_14 = vec_23;

    // vec_25 = _mm512_mask3_fmadd_ps(vec_24, vec_4, mem_vec_15);
    vec_25 = _mm512_mask3_fmadd_ps(vec_24, vec_4, mem_vec_15, mask7);
    mem_vec_15 = vec_25;
    scal_8 = input[strX_in * (x + w) + strY_in * (y + 8 + h) + c];
    vec_27 = _mm512_set1_ps(scal_8);

    // vec_26 = _mm512_mask3_fmadd_ps(vec_27, vec_2, mem_vec_16);
    vec_26 = _mm512_mask3_fmadd_ps(vec_27, vec_2, mem_vec_16, mask8);
    mem_vec_16 = vec_26;

    // vec_28 = _mm512_mask3_fmadd_ps(vec_27, vec_4, mem_vec_17);
    vec_28 = _mm512_mask3_fmadd_ps(vec_27, vec_4, mem_vec_17, mask8);
    mem_vec_17 = vec_28;
    scal_9 = input[strX_in * (x + w) + strY_in * (y + 9 + h) + c];
    vec_30 = _mm512_set1_ps(scal_9);

    // vec_29 = _mm512_mask3_fmadd_ps(vec_30, vec_2, mem_vec_18);
    vec_29 = _mm512_mask3_fmadd_ps(vec_30, vec_2, mem_vec_18, mask9);
    mem_vec_18 = vec_29;

    // vec_31 = _mm512_mask3_fmadd_ps(vec_30, vec_4, mem_vec_19);
    vec_31 = _mm512_mask3_fmadd_ps(vec_30, vec_4, mem_vec_19, mask9);
    mem_vec_19 = vec_31;
    scal_10 = input[strX_in * (x + w) + strY_in * (y + 10 + h) + c];
    vec_33 = _mm512_set1_ps(scal_10);

    // vec_32 = _mm512_mask3_fmadd_ps(vec_33, vec_2, mem_vec_20);
    vec_32 = _mm512_mask3_fmadd_ps(vec_33, vec_2, mem_vec_20, mask10);
    mem_vec_20 = vec_32;

    // vec_34 = _mm512_mask3_fmadd_ps(vec_33, vec_4, mem_vec_21);
    vec_34 = _mm512_mask3_fmadd_ps(vec_33, vec_4, mem_vec_21, mask10);
    mem_vec_21 = vec_34;
    scal_11 = input[strX_in * (x + w) + strY_in * (y + 11 + h) + c];
    vec_36 = _mm512_set1_ps(scal_11);

    // vec_35 = _mm512_mask3_fmadd_ps(vec_36, vec_2, mem_vec_22);
    vec_35 = _mm512_mask3_fmadd_ps(vec_36, vec_2, mem_vec_22, mask11);
    mem_vec_22 = vec_35;

    // vec_37 = _mm512_mask3_fmadd_ps(vec_36, vec_4, mem_vec_23);
    vec_37 = _mm512_mask3_fmadd_ps(vec_36, vec_4, mem_vec_23, mask11);
    mem_vec_23 = vec_37;
  }
  _mm512_store_ps(&output[strX_o * x + strY_o * y + f], mem_vec_0);
  _mm512_store_ps(&output[strX_o * x + strY_o * y + f + 16], mem_vec_1);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 1) + f], mem_vec_2);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16], mem_vec_3);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 2) + f], mem_vec_4);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16], mem_vec_5);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 3) + f], mem_vec_6);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16], mem_vec_7);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 4) + f], mem_vec_8);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16], mem_vec_9);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 5) + f], mem_vec_10);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16], mem_vec_11);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 6) + f], mem_vec_12);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16], mem_vec_13);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 7) + f], mem_vec_14);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16], mem_vec_15);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 8) + f], mem_vec_16);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16], mem_vec_17);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 9) + f], mem_vec_18);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16], mem_vec_19);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 10) + f], mem_vec_20);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16], mem_vec_21);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 11) + f], mem_vec_22);
  _mm512_store_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16], mem_vec_23);
}

void microk_2_12_mask_store_cin_param_stride2(float *const __restrict__ output,
                                              float const *const __restrict__ input, float const *const __restrict__ params,
                                              int Y,
                                              int C,
                                              int strF_o, int strY_o, int strX_o,
                                              int strC_in, int strY_in, int strX_in,
                                              int strF_par, int strC_par, int strH_par, int strW_par)
{
  /*
     [V f; U (2, f); U (12, y); T (256, c); Hoist_vars [c]]
     */
  int c, cp_0;

  assert((Y <= 12));
  int y = 0;
  int x = 0;
  int h = 0;
  int w = 0;
  int c0 = 0;
  int f = 0;
  float scal_0, scal_1, scal_10, scal_11, scal_2, scal_3, scal_4, scal_5, scal_6, scal_7, scal_8, scal_9;
  __m512 mem_vec_0, mem_vec_1, mem_vec_10, mem_vec_11, mem_vec_12, mem_vec_13, mem_vec_14, mem_vec_15, mem_vec_16, mem_vec_17, mem_vec_18, mem_vec_19, mem_vec_2, mem_vec_20, mem_vec_21, mem_vec_22, mem_vec_23, mem_vec_3, mem_vec_4, mem_vec_5, mem_vec_6, mem_vec_7, mem_vec_8, mem_vec_9, vec_0, vec_1, vec_10, vec_11, vec_12, vec_13, vec_14, vec_15, vec_16, vec_17, vec_18, vec_19, vec_2, vec_20, vec_21, vec_22, vec_23, vec_24, vec_25, vec_26, vec_27, vec_28, vec_29, vec_3, vec_30, vec_31, vec_32, vec_33, vec_34, vec_35, vec_36, vec_37, vec_4, vec_5, vec_6, vec_7, vec_8, vec_9;

  // for ( int i = 0; i < Y % 12; i++) {
  // }
  //  mask_fma : __m512 _mm512_mask_fmadd_ps (__m512 a, __mmask16 k, __m512 b, __m512 c)
  //  FOR j := 0 to 15
  //	i := j*32
  //	IF k[j]
  //		dst[i+31:i] := (a[i+31:i] * b[i+31:i]) + c[i+31:i]
  //	ELSE
  //		dst[i+31:i] := a[i+31:i]
  //	FI
  // ENDFOR
  // dst[MAX:512] := 0

  __mmask16 mask_zeros = _cvtu32_mask16(0);
  __mmask16 mask_ones = _cvtu32_mask16((1 << 30) - 1);
  __mmask16 mask0 = (Y >= 1) ? mask_ones : mask_zeros;
  __mmask16 mask1 = (Y >= 2) ? mask_ones : mask_zeros;
  __mmask16 mask2 = (Y >= 3) ? mask_ones : mask_zeros;
  __mmask16 mask3 = (Y >= 4) ? mask_ones : mask_zeros;
  __mmask16 mask4 = (Y >= 5) ? mask_ones : mask_zeros;
  __mmask16 mask5 = (Y >= 6) ? mask_ones : mask_zeros;
  __mmask16 mask6 = (Y >= 7) ? mask_ones : mask_zeros;
  __mmask16 mask7 = (Y >= 8) ? mask_ones : mask_zeros;
  __mmask16 mask8 = (Y >= 9) ? mask_ones : mask_zeros;
  __mmask16 mask9 = (Y >= 10) ? mask_ones : mask_zeros;
  __mmask16 mask10 = (Y >= 11) ? mask_ones : mask_zeros;
  __mmask16 mask11 = (Y >= 12) ? mask_ones : mask_zeros;

  mem_vec_0 = _mm512_load_ps(&output[strX_o * x + strY_o * y + f]);
  mem_vec_1 = _mm512_load_ps(&output[strX_o * x + strY_o * y + f + 16]);
  mem_vec_2 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 1) + f]);
  mem_vec_3 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16]);
  mem_vec_4 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 2) + f]);
  mem_vec_5 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16]);
  mem_vec_6 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 3) + f]);
  mem_vec_7 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16]);
  mem_vec_8 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 4) + f]);
  mem_vec_9 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16]);
  mem_vec_10 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 5) + f]);
  mem_vec_11 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16]);
  mem_vec_12 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 6) + f]);
  mem_vec_13 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16]);
  mem_vec_14 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 7) + f]);
  mem_vec_15 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16]);
  mem_vec_16 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 8) + f]);
  mem_vec_17 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16]);
  mem_vec_18 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 9) + f]);
  mem_vec_19 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16]);
  mem_vec_20 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 10) + f]);
  mem_vec_21 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16]);
  mem_vec_22 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 11) + f]);
  mem_vec_23 = _mm512_load_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16]);
  // y = 12, x = 1, h = 1, w = 1, c = 256, f = 32
  // T (c, 256) (256 / 1)
  for (c = c0, cp_0 = 0;
       c < c0 + C;
       c += 1, cp_0 += 1)
  {
    scal_0 = input[(strX_in * strY_in) * (2 * x + w) + strY_in * (2 * y + h) + c];
    vec_1 = _mm512_set1_ps(scal_0);
    vec_2 = _mm512_loadu_ps(&params[strW_par * w + strH_par * h + strC_par * c + f]);

    vec_0 = _mm512_fmadd_ps(vec_1, vec_2, mem_vec_0);
    mem_vec_0 = vec_0;

    vec_4 = _mm512_loadu_ps(&params[strW_par * w + strH_par * h + strC_par * c + f + 16]);

    // vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
    vec_3 = _mm512_fmadd_ps(vec_1, vec_4, mem_vec_1);
    mem_vec_1 = vec_3;
    scal_1 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 1) + h) + c];
    vec_6 = _mm512_set1_ps(scal_1);

    vec_5 = _mm512_fmadd_ps(vec_6, vec_2, mem_vec_2);
    mem_vec_2 = vec_5;

    // vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
    vec_7 = _mm512_fmadd_ps(vec_6, vec_4, mem_vec_3);
    mem_vec_3 = vec_7;
    scal_2 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 2) + h) + c];
    vec_9 = _mm512_set1_ps(scal_2);

    // vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
    vec_8 = _mm512_fmadd_ps(vec_9, vec_2, mem_vec_4);
    mem_vec_4 = vec_8;

    // vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
    vec_10 = _mm512_fmadd_ps(vec_9, vec_4, mem_vec_5);
    mem_vec_5 = vec_10;
    scal_3 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 3) + h) + c];
    vec_12 = _mm512_set1_ps(scal_3);

    // vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
    vec_11 = _mm512_fmadd_ps(vec_12, vec_2, mem_vec_6);
    mem_vec_6 = vec_11;

    // vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
    vec_13 = _mm512_fmadd_ps(vec_12, vec_4, mem_vec_7);
    mem_vec_7 = vec_13;
    scal_4 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 4) + h) + c];
    vec_15 = _mm512_set1_ps(scal_4);

    // vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
    vec_14 = _mm512_fmadd_ps(vec_15, vec_2, mem_vec_8);
    mem_vec_8 = vec_14;

    // vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
    vec_16 = _mm512_fmadd_ps(vec_15, vec_4, mem_vec_9);
    mem_vec_9 = vec_16;
    scal_5 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 5) + h) + c];
    vec_18 = _mm512_set1_ps(scal_5);

    // vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
    vec_17 = _mm512_fmadd_ps(vec_18, vec_2, mem_vec_10);
    mem_vec_10 = vec_17;

    // vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
    vec_19 = _mm512_fmadd_ps(vec_18, vec_4, mem_vec_11);
    mem_vec_11 = vec_19;
    scal_6 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 6) + h) + c];
    vec_21 = _mm512_set1_ps(scal_6);

    // vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_12);
    vec_20 = _mm512_fmadd_ps(vec_21, vec_2, mem_vec_12);
    mem_vec_12 = vec_20;

    // vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_13);
    vec_22 = _mm512_fmadd_ps(vec_21, vec_4, mem_vec_13);
    mem_vec_13 = vec_22;
    scal_7 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 7) + h) + c];
    vec_24 = _mm512_set1_ps(scal_7);

    // vec_23 = _mm512_mask_fmadd_ps(vec_24, vec_2, mem_vec_14);
    vec_23 = _mm512_fmadd_ps(vec_24, vec_2, mem_vec_14);
    mem_vec_14 = vec_23;

    // vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_15);
    vec_25 = _mm512_fmadd_ps(vec_24, vec_4, mem_vec_15);
    mem_vec_15 = vec_25;
    scal_8 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 8) + h) + c];
    vec_27 = _mm512_set1_ps(scal_8);

    // vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_16);
    vec_26 = _mm512_fmadd_ps(vec_27, vec_2, mem_vec_16);
    mem_vec_16 = vec_26;

    // vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_17);
    vec_28 = _mm512_fmadd_ps(vec_27, vec_4, mem_vec_17);
    mem_vec_17 = vec_28;
    scal_9 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 9) + h) + c];
    vec_30 = _mm512_set1_ps(scal_9);

    // vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_18);
    vec_29 = _mm512_fmadd_ps(vec_30, vec_2, mem_vec_18);
    mem_vec_18 = vec_29;

    // vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_19);
    vec_31 = _mm512_fmadd_ps(vec_30, vec_4, mem_vec_19);
    mem_vec_19 = vec_31;
    scal_10 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 10) + h) + c];
    vec_33 = _mm512_set1_ps(scal_10);

    // vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_20);
    vec_32 = _mm512_fmadd_ps(vec_33, vec_2, mem_vec_20);
    mem_vec_20 = vec_32;

    // vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_21);
    vec_34 = _mm512_fmadd_ps(vec_33, vec_4, mem_vec_21);
    mem_vec_21 = vec_34;
    scal_11 = input[strX_in * (2 * x + w) + strY_in * (2 * (y + 11) + h) + c];
    vec_36 = _mm512_set1_ps(scal_11);

    // vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_22);
    vec_35 = _mm512_fmadd_ps(vec_36, vec_2, mem_vec_22);
    mem_vec_22 = vec_35;

    // vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_23);
    vec_37 = _mm512_fmadd_ps(vec_36, vec_4, mem_vec_23);
    mem_vec_23 = vec_37;
  }
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * y + f], mask0, mem_vec_0);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * y + f + 16], mask0, mem_vec_1);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 1) + f], mask1, mem_vec_2);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 1) + f + 16], mask1, mem_vec_3);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 2) + f], mask2, mem_vec_4);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 2) + f + 16], mask2, mem_vec_5);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 3) + f], mask3, mem_vec_6);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 3) + f + 16], mask3, mem_vec_7);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 4) + f], mask4, mem_vec_8);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 4) + f + 16], mask4, mem_vec_9);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 5) + f], mask5, mem_vec_10);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 5) + f + 16], mask5, mem_vec_11);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 6) + f], mask6, mem_vec_12);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 6) + f + 16], mask6, mem_vec_13);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 7) + f], mask7, mem_vec_14);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 7) + f + 16], mask7, mem_vec_15);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 8) + f], mask8, mem_vec_16);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 8) + f + 16], mask8, mem_vec_17);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 9) + f], mask9, mem_vec_18);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 9) + f + 16], mask9, mem_vec_19);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 10) + f], mask10, mem_vec_20);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 10) + f + 16], mask10, mem_vec_21);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 11) + f], mask11, mem_vec_22);
  _mm512_mask_store_ps(&output[strX_o * x + strY_o * (y + 11) + f + 16], mask11, mem_vec_23);
}
#endif

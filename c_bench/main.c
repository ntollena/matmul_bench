#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <x86intrin.h>
#include <assert.h>
#include <papi.h>
#include "timing.h"

#ifdef RUI
#include "rui_conv/ukr10x3vCnnb1f128x112y112c128r3s3.h"
#endif

#include "gen/params.h"
#include "gen/layout_params.h"
#include "handle_counters.h"
#include "mem_utils.h"
#include <time.h>

#ifdef BLIS
#include <blis/blis.h>
#endif

#ifdef XSMM
void xsmm(papi_info_t info, long long * results,
        float * __restrict__ a, float * __restrict__ b, float * __restrict__ c,
        int nI, int nJ, int nK);
#endif

#ifdef USE_TC
#include "gen_tc.h"
#endif

#ifdef USE_CONV
#include "gen_conv.h"
#endif

#ifdef USE_MM
#include "gen_matmul.h"
#endif

#ifdef MKL
#include <mkl.h>
#endif

typedef enum {CONV, MM, TC} kernel_type;


// Signature of the test function (with wrapper to measure counters)
typedef void (mm_kernel_t) (
        papi_info_t info,
        long long* results,
        M_TYPE * __restrict__ c,
        M_TYPE * __restrict__ a, M_TYPE* __restrict__ b,
        IND_TYPE x_size, IND_TYPE y_size,
        IND_TYPE k_size);

typedef void (conv_kernel_t) (
        papi_info_t info,
        long long* results,
        M_TYPE * __restrict__ output,
		M_TYPE * __restrict__ input, M_TYPE* __restrict__ params,
		IND_TYPE x_size, IND_TYPE w_size,
		IND_TYPE y_size, IND_TYPE h_size,
		IND_TYPE c_size, IND_TYPE f_size,
		IND_TYPE str_x, IND_TYPE str_y);

typedef void (tc_kernel_t) (
        papi_info_t info,
        long long* results,
        M_TYPE * __restrict__ A,
        M_TYPE * __restrict__ B,
        M_TYPE * __restrict__ C,
        IND_TYPE * __restrict__ SIZES,
        IND_TYPE nleft,
        IND_TYPE nred,
        IND_TYPE nright);

// Structure containing the arrays, the sizes and the (wrapped) function itself
typedef struct mm_args {
	mm_kernel_t* kernel;
	M_TYPE * a;
	M_TYPE * b;
	M_TYPE * c;
	IND_TYPE nx; IND_TYPE ny; IND_TYPE nk;
    IND_TYPE emb_nx; IND_TYPE emb_ny; IND_TYPE emb_nk;
} mm_args_t;

typedef struct conv_args {
    conv_kernel_t* kernel;
    M_TYPE * output;
    M_TYPE * input;
    M_TYPE * params;
    IND_TYPE nx; IND_TYPE nw;
    IND_TYPE ny; IND_TYPE nh;
    IND_TYPE nc; IND_TYPE nf;
    IND_TYPE str_x; IND_TYPE str_y;
    IND_TYPE emb_nx; IND_TYPE emb_nw;
    IND_TYPE emb_ny; IND_TYPE emb_nh;
    IND_TYPE emb_nc; IND_TYPE emb_nf;
} conv_args_t;

typedef struct tc_args {
    tc_kernel_t* kernel;
    M_TYPE * a;
    M_TYPE * b;
    M_TYPE * c;
    IND_TYPE* sizes;
    IND_TYPE* emb_sizes;
    IND_TYPE ndim_left; IND_TYPE ndim_red; IND_TYPE ndim_right;
} tc_args_t;

// Union type (kernel_args) + constructors
typedef struct kernel_args {kernel_type typ;
	union {mm_args_t mm_args; conv_args_t conv_args; tc_args_t tc_args; } args;
} kernel_args_t;


kernel_args_t build_conv_args(
	M_TYPE * output,
	M_TYPE * input,
	M_TYPE * params,
	IND_TYPE nx, IND_TYPE nw,
	IND_TYPE ny, IND_TYPE nh,
	IND_TYPE nc, IND_TYPE nf,
	IND_TYPE str_x, IND_TYPE str_y,
    IND_TYPE emb_nx, IND_TYPE emb_nw,
    IND_TYPE emb_ny, IND_TYPE emb_nh,
    IND_TYPE emb_nc, IND_TYPE emb_nf,
	conv_kernel_t kernel) {
	kernel_args_t kargs;
	conv_args_t cargs;
	cargs.output = output;
	cargs.input = input;
	cargs.params = params;
	cargs.nx = nx;
	cargs.nw = nw;
	cargs.ny = ny;
	cargs.nh = nh;
	cargs.nc = nc;
	cargs.nf = nf;
    cargs.emb_nx = emb_nx;
    cargs.emb_nw = emb_nw;
    cargs.emb_ny = emb_ny;
    cargs.emb_nh = emb_nh;
    cargs.emb_nc = emb_nc;
    cargs.emb_nf = emb_nf;
	cargs.str_x = str_x;
	cargs.str_y = str_y;
	cargs.kernel = kernel;
	kargs.args.conv_args = cargs;
	kargs.typ = CONV;
	return kargs;
}

kernel_args_t build_mm_args(
	M_TYPE * c,
	M_TYPE * a,
	M_TYPE * b,
	IND_TYPE nx,
	IND_TYPE ny,
	IND_TYPE nk,
    IND_TYPE emb_nx,
    IND_TYPE emb_ny,
    IND_TYPE emb_nk,
	mm_kernel_t kernel) {
	kernel_args_t kargs;
	mm_args_t margs;
	margs.c = c;
	margs.a = a;
	margs.b = b;
	margs.nx = nx;
	margs.ny = ny;
	margs.nk = nk;
    margs.emb_nx = emb_nx;
    margs.emb_ny = emb_ny;
    margs.emb_nk = emb_nk;
	margs.kernel = kernel;
	kargs.args.mm_args = margs;
	kargs.typ = MM;
	return kargs;
}

kernel_args_t build_tc_args(
        M_TYPE * a,
        M_TYPE * b,
        M_TYPE * c,
        IND_TYPE * sizes,
        IND_TYPE * emb_sizes,
        IND_TYPE ndim_left,
        IND_TYPE ndim_red,
        IND_TYPE ndim_right,
        tc_kernel_t kernel) {
    tc_args_t targs;
    targs.a = a;
    targs.b = b;
    targs.c = c;
    targs.sizes = sizes;
    targs.emb_sizes = emb_sizes;
    targs.ndim_left = ndim_left;
    targs.ndim_red = ndim_red;
    targs.ndim_right = ndim_right;
    targs.kernel = kernel;
    
    kernel_args_t kargs;
    kargs.args.tc_args = targs;
    kargs.typ = TC;    
    return kargs;
}


// Fill the output matrix with 0
// TODO replace this boilerplate by proper generic polymorphism
void init_kernel(kernel_args_t *k_args) {
	mm_args_t mm_args;
	conv_args_t conv_args;
    tc_args_t tc_args;
	switch (k_args->typ ) {
		case MM :
			mm_args = (k_args->args).mm_args;
			init_zero(mm_args.c, mm_args.emb_nx * mm_args.emb_ny);
			break;
		case CONV :
			conv_args = k_args->args.conv_args;
			init_zero(conv_args.output, conv_args.emb_nx * conv_args.emb_ny * conv_args.emb_nf);
			break;
        case TC:
            tc_args = k_args->args.tc_args;
            int dim_c = 1;
            for (int i=0; i < tc_args.ndim_left; i++) {
                dim_c *= tc_args.emb_sizes[i];
            }
            for (int i=0; i < tc_args.ndim_right; i++) {
                dim_c *= tc_args.emb_sizes[tc_args.ndim_left + tc_args.ndim_red + i];
            }
            init_zero(tc_args.c, dim_c);
            break;
	}
}

// Add kernel function after initialisation
void set_mm_kernel(kernel_args_t *k_args, mm_kernel_t kernel) {
	assert(k_args->typ == MM);
	k_args->args.mm_args.kernel = kernel;
}

void set_conv_kernel(kernel_args_t *k_args, conv_kernel_t kernel) {
	assert(k_args->typ == CONV);
	k_args->args.conv_args.kernel = kernel;
}

void set_tc_kernel(kernel_args_t *k_args, tc_kernel_t kernel) {
    assert(k_args->typ == TC);
    k_args->args.tc_args.kernel = kernel;
}

// Repeated exection (not measured)
__attribute__((always_inline)) void exec_kernel_rep(kernel_args_t *k_args, int num_rep, papi_info_t info, long long * results) {
	mm_args_t mm_args;
	conv_args_t conv_args;
    tc_args_t tc_args;
	switch (k_args->typ) {
		case MM:
			mm_args = k_args->args.mm_args;
            for (int r = 0; r < num_rep; r++) {
                init_kernel(k_args);
                mm_args.kernel(
                        info, (long long *)&results[r * info.num_events],
                        mm_args.a, mm_args.b, mm_args.c,
                        mm_args.nx, mm_args.ny, mm_args.nk);
            }
			break;
		case CONV:
			conv_args =  k_args->args.conv_args;
            for (int r = 0; r < num_rep; r++) {
                init_kernel(k_args);
                conv_args.kernel(
                        info, (long long *)&results[r * info.num_events],
                        conv_args.output, conv_args.input, conv_args.params,
                        conv_args.nx, conv_args.nw,
                        conv_args.ny, conv_args.nh,
                        conv_args.nc, conv_args.nf,
                        conv_args.str_x, conv_args.str_y
						);
            }
			break;
        case TC:
            tc_args =  k_args->args.tc_args;
            for (int r = 0; r < num_rep; r++) {
                init_kernel(k_args);
                tc_args.kernel(
                        info, (long long *)&results[r * info.num_events],
                        tc_args.a, tc_args.b, tc_args.c,
                        tc_args.sizes,
                        tc_args.ndim_left, tc_args.ndim_red, tc_args.ndim_right);
            }
            break;
	}
}



// Convolution implementations
#ifdef USE_CONV

// oneDNN implementation
#ifdef ONEDNN
#include "onednn/oneDNN_conv.h"
void onednn( papi_info_t info, long long * results,
        M_TYPE * O, M_TYPE * I, M_TYPE * P,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		IND_TYPE str_x, IND_TYPE str_y) {
    conv_onednn(info, results, P, I, O, NW, NH, NC, NF, NX, NY,
			str_x, str_y);
}
#endif

// Rui implementation
#ifdef RUI
void rui_conv(papi_info_t info, long long * results,
        M_TYPE * I, M_TYPE * P, M_TYPE * O,
        IND_TYPE NX, IND_TYPE NW,
        IND_TYPE NY, IND_TYPE NH,
        IND_TYPE NC, IND_TYPE NF) {
    record_events(info);
    ukr10x3vCnnb1f128x112y112c128r3s3(I, P, O, 0);
    retrieve_results(info, results);
}
#endif

// Generated convolution implementation
#ifdef GEN
void gen_wrap_conv(papi_info_t info, long long * results,
        M_TYPE * O, M_TYPE * I, M_TYPE * P,
		IND_TYPE NX, IND_TYPE NW,
		IND_TYPE NY, IND_TYPE NH,
		IND_TYPE NC, IND_TYPE NF,
		IND_TYPE str_x, IND_TYPE str_y) {
    record_events(info);
    gen_conv(O, I,P,
		 NX, NW,
		 NY, NH,
		 NC, NF);
    retrieve_results(info, results);
}
#endif
#endif

// Matrix multiplication implementations
#ifdef USE_MM

// Generated matmult implementation
#ifdef GEN
void gen_wrap_matmul(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
    record_events(info);
    gen_matmul(
            a, b,  c,
            nI, nJ, nK);
    retrieve_results(info, results);
}
#endif

// BLIS implementation
#ifdef BLIS
// A[i, k], B[k, j] and C[i, j] are all stored row-major
void blis_matmul(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
    // Set the scalars to use.
    M_TYPE alpha, beta;
    IND_TYPE rsc, csc, rsa, csa, rsb, csb;
    alpha = 1.0;
    beta  = 0.;
    // Row-major, so the stride to from a column to the next one is 1,
    // stride from a row to another is sizeof(row)
    rsa = nK; csa = 1;
    rsb = nJ; csb = 1;
    rsc = nJ; csc = 1;
    // c := beta * c + alpha * a * b, where 'a', 'b', and 'c' are general.
    // RECORD COUNTERS
    record_events(info);

#ifdef USE_DOUBLE
    bli_dgemm( BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
            nI, nJ, nK,
            &alpha,
            a, rsa, csa,
            b, rsb, csb,
            &beta,
            c, rsc, csc);
#else
    bli_sgemm( BLIS_NO_TRANSPOSE, BLIS_NO_TRANSPOSE,
            nI, nJ, nK,
            &alpha,
            a, rsa, csa,
            b, rsb, csb,
            &beta,
            c, rsc, csc);
#endif
    // WRITE COUNTER RESULTS
    retrieve_results(info, results);
}
#endif //BLIS

// MKL implementation
#ifdef MKL
void mkl_matmul(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE nI, IND_TYPE nJ, IND_TYPE nK) {
    MKL_INT         n, k;
    MKL_INT         lda, ldb, ldc;
    CBLAS_LAYOUT    layout;
    float           alpha, beta;
    alpha = 1.0;
    beta  = 0.;
    CBLAS_TRANSPOSE transp = CblasNoTrans;
    layout = CblasRowMajor;
    lda = nK;
    ldb = nJ;
    ldc = nJ;
    // RECORD COUNTERS
    record_events(info);
#ifdef USE_DOUBLE
    cblas_sgemm(layout, transp, transp, nI, nJ, nK, alpha, a, lda,
            b, ldb, beta, c, ldc);
#else
    cblas_sgemm(layout, transp, transp, nI, nJ, nK, alpha, a, lda,
            b, ldb, beta, c, ldc);
#endif
    // WRITE COUNTER RESULTS
    retrieve_results(info, results);
}
#endif // MKL

#endif // USE_MM

// Tensor contraction implementation
#ifdef USE_TC

// Generated tensor contraction implementation
#ifdef GEN
void gen_wrap_tc(papi_info_t info, long long * results,
        M_TYPE * __restrict__ a, M_TYPE * __restrict__ b, M_TYPE * __restrict__ c,
        IND_TYPE * __restrict__ sizes,
        IND_TYPE nleft, IND_TYPE nred, IND_TYPE nright) {
    record_events(info);
    gen_tc(a, b, c, sizes, nleft, nred, nright);
    retrieve_results(info, results);
}
#endif

#endif // USE_TC



// Statistical utility functions
int compare_long(const void * i1, const void * i2) {
    long long l1, l2;
    l1 = *(long long *) i1;
    l2 = *(long long *) i2;
    return (l1 - l2);
}

long long median_long_inplace(long long * results, int n_run) {
    qsort(results, n_run, sizeof(long long), compare_long);
    long long res = results[n_run / 2];
    return res;
}


// Launch several time a kernel & gather stats
void many_try(int64_t rep,
		kernel_args_t * kargs,
        long long* counter_results
        ) {
    papi_info_t papi_info = build_papi_info();
    size_t num_events = papi_info.num_events;
    long long results [rep][num_events];
    exec_kernel_rep(kargs, rep, papi_info, (long long*)results);
    //for (int r = 0; r < rep; r++) {
    //    init_kernel(kargs);
    //    record_events(papi_info);
	//	exec_kernel(kargs);
    //    retrieve_results(papi_info, (long long *)results[r]);
    //}
    long long intermediate[rep];
    for (size_t i = 0; i < num_events; i++) {
        for (int r = 0; r < rep; r++) {
            intermediate[r] = results[r][i];
        }
#ifdef PRINT_RESULTS
        fprintf(stderr, "UNSORTED INTERMEDIATE ");
        for (int r = 0; r < rep  ; r++) {
            fprintf(stderr, "%lld ", intermediate[r]);
        }
        fprintf(stderr, "\n");
#endif
        qsort(intermediate, rep, sizeof(long long), compare_long);
        counter_results[i] = intermediate[rep / 2];
    }
}

// Parse the problem sizes as the arguments of the command line
void get_mm_sizes(int argc, char** argv, int* i, int* j, int* k) {
    assert(argc == 4);
    char* ptr;
    *i = strtol(argv[1], &ptr, 10);
    assert(*ptr == '\0');
    *j = strtol(argv[2], &ptr, 10);
    assert(*ptr == '\0');
    *k = strtol(argv[3], &ptr, 10);
    assert(*ptr == '\0');
}

void get_conv_sizes(int argc, char** argv, int* i, int* w, int* j, int* h, int* c, int* f,
		int* str_x, int* str_y,
        int* emb_i, int* emb_w, int* emb_j, int* emb_h, int* emb_c, int* emb_f) {
	assert(argc == 7 || argc == 9 || argc == 13 || argc == 15);
    // 7: only the conv sizes
    // 9: conv sizes, then strides
    // 13: conv sizes and embedded conv sizes
    // 15: conv sizes, stride, then embedded conv sizes

	char* ptr;
	*i = strtol(argv[1], &ptr, 10);
	assert(*ptr == '\0');
	*w = strtol(argv[2], &ptr, 10);
	assert(*ptr == '\0');
	*j = strtol(argv[3], &ptr, 10);
	assert(*ptr == '\0');
	*h = strtol(argv[4], &ptr, 10);
	assert(*ptr == '\0');
	*c = strtol(argv[5], &ptr, 10);
	assert(*ptr == '\0');
	*f = strtol(argv[6], &ptr, 10);
	assert(*ptr == '\0');

    int emb_size_ind_start = 7;
	if (argc == 9 || argc == 16) {
		*str_x = strtol(argv[7], &ptr, 10);
		assert(*ptr == '\0');
		*str_y = strtol(argv[8], &ptr, 10);
		assert(*ptr == '\0');

        emb_size_ind_start += 2;
	} else {
		*str_x = 1;
		*str_y = 1;
	}
    if (argc<=9) {
        *emb_i = *i;
        *emb_w = *w;
        *emb_j = *j;
        *emb_h = *h;
        *emb_c = *c;
        *emb_f = *f;
    } else {
        *emb_i = strtol(argv[emb_size_ind_start], &ptr, 10);
        assert(*ptr == '\0');
        *emb_w = strtol(argv[emb_size_ind_start+1], &ptr, 10);
        assert(*ptr == '\0');
        *emb_j = strtol(argv[emb_size_ind_start+2], &ptr, 10);
        assert(*ptr == '\0');
        *emb_h = strtol(argv[emb_size_ind_start+3], &ptr, 10);
        assert(*ptr == '\0');
        *emb_c = strtol(argv[emb_size_ind_start+4], &ptr, 10);
        assert(*ptr == '\0');
        *emb_f = strtol(argv[emb_size_ind_start+5], &ptr, 10);
        assert(*ptr == '\0');
    }
}

void get_tc_sizes(int argc, char** argv, int* dim_left, int* dim_red, int* dim_right, IND_TYPE** sizes) {
    // Note: sizes has the left, red then right dimension sizes inside (in that order)
    assert(argc > 4);
    char* ptr;

    *dim_left = strtol(argv[1], &ptr, 10);
    assert(*ptr == '\0');

    *dim_red = strtol(argv[2], &ptr, 10);
    assert(*ptr == '\0');

    *dim_right = strtol(argv[3], &ptr, 10);
    assert(*ptr == '\0');

    int nsizes = (*dim_left) + (*dim_red) + (*dim_right);
    assert(argc == 4 + nsizes);

    *sizes = (IND_TYPE*) malloc(nsizes * sizeof(IND_TYPE));
    for (int i=0; i<nsizes; i++) {
        (*sizes)[i] = (IND_TYPE) strtol(argv[4+i], &ptr, 10);
        assert(*ptr == '\0');
    }
}


int main(int argc, char** argv) {

    // Error management declaration
#ifdef ERROR_EPS
	M_TYPE epsilon = EPSILON;
#ifdef USE_CONV
	cerror_info_t err;
#else
	error_info_t err;
#endif
#endif

#ifdef ERROR_EXACT
#ifdef USE_CONV
	cexact_einfo_t exact_err;
#else
	exact_einfo_t exact_err;
#endif
#endif


    // Initialize memory allocation
#ifdef USE_CUSTOM_ALLOC
    mem_init();
#endif

    //M_TYPE* m =  mat;
	kernel_args_t kargs;
#ifdef USE_CONV
    int x_size, y_size, w_size, h_size, c_size, f_size, str_x, str_y;
    int emb_x_size, emb_y_size, emb_w_size, emb_h_size, emb_c_size, emb_f_size;
    srand(time(NULL));

    get_conv_sizes(argc, argv, &x_size, &w_size, &y_size, &h_size, &c_size, &f_size, &str_x, &str_y,
            &emb_x_size, &emb_w_size, &emb_y_size, &emb_h_size, &emb_c_size, &emb_f_size);

    /* DEBUG
    printf("Sizes of loops: x=%d | w=%d | y=%d | h=%d | c=%d | f=%d\n", x_size, y_size, w_size, h_size, c_size, f_size);
    printf("Sizes of emb: x=%d | w=%d | y=%d | h=%d | c=%d | f=%d\n",  emb_x_size, emb_y_size, emb_w_size, emb_h_size, emb_c_size, emb_f_size);
    //*/

    // Memory allocation: based on "embedded" sizes, which might be larger than the conv computation sizes
	IND_TYPE out_size = emb_x_size * emb_y_size * emb_f_size; 
	IND_TYPE in_size = (str_x * emb_x_size + emb_w_size - 1) * (str_y * emb_y_size + emb_h_size - 1) * emb_c_size; 
	IND_TYPE params_size = emb_w_size * emb_h_size * emb_f_size * emb_c_size; 
    M_TYPE* m =  (M_TYPE *)ALLOC(64,
			(out_size + in_size + params_size) * sizeof(M_TYPE));

    M_TYPE * output = m;
    M_TYPE * input = output + out_size;
    M_TYPE * params = input + in_size;
	kargs = build_conv_args(output, input, params,
			x_size, w_size, y_size, h_size,
			c_size, f_size,
			str_x, str_y,
            emb_x_size, emb_w_size, emb_y_size, emb_h_size,
            emb_c_size, emb_f_size,
			NULL);
    if (m == NULL) {
        fprintf(stderr, "failed to allocate buffers\n");
        return -1;
    }
    fflush(stderr);
    // INIT matrices
    init_zero(output, out_size);
    init_rand_zero_one(input, in_size);
    init_rand_zero_one(params, params_size);

    /* DEBUG
    init_incr(input, in_size);
    init_incr(params, params_size);
    printf("Input (init) =\n");
    fprint_linearized_array(stdout, input, in_size);
    printf("Params (init) =\n");
    fprint_linearized_array(stdout, params, params_size);
    */



    size_t nb_check = 1000;
    cindice_t* to_check = malloc(sizeof(cindice_t) * nb_check);
    for(size_t idx = 0; idx < nb_check; idx++) {
        to_check[idx].x = rand() % x_size;
        to_check[idx].y = rand() % y_size;
        to_check[idx].f = rand() % f_size;
    }
#endif // USE_CONV

#ifdef USE_MM // USE_MM
    int i_size, j_size, k_size;
    get_mm_sizes(argc, argv, &i_size, &j_size, &k_size);

    // Temp / TODO: get these values from get_mm_sizes
    int emb_i_size = i_size;
    int emb_j_size = j_size;
    int emb_k_size = k_size;

    IND_TYPE size_a = emb_i_size * emb_k_size;
    IND_TYPE size_b = emb_k_size * emb_j_size;
    IND_TYPE size_c = emb_i_size * emb_j_size;

    M_TYPE* m =  (M_TYPE *)ALLOC(64,
			(size_c + size_a + size_b) * sizeof(M_TYPE));
    M_TYPE * c = m;
    M_TYPE * a = c + size_c;
    M_TYPE * b = a + size_a;
	kargs = build_mm_args(c, a, b, i_size, j_size, k_size, emb_i_size, emb_j_size, emb_k_size, NULL);

    srand(time(NULL));
    size_t nb_check = 1000;
    init_zero(c, emb_i_size * emb_j_size);
    init_bounded(a, emb_i_size, emb_k_size, 15);
    init_bounded(b, emb_k_size, emb_j_size, 15);

    // Note: indice_t is from matrix_utils.h
    indice_t* to_check = malloc(sizeof(indice_t) * nb_check);
    for(size_t idx = 0; idx < nb_check; idx++) {
        IND_TYPE i = rand() % i_size;
        IND_TYPE j = rand() % j_size;
        IND_TYPE* indices = malloc(2 * sizeof(IND_TYPE));
        indices[0] = i;
        indices[1] = j;

        to_check[idx].indices = indices;
        to_check[idx].num_dims = 2;
    }
#endif

#ifdef USE_TC
    // Get the problem sizes and build the tc_args (minus the kernel)

    int ndim_left, ndim_red, ndim_right;
    IND_TYPE* sizes;
    get_tc_sizes(argc, argv, &ndim_left, &ndim_red, &ndim_right, &sizes);

    // Temp / TODO: get these values from get_mm_sizes
    IND_TYPE* emb_sizes = malloc( (ndim_left+ndim_red+ndim_right) * sizeof(IND_TYPE));
    for (int i=0; i<ndim_left+ndim_red+ndim_right; i++) {
        emb_sizes[i] = sizes[i];
    }

    // Compute the sizes of the tensors
    IND_TYPE size_a = 1;
    IND_TYPE size_b = 1;
    IND_TYPE size_c = 1;
    for (int i=0; i<ndim_left; i++) {
        IND_TYPE si = emb_sizes[i];
        size_a *= si;
        size_c *= si;
    }
    for (int i=0; i<ndim_red; i++) {
        IND_TYPE si = emb_sizes[i+ndim_left];
        size_a *= si;
        size_b *= si;
    }
    for (int i=0; i<ndim_right; i++) {
        IND_TYPE si = emb_sizes[i+ndim_left+ndim_red];
        size_b *= si;
        size_c *= si;
    }

    // Allocate all arrays
    M_TYPE* m =  (M_TYPE *) ALLOC(64, (size_a + size_b + size_c) * sizeof(M_TYPE));
    M_TYPE * c = m;
    M_TYPE * a = c + size_c;
    M_TYPE * b = a + size_a;

    kargs = build_tc_args(a, b, c, sizes, emb_sizes, ndim_left, ndim_red, ndim_right, NULL);

    // Initialize arrays with random values or 0 (for output array)
    srand(time(NULL));
    size_t nb_check = 1000;

    init_zero(c, size_c);

    // Note: sizes got more element than needed, but the correct one at the start
    init_bounded_multidim(a, emb_sizes, (ndim_left + ndim_red), 15);

    // For tensor b, we need to build the "sizes_b" array
    IND_TYPE* sizes_b = malloc( (ndim_red + ndim_right) * sizeof(IND_TYPE));
    for (int i=0; i<ndim_red; i++) {
        sizes_b[i] = emb_sizes[ndim_left+i];
    }
    for (int i=0; i<ndim_right; i++) {
        sizes_b[ndim_red+i] = emb_sizes[ndim_left+ndim_red+i];
    }
    init_bounded_multidim(b, sizes_b, (ndim_red + ndim_right), 15);
    free(sizes_b);


    /* DEBUG
    printf("A (init) =\n");
    fprint_linearized_array(stdout, a, size_a);
    printf("B (init) =\n");
    fprint_linearized_array(stdout, b, size_b);
    //*/



    // Initialize coordinate in "c" to be checked
    // Note: indice_t is from matrix_utils.h
    indice_t* to_check = malloc(sizeof(indice_t) * nb_check);
    for(size_t idx = 0; idx < nb_check; idx++) {
        IND_TYPE* indices = malloc( (ndim_left+ndim_right) * sizeof(IND_TYPE));
        for (int i=0; i<ndim_left; i++) {
            indices[i] = rand() % sizes[i];
        }
        for (int i=0; i<ndim_right; i++) {
            indices[ndim_left+i] = rand() % sizes[ndim_left+ndim_red+i];
        }

        to_check[idx].indices = indices;    // Indices on the logical layout
        to_check[idx].num_dims = (ndim_left+ndim_right);
    }
#endif // USE_TC


    // Initialize Papi
    init_papi();



    // Set kernel to use
#ifdef USE_CONV

    // Convolution - generated kernel
#ifdef GEN
    long long conv_ctr_results[N_CTR];
	set_conv_kernel(&kargs, gen_wrap_conv);
    many_try(NUM_REP, &kargs, conv_ctr_results);
    print_counters(conv_ctr_results);

    /* DEBUG
    printf("Out (val) =\n");
    fprint_linearized_array(stdout, output, out_size);
    //*/

#ifdef ERROR_EXACT
    exact_err = is_conv_sample_exact(input, params, output, x_size, y_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_conv_sample(input, params, output,
			x_size, w_size,
			y_size, h_size,
			c_size, f_size,
			str_x, str_y,
            GEN_LAYOUT_IN,
            GEN_LAYOUT_PAR,
            GEN_LAYOUT_OUT,
            emb_x_size, emb_w_size,
            emb_y_size, emb_h_size,
            emb_c_size, emb_f_size,
            nb_check,
			to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // GEN


    // Convolution - oneDNN
#ifdef ONEDNN
    long long onednn_ctr_results[N_CTR];
	set_conv_kernel(&kargs, onednn);
    many_try(NUM_REP, &kargs, onednn_ctr_results);
    print_counters(onednn_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_conv_sample_exact(a, b, c, x_size, y_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_conv_sample(input, params, output, x_size, w_size, y_size, h_size, c_size, f_size,
			str_x, str_y,
            DNN_LAYOUT_IN,
            DNN_LAYOUT_PAR,
            DNN_LAYOUT_OUT,
            emb_x_size, emb_w_size,
            emb_y_size, emb_h_size,
            emb_c_size, emb_f_size,
            nb_check,
			to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS

#endif // ONEDNN

#endif // USE_CONV



#ifdef USE_TC

    // Generated TC implementation
#ifdef GEN

    // Launch experiment & measurement, then print them
    long long tc_ctr_results[N_CTR];
    set_tc_kernel(&kargs, gen_wrap_tc);
    many_try(NUM_REP, &kargs, tc_ctr_results);
    print_counters(tc_ctr_results);

    /* DEBUG
    printf("C (val) =\n");
    fprint_linearized_array(stdout, c, size_c);
    //*/

    // Error check - exact
#ifdef ERROR_EXACT
    // Build the layout objects (content is defined in "gen/params.h")
    layout_tensor_t layout_left = {permut_tc_left, ndim_tc_left };
    layout_tensor_t layout_right = {permut_tc_right, ndim_tc_right };
    layout_tensor_t layout_out = {permut_tc_out, ndim_tc_out };
    exact_err = is_tc_sample_exact(a, b, c, sizes, ndim_left, ndim_red, ndim_right,
        layout_left, layout_right, layout_out,
        nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT

    // Error check - epsilon
#ifdef ERROR_EPS
    // Build the layout objects (content is defined in "gen/params.h")
    layout_tensor_t layout_left = {permut_tc_left, ndim_tc_left };
    layout_tensor_t layout_right = {permut_tc_right, ndim_tc_right };
    layout_tensor_t layout_out = {permut_tc_out, ndim_tc_out };
    err = is_tc_sample(a, b, c,
            sizes, ndim_left, ndim_red, ndim_right,
            layout_left, layout_right, layout_out,
            nb_check, to_check, epsilon);
    print_eps_error(err);
#endif // ERROR_EPS

#endif // GEN

#endif  // USE_TC



#ifdef USE_MM // USE_MM

#ifdef BLIS
    long long blis_ctr_results[N_CTR];
#endif// BLIS
#ifdef MKL
    long long mkl_ctr_results[N_CTR];
#endif //MKL
#ifdef XSMM
    long long xsmm_ctr_results[N_CTR];
#endif //XSMM
#ifdef GEN
    long long gen_ctr_results[N_CTR];
#endif // GEN

#ifdef BLIS
	set_mm_kernel(&kargs, blis_matmul);
    many_try(NUM_REP, &kargs, blis_ctr_results);
    print_counters(blis_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon, true);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // BLIS

#ifdef MKL
	set_mm_kernel(&kargs, mkl_matmul);
    many_try(NUM_REP, &kargs, mkl_ctr_results);
    print_counters(mkl_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon, true);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // MKL

#ifdef XSMM
	set_mm_kernel(&kargs, xsmm);
    many_try(NUM_REP, &kargs, xsmm_ctr_results);
    print_counters(xsmm_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif // ERROR_EXACT
#ifdef ERROR_EPS
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon, false);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // XSMM

#ifdef GEN
	set_mm_kernel(&kargs, gen_wrap_matmul);
    many_try(NUM_REP, &kargs, gen_ctr_results);
    print_counters(gen_ctr_results);
#ifdef ERROR_EXACT
    exact_err = is_matmul_sample_exact(a, b, c, i_size, j_size, k_size, nb_check, to_check);
    print_exact_error(exact_err);
#endif// ERROR_EXACT
#ifdef ERROR_EPS
    /* DEBUG - check "to_check"
    for (int i=0; i<nb_check; i++) {
        indice_t ind_checki = to_check[i];
        fprintf(stdout, "to_check[i] : num_dims = %d | values = [ ", ind_checki.num_dims);
        for (int j=0; j<ind_checki.num_dims; j++)
            fprintf(stdout, "%ld, ", ind_checki.indices[j]);
        fprintf(stdout, "]\n");
    }*/
    err = is_matmul_sample(a, b, c, i_size, j_size, k_size, nb_check, to_check, epsilon, true);
    print_eps_error(err);
#endif // ERROR_EPS
#endif // GEN

#endif // USE_MM


    // Flush printed outputs
    fflush(stdout);


    // Free memory
    //FREE(m);

#ifdef USE_CUSTOM_ALLOC
    mem_close();
#endif
}

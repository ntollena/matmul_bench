#ifndef BUFFER_H
#define BUFFER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_ALLOC_SIZE (1 << 10)

typedef struct buf {int size; int capacity; char* content;} buffer_t; 

buffer_t init_buffer(int capacity);
void grow(buffer_t *buf);
void push_char(buffer_t *buf, char str);
void push(buffer_t *buf, char* str, int size);
char* flush(buffer_t *buf);
void reset(buffer_t *buf);
void buf_free(buffer_t *buf);

#endif

#include "tc_utils.h"

// Deterministic initialization function
void init_bounded_multidim(M_TYPE *tab, IND_TYPE *sizes, IND_TYPE num_sizes,
                           int64_t bound) {

  // Get the dimension of tab
  IND_TYPE size_tab = 1;
  for (IND_TYPE i = 0; i < num_sizes; i++) {
    size_tab *= sizes[i];
  }

  // Assuming tab is linearized, iterate over all of its cells
  for (IND_TYPE k = 0; k < size_tab; k++) {
    tab[k] = ((M_TYPE)(k % bound));
  }

  return;
}

// === Linearisation functions ===

// Aux function: (ind1@ind2)[i]  / n1 is the size of ind1
IND_TYPE get_contactenation_indices(IND_TYPE n1, IND_TYPE *ind1, IND_TYPE *ind2,
                                    IND_TYPE i) {
  if (i < n1)
    return ind1[i];
  else
    return ind2[i - n1];
}

// Get the access of a linearized tensor A at (indices_left, indices_red)
IND_TYPE access_function_tc_A(IND_TYPE *SIZES, IND_TYPE ndim_left,
                              IND_TYPE ndim_red, layout_tensor_t layout_left,
                              IND_TYPE *indices_left, IND_TYPE *indices_red) {
  // Note: for tensor A, layout is left dim first, then red dims innemost
  // SIZES: left, then reduction, then right

  const IND_TYPE *permut = layout_left.permut;
  assert(layout_left.n_dims == (ndim_left + ndim_red));

  IND_TYPE offset = 0;
  // i following the  order of the layout
  // permut convert an order of the layout to a logical order (same as SIZES,
  // left then red then right)
  for (int i = 0; i < layout_left.n_dims; i++) {
    offset = offset * SIZES[permut[i]] +
             get_contactenation_indices(ndim_left, indices_left, indices_red,
                                        permut[i]);
  }

  return offset;
}

// Get the access of a linearized tensor B at (indices_red, indices_right)
IND_TYPE access_function_tc_B(IND_TYPE *SIZES, IND_TYPE ndim_left,
                              IND_TYPE ndim_red, IND_TYPE ndim_right,
                              layout_tensor_t layout_right,
                              IND_TYPE *indices_red, IND_TYPE *indices_right) {
  // Note: for tensor B, layout is red dim first, then right dims innemost
  // SIZES: left, then reduction, then right

  const IND_TYPE *permut = layout_right.permut;
  assert(layout_right.n_dims == (ndim_red + ndim_right));
  // i following the  order of the layout
  // permut convert an order of the layout to a logical order (same as SIZES,
  // left then red then right)
  IND_TYPE offset = 0;
  for (int i = 0; i < layout_right.n_dims; i++) { // Red part first
    // need to start picking in SIZES where it contains indices of the tensor B
    // (so after the ndim_left elements only in A)
    offset = offset * SIZES[permut[i] + ndim_left] +
             get_contactenation_indices(ndim_red, indices_red, indices_right,
                                        permut[i]);
  }

  return offset;
}

// Get the access of a linearized tensor C at "indices" (include left and right)
IND_TYPE access_function_tc_C(IND_TYPE *SIZES, IND_TYPE ndim_left,
                              IND_TYPE ndim_red, IND_TYPE ndim_right,
                              layout_tensor_t layout_out, IND_TYPE *indices) {
  // Note: for tensor C, layout is left dim first, then right dims innemost
  // SIZES: left, then reduction, then right

  const IND_TYPE *permut = layout_out.permut;
  assert(layout_out.n_dims == (ndim_left + ndim_right));

  IND_TYPE offset = 0;
  for (int i = 0; i < layout_out.n_dims; i++) { // Left then right part
    offset = offset * get_contactenation_indices(ndim_left, SIZES,
                                                 &(SIZES[ndim_left + ndim_red]),
                                                 permut[i]) +
             indices[permut[i]];
  }
  return offset;
}

// === Correctness functions ===

// Check that the reduction at different random_point (given by to_check) are
// valid Note: to_check has point from the logical layout, not the real one
exact_einfo_t is_tc_sample_exact(M_TYPE *A, M_TYPE *B, M_TYPE *C,
                                 IND_TYPE *SIZES, IND_TYPE ndim_left,
                                 IND_TYPE ndim_red, IND_TYPE ndim_right,
                                 layout_tensor_t layout_left,
                                 layout_tensor_t layout_right,
                                 layout_tensor_t layout_out, int nb_check,
                                 indice_t *to_check) {

  exact_einfo_t max_error = init_exact_einfo();
  for (int num_test = 0; num_test < nb_check; num_test++) {
    IND_TYPE *rand_indices = to_check[num_test].indices;
    int rand_numdims = to_check[num_test].num_dims;

    float red = 0.;
    // alloc array and initialize its elements to zero
    IND_TYPE *k_red = calloc(ndim_red, sizeof(IND_TYPE));

    // n_dim_red nested for loops of size known in the SIZES array at offset
    // ndim_left
    while (k_red[0] < SIZES[ndim_left + 0]) {

      // Computation
      red += A[access_function_tc_A(SIZES, ndim_left, ndim_red, layout_left,
                                    rand_indices, k_red)] *
             B[access_function_tc_B(SIZES, ndim_left, ndim_red, ndim_right,
                                    layout_right, k_red,
                                    &(rand_indices[ndim_left]))];

      // Incrementing k_red
      k_red[ndim_red - 1]++;
      for (int i = ndim_red - 1; i > 0; i--) {
        if (k_red[i] >= SIZES[ndim_left + i]) {
          k_red[i] = 0;
          k_red[i - 1]++; // Note: might trigger a cascade of carry :)
        }
      }
    }
    free(k_red);

    max_error = handle_exact(
        max_error, rand_indices, rand_numdims, red,
        C[access_function_tc_C(SIZES, ndim_left, ndim_red, ndim_right,
                               layout_out, rand_indices)]);
  }
  return max_error;
}

// Check that the reduction at different random_point (given by to_check) are
// valid
//	More detailled error reporting, compared to previous function
// Note: to_check has point from the logical layout, not the real one
error_info_t is_tc_sample(M_TYPE *A, M_TYPE *B, M_TYPE *C, IND_TYPE *SIZES,
                          IND_TYPE ndim_left, IND_TYPE ndim_red,
                          IND_TYPE ndim_right, layout_tensor_t layout_left,
                          layout_tensor_t layout_right,
                          layout_tensor_t layout_out, int nb_check,
                          indice_t *to_check, M_TYPE epsilon) {

  error_info_t max_error = init_error_info(epsilon);
  for (int num_test = 0; num_test < nb_check; num_test++) {
    IND_TYPE *rand_indices = to_check[num_test].indices;
    int num_dims = to_check[num_test].num_dims;
    // assert(num_dims == ndim_left + ndim_right);

    M_TYPE red = 0;
    // alloc + set all elements of the array to 0
    IND_TYPE *k_red = calloc(ndim_red, sizeof(IND_TYPE));
    // n_dim_red nested for loops of size known in the SIZES array at offset
    // ndim_left
    while (k_red[0] < SIZES[ndim_left + 0]) {
      // Computation
      red += A[access_function_tc_A(SIZES, ndim_left, ndim_red, layout_left,
                                    rand_indices, k_red)] *
             B[access_function_tc_B(SIZES, ndim_left, ndim_red, ndim_right,
                                    layout_right, k_red,
                                    &(rand_indices[ndim_left]))];

      // Incrementing most nested loop iteration by 1
      k_red[ndim_red - 1]++;
      // Checks loops which ended their iterations and reset the most nesteds +
      // propagate to above
      for (int i = ndim_red - 1; i > 0; i--) {
        if (k_red[i] >= SIZES[ndim_left + i]) {
          k_red[i] = 0;
          k_red[i - 1]++; // Note: might trigger a cascade of carry :)
        }
      }
    }
    // we observe k_red = [SIZES[n_dim_left + 0], 0, 0, 0, ..., 0] in the end
    free(k_red);
    max_error = handle_res(
        max_error, rand_indices, num_dims, red,
        C[access_function_tc_C(SIZES, ndim_left, ndim_red, ndim_right,
                               layout_out, rand_indices)]);
  }
  return max_error;
}

// Reference TC operation (for correctness comparison)
// probably buggy
void basic_tc(M_TYPE *__restrict__ A, M_TYPE *__restrict__ B,
              M_TYPE *const __restrict__ C, IND_TYPE *SIZES, IND_TYPE ndim_left,
              IND_TYPE ndim_red, IND_TYPE ndim_right,
              layout_tensor_t layout_left, layout_tensor_t layout_right,
              layout_tensor_t layout_out) {

  IND_TYPE *indices_C = malloc((ndim_left + ndim_right) * sizeof(IND_TYPE));
  for (int i = 0; i < ndim_left + ndim_right; i++) {
    indices_C[i] = (IND_TYPE)0;
  }
  while (indices_C[0] < SIZES[0]) {
    float red = 0.;
    IND_TYPE *k_red = malloc(ndim_red * sizeof(IND_TYPE));
    for (int i = 0; i < ndim_red; i++) {
      k_red[i] = (IND_TYPE)0;
    }
    while (k_red[0] < SIZES[ndim_left + 0]) {
      // Computation
      red +=
          A[access_function_tc_A(SIZES, ndim_left, ndim_red, layout_left,
                                 indices_C, k_red)] *
          B[access_function_tc_B(SIZES, ndim_left, ndim_red, ndim_right,
                                 layout_right, k_red, &(indices_C[ndim_left]))];

      // Incrementing k_red
      k_red[ndim_red - 1] = k_red[ndim_red - 1] + 1;
      for (int i = ndim_red - 1; i > 0; i--) {
        if (k_red[i] >= SIZES[ndim_left + i]) {
          k_red[i] = 0;
          k_red[i - 1] =
              k_red[i - 1] + 1; // Note: might trigger a cascade of carry :)
        }
      }
    }
    free(k_red);
    C[access_function_tc_C(SIZES, ndim_left, ndim_red, ndim_right, layout_out,
                           indices_C)] = red;

    // Incrementing indices_C
    indices_C[ndim_left + ndim_right - 1] =
        indices_C[ndim_left + ndim_right - 1] + 1;
    for (int i = ndim_left + ndim_right - 1; i >= ndim_left; i--) {
      if (indices_C[i] >= SIZES[ndim_left + ndim_red + i]) {
        indices_C[i] = 0;
        indices_C[i - 1] = indices_C[i - 1] + 1;
      }
    }
    for (int i = ndim_left - 1; i > 0; i++) {
      if (indices_C[i] >= SIZES[i]) {
        indices_C[i] = 0;
        indices_C[i - 1] = indices_C[i - 1] + 1;
      }
    }
  }

  free(indices_C);
  return;
}

void fprint_linearized_array(FILE *file, M_TYPE *tab, IND_TYPE size) {
  for (IND_TYPE i = 0; i < size; i++) {
    fprintf(file, "%f ", tab[i]);
  }
  fprintf(file, "\n");
  fflush(file);
}

#ifndef GEN_H
#include "tc_utils.h"
#include "assert.h"
#include "mem_utils.h"
#include "gen/params.h"
#include <omp.h>
#include <x86intrin.h>

void gen_tc( M_TYPE const * const __restrict__ A, M_TYPE const * const __restrict__ B,
		M_TYPE * const  __restrict__ C,
        IND_TYPE const * const __restrict__ SIZES,
        IND_TYPE nleft, IND_TYPE nred, IND_TYPE nright);
#endif

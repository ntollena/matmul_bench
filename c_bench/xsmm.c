#include <assert.h>
#include <stdio.h>
#include "timing.h"
#include <libxsmm_source.h>

void print_mat(float* m, int nrows, int ncol) {
  for (int i = 0; i < nrows; i++){
    int j;
    for (j = 0; j < ncol - 1; j++){
      fprintf(stderr, "%f,", m[i* nrows + j]);
    }
    fprintf(stderr, "%f\n", m[i* nrows + j]);
  }
}

void xsmm(papi_info_t info, long long * results,
        float * __restrict__ a, float * __restrict__ b, float * __restrict__ c,
        int nI, int nJ, int nK) {
    // Set the scalars to use.
    float alpha, beta;
    alpha = 1.0;
    beta  = 0.;
    int flags = LIBXSMM_GEMM_FLAG_NONE;
    int lda = nI, ldb = nK, ldc = nI;
  /* generates and dispatches a matrix multiplication kernel */
  libxsmm_smmfunction kernel = libxsmm_smmdispatch(
    nI, nJ, nK, &lda /*lda*/, &ldb /*ldb*/, &ldc /*ldc*/, &alpha, &beta, &flags, NULL /*prefetch*/);
    // c := beta * c + alpha * a * b, where 'a', 'b', and 'c' are general.
    assert(kernel);
    // RECORD COUNTERS
    record_events(info);
    kernel(a, b, c);
    // WRITE COUNTER RESULTS
    retrieve_results(info, results);
}

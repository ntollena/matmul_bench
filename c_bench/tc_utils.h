#ifndef TC_UTILS_H
#define TC_UTILS_H

#include <assert.h>
#include "matrix_utils.h"
// Previous include reuse everything from matmul
//	Missing: comparison with reference implementation


// Initialisation of multi-dim function
void init_bounded_multidim(M_TYPE * tab, IND_TYPE* sizes, IND_TYPE num_sizes, int64_t bound);


// Layout management: permutation over the "logical layout", which is a classical matmult layout
typedef struct layout_tensor {
	const IND_TYPE* permut;  			// Array of integer from 0 to (n_dims-1), permuted. 0 = outer / max = inner
	IND_TYPE n_dims;
} layout_tensor_t;


// Utility functions - Get the access of a linearized tensor A at indices
IND_TYPE access_function_tc_A(IND_TYPE* SIZES, IND_TYPE ndim_left, IND_TYPE ndim_red,
		layout_tensor_t layout_left,
		IND_TYPE* indices_left, IND_TYPE* indices_red);
IND_TYPE access_function_tc_B(IND_TYPE* SIZES, IND_TYPE ndim_left, IND_TYPE ndim_red, IND_TYPE ndim_right,
		layout_tensor_t layout_right,
		IND_TYPE* indices_red, IND_TYPE* indices_right);
IND_TYPE access_function_tc_C(IND_TYPE* SIZES, IND_TYPE ndim_left, IND_TYPE ndim_red, IND_TYPE ndim_right,
		layout_tensor_t layout_out,
		IND_TYPE* indices);



// Check that the reduction at different random_point (given by to_check) are valid
exact_einfo_t is_tc_sample_exact(M_TYPE * A, M_TYPE * B, M_TYPE * C,
	IND_TYPE* SIZES, IND_TYPE ndim_left, IND_TYPE ndim_red, IND_TYPE ndim_right,
    layout_tensor_t layout_left, layout_tensor_t layout_right, layout_tensor_t layout_out,
    int nb_check, indice_t * to_check);

error_info_t is_tc_sample(M_TYPE * A, M_TYPE * B, M_TYPE * C,
	IND_TYPE* SIZES, IND_TYPE ndim_left, IND_TYPE ndim_red, IND_TYPE ndim_right,
    layout_tensor_t layout_left, layout_tensor_t layout_right, layout_tensor_t layout_out,
    int nb_check, indice_t * to_check, M_TYPE epsilon);


// Reference TC operation (for correctness comparison)
void basic_tc( M_TYPE* __restrict__ A, M_TYPE* __restrict__ B, M_TYPE* const __restrict__ C,
		IND_TYPE* SIZES, IND_TYPE ndim_left, IND_TYPE ndim_red, IND_TYPE ndim_right,
	    layout_tensor_t layout_left, layout_tensor_t layout_right, layout_tensor_t layout_out);

// For debugging
void fprint_linearized_array(FILE* file, M_TYPE* tab, IND_TYPE size);


#endif
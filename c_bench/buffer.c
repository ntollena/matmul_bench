#include "buffer.h"

buffer_t init_buffer(int capacity) {
	buffer_t buf;
	int cap =(capacity <= 0)? DEFAULT_ALLOC_SIZE: capacity;
	char* content = (char*)malloc(cap);
	buf.size = 0;
	buf.capacity = cap;
	buf.content = content;
	return buf;
}

void grow(buffer_t *buf) {
	char* content = (char *) malloc(buf->capacity * 2);
	memcpy(content, buf->content, buf->size);
	free(buf->content);
	buf->capacity *= 2;
	buf->content = content;
}

void push_char(buffer_t * buf, char c) {
	if (buf->size + 1 > buf->capacity) grow(buf);
	*(buf->content + buf->size) = c;
	buf->size++;
}

void push(buffer_t *buf, char* str, int size) {
	while (buf->size + size > buf->capacity) {
		grow(buf);
	}
	memcpy(buf->content + buf->size, str, size);
	buf->size += size;
}

char* flush(buffer_t *buf) {
	if (buf->size + 1 > buf->capacity) grow(buf);
	push_char(buf, '\0');
	return buf->content;
}

void reset(buffer_t *buf) {
	buf->size = 0;
}

void buf_free(buffer_t *buf) {
	free(buf->content);
}

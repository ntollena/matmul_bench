#include "oneDNN_conv.h"

// --- Main function (for testing) ---
int main() {

	// Problem sizes (MobiNet-1 for the sizes)
	const int W = 3;
	const int H = 3;
	const int C = 32;
	const int F = 32;
	const int X = 112;
	const int Y = 112;

	// Fixed
	const int B = 1;

	const int size_K = W * H * C * F;
	const int size_Input = B * (X+W-1) * (Y+H-1) * C;
	const int size_Output = B * X * Y * F;

	// Data preparation - build the input arrays
	float* K = (float*) malloc(sizeof(float) * size_K);
	float* Input = (float*) malloc(sizeof(float) * size_Input);
	float* Output = (float*) malloc(sizeof(float) * size_Output);

	int64_t bound = 128;		// For bounded random generation

	// Filling the data with random vals
	for (int count=0; count < size_K; count++) {
		int64_t val = (rand() % bound) - 64;
		K[count] = (float) val;
	}
	for (int count=0; count<size_Input; count++) {
		int64_t val = (rand() % bound) - 64;
		Input[count] = (float) val;
	}

	// Launch!
	conv_onednn_no_record(K, Input, Output, W, H, C, F, X, Y);


	// Compute it from "naive" version (easy to validate)
	float* Output_check = (float*) malloc(sizeof(float) * size_Output);
	conv_naive_impl(K, Input, Output_check, W, H, C, F, X, Y);

	// Compare result of Output_check with result of Output
 	float epsilon = 0.00001;
 	float max_known = 0.0;
 	for (int count=0; count<size_Output; count++) {
 		float temp = fabs(Output_check[count]);
 		if (temp > max_known) {
 			max_known = temp;
 		}
 	}


 	float acceptable_error = epsilon * max_known;
 	bool isIdentical = true;

 	for (int count=0; count<size_Output; count++) {
 		float valOut = Output[count];
 		float valCheck = Output_check[count];

 		float error = fabs(valOut - valCheck);

 		if (error > acceptable_error) {
 			isIdentical = false;
 		}
 	}

 	if (isIdentical) {
 		printf("Convolution result is correct !\n");
 	} else {
 		printf("Convolution result is INCORRECT !\n");	// Go fix that !
 	}

    return 0;
}

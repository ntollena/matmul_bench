#ifndef REORDER_AVX512_H
#define REORDER_AVX512_H

#include <x86intrin.h>

//__attribute__((always_inline)) void transpose_base_stride_avx2(float* __restrict__ base_in, int stride_in, float* __restrict__ base_out, int stride_out);

__attribute__((always_inline)) void transpose_base_stride_avx512(float* __restrict__ base_in, int stride_in,
        float* __restrict__ base_out, int stride_out) {
    __m512  t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, ta, tb, tc, td, te, tf;
    __m512  r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, ra, rb, rc, rd, re, rf;

    r0 = _mm512_load_ps(base_in);
    r1 = _mm512_load_ps(base_in + stride_in);
    r2 = _mm512_load_ps(base_in + 2 * stride_in);
    r3 = _mm512_load_ps(base_in + 3 * stride_in);
    r4 = _mm512_load_ps(base_in + 4 * stride_in);
    r5 = _mm512_load_ps(base_in + 5 * stride_in);
    r6 = _mm512_load_ps(base_in + 6 * stride_in);
    r7 = _mm512_load_ps(base_in + 7 * stride_in);
    r8 = _mm512_load_ps(base_in + 8 * stride_in);
    r9 = _mm512_load_ps(base_in + 9 * stride_in);
    ra = _mm512_load_ps(base_in + 10 * stride_in);
    rb = _mm512_load_ps(base_in + 11 * stride_in);
    rc = _mm512_load_ps(base_in + 12 * stride_in);
    rd = _mm512_load_ps(base_in + 13 * stride_in);
    re = _mm512_load_ps(base_in + 14 * stride_in);
    rf = _mm512_load_ps(base_in + 15 * stride_in);

    // unpack_lo
    // Shuffle (0,_,_)
    //   [0,1, *, *, 2, 3, *, *, 4, 5, *, *, 6, 7, *, * ]
    // , [0',1', *, *, 2', 3', *, *, 4', 5', *, *, 6', 7', *, * ]
    // -> [0, 0', 1, 1', 2, 2', 3, 3', 4, 4', 5, 5', 6, 6', 7, 7']
    // unpack_hi
    //   [ *, *, 0, 1, *, *, 2, 3, *, *, 4, 5, *, *, 6, 7]
    // , [ *, *, 0', 1', *, *, 2', 3', *, *, 4', 5', *, *, 6', 7']
    // -> [0, 0', 1, 1', 2, 2', 3, 3', 4, 4', 5, 5', 6, 6', 7, 7']
    t0  = _mm512_unpacklo_ps(r0,   r1);
    t1  = _mm512_unpacklo_ps(r2,   r3);
    t2  = _mm512_unpacklo_ps(r4,   r5);
    t3  = _mm512_unpacklo_ps(r6,   r7);
    t4  = _mm512_unpacklo_ps(r8,   r9);
    t5  = _mm512_unpacklo_ps(ra,  rb);
    t6  = _mm512_unpacklo_ps(rc,  rd);
    t7  = _mm512_unpacklo_ps(re,  rf);
    t8  = _mm512_unpackhi_ps(r0,   r1);
    t9  = _mm512_unpackhi_ps(r2,   r3);
    ta = _mm512_unpackhi_ps(r4,   r5);
    tb = _mm512_unpackhi_ps(r6,   r7);
    tc = _mm512_unpackhi_ps(r8,   r9);
    td = _mm512_unpackhi_ps(ra,  rb);
    te = _mm512_unpackhi_ps(rc,  rd);
    tf = _mm512_unpackhi_ps(re,  rf);

    // Shuffle (0,_,_)
    r0 = _mm512_shuffle_ps(t0,t1,_MM_SHUFFLE(0,1,0,1));  
    r1 = _mm512_shuffle_ps(t2,t3,_MM_SHUFFLE(0,1,0,1));
    r2 = _mm512_shuffle_ps(t4,t5,_MM_SHUFFLE(0,1,0,1));
    r3 = _mm512_shuffle_ps(t6,t7,_MM_SHUFFLE(0,1,0,1));
    r4 = _mm512_shuffle_ps(t8,t9,_MM_SHUFFLE(0,1,0,1));
    r5 = _mm512_shuffle_ps(ta,tb,_MM_SHUFFLE(0,1,0,1));
    r6 = _mm512_shuffle_ps(tc,td,_MM_SHUFFLE(0,1,0,1));
    r7 = _mm512_shuffle_ps(te,tf,_MM_SHUFFLE(0,1,0,1));
    r8 = _mm512_shuffle_ps(t0,t1,_MM_SHUFFLE(2,3,2,3));
    r9 = _mm512_shuffle_ps(t2,t3,_MM_SHUFFLE(2,3,2,3));
    ra = _mm512_shuffle_ps(t4,t5,_MM_SHUFFLE(2,3,2,3));
    rb = _mm512_shuffle_ps(t6,t7,_MM_SHUFFLE(2,3,2,3));
    rc = _mm512_shuffle_ps(t8,t9,_MM_SHUFFLE(2,3,2,3));
    rd = _mm512_shuffle_ps(ta,tb,_MM_SHUFFLE(2,3,2,3));
    re = _mm512_shuffle_ps(tc,td,_MM_SHUFFLE(2,3,2,3));
    rf = _mm512_shuffle_ps(te,tf,_MM_SHUFFLE(2,3,2,3));

    // Problem Here
    t0  = _mm512_shuffle_f32x4(r0, r1, _MM_SHUFFLE(0,2,0,2));
    t1  = _mm512_shuffle_f32x4(r2, r3, _MM_SHUFFLE(0,2,0,2));
    t2  = _mm512_shuffle_f32x4(r4, r5, _MM_SHUFFLE(0,2,0,2));
    t3  = _mm512_shuffle_f32x4(r6, r7, _MM_SHUFFLE(0,2,0,2));
    t4  = _mm512_shuffle_f32x4(r8, r9, _MM_SHUFFLE(0,2,0,2));
    t5  = _mm512_shuffle_f32x4(ra, rb, _MM_SHUFFLE(0,2,0,2));
    t6  = _mm512_shuffle_f32x4(rc, rd, _MM_SHUFFLE(0,2,0,2));
    t7  = _mm512_shuffle_f32x4(re, rf, _MM_SHUFFLE(0,2,0,2));
    t8  = _mm512_shuffle_f32x4(r0, r1, _MM_SHUFFLE(1,3,1,3));
    t9  = _mm512_shuffle_f32x4(r2, r3, _MM_SHUFFLE(1,3,1,3));
    ta  = _mm512_shuffle_f32x4(r4, r5, _MM_SHUFFLE(1,3,1,3));
    tb  = _mm512_shuffle_f32x4(r6, r7, _MM_SHUFFLE(1,3,1,3));
    tc  = _mm512_shuffle_f32x4(r8, r9, _MM_SHUFFLE(1,3,1,3));
    td  = _mm512_shuffle_f32x4(ra, rb, _MM_SHUFFLE(1,3,1,3));
    te  = _mm512_shuffle_f32x4(rc, rd, _MM_SHUFFLE(1,3,1,3));
    tf  = _mm512_shuffle_f32x4(re, rf, _MM_SHUFFLE(1,3,1,3));

    r0 = _mm512_shuffle_f32x4(t0,  t1, _MM_SHUFFLE(0,2,0,2));
    r1 = _mm512_shuffle_f32x4(t2,  t3, _MM_SHUFFLE(0,2,0,2));
    r2 = _mm512_shuffle_f32x4(t4,  t5, _MM_SHUFFLE(0,2,0,2));
    r3 = _mm512_shuffle_f32x4(t6,  t7, _MM_SHUFFLE(0,2,0,2));
    r4 = _mm512_shuffle_f32x4(t8,  t9, _MM_SHUFFLE(0,2,0,2));
    r5 = _mm512_shuffle_f32x4(ta,  tb, _MM_SHUFFLE(0,2,0,2));
    r6 = _mm512_shuffle_f32x4(tc,  td, _MM_SHUFFLE(0,2,0,2));
    r7 = _mm512_shuffle_f32x4(te,  tf, _MM_SHUFFLE(0,2,0,2));
    r8 = _mm512_shuffle_f32x4(t0,  t1, _MM_SHUFFLE(1,3,1,3));
    r9 = _mm512_shuffle_f32x4(t2,  t3, _MM_SHUFFLE(1,3,1,3));
    ra = _mm512_shuffle_f32x4(t4,  t5, _MM_SHUFFLE(1,3,1,3));
    rb = _mm512_shuffle_f32x4(t6,  t7, _MM_SHUFFLE(1,3,1,3));
    rc = _mm512_shuffle_f32x4(t8,  t9, _MM_SHUFFLE(1,3,1,3));
    rd = _mm512_shuffle_f32x4(ta,  tb, _MM_SHUFFLE(1,3,1,3));
    re = _mm512_shuffle_f32x4(tc,  td, _MM_SHUFFLE(1,3,1,3));
    rf = _mm512_shuffle_f32x4(te,  tf, _MM_SHUFFLE(1,3,1,3));

    _mm512_store_ps(base_out, r0);
    _mm512_store_ps(base_out + stride_out, r1);
    _mm512_store_ps(base_out + 2 * stride_out, r2);
    _mm512_store_ps(base_out + 3 * stride_out, r3);
    _mm512_store_ps(base_out + 4 * stride_out, r4);
    _mm512_store_ps(base_out + 5 * stride_out, r5);
    _mm512_store_ps(base_out + 6 * stride_out, r6);
    _mm512_store_ps(base_out + 7 * stride_out, r7);
    _mm512_store_ps(base_out + 8 * stride_out, r8);
    _mm512_store_ps(base_out + 9 * stride_out, r9);
    _mm512_store_ps(base_out + 10 * stride_out, ra);
    _mm512_store_ps(base_out + 11 * stride_out, rb);
    _mm512_store_ps(base_out + 12 * stride_out, rc);
    _mm512_store_ps(base_out + 13 * stride_out, rd);
    _mm512_store_ps(base_out + 14 * stride_out, re);
    _mm512_store_ps(base_out + 15 * stride_out, rf);
}

#endif

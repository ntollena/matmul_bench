open Batteries
open Common
open Execution
open Bench_types 
open Search

open Conv_search_type
open Conv_spec

module S(Ainfo : Arch_info_t) = struct
  open Search.Schemes(Ainfo)
  module A = Ainfo.A
  module B = Execution.Bench(Ainfo.A)
  module Pred_conv = Arch.Pred_conv(Ainfo.A)

  open Streaming

  type select_mode = Rand of int
                   | Sorted of int
                   | Sorted_mixed of int
                   | All

  let gen_rand_seed () =
    Random.self_init ();
    Random.bits ()

  let show_schemes size =
    print_endline @@ "Size : " ^ show_dict size;
    show_random_partial_tile size 10

  let gen_random_partial num_cand size stride =
    (*     print_endline @@ "Size : " ^ show_dict size; *)
    gen_random_partial_tile (dict_from_tuple size) num_cand stride
    |> Stream.of_list

  let gen_schemes size =
    gen_all_candidates size
    |> Stream.of_list
    |> Stream.map to_scheme

  let gen_random_schemes num_cand size _stride =
    gen_random_from_tree size num_cand None
    |> Stream.of_list
    |> Stream.map to_scheme

  (* Same than above, but with some iteration of the outermost loop imposed to be parallel *)
  let gen_random_schemes_par_resa num_cand o_numthread size _stride =
    gen_random_from_tree size num_cand o_numthread
    |> Stream.of_list
    |> Stream.map to_scheme


  let exec_scheme counters_list (f, c, x, y, h, w) stride scheme =
(*      print_endline @@ [%show: Conv.ConvTypes.tile list] scheme; *)
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01)
        ~num_rep:10
        (Some (true, true, Gcc,  O3))
        [Conv;] counters_list in
(*      print_endline @@ [%show: Conv.tile list] scheme; *)
    let args = match stride with None -> Config.conv_args x w y h c f
                               |Some (sx, sy) ->
                                 Config.conv_args_stride x w y h c f sx sy
    in
     scheme,
     let res =
       (B.exec conf_gen args
                  (Conv_tile scheme) Conv_shape) in
     List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
       counters_list



  let sort_output stream =
    Stream.into Sink.list stream
    |> Utils.sort_by (List.assoc Counter.CYCLES % snd)

  let format_first_line counter_list =
    let open Counter in
    List.map (function CYCLES -> "perf"
                     | c -> show c)
      counter_list
    |> String.concat ","

  let format_tile tile =
    Conv.tile_to_string tile
    |>  String.filter (function '\n' -> false | _ -> true)
    |> fun s -> "\"" ^ s ^ "\""

  let format_flow_line (f,c,y,x,h,w) counter_list res =
    let open Counter in
    List.map (function
        | CYCLES ->
          (List.assoc CYCLES res
           |> Pred_conv.peak_percent f c y x h w
           |> Printf.sprintf "%.2f")
        | c -> List.assoc c res
               |> string_of_int
      ) counter_list
    |> String.concat "," 

  (* Write results to file *)
  let res_write name sizes counter_list =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
      let f = File.open_out (loop (A.arch_name ^ "_" ^ name ^ ".csv")) in
      let first_line = format_first_line counter_list ^ ",scheme" in
      IO.write_line f first_line; f
    and push file (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.fprintf file "%s,%s\n" line (format_tile tile);
      IO.flush file;
      file
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop ()


  (* Write to stdout *)
  let print_flow sizes counter_list =
    let init () =
      let first_line = format_first_line counter_list in
      print_endline first_line
    and push () (_tile, res) =
      let line = format_flow_line sizes counter_list res in
(*       Printf.printf "%s\n%s\n" (Conv.tile_to_string tile) line *)
      Printf.printf "%s\n"  line
    and stop () = () in
    Sink.make ~init ~push ~stop ()

  let exec_all_and_keep_best gen_fun name_csv conv_spec =
    File.with_file_out name_csv @@ fun file -> 
    Printf.fprintf file "name,num_cands,perf\n";
    List.iter
      (fun {name; adapted_size = (f,c,y,x,h,w) as adapted_size; stride;_} ->
         Printf.printf "\n\nNAME : %s\n" name;
         let l = gen_fun adapted_size false
                 |> Stream.map (exec_scheme [Counter.CYCLES] adapted_size stride)
                 |> Stream.into Sink.list in
         let sorted = Utils.sort_by (List.assoc Counter.CYCLES % snd) l in
         Printf.fprintf file "%s,%d,%.3f%%\n" name (List.length l) (
           if List.is_empty sorted then 0.
           else let _, counters = List.hd sorted in
           let cycles = List.assoc Counter.CYCLES  counters in
           Pred_conv.peak_percent f c y x h w cycles
         );
           IO.flush file
      ) conv_spec 

  let exec_rand = exec_all_and_keep_best (gen_random_schemes 20) "tree_rand.csv"

  let exec_multiples gen_rand num_cand num_exec conv_spec =
    File.with_file_out "multiple_rand_more_rep.csv" @@ fun file ->
    Printf.fprintf file "name,num_cand,num_exec,mean,std_dev,scores\n";
    List.iter
      (fun {name; adapted_size = (f,c,y,x,h,w) as adapted_size; stride;_} ->
         let pp = Pred_conv.peak_percent f c y x h w in
         let once () =
           let l = gen_rand num_cand adapted_size
                   |> Stream.map (exec_scheme [Counter.CYCLES] adapted_size stride)
                   |> Stream.into Sink.list in
           let pps = List.map (pp % List.assoc Counter.CYCLES % snd) l in
           Printf.printf "Results : %s\n" @@ [%show: float list] @@ pps;
           List.max pps in
         let scores = List.init num_exec (fun _ -> once ()) in
         let open Common.Stats in
         let scores_string = String.concat "," @@ List.map (Printf.sprintf
                                                              "%.2f") scores in
         let m = mean scores
         and stdd = std_dev scores in
         Printf.fprintf file "%s,%d,%d,%.2f,%.2f,%s\n" name num_cand num_exec m
           stdd scores_string;
         flush file
      ) conv_spec

  let exec_all_random gen_rand counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         Printf.printf "\n\nNAME : %s\n" name;
         gen_rand 20 adapted_size (Option.is_some stride)
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
             <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_all counter_list conv_spec =
    let open Sink in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         Printf.printf "\n\nNAME : %s\n" name;
         gen_schemes adapted_size
         |> Stream.map (exec_scheme counter_list adapted_size stride)
         |> Stream.into
           (res_write name adapted_size counter_list
             <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_all_random_tree counter_list conv_spec =
    exec_all_random gen_random_schemes counter_list conv_spec

  let exec_all_random_partial counter_list conv_spec =
    exec_all_random gen_random_partial counter_list conv_spec


  (* === Guillaume: experiment of stability with random === *)

  (* Experiment protocol:
     - for a given list of microkernel specification (l_conv_spec),
     - do "num_repet" times,
     - for all num_draw in l_num_draw:
        draw randomly num_draw times a scheme,
          run them (with repeat of 30 for stability)
          and keep the performance value of the best performing one
    Output the results on a csv file (filename) whose columns are
        "name_conv, num_draw, repet, best_perf"
  *)
  let exec_rand_stability_experiment name_csv l_conv_spec =
    (* Number of times an experiment is repeated for a given draw *)
    let num_repet = 20 in

    (* List of draw*)
    let l_num_draw = [1; 5; 10; 15; 20; 25; 30] in

    (* Csv file initialization*)
    let out_chan = open_out name_csv in 
    Printf.fprintf out_chan "name_conv,num_draw,repet,best_perf\n";

    (* Experiments! *)
    List.iter (fun {name; adapted_size = (f,c,y,x,h,w) as adapted_size; stride;_} ->
      (* Number of random strategy draw we perform*)
      List.iter (fun num_draw ->
        (* Repetition: number of times we do exactly the same experiment
            (for stability analysis) *)
        for i_repet = 0 to num_repet do
   
          (* Scheme generating function, with number of times it is called *)
          let gen_fun = gen_random_schemes num_draw in

          (* Run these num_draw schemes + collect their results *)
          let l_results = gen_fun adapted_size false
                 |> Stream.map (exec_scheme [Counter.CYCLES] adapted_size stride)
                 |> Stream.into Sink.list in

          (* Sort the results *)
          let sorted_results = Utils.sort_by (List.assoc Counter.CYCLES % snd) l_results in

          (* Best perf is the results with the least cycles + convert it to peak_perf*)
          let perf =
            if List.is_empty sorted_results then 0.
            else let _, counters = List.hd sorted_results in
            let cycles = List.assoc Counter.CYCLES  counters in
            Pred_conv.peak_percent f c y x h w cycles
          in
          Printf.fprintf out_chan "%s,%d,%d,%.3f%%\n"
            name num_draw i_repet perf;

          flush out_chan
        done
      ) l_num_draw
    ) l_conv_spec;
    ()

  (* Same random function called, keep track of all draws *)
  let exec_rand_many_try name_csv o_numthread num_total_draw l_conv_spec =
    (* Csv file initialization*)
    let out_chan = open_out name_csv in 
    Printf.fprintf out_chan "name_conv,num_draw,best_perf\n";

    (* Experiments! *)
    List.iter (fun {name; adapted_size = (f,c,y,x,h,w) as adapted_size; stride;_} ->

      (* Generation function *)
      let gen_fun = gen_random_schemes_par_resa num_total_draw o_numthread in

      (* Run these num_draw schemes + collect their results *)
      let l_results = gen_fun adapted_size false 
            |> Stream.map (exec_scheme [Counter.CYCLES] adapted_size stride)
            |> Stream.into Sink.list
      in
      (* Adding perf information *)
      let l_results = List.map (fun (conv_strat, counters) ->
        let cycles = List.assoc Counter.CYCLES  counters in
        let perf = Pred_conv.peak_percent f c y x h w cycles in
        (conv_strat, perf)
      ) l_results in

      (* Output the result directly (raw data) *)
      List.iter (fun (conv_strat, perf) ->
        Printf.fprintf out_chan "%s,%.3f,\"%s\"\n" name perf (Conv.tile_to_string conv_strat);
        flush out_chan
      ) l_results;
      ()      
    ) l_conv_spec;
    ()

  (* Guillaume - random for a specified list of microkernel *)
  let exec_rand_test_lukernel name_csv num_total_draw 
    ({name; adapted_size = (f,c,y,x,h,w) as adapted_size; stride; _ } as _conv_spec) =

    (* Aux generation function, parametrized by the list of considered µkernel *)
    let gen_random_schemes_test_lukernel lukern num_cand size _stride =
      gen_random_from_tree_aux (Some lukern) size num_cand None
      |> Stream.of_list
      |> Stream.map to_scheme
    in

    (* Building the list of µkernel we consider *)
    (* HARD CODED PART *)
    let uk_class = 
     { varying_dim = Y; range = (17, 17);
       base_kern = [(F, 16); (C, 1); (X, 1); (H, 1); (W, 1)];
       kern_order = [F; X; Y] }
    in
    let y_val = 17 in
    let lukern = [(uk_class, y_val)] in
    (* END OF HARDCODED PART *)


    (* Csv file initialization*)
    let out_chan = open_out name_csv in 
    Printf.fprintf out_chan "name_conv,num_draw,best_perf\n";
   
    (* Generation function *)
    let gen_fun = gen_random_schemes_test_lukernel lukern num_total_draw in

    (* Run these num_draw schemes + collect their results *)
    let l_results = gen_fun adapted_size false 
          |> Stream.map (exec_scheme [Counter.CYCLES] adapted_size stride)
          |> Stream.into Sink.list
    in
    (* Adding perf information *)
    let l_results = List.map (fun (conv_strat, counters) ->
      let cycles = List.assoc Counter.CYCLES  counters in
      let perf = Pred_conv.peak_percent f c y x h w cycles in
      (conv_strat, perf)
    ) l_results in

    (* Output the result directly (raw data) *)
    List.iter (fun (conv_strat, perf) ->
      Printf.fprintf out_chan "%s,%.3f,\"%s\"\n" name perf (Conv.tile_to_string conv_strat);
      flush out_chan
    ) l_results;
    ()

end


(* === Launching  part === *)

(* Warning : change that *)
(* module S_avx512 = S(Arch_info.Pinocchio) *)
module S_avx2 = S(Arch_info.Dracula)


let zip_pair l1 l2 = List.map2 (fun x y -> x, y) l1 l2

(*
let () = S_avx512.exec_all_random Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2]
    Conv_sizes.all_spec
*)
(*let () = S_avx2.exec_rand Conv_sizes.all_spec *)
(* let () = S_avx2.exec_rand_stability_experiment "random_stab_experiment.csv" Conv_sizes.all_spec *)

let () = S_avx2.exec_rand_many_try "test_random_par_selection.csv" (Some 17) 10 [Conv_sizes.yolo9000_18_spec]


(* let () = S_avx2.exec_rand_test_lukernel "test_singleuk_Yolo18.csv" 500 Conv_sizes.yolo9000_18_spec *)

(* let () = S_avx512.exec_all_random_partial Counter.[CYCLES]
  (*     [Conv_sizes.yolo9000_12_spec] *)
  @@ List.filter (fun {name; _} -> not (String.equal "Yolo9000_23" name)) Conv_sizes.all_spec*
*)

(* let () = S_avx512.see_all_l1 Conv_sizes.all_spec *)
(* let () = S_avx512.print_all_status Conv_sizes.all_spec *)

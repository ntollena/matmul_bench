open Common
open Execution
open Batteries

module Exec(A: Arch.Arch_t) = struct
  include Bench(A)
  module Pred = Arch.Pred_mm(A)
  open Codegen

  let dim_sizes tile =
    let open MM in
    let open Gen(A) in
    let ds = dim_tile_size tile in
    ds I, ds J, ds K

  let exec_scheme_size_stride benches counters_list (i, j, k) scheme  =
    let conf_gen = Config.build_mm_config ~error:(Config.EPSILON 0.001) ~num_rep:30
        (Some (true, true, Gcc,  O2))
        benches counters_list in
    let args = Config.mm_args i j k in
    exec conf_gen args (MM_tile scheme)

  let exec_scheme_size counters_list size scheme  =
    let res = exec_scheme_size_stride [Gen] counters_list size scheme MM_shape in
    List.map (fun counter -> counter, Config.select_bench_counter Gen counter res)
      counters_list

  let exec_get_peak_perf_percent ((i, j, k) as size) scheme =
    let open Counter in
    let cycles = List.assoc CYCLES (exec_scheme_size [CYCLES] size scheme) in 
    Pred.peak_percent i j k cycles


  let gen_file_size postfix (i,j,k) scheme =
    let args = Config.mm_args i j k in
    let open Config in
    gen_file MM_layout args (MM_tile scheme) postfix

end

(* simple helper function to create a range of n_sample points linearly distributed betwen min_x and max_x*)
let linspace n_sample min_x max_x = 
    let s = (max_x - min_x) / n_sample in
    List.init n_sample (fun i -> min_x + i * s)


(* Execute the generated code for a given pb size fixed by k*)
let test_pb_size k =
  let open Exec(Arch.Dracula) in
  let unr_scheme = MM.Tile_T.[ V J;  U (2, J); U (6, I); T (k, K); Hoist_vars [K]
                                ] in
  exec_get_peak_perf_percent (dim_sizes unr_scheme) unr_scheme

(* Measure 3 times because of error and get the median before plot *)
let test_3_times_pb_size k = 
    ((test_pb_size k), (test_pb_size k), (test_pb_size k))
    
let print_as_csv tab_k_pp = 
    Printf.printf "pb_size,pk_perf1,pk_perf2,pk_perf3\n";
    List.iter (fun (k, (pp1, pp2, pp3)) -> 
        Printf.printf "%d,%f,%f,%f\n" (k * 16 * 6) pp1 pp2 pp3
    ) tab_k_pp


(* num of floats so divide by 4
L1 = 256KiB / 4
L2 = 4 MiB / 4 
L3 = 8 MiB / 4 *)
let l1_size = 256 * 256
let l2_size = 1024 * 1024
let l3_size = 2 * 1024 * 1024

let max_k_for_cache_level i = 
    match i with 
    | 1 -> (l1_size - 6 * 16) / (6 + 16)
    | 2 -> (l2_size - 6 * 16) / (6 + 16)
    | 3 -> (l3_size - 6 * 16) / (6 + 16)
    | _ -> -1


(* 200 * 3 sample in total distributed between L1, L2, L3 and cache limited pbs*)
let n_samples = [|50; 70; 60; 20|]
let min_ks = [|1; (max_k_for_cache_level 1) + 1; (max_k_for_cache_level 2) + 1; (max_k_for_cache_level 3) + 1|]
let max_ks = [|max_k_for_cache_level 1; max_k_for_cache_level 2; max_k_for_cache_level 3; 100000|]
let range_k = List.init 4 (fun i -> linspace n_samples.(i) min_ks.(i) max_ks.(i)) |> List.flatten
let peak_perfs = List.map test_3_times_pb_size range_k
let tab_k_pp  = List.combine range_k peak_perfs
let _ = print_as_csv tab_k_pp
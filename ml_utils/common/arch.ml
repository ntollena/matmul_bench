open Batteries
open Tensor_loops

module type Arch_t = sig
  val vec_size : int
  val num_fma_port : int
  val descr : string

  (* Name of arch in blis/lib *)
  val arch_name : string
  val arch_type : string
  val init : unit Shexp_process.t
  val ld_libraries : string list
  val include_dirs : string list
  val runtime_vars : (string * string) list

  module M : Arch.Vec_arch_t
end

module Nehalem : Arch_t = struct
  let vec_size = 8
  let num_fma_port = 2

  let descr =
    Printf.sprintf
      "Nehalem Intel(R) Xeon(R) CPU E3-1240 v6 @ 3.70GHz AVX2, vec_size: %d, \
       num_fma_port: %d"
      vec_size num_fma_port

  let arch_name = "nehalem"
  let arch_type = "AVX2"
  let init = Shexp_process.return ()
  let include_dirs = [ "/home/hbrunie/usrpar/include" ]
  let ld_libraries = [ "/home/hbrunie/usrpar/lib" ]
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "AVX2") ]

  module M = Arch.AVX2_unaligned
end

module Haswell : Arch_t = struct
  let vec_size = 8
  let num_fma_port = 2

  let descr =
    Printf.sprintf "Haswell AVX2, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "haswell"
  let arch_type = "AVX2"
  let init = Shexp_process.return ()
  let ld_libraries = []

  (* ["/home/nico/onednn-git/pkg/onednn-git/usr/lib/"] *)
  (* LOCAL MODIF GUI *)
  let include_dirs = []
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "AVX2") ]

  module M = Arch.AVX2_unaligned
end

module Dracula : Arch_t = struct
  let vec_size = 8
  let num_fma_port = 2

  let descr =
    Printf.sprintf "Haswell AVX2, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "haswell"
  let arch_type = "AVX2"
  let init = Shexp_process.return ()
  let ld_libraries = []

  (* ["/home/ntollenaere/.local/lib/"] *)
  (* LOCAL MODIF GUI - DO NOT COMMIT !!! *)
  let include_dirs = [] (* ["/home/ntollenaere/.local/include/"] *)
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "AVX2") ]

  module M = Arch.AVX2_unaligned
end

module Silver : Arch_t = struct
  let vec_size = 16
  let num_fma_port = 1

  let descr =
    Printf.sprintf "Silver AVX512, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "skx"
  let arch_type = "AVX512"

  (* For some reasons absolute paths are apparently required*)
  let ld_libraries = [ "/home/nico/.local/lib/" ]
  let include_dirs = [ "/home/nico/.local/include/" ]
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "ALL") ]

  (*let init = Shexp_process.run "full_power" []*)
  let init = Shexp_process.return ()

  module M = Arch.AVX512_unaligned
end

module Sky_lake : Arch_t = struct
  let vec_size = 16
  let num_fma_port = 2

  let descr =
    Printf.sprintf "Sky_lake AVX512, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "skx"
  let arch_type = "AVX512"
  let ld_libraries = [ "/home/nico/onednn-git/pkg/onednn-git/usr/lib/" ]
  let include_dirs = []
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "ALL") ]
  let init = Shexp_process.run "full_power" []

  (*let init = Shexp_process.return ()*)
  module M = Arch.AVX512_unaligned
end

module XeonGold5220 : Arch_t = struct
  let vec_size = 16
  let num_fma_port = 1

  let descr =
    Printf.sprintf "Sky_lake AVX512, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "skx"
  let arch_type = "AVX512"
  let ld_libraries = []
  let include_dirs = []
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "ALL") ]
  let init = Shexp_process.return ()

  module M = Arch.AVX512_unaligned
end

module XeonGold6130 : Arch_t = struct
  let vec_size = 16
  let num_fma_port = 2

  let descr =
    Printf.sprintf "Sky_lake AVX512, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "skx"
  let arch_type = "AVX512"
  let ld_libraries = []
  let include_dirs = []
  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "ALL") ]
  let init = Shexp_process.return ()

  module M = Arch.AVX512_unaligned
end

module Pinocchio : Arch_t = struct
  let vec_size = 16
  let num_fma_port = 2

  let descr =
    Printf.sprintf "Sky_lake AVX512, vec_size: %d, num_fma_port: %d" vec_size
      num_fma_port

  let arch_name = "skx"
  let arch_type = "AVX512"
  let ld_libraries = [ "/home/nico/.local/lib/"; "/home/nico/libxsmm/lib/" ]

  let include_dirs =
    [ "/home/nico/.local/include/"; "/home/nico/libxsmm/include/" ]

  let runtime_vars = [ ("DNNL_MAX_CPU_ISA", "ALL") ]
  let init = Shexp_process.return ()

  module M = Arch.AVX512_unaligned
end

module Pred_mm (Arch : Arch_t) = struct
  let peak_perf i_size j_size k_size =
    i_size * j_size * k_size / Arch.(vec_size * num_fma_port)

  let peak_percent i_size j_size k_size cyc =
    let peak = peak_perf i_size j_size k_size in
    float_of_int peak /. float_of_int cyc *. 100.
end

module Pred_conv (Arch : Arch_t) = struct
  let peak_perf i_size j_size w_size h_size c_size f_size =
    i_size * j_size * w_size * h_size * c_size * f_size
    / Arch.(vec_size * num_fma_port)

  let peak_percent x_size y_size w_size h_size c_size f_size cyc =
    let peak = peak_perf x_size y_size w_size h_size c_size f_size in
    float_of_int peak /. float_of_int cyc *. 100.
end

module Pred_tc (Arch : Arch_t) = struct
  let peak_perf lsizes_left lsizes_right lsizes_red =
    let prod_left = List.fold_left (fun acc size -> acc * size) 1 lsizes_left in
    let prod_right =
      List.fold_left (fun acc size -> acc * size) 1 lsizes_right
    in
    let prod_red = List.fold_left (fun acc size -> acc * size) 1 lsizes_red in
    prod_left * prod_right * prod_red / Arch.(vec_size * num_fma_port)

  let peak_percent lsizes_left lsizes_right lsizes_red cyc =
    let peak = peak_perf lsizes_left lsizes_right lsizes_red in
    float_of_int peak /. float_of_int cyc *. 100.
end

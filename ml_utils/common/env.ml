open Shexp_process
open Shexp_process.Infix
open Batteries

let find_root () =
  let expr = get_env "TTILE_ROOT" 
    >>|  Option.default "../" in
  eval expr

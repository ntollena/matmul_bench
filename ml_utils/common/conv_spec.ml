type conv_spec = { original_size: int * int * int * int * int * int;
                   adapted_size: int * int * int * int * int * int;
                   name : string;
                   stride: (int * int) option
                 }

let adapt_to_stride stride ((f,c,y,x,h,w) as sizes) =
  match stride with
    Some (sx, sy) -> (f,c,y / sy,x / sx,h,w)
  | None -> sizes

let adapt_to_stride_list strides =
  List.map2 adapt_to_stride strides

let to_spec name original_size stride =
  let adapted_size = adapt_to_stride stride original_size in
  {name; original_size; adapted_size; stride}

let to_spec_list names sizes strides =
  Utils.map3 to_spec names sizes strides

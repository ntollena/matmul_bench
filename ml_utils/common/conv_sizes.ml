open Conv_spec

(* f, c, y, x, h, w *)
let yolo9000_0_size =
  32,3,544,544,3,3

let yolo9000_2_size =
  64,32,272,272,3,3

let yolo9000_4_size =
  128,64,136,136,3,3

let yolo9000_5_size =
  64,128,136,136,1,1

let yolo9000_8_size =
  256,128,68,68,3,3

let yolo9000_9_size =
  128,256,68,68,1,1

let yolo9000_12_size =
  512,256,34,34,3,3

let yolo9000_13_size =
  256,512,34,34,1,1

let yolo9000_18_size =
  1024,512,17,17,3,3

let yolo9000_19_size =
  512,1024,17,17,1,1

let yolo9000_23_size_real =
  28269, 1024, 17, 17, 1,1

let yolo9000_23_size =
  28272, 1024, 17, 17, 1,1

let yolo_names =
  ["Yolo9000_00"; "Yolo9000_02"; "Yolo9000_04";
   "Yolo9000_05"; "Yolo9000_08"; "Yolo9000_09";
   "Yolo9000_12"; "Yolo9000_13"; "Yolo9000_18";
   "Yolo9000_19"; "Yolo9000_23"]

let yolo_names_reasonable =
  ["Yolo9000_00"; "Yolo9000_02"; "Yolo9000_04";
   "Yolo9000_05"; "Yolo9000_08"; "Yolo9000_09";
   "Yolo9000_12"; "Yolo9000_13"; "Yolo9000_18";
   "Yolo9000_19"; ]

let yolo_names_no19 =
  ["Yolo9000_00"; "Yolo9000_02"; "Yolo9000_04";
   "Yolo9000_05"; "Yolo9000_08"; "Yolo9000_09";
   "Yolo9000_12"; "Yolo9000_13"; "Yolo9000_18"; ] 

let all_none l = List.init (List.length l) (Fun.const None)

let yolo9000_0_spec = to_spec "Yolo9000_00" yolo9000_0_size None
let yolo9000_2_spec = to_spec "Yolo9000_02" yolo9000_2_size None
let yolo9000_4_spec = to_spec "Yolo9000_04" yolo9000_4_size None
let yolo9000_5_spec = to_spec "Yolo9000_05" yolo9000_5_size None
let yolo9000_8_spec = to_spec "Yolo9000_08" yolo9000_8_size None
let yolo9000_9_spec = to_spec "Yolo9000_09" yolo9000_9_size None
let yolo9000_12_spec = to_spec "Yolo9000_12" yolo9000_12_size None
let yolo9000_13_spec = to_spec "Yolo9000_13" yolo9000_13_size None
let yolo9000_18_spec = to_spec "Yolo9000_18" yolo9000_18_size None
let yolo9000_19_spec = to_spec "Yolo9000_19" yolo9000_19_size None
let yolo9000_23_spec = to_spec "Yolo9000_23" yolo9000_23_size None

let yolo_sizes =
  [yolo9000_0_size; yolo9000_2_size; yolo9000_4_size;
   yolo9000_5_size; yolo9000_8_size; yolo9000_9_size;
   yolo9000_12_size; yolo9000_13_size; yolo9000_18_size;
   yolo9000_19_size; yolo9000_23_size]

let yolo_strides = all_none yolo_sizes

let yolo_sizes_reasonable =
  [yolo9000_0_size; yolo9000_2_size; yolo9000_4_size;
   yolo9000_5_size; yolo9000_8_size; yolo9000_9_size;
   yolo9000_12_size; yolo9000_13_size; yolo9000_18_size;
   yolo9000_19_size; ]

let yolo_strides_reasonable = all_none yolo_sizes_reasonable

let yolo_sizes_no19 =
  [yolo9000_0_size; yolo9000_2_size; yolo9000_4_size;
   yolo9000_5_size; yolo9000_8_size; yolo9000_9_size;
   yolo9000_12_size; yolo9000_13_size; yolo9000_18_size;
  ]

let yolo_strides_no19 = all_none yolo_sizes_no19

let mobilNet_1_size =
  32,32,112,112,3,3

let mobilNet_1_stride = None

let mobilNet_2_size =
  64,64,112,112,3,3

let mobilNet_2_stride = Some (2, 2)

let mobilNet_3_size =
  128,128,56,56,3,3

let mobilNet_3_stride = None

let mobilNet_4_size =
  128,128,56,56,3,3

let mobilNet_4_stride = Some (2, 2)

let mobilNet_5_size =
  256,256,28,28,3,3

let mobilNet_5_stride = None

let mobilNet_6_size =
  256,256,28,28,3,3

let mobilNet_6_stride = Some (2, 2)

let mobilNet_7_size =
  512,512,14,14,3,3

let mobilNet_7_stride = None

let mobilNet_8_size =
  512,512,14,14,3,3

let mobilNet_8_stride = Some (2, 2)

let mobilNet_9_size =
  1024,1024,7,7,3,3

let mobilNet_9_stride = None

let mobilNet_names = [
  "MobilNet_01"; "MobilNet_02"; "MobilNet_03";
  "MobilNet_04"; "MobilNet_05"; "MobilNet_06";
  "MobilNet_07"; "MobilNet_08"; "MobilNet_09";
]

let mobilNet_1_spec = to_spec "MobilNet_01" mobilNet_1_size mobilNet_1_stride
let mobilNet_2_spec = to_spec "MobilNet_02" mobilNet_2_size mobilNet_2_stride
let mobilNet_3_spec = to_spec "MobilNet_03" mobilNet_3_size mobilNet_3_stride
let mobilNet_4_spec = to_spec "MobilNet_04" mobilNet_4_size mobilNet_4_stride
let mobilNet_5_spec = to_spec "MobilNet_05" mobilNet_5_size mobilNet_5_stride
let mobilNet_6_spec = to_spec "MobilNet_06" mobilNet_6_size mobilNet_6_stride
let mobilNet_7_spec = to_spec "MobilNet_07" mobilNet_7_size mobilNet_7_stride
let mobilNet_8_spec = to_spec "MobilNet_08" mobilNet_8_size mobilNet_8_stride
let mobilNet_9_spec = to_spec "MobilNet_09" mobilNet_9_size mobilNet_9_stride

let mobilNet_sizes = [
  mobilNet_1_size; mobilNet_2_size; mobilNet_3_size;
  mobilNet_4_size; mobilNet_5_size; mobilNet_6_size;
  mobilNet_7_size; mobilNet_8_size; mobilNet_9_size;
]

let mobilNet_strides_none = all_none mobilNet_sizes

let mobilNet_strides = [
  mobilNet_1_stride; mobilNet_2_stride; mobilNet_3_stride;
  mobilNet_4_stride; mobilNet_5_stride; mobilNet_6_stride;
  mobilNet_7_stride; mobilNet_8_stride; mobilNet_9_stride;
]

let resNet18_1_size =
  64,3,224,224,7,7

let resNet18_1_stride = Some (2, 2)

let resNet18_2_size =
  64,64,56,56,3,3

let resNet18_2_stride = None

let resNet18_3_size =
  64,64,56,56,1,1

let resNet18_3_stride = None

let resNet18_4_size =
  128,64,56,56,3,3

let resNet18_4_stride = Some (2, 2)

(* stride = 2 *)
let resNet18_5_size =
  128,64,56,56,1,1
  (* TODO Why is 5 similar to 4 ? *) 
let resNet18_5_stride = Some (2, 2)

let resNet18_6_size =
  128,128,28,28,3,3

let resNet18_6_stride = None

let resNet18_7_size =
  256,128,28,28,3,3

let resNet18_7_stride = Some (2, 2)

let resNet18_8_size =
  256,128,28,28,3,3

let resNet18_8_stride = None

let resNet18_9_size =
  256,256,14,14,3,3

let resNet18_9_stride = None

let resNet18_10_size =
  512,512,14,14,3,3

let resNet18_10_stride = Some (2, 2)

let resNet18_11_size =
  512,256,14,14,1,1

let resNet18_11_stride = Some (2, 2)

let resNet18_12_size =
  512,512,7,7,3,3

let resNet18_12_stride = None

let resNet18_1_spec = to_spec "ResNet18_01" resNet18_1_size resNet18_1_stride
let resNet18_2_spec = to_spec "ResNet18_02" resNet18_2_size resNet18_2_stride
let resNet18_3_spec = to_spec "ResNet18_03" resNet18_3_size resNet18_3_stride
let resNet18_4_spec = to_spec "ResNet18_04" resNet18_4_size resNet18_4_stride
let resNet18_5_spec = to_spec "ResNet18_05" resNet18_5_size resNet18_5_stride
let resNet18_6_spec = to_spec "ResNet18_06" resNet18_6_size resNet18_6_stride
let resNet18_7_spec = to_spec "ResNet18_07" resNet18_7_size resNet18_7_stride
let resNet18_8_spec = to_spec "ResNet18_08" resNet18_8_size resNet18_8_stride
let resNet18_9_spec = to_spec "ResNet18_09" resNet18_9_size resNet18_9_stride
let resNet18_10_spec = to_spec "ResNet18_10" resNet18_10_size resNet18_10_stride
let resNet18_11_spec = to_spec "ResNet18_11" resNet18_11_size resNet18_11_stride
let resNet18_12_spec = to_spec "ResNet18_12" resNet18_12_size resNet18_12_stride

let resNet_names = [
  "ResNet18_01"; "ResNet18_02"; "ResNet18_03";
  "ResNet18_04"; "ResNet18_05"; "ResNet18_06";
  "ResNet18_07"; "ResNet18_08"; "ResNet18_09";
  "ResNet18_10"; "ResNet18_11"; "ResNet18_12";
]


let resNet_sizes = [
  resNet18_1_size; resNet18_2_size; resNet18_3_size;
  resNet18_4_size; resNet18_5_size; resNet18_6_size;
  resNet18_7_size; resNet18_8_size; resNet18_9_size;
  resNet18_10_size; resNet18_11_size; resNet18_12_size;
]

let resNet_strides_none = all_none resNet_sizes

let resNet_strides = [
  resNet18_1_stride; resNet18_2_stride; resNet18_3_stride;
  resNet18_4_stride; resNet18_5_stride; resNet18_6_stride;
  resNet18_7_stride; resNet18_8_stride; resNet18_9_stride;
  resNet18_10_stride; resNet18_11_stride; resNet18_12_stride;
]


let all_names = yolo_names @ mobilNet_names @ resNet_names
let all_sizes_no_stride = yolo_sizes @ mobilNet_sizes @ resNet_sizes

let all_strides = yolo_strides @ mobilNet_strides @ resNet_strides
let all_sizes = (yolo_sizes @ mobilNet_sizes @ resNet_sizes)

let all_sizes_adapted = adapt_to_stride_list all_strides all_sizes

let strides_none = all_none all_names

let exhaustives_names = ["Yolo9000_00"; "Yolo9000_02"; "Yolo9000_04"; "Yolo9000_05";
                        "Yolo9000_08"; "Yolo9000_09"; "Yolo9000_12"; "Yolo9000_13";
                        "Yolo9000_18"; "Yolo9000_19";
                        "MobilNet_02"; "MobilNet_03"; "MobilNet_05";
                        "MobilNet_06"; "MobilNet_08"; "MobilNet_09"; 
                          "ResNet18_04"; "ResNet18_07"; "ResNet18_08"; "ResNet18_10"
               (*yolo23 TODO *)]

let exhaustive_sizes = [yolo9000_0_size; yolo9000_2_size; yolo9000_4_size; yolo9000_5_size;
                        yolo9000_8_size; yolo9000_9_size; yolo9000_12_size; yolo9000_13_size;
                        yolo9000_18_size; yolo9000_19_size;
                        mobilNet_2_size; mobilNet_3_size; mobilNet_5_size;
                        mobilNet_6_size; mobilNet_8_size; mobilNet_9_size; 
                          resNet18_4_size; resNet18_7_size; resNet18_8_size; resNet18_10_size
               (*yolo23 TODO *)]

let exhaustive_strides = List.init 10 (Fun.const None) @
                         [mobilNet_2_stride; mobilNet_3_stride; mobilNet_5_stride;
                        mobilNet_6_stride; mobilNet_8_stride; mobilNet_9_stride; 
                          resNet18_4_stride; resNet18_7_stride; resNet18_8_stride; resNet18_10_stride
               ]

let all_spec = to_spec_list all_names all_sizes all_strides

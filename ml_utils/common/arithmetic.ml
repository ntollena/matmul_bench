open Batteries

let range_exc n m = if m < n then [] else List.init (m - n) (fun i -> n + i)

let range_incl n m = range_exc n (m + 1)

let (--) = range_exc 

let decompose n k = 
  let rec loop l n k = match k, n with
    | 0, 1 -> [l]
    | 1, _ -> [n::l]
    | 0, _ -> []
    | _, _ ->
      range_incl 1 n
      |> List.concat_map
        (fun i -> if n mod i <> 0 then []
          else loop (i::l) (n / i) (k - 1)
        )
  in loop [] n k

let all_divisor n =
  List.init n  ((+) 1)
|> List.filter (fun k -> n mod k = 0)


(* Output the list of prime factor of n. Increasing order, might have multiples *)
let list_prime_divisor n =
  let rec list_prime_divisor_aux res k n =
    (* DEBUG
    Printf.printf "list_prime_divisor : k = %d , n= %d\n" k n;
    *)

    (* Init cases *)
    if (n==1) then res else
    if (k>=n) then n::res else

    (* Recursion cases *)
    if (n mod k == 0) then
      list_prime_divisor_aux (k::res) k (n/k)
    else
      list_prime_divisor_aux res (k+1) n
  in
  list_prime_divisor_aux [] 2 n




(* Find u, v such that u * a + v * b = n if it exists *)
let pair_dec a b n =
  let rec loop a b k  =
    if (n - k * a < b) then None else 
    if (n - k * a) mod b = 0
    then Some (k, (n - k * a) / b)
    else loop a b (k + 1) in
  if a < b then loop a b 1
  else Option.map (fun (x, y) -> y, x) @@ loop b a 1

let all_pair_dec a b n =
  let rec loop l a b k  =
    if (n - k * a < b) then l else 
    if (n - k * a) mod b = 0
    then loop ((k, (n - k * a) / b) :: l) a b (k + 1)
    else loop l a b (k + 1) in
  if a < b then loop [] a b 1
  else List.map (fun (x, y) -> y, x) @@ loop [] b a 1

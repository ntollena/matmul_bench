open Batteries

let mean lf =
  let len = List.length lf in
  if len = 0 then 0.
  else let s = List.fold_left (+.) 0. lf in
    s /. (float_of_int len)
         
let geomean lf =
  let len = List.length lf in
  if len = 0 then 1.
  else
    let prod = List.fold_left ( *. ) 1. lf in
    Float.pow prod (1. /. float_of_int len)

let median lf =
  let a = Array.of_list lf in
  Array.sort Float.compare  a;
  let len =  Array.length a in
  if len = 0 then failwith "No median for Empty List"
  else a.(len / 2)

let std_dev lf =
  let m = mean lf in
  Float.sqrt @@ mean @@ List.map (fun  x -> (x -. m) *. (x -. m)) lf

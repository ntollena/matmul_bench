type 'a t = (::) of 'a * 'a list

let hd ( hd :: _) = hd

let tl (_ :: tl) = tl

let map f (head :: tail) = f head :: (List.map f tail)

let cons h t = h :: t

let to_list (head :: tail) = List.(head :: tail)

let from_list = function
  | List.(h :: t) -> Some (h :: t)
  | [] -> None

let filter_map f (head :: tail) = match f head with
  Some x -> List.(x :: List.filter_map f tail)
  | None -> List.filter_map f tail

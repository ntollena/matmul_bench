open Batteries
open Common
open Execution
open Bench_types
open Tc_sugar

module Exec (A : Arch.Arch_t) = struct
  include Bench (A)
  module Pred = Arch.Pred_tc (A)

  (* Execution functions *)
  let exec_scheme_size_stride ?(wrap_code = ("", "")) ?(exec_args_bench = None)
      ?(regen_code = true) ?(nthreads = 1) benches_w_lay counters_list
      (lsizes_left, lsizes_red, lsizes_right) scheme shape =
    let conf_gen =
      Config.build_tc_config_with_layout ~error:(Config.EPSILON 0.001)
        ~num_rep:5
        (Some (regen_code, true, Gcc, O2))
        (* FIRST bool to false means that code in gen_tc.c is not re gen on evry call to this function. *)
        benches_w_lay counters_list
    in
    let args = Config.tc_args lsizes_left lsizes_red lsizes_right in
    let exec_args_conf =
      match exec_args_bench with
      | None -> None
      | Some ((ll, lr, lrr), bench) ->
          let old_conf =
            Config.build_tc_config_with_layout ~error:(Config.EPSILON 0.001)
              ~num_rep:5
              (Some (regen_code, true, Gcc, O2))
              (* FIRST bool to false means that code in gen_tc.c is not re gen on evry call to this function. *)
              bench counters_list
          in
          Some (Config.tc_args ll lr lrr, old_conf)
    in
    exec ~wrap_code ~exec_args_conf ~nthreads conf_gen args (TC_tile scheme)
      (TC_shape shape)

  let exec_scheme_size_tc_only ?(wrap_code = ("", "")) ?(exec_args_conf = None)
      ?(regen_code = true) ?(nthreads = 1) counters_list llsizes
      (l_layout_A, l_layout_B, l_layout_C) scheme shape =
    let layout = Config.TC_layout (l_layout_A, l_layout_B, l_layout_C) in
    let benches_w_lay = [ (Config.TcGen, layout) ] in

    let exec_args_bench =
      match exec_args_conf with
      | None -> None
      | Some (arg, (lA, lB, lC)) ->
          let old_layout = Config.TC_layout (lA, lB, lC) in
          Some (arg, [ (Config.TcGen, old_layout) ])
    in
    let res =
      exec_scheme_size_stride ~wrap_code ~exec_args_bench ~regen_code ~nthreads
        benches_w_lay counters_list llsizes scheme shape
    in

    List.map
      (fun counter -> (counter, Config.select_bench_counter TcGen counter res))
      counters_list

  let exec_print_counters_size ?(wrap_code = ("", "")) ?(exec_args_conf = None)
      ?(regen_code = true) ?(nthreads = 1) counters_list
      ((lsizes_left, lsizes_red, lsizes_right) as llsizes) ll_layout scheme
      shape =
    let open Counter in
    List.iter
      (fun (counter, n) ->
        match counter with
        | CYCLES ->
            let p = Pred.peak_percent lsizes_left lsizes_right lsizes_red n in
            Printf.printf "%d cycles, peak perf : %.2f%%\n" n p
        | _ -> Printf.printf "%s : %d\n" (show counter) n)
      (exec_scheme_size_tc_only ~wrap_code ~exec_args_conf ~regen_code ~nthreads
         counters_list llsizes ll_layout scheme shape)
end

(* ====================================== *)

let tc_debug_test ?(args_to_wrap = None) ?(regen_code = true) ?(nthreads = 1)
    parameters =
  let module A = Arch.Dracula in
  let open Exec (A) in
  let open Inject_code.WrapperChangingLayout (A) in
  (* Suppose AVX 2 *)
  let (scheme, lldimnames, llsizes_tc, ll_layout), wrap_code, exec_args_conf =
    match args_to_wrap with
    | None ->
        let usual_params_gen = Tc_sugar.build_basic_tc_args parameters in
        (usual_params_gen, ("", ""), None)
    | Some new_param ->
        let new_gen = Tc_sugar.build_basic_tc_args new_param in
        let wrapper = gen_packing_wrapper_code new_param parameters in
        let _, _, old_args, old_layout =
          Tc_sugar.build_basic_tc_args parameters
        in
        (new_gen, wrapper, Some (old_args, old_layout))
  in

  exec_print_counters_size ~wrap_code ~exec_args_conf ~regen_code ~nthreads
    Counter.[ CYCLES; CACHE_MISS_L2; BR_MISPRED ]
    llsizes_tc ll_layout scheme lldimnames

let tc_test_with_and_without_packing parameters_pack =
  Printf.printf "With packing: ";
  let _ = tc_debug_test parameters_pack in
  let scheme_np =
    List.filter
      (fun tile ->
        match tile with
        | Tc_sugar.Pack_trans _ | Tc_sugar.Pack_tens _ -> false
        | _ -> true)
      parameters_pack.scheme
  in
  let parameters_non_pack = { parameters_pack with scheme = scheme_np } in
  Printf.printf "Without packing: ";
  tc_debug_test parameters_non_pack

let parallel_example () =
  let _old_param =
    {
      dim_sizes_left = [ ("a", 27); ("c", 72 * 4) ];
      dim_sizes_red = [ ("e", 72); ("d", 72) ];
      dim_sizes_right = [ ("b", 27) ];
      layout_out = [ "a"; "b"; "c" ];
      layout_left = [ "a"; "d"; "e"; "c" ];
      layout_right = [ "e"; "b"; "d" ];
      scheme =
        [
          V "c";
          U (3, "c");
          U (3, "b");
          T (36, "d");
          T (8, "e");
          Hoist_vars [ "d"; "e" ];
          (*^ Micro kernel ^*)
          T (9, "a");
          T (3, "b");
          T (3, "c");
          (*^ Level 1 tiling ^*)
          T (3, "a");
          T (3, "b");
          T (2, "d");
          T (9, "e");
          T_par (4, "c");
          (*^ Level 2 tiling ^*)
        ];
    }
  in

  let _new_dim_param =
    {
      _old_param with
      dim_sizes_left = [ ("a", 27); ("x", 72); ("y", 4) ];
      layout_out = [ "y"; "a"; "b"; "x" ];
      layout_left = [ "a"; "d"; "e"; "y"; "x" ];
      scheme =
        [
          V "x";
          U (3, "x");
          U (3, "b");
          T (36, "d");
          T (8, "e");
          Hoist_vars [ "d"; "e" ];
          (*^ Micro kernel ^*)
          T (9, "a");
          T (3, "b");
          T (3, "x");
          (*^ Level 1 tiling ^*)
          T (3, "a");
          T (3, "b");
          T (2, "d");
          T (9, "e");
          T_par (4, "y");
          (*^ Level 2 tiling ^*)
        ];
    }
  in

  let _pack_param =
    {
      _old_param with
      scheme =
        [
          V "c";
          U (3, "c");
          U (3, "b");
          T (36, "d");
          T (8, "e");
          Hoist_vars [ "d"; "e" ];
          (*^ Micro kernel ^*)
          T (9, "a");
          T (3, "b");
          T (3, "c");
          (*^ Level 1 tiling ^*)
          T (3, "a");
          T (3, "b");
          T (2, "d");
          T (9, "e");
          Pack_tens Tc.Out;
          T_par (4, "c");
          (*^ Level 2 tiling ^*)
        ];
    }
  in

  Printf.printf "Layout changed, only compute time: ";
  tc_debug_test ~args_to_wrap:None ~regen_code:true ~nthreads:4 _new_dim_param;
  Printf.printf "Layout basic,   only compute time: ";
  tc_debug_test ~args_to_wrap:None ~regen_code:true ~nthreads:4 _old_param;
  Printf.printf "Layout changed online, total time: ";
  tc_debug_test ~args_to_wrap:None ~regen_code:true ~nthreads:4 _pack_param;
  Printf.printf "Layout changed offline total time: ";
  tc_debug_test ~args_to_wrap:(Some _new_dim_param) ~regen_code:true ~nthreads:4
    _old_param

let _ = parallel_example ()

(* Note - Compilation command (to write as a single line):
   gcc -O2 -g -Wall -fopenmp -Wno-unused-function -march=native -fno-align-loops
    buffer.c timing.c main.c mem_utils.c gen/handle_counters.c tc_utils.c matrix_utils.c gen/gen_tc.c
    -L/usr/lib -lm -lpthread -lpapi -o mm_bench.x
*)

open Batteries
open Common
open Execution
open Bench_types 
open Search

open Conv_search_type
open Conv_spec


(* This file contains the exhaustive and selective exploration of the Ttile space
  assuming that a permutation is provided (by Ioopt, through the cache)
  and the microkernel classes are formed *)

module S(Ainfo : Arch_info_t) = struct
  open Search.Schemes(Ainfo)
  open Search.NonDivRandomSelection(Ainfo)
  open Search.IooptSelection(Ainfo)

  module A = Ainfo.A
  module B = Execution.Bench(Ainfo.A)
  module Pred_conv = Arch.Pred_conv(Ainfo.A)
  open Conv.Gen(A)

  open Streaming


  (* === Generator + main execution functions === *)

  (* For gen_schemes *)
  type select_mode = Rand of int
                   | Sorted of int
                   | Sorted_mixed of int
                   | All

  (* For draw_schemes *)
  type draw_mode = Non_div | Div | Ioopt_partial_mickerdiv

  let gen_rand_seed () =
    Random.self_init ();
    Random.bits ()

  (* For a given scheme, flatten the tile specification above the microkernel *)
  let flatten_scheme (uk, parameters, (l1,l2,l3,mem)) =
    (uk, parameters, l1@l2@l3@mem)

  (* Main function for scheme generation - generate the list of possibilities, then select in it *)
  let gen_schemes perms size num_candidates =
    gen_all perms size
    |> (match num_candidates with
          Sorted num_candidates -> sort_and_select_volume num_candidates
        | Sorted_mixed num_candidates -> select_mixed 50 num_candidates 40
        | Rand num_candidates -> random_select (gen_rand_seed ()) num_candidates
        | All -> Fun.id)
    |> Stream.of_list
    |> Stream.map flatten_scheme
    |> Stream.map to_scheme

  (* Main function for scheme generation - generate at the demand *)
  let draw_schemes draw_method size num_candidates =
    let fdraw = match draw_method with
      | Non_div -> random_select_non_div
      | Div -> random_select_div
      | Ioopt_partial_mickerdiv -> random_select_ioopt_partial_mickerdiv
    in
    repeat_random_draw fdraw num_candidates size
    |> Stream.of_list

  let exec_scheme counters_list (f, c, x, y, h, w) stride shape scheme =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:1
        (Some (true, true, Gcc,  O3))
        [Conv;] counters_list in
    let args = match stride with None -> Config.conv_args x w y h c f
                               |Some (sx, sy) ->
                                 Config.conv_args_stride x w y h c f sx sy
    in
     scheme,
     let res = (B.exec conf_gen args
                  (Conv_tile scheme) shape) in
     List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
       counters_list

  (* === Generator + main execution functions with prefix (added by Guillaume) === *)

  (* SchemeSet: use ConvTypes.type (in conv.ml / open in Search.Schemes(Ainfo) ) *)
  module SchemeSet = Set.Make(struct type t=(Conv.tile list) let compare = Stdlib.compare end)

  (* Recover the prefix schemes of a list of schemes *)
  let generate_prefixes lschemes =

    (* Auxilliary function: check if there is a lambda and get its values *)
    let rec extract_lambda_values (scheme:Conv.tile list) = match scheme with
      | [] -> []
      | (Conv.ConvTypes.Lambda_apply (_, liter_args) ) :: t -> (
          (* ASSUMPTION! Assume that we have a single lambda in the whole scheme *)
          assert((extract_lambda_values t) == []);
          let lval = List.map (fun (_, (Tensor_loops.Arg arg)) -> arg) liter_args in
          lval
        )
      | _::t -> (extract_lambda_values t)
    in

    (* Auxilliary function: replace lambda by their value *)
    let rec substitute_lambda_scheme arg (scheme:Conv.tile list) = match scheme with
      | [] -> []
      | (Conv.ConvTypes.ULambda d) :: t ->
        (Conv.ConvTypes.U (arg,d)) :: (substitute_lambda_scheme arg t)
      | (Conv.ConvTypes.TLambda d) :: t ->
        (Conv.ConvTypes.T (arg,d)) :: (substitute_lambda_scheme arg t)
      | h::t -> h::(substitute_lambda_scheme arg t)
    in

    (* Auxilliary function: get a decreasing list of prefix from a single scheme *)
    let extract_prefix_schemes (scheme:Conv.tile list) =

      (* Extract the values of lambda here, before doing the fold *)
      let larg_lambda = extract_lambda_values scheme in

      let (lprefix, _, _, _, _) = List.fold_left (fun
          (lpref_acc, l_prev_elems, b_start, b_pending_lambda, b_lambda_reached)
          scheme_elem ->
        (* lpref_acc : list of prefix schemes / l_prev_elems: previously encountered elements *)
        (* b_start : boolean marking if we went above the Hoist_var loop / reuse loop *)

        (* Are we at the last element of the microkernel? *)
        (* Implicit assumption that the microkernel is unroll loops + a tile loop on c + Hoist_vars above *)
        let n_b_start = if b_start then true else
          match scheme_elem with
          | Conv.ConvTypes.Hoist_vars _ -> true
          | _ -> false
        in

        (* Managing lambda expressions => Need to get the lambda apply beforehand *)
        (* ASSUMPTION! Assume that we have a single lambda in the whls scheme *)
        (* b_pending_lambda => Was a lambda encountered?
           b_lambda_reached => Was a lambda_apply encountered?
        *)
        let n_b_pending_lambda = match scheme_elem with
          | Conv.ConvTypes.ULambda _ -> true
          | Conv.ConvTypes.TLambda _ -> true
          | _ -> b_pending_lambda
        in
        let n_b_lambda_reached = match scheme_elem with
          | Conv.ConvTypes.Lambda_apply _ -> true
          | _ -> b_lambda_reached
        in


        (* Recursion ! *)
        let nprefix = l_prev_elems @ [scheme_elem] in

        (* Remplace the lambda in the expression, if we have a lamba without apply *)
        let lnprefix =
          if ((not n_b_pending_lambda) || n_b_lambda_reached) then [nprefix] else
          (
            assert(larg_lambda!=[]);
            List.map (fun arg -> substitute_lambda_scheme arg nprefix) larg_lambda
          )
        in

        let nlpref_acc = if n_b_start then lnprefix @ lpref_acc else lpref_acc in
        (nlpref_acc, nprefix, n_b_start, n_b_pending_lambda, n_b_lambda_reached)

      ) ([], [], false, false, false) scheme in
      lprefix
    in

    (* Build the set of all schemes and prefixes of schemes (for unicity of elements) *)
    let (sprefix : SchemeSet.t) = List.fold_left (fun sacc scheme -> 
      (* Getting all the prefix schemes of "scheme" *)
      let lprefix = extract_prefix_schemes scheme in

      (* Adding these schemes until one of them are already in nsacc*)
      (* /!\ Assume that lprefix is in decreasing order of sizes, in order to stop when it already matches *)
      let (nsacc,_) = List.fold_left (fun (sacc, b_alreadythere) pref_scheme ->
        (* Note: optimisation not possible anymore, if we have lambda
        if (b_alreadythere) then (sacc, true) else
        let b_alreadythere = SchemeSet.mem pref_scheme sacc in *)
        let nsacc = SchemeSet.add pref_scheme sacc in

        (nsacc, b_alreadythere)
      ) (sacc,false) lprefix in
      nsacc
    ) SchemeSet.empty lschemes in

    (* Convert sprefix back into a list *)
    let (nlschemes : Conv.tile list list) = SchemeSet.fold (fun (elem:Conv.tile list) lacc -> elem::lacc) sprefix [] in

    (* Return the result *)
    nlschemes

  (* Added by Guillaume : generate all the schemes and their prefix above the microkernel *)
  let gen_schemes_prefix perms size =
    (* Reconstruction de préfixe: appartenance à un Set / le faire outer vers inner (en s'arrétant quand ça y est) *)
    let l_schemes = List.map to_scheme (List.map flatten_scheme (gen_all perms size)) in
    Printf.printf "l_schemes.length = %d\n" (List.length l_schemes);

    (* Recover all the prefixes of the previous schemes *)
    let (l_pref : Conv.tile list list) = generate_prefixes l_schemes in
    Printf.printf "l_pref.length = %d\n" (List.length l_pref);
    
    Stream.of_list l_pref

  (* b_embedded : True = keep the full_sizes and iterate over a small portion *)
  (* False = find the sizes of the scheme, and fit the tensors to these sizes *)
  let exec_scheme_prefix b_embedded counters_list full_sizes stride shape scheme =
    let (f, c, x, y, h, w) = full_sizes in
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:30
        (Some (true, true, Gcc,  O2))
        [Conv;] counters_list in

    (* Note: "full_sizes" are the sizes of the full convolution, that might not match the
      sizes of the "scheme" *)
    let f_size_sch = dim_tile_size scheme (Conv.F : Conv.dim) in
    let c_size_sch = dim_tile_size scheme (Conv.C : Conv.dim) in
    let x_size_sch = dim_tile_size scheme (Conv.X : Conv.dim) in
    let y_size_sch = dim_tile_size scheme (Conv.Y : Conv.dim) in
    let h_size_sch = dim_tile_size scheme (Conv.H : Conv.dim) in
    let w_size_sch = dim_tile_size scheme (Conv.W : Conv.dim) in

    (* 2 variations: with/without embedding *)
    let args = if (b_embedded) then
      (* f, c, y, x, h, w *)
       let englobing_problem_sizes = (f, c, y, x, h, w) in
       (match stride with
        | None -> Config.conv_args
          ~englobing_output:englobing_problem_sizes
          ~englobing_input:englobing_problem_sizes
          ~englobing_params:englobing_problem_sizes
           x_size_sch w_size_sch y_size_sch h_size_sch c_size_sch f_size_sch
        | Some (sx, sy) -> Config.conv_args_stride
          ~englobing_output:englobing_problem_sizes
          ~englobing_input:englobing_problem_sizes
          ~englobing_params:englobing_problem_sizes
           x_size_sch w_size_sch y_size_sch h_size_sch c_size_sch f_size_sch sx sy
      ) else (match stride with
        | None -> Config.conv_args x_size_sch w_size_sch
            y_size_sch h_size_sch c_size_sch f_size_sch
        | Some (sx, sy) -> Config.conv_args_stride x_size_sch w_size_sch
            y_size_sch h_size_sch c_size_sch f_size_sch sx sy
      )
    in
    let scheme_sizes = (f_size_sch, c_size_sch, x_size_sch, y_size_sch, h_size_sch, w_size_sch) in

    let res = (B.exec conf_gen args (Conv_tile scheme) shape) in
    let lcounter_res = List.map (fun counter ->
        counter, Config.select_bench_counter Conv counter res
      ) counters_list
    in
    (scheme, scheme_sizes, lcounter_res)


  (* === Formating functions === *)

  let sort_output stream =
    Stream.into Sink.list stream
    |> Utils.sort_by (List.assoc Counter.CYCLES % snd)

  let format_first_line counter_list =
    let open Counter in
    List.map (function CYCLES -> "perf"
                     | c -> show c)
      counter_list
    |> String.concat ","

  let format_tile tile =
    Conv.tile_to_string tile
    |>  String.filter (function '\n' -> false | _ -> true)
    |> fun s -> "\"" ^ s ^ "\""

  let format_flow_line (f,c,y,x,h,w) counter_list res =
    let open Counter in
    List.map (function
        | CYCLES ->
          (List.assoc CYCLES res
           |> Pred_conv.peak_percent f c y x h w
           |> Printf.sprintf "%.2f")
        | c -> List.assoc c res
               |> string_of_int
      ) counter_list
    |> String.concat "," 

  (* Write results to file *)
  let res_write name sizes counter_list =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
      let f = File.open_out (loop (A.arch_name ^ "_" ^ name ^ ".csv")) in
      let first_line = format_first_line counter_list ^ ",scheme" in
      IO.write_line f first_line; f
    and push file (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.fprintf file "%s,%s\n" line (format_tile tile);
      IO.flush file;
      file
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop ()


  (* Write to stdout *)
  let print_flow sizes counter_list =
    let init () =
      let first_line = format_first_line counter_list in
      print_endline first_line
    and push () (tile, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.printf "%s\n%s\n" (Conv.tile_to_string tile) line
    and stop () = () in
    Sink.make ~init ~push ~stop ()

  (* Variant for prefix, with changing sizes *)
  let res_write_prefix name counter_list =
    let init () =
      let rec loop fn = if Sys.file_exists fn then loop (fn ^ "_x") else fn in
      let f = File.open_out (loop (A.arch_name ^ "_" ^ name ^ ".csv")) in
      let first_line = format_first_line counter_list ^ ",scheme" in
      IO.write_line f first_line; f
    and push file (tile, sizes, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.fprintf file "%s,%s\n" line (format_tile tile);
      IO.flush file;
      file
    and stop f = IO.close_out f in
    Sink.make ~init ~push ~stop ()


  (* Write to stdout *)
  let print_flow_prefix counter_list =
    let init () =
      let first_line = format_first_line counter_list in
      print_endline first_line
    and push () (tile, sizes, res) =
      let line = format_flow_line sizes counter_list res in
      Printf.printf "%s\n%s\n" (Conv.tile_to_string tile) line
    and stop () = () in
    Sink.make ~init ~push ~stop ()



  (* === Execution functions (exposed to the user) === *)

  let exec_with_perm counter_list perm num_candidate conv_spec =
    let open Sink in
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes (Some perm) adapted_size (Sorted num_candidate)
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
           )
      ) conv_spec

  (* Added by Gui: exec_all, but also all prefix scheme too *)
  let exec_all_prefix b_embedded counter_list conv_spec = 
    let open Sink in
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
        (* Note: not the same function than the other exec_XXX *)
        let stream_scheme_pref = gen_schemes_prefix None adapted_size in

        Stream.map (exec_scheme_prefix b_embedded counter_list adapted_size stride shape) stream_scheme_pref
          |> Stream.into
            (res_write_prefix name counter_list
             <& print_flow_prefix counter_list
            )
      ) conv_spec

  (* Exhaustive exploration: run all the possible schemes*)
  let exec_all counter_list conv_spec =
    let open Sink in
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size All
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  (* Random exploration: run a fixed number of randomly selected schemes *)
  let exec_rand counter_list conv_spec =
    let open Sink in
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size (Rand 200)
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           (res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_new_metric_def_perms counter_list num_candidates conv_spec =
    let shape = Config.Conv_shape in
    List.iter
      (fun (perm, {name; adapted_size; stride;_}) ->
         gen_schemes (Some perm) adapted_size (Sorted_mixed num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_new_metric counter_list num_candidates conv_spec =
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         gen_schemes None adapted_size (Sorted_mixed num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_and_sort_results counter_list num_candidates conv_spec =
    let shape = Config.Conv_shape in
    List.map
      (fun {name; adapted_size; stride;_} ->
         name,
         stride,
         adapted_size,
         gen_schemes None adapted_size (Sorted num_candidates)
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> sort_output
      ) conv_spec


  (* Divisibility vs non divisibility (above µkernel) comparison *)
  
  let exec_rand_non_div counter_list num_candidates conv_spec =
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         draw_schemes Non_div adapted_size num_candidates
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec

  let exec_rand_div counter_list num_candidates conv_spec =
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
         draw_schemes Div adapted_size num_candidates
         |> Stream.map (exec_scheme counter_list adapted_size stride shape)
         |> Stream.into
           Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
            )
      ) conv_spec


  (* Ioopt evaluation *)

  (* Using the sizes of Ioopt, but adapted to be multiple of the whole µkernel *)
  (*  => This uses partial tiles *)
  (* Note: given a valid (pair of) µkernel, there is only one option *)
  let exec_ioopt_partial_mickerdiv counter_list num_candidates conv_spec =
    let shape = Config.Conv_shape in
    List.iter
      (fun {name; adapted_size; stride;_} ->
        draw_schemes Ioopt_partial_mickerdiv adapted_size num_candidates
        |> Stream.map (exec_scheme counter_list adapted_size stride shape)
        |> Stream.into
          Sink.(res_write name adapted_size counter_list
            <& print_flow adapted_size counter_list
          )
      ) conv_spec





end


(* Launch!  "dune exec ./micro_search.exe" *)
(* Warning : change that depending on the architecture *)
(*module S_avx512 = S(Arch_info.Skylake_info)
let () = S_avx512.exec_new_metric Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2]
    50 Conv_sizes.all_spec
*)

(* Not the correct one for my laptop /!\ TODO: add that to arch_info *)
(* Check that the intervals are correct for my laptop... :/ *)
module S_avx2 = S(Arch_info.Dracula)

(*let () = S_avx2.exec_all_prefix false Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2]
  [Conv_sizes.mobilNet_9_spec]
*)
let () = S_avx2.exec_rand_non_div Counter.[CYCLES; CACHE_MISS_L1; CACHE_MISS_L2]
            1 [Conv_sizes.yolo9000_2_spec]

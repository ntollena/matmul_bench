open Batteries
open Common
open Conv_spec

open Search
open Conv_search_type

open Arch_info

type arch_info_t = (module Arch_info_t)

(* All currently supported architectures *)
let all_archs : arch_info_t list =
  [(module Xeon); (module Silver_info); (module Skylake_avx2_info);
     (module Skylake_info)]

(* Build a list of params * permutation for every pair
 * of a convolution and a architecture in conv_list and arch_list.
 * Writes the result to a file named cachefile *)
let build_cache conv_list arch_list cachefile =
  List.concat_map (fun (module A_info: Arch_info_t) ->
      let module Sa = S(A_info) in
      Sa.gen_all_perm_sizes conv_list
    ) arch_list
  |> Ioopt.write_to_file cachefile

(* Reads the permutation cache given as oldcache and
 * generates a permutation for every tuple of (convolution, architecture)
 * that does not already exist in it. Writes the result to a file named newcache *)
let complete_cache conv_list archlist oldcache newcache =
  let open Conv_search_type in
  let cachefile = oldcache in
  let params_perm = Ioopt.read_from_file cachefile in
  let build_all_params {adapted_size;_} (module A: Arch_info_t) =
    List.filter_map (fun uk -> Ioopt.build_ioopt_params (module A) adapted_size uk) A.ukernels in
  let params_to_compute =
    List.cartesian_product conv_list archlist
    |> List.concat_map (uncurry build_all_params)
    |> List.filter
      (fun p -> List.for_all (Bool.not % Ioopt.equal_ioopt_params p % fst) params_perm) in
  let new_perms =
    Parmap.parmap
      (fun params -> let open Utils.Opt_syntax in
        let+ perms = Ioopt.ioopt_permutation_from_params params in
        params, perms)
      (Parmap.L params_to_compute)
    |> List.filter_map Fun.id in
  let new_params_perms = params_perm @ new_perms in
  Ioopt.write_to_file newcache new_params_perms

(* Example building a cache from scratch for all convolutions and architecture *)
(*
 * let () = build_cache Conv_sizes.all_spec all_archs "perm_example.json"
 *)

(* Example completing a cache with a new architecture *)
(*
 * let () = complete_cache Conv_sizes.all_spec [(module Broadwell)]
 *    "perm_cache.json" "perm_cache_with_broadwell.json"
 *)

let () = complete_cache Conv_sizes.all_spec [(module Pinocchio_extended)]
    "perm_cache.json" "perm_cache_with_pinext.json"

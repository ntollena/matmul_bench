open Batteries
open Common
open Execution
open Bench_types

(* Module to run easily a standalone convolution *)
module Exec(A: Arch.Arch_t) = struct
  include Bench(A)
  module Pred = Arch.Pred_conv(A)
  open Conv_spec

  let dim_sizes tile =
    let open Conv in
    let open Gen(A) in
    let ds = dim_tile_size tile in
    ds F, ds C, ds Y, ds X, ds H, ds W

  let build_args (f,c,y,x,h,w) stride =
    match stride with
      None -> Config.conv_args x w y h c f
    | Some (sx, sy) ->
      Config.conv_args_stride x w y h c f sx sy

  let exec_manual_code ?(stride=None) size =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.001)
        ~num_rep:1
        (Some (false, true, Gcc,  O3))
        [Conv] Counter.[CYCLES] in
    let args = build_args size stride in
    ignore @@ exec conf_gen args (Conv_tile [])


  let exec_scheme_size_stride benches counters_list size stride scheme  =
    let conf_gen = Config.build_conv_config ~error:(Config.EPSILON 0.001) ~num_rep:300
        (Some (true, true, Gcc,  O3))
        benches counters_list in
    let args = build_args size stride in
    exec conf_gen args (Conv_tile scheme)

  let exec_scheme_size_stride_conv_only counters_list size stride scheme  =
    let res = exec_scheme_size_stride [Conv] counters_list size stride scheme Conv_shape in
    List.map (fun counter -> counter, Config.select_bench_counter Conv counter res)
      counters_list

  let exec_scheme counters_list {adapted_size; stride;_} scheme =
    exec_scheme_size_stride_conv_only counters_list adapted_size stride scheme

  let exec_print_counters_size_stride counters_list ((f,c,y,x,h,w) as size) stride  scheme =
    let open Counter in
    List.iter (fun (counter, n) -> match counter with
        | CYCLES ->
          let p = Pred.peak_percent f c y x h w n in
          Printf.printf "%d cycles, peak perf : %.2f%%\n" n p
        | _ -> Printf.printf "%s : %d\n" (show counter) n)
      (exec_scheme_size_stride_conv_only counters_list size stride scheme)

  let exec_print_counters counters_list {adapted_size ; stride;_}  scheme =
    exec_print_counters_size_stride counters_list adapted_size stride scheme

  let gen_file_size_stride postfix size stride scheme =
    let args = build_args size stride in
    let open Config in
    gen_file (Conv_layout (XYF, XYC, HWCF))
      args (Conv_tile scheme) postfix

  let gen_file postfix {adapted_size; stride;_} scheme =
    gen_file_size_stride postfix adapted_size stride scheme
end

(* Example with MobileNet2 *)
let mbnet2 () =
  let open Conv_sizes in
  let open Exec(Arch.Dracula) in
  (*let open Config in *)
  let open Conv in
  let mb_scheme = ConvTypes.[V F; U (4, F); U (7, Y); U (3, H);
                                       T (32, C); Hoist_vars [C]; T (8, X);
                                       Pack_tens Params;
                                       T (3, W); T (2, C); T (1, F); T (8, Y);
                                       T (7, X); T (2, F); T (1, F)] in
  exec_print_counters Counter.[CYCLES] mobilNet_2_spec mb_scheme


(* === Example with our best microkernel === *)
let vec_size = 8

let footprint f_size c_size h_size w_size y_size =
  let input= c_size * (y_size + h_size - 1) * w_size * 4
  and params = c_size * vec_size * f_size * h_size * w_size * 4
  and output = f_size * vec_size * y_size * 4 in
  input, params, output


let build_microk (module A: Arch.Arch_t) f_size c_size ?(h_size=1) ?(w_size=1) y_size =
  let open Exec(A) in
  let pp = Pred.peak_perf (f_size * vec_size) c_size y_size 1 h_size w_size in
  Printf.printf "peak perf : %d cyc\n" pp;
  let i, p, o = footprint f_size c_size h_size w_size y_size in
  Printf.printf "input : %dB, output : %dB, params : %dB, total : %dB\n"
    i o p (i + o + p);
  Conv.(ConvTypes.[V F; U (f_size, F); U (y_size, Y);
             U (h_size, H); U (w_size, W); T (c_size, C);
(*              Pack_tens Params; *)
             Hoist_vars [C]])

let microk () =
  let open Exec(Arch.Pinocchio) in
  let microkern = build_microk (module Arch.Pinocchio) 1 128 ~h_size:3 ~w_size:3 1 in
  exec_print_counters_size_stride Counter.[CYCLES]
    (dim_sizes microkern) None microkern

let call_ () =
  let open Exec(Arch.Pinocchio) in
  let call_spec = Conv.ConvTypes.{name = "microk_2_12_mask_store"; var_dim = Y;
                                  dynamic_sizes = [];
                                  max_range = 12; fixed_size = [X, 1; H, 1;
                                                                W, 1; F, 32; C,
                                                               256]} in
  let microkern = Conv.ConvTypes.[External_call call_spec; Tile_exact (28, Y);
                                  T (4, F); T (12, X)] in
  exec_print_counters_size_stride Counter.[CYCLES]
(*(32, 256, 28, 1, 1, 1)*)
    (dim_sizes microkern) None microkern

let external_tile = 
  let call_spec = Conv.ConvTypes.{name = "microk_2_12_mask_store"; var_dim = Y;
                                  dynamic_sizes = [];
                                  max_range = 12; fixed_size = [X, 1; H, 1;
                                                                W, 1; F, 32; C,
                                                                             256]} in
  Conv.ConvTypes.[External_call call_spec; Tile_gexact (28, Y);

                   T (4, F); Tile_gexact (45, Y); T (12, X);
                 Tile_exact (70, Y); ]

let extern () =
  let open Exec(Arch.Pinocchio) in
  exec_print_counters_size_stride Counter.[CYCLES]
(*(32, 256, 28, 1, 1, 1)*)
    (dim_sizes external_tile) None external_tile

let exec_scheme_with_counters scheme = 
  let open Exec(Arch.Dracula) in
  exec_scheme_size_stride [Conv] Counter.[CYCLES; TOT_INS; LST_INS; RES_STL] (dim_sizes scheme) None
    scheme Conv_shape
  |>  Config.show_results
  |> print_endline

let flu () = extern ()

let partial_tile = Conv.ConvTypes.[V F; U (2, F);  T (256, C); Tile_partial (8, Y);
                                   Tile_gexact (16, Y); Tile_exact (28, Y)]
let test_partial () =
  let open Exec(Arch.Pinocchio) in
  let scheme = partial_tile in
  exec_scheme_size_stride [Conv] Counter.[CYCLES] (dim_sizes scheme) None scheme Conv_shape
  |>  Config.show_results
  |>  print_endline

let () = let open Exec(Arch.Pinocchio) in
  exec_manual_code ~stride:(Some (2,2)) (64, 64, 56, 56, 3, 3)

let nope () = extern ()


let blu () =
  print_endline "\ny = 5";
  exec_scheme_with_counters @@ build_microk (module Arch.Pinocchio) 2 256 ~h_size:1 5;
  print_endline "\ny = 6";
  exec_scheme_with_counters @@ build_microk (module Arch.Pinocchio) 2 256 ~h_size:1 6



(* === Local tests - searching optimal strat on "giooss-inria" laptop (i5-8365U) === *)
let mobileNet_9_manual_explo () =
  let open Exec(Arch.Dracula) in     (* AVX2 + 2 fma par port *)
  (*let open Config in *)
  let open Conv in
  
  (*let open Conv_sizes in *)
  (* MobileNet_9 full problem size - f, c, y, x, h, w = 1024,1024,7,7,3,3 *)
  let open Conv_spec in

  let mobilNet_9_size = 1024, 1024, 7,7, 3,3 in
  let mobilNet_9_stride = None in
  let mobilNet_9_spec = to_spec "MobilNet_09" mobilNet_9_size mobilNet_9_stride in


  (* Choices: lambda on C, granularite 32 | reuse along X on params at L2 *)
  let mb9_scheme = ConvTypes.[
      V F; (* U (1, F); *) U (3, H); U(7, Y);   (* µkernel *)
      T (256, C);
      (*TLambda C;*) Hoist_vars [C];                (* Streaming on µkernel to fill the L1 *)
      T(7, X);
      T(3, W);
      (* Pack_tens Params; *)
      T(4, C);
      T(128, F);
      (*Lambda_apply (C, [(Iter 4, Arg 240); (Iter 1, Arg 64)]); *)
    ] in
  (* Note: lambda does not work below a packing *)

  exec_print_counters Counter.[CYCLES] mobilNet_9_spec mb9_scheme

(*
let () = mobileNet_9_manual_explo ()
let truc () = mobileNet_9_manual_explo ()
let () = ignore @@ very_small ()
*)


(* Microkernel variation basis *)
let mobileNet_9_microkernel () =
  let open Exec(Arch.Dracula) in     (* AVX2 + 2 fma par port *)
  let open Conv in
  let module Pred_conv = Arch.Pred_conv(Arch.Dracula) in
  
  (* f, c, y, x, h, w = *)

  (* Subcomputation we want to perform *)
  (* O[x,y,f] = K[c,h,w,f] * I[x+h,y+w,c] *)
  let size_w = 1 in
  let size_y = 1 in
  let size_h = 1 in

  let size_f_unroll = 2 in
  let size_f = 8 * size_f_unroll in
  let size_x = 6 in
  let size_c = 256 in
  let subcomp_scheme = ConvTypes.[ V F; U(size_f_unroll,F); U(size_x,X); T(size_c,C); Hoist_vars [C] ] in


  (* Build the corresponding configuration data structure (functions from Config.ml) *)
  let open Config in
  let args = conv_args size_x size_w size_y size_h size_c size_f in
  let conf_gen = build_conv_config_with_layout ~error:(EPSILON 0.01) ~num_rep:100
      (Some (true, true, Gcc,  O3))
      [ (Conv, default_layout Conv); ] [CYCLES]  (* default_layout = Conv_layout (XYF, XYC, WHCF) *)
  in

  (* Run it *)
  let gen_score = exec conf_gen args (Conv_tile subcomp_scheme) Conv_shape
                 |> select_bench_counter Conv CYCLES
                 |> Pred_conv.peak_percent size_x size_y size_w size_h size_c size_f
  in
  Printf.printf "\nPerf = %.5f %% of machine peak\n" gen_score;
  ()

(* Launch *)
let () = mobileNet_9_microkernel ()



(* === Local tests - debugging of the stride === *)
let mobileNet_9_subcomputation_with_stride () =
  let open Exec(Arch.Dracula) in     (* AVX2 + 2 fma par port *)
  let open Conv in
  let module Pred_conv = Arch.Pred_conv(Arch.Dracula) in

  (* f, c, y, x, h, w = *)
  let englobing_problem_sizes = 1024, 1024, 7,7, 3,3 in

  (* Subcomputation we want to perform *)
  (* O[x,y,f] = K[c,h,w,f] * I[x+h,y+w,c] *)
  let subcomp_scheme = ConvTypes.[ V F; U(2,F); U(7,X); T(16,C) ] in
  let size_x = 7 in
  let size_w = 1 in
  let size_y = 1 in
  let size_h = 1 in
  let size_c = 16 in
  let size_f = 16 in

  (* Build the corresponding configuration data structure (functions from Config.ml) *)
  let open Config in
  let args = conv_args
    ~englobing_output:englobing_problem_sizes
    ~englobing_input: englobing_problem_sizes
    ~englobing_params:englobing_problem_sizes
    size_x size_w size_y size_h size_c size_f
  in
  let conf_gen = build_conv_config_with_layout ~error:(EPSILON 0.01) ~num_rep:20
      (Some (true, true, Gcc,  O3))
      [ (Conv, default_layout Conv); ] [CYCLES]  (* default_layout = Conv_layout (XYF, XYC, WHCF) *)
  in

  (* Run it *)
  let gen_score = exec conf_gen args (Conv_tile subcomp_scheme) Conv_shape
                 |> select_bench_counter Conv CYCLES
                 |> Pred_conv.peak_percent size_x size_y size_w size_h size_c size_f
  in
  Printf.printf "\nPerf = %.5f %% of machine peak\n" gen_score;
  ()

(* Launch it ! *)
(* let () = mobileNet_9_subcomputation_with_stride () *)

(* Note - Compilation command (to write as a single line):
  gcc -O2 -g -Wall -fopenmp -Wno-unused-function -march=native -fno-align-loops
    buffer.c timing.c main.c mem_utils.c gen/handle_counters.c tc_utils.c conv_utils.c gen/gen_conv.c
    -L/usr/lib -lm -lpthread -lpapi -o mm_bench.x

    gcc -O2 -g -Wall -fopenmp -Wno-unused-function -march=native -fno-align-loops buffer.c timing.c main.c mem_utils.c gen/handle_counters.c tc_utils.c conv_utils.c gen/gen_conv.c -L/usr/lib -lm -lpthread -lpapi -o mm_bench.x

    ./mm_bench.x 7 1 1 1 2 2  7 1 7 1 4 4
    Order of arguments: [conv sizes] + [embedded conv sizes]
      x w y h c f
*)
let () = exec_scheme_with_counters @@ build_microk(module Arch.Pinocchio) 2 256 ~h_size:1 6

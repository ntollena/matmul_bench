open Batteries
open Execution
open Custom_utils
open Tc_sugar

module GenSchemes (A : Search.Conv_search_type.Arch_info_t) : sig
  val random_non_packed : int -> int -> tc_sugar_args -> tc_sugar_args list

  val insert_packing :
    tc_sugar_args -> (Tc.tensor * string list option) list -> tc_sugar_args
end = struct
  (** returns cache size and occupancy of the greatest non empty cache *)
  let count_cache_occupation fp =
    let cache_sizes = A.[ l1_size; l2_size; l3_size ] in
    let opt_res =
      List.find_map_opt
        (fun size ->
          if fp <= size then
            let occupation = float_of_int fp /. float_of_int size in
            Some (size, occupation)
          else None)
        cache_sizes
    in
    match opt_res with
    | None -> (Int.max_num, 0.0)
    | Some (cache_size, occupation) -> (cache_size, occupation)

  let dim_begin_end_sizes ({ scheme; _ } as sugar_params) =
    let parsed =
      List.filter_map
        (fun tile ->
          match tile with
          | T (i, d) | U (i, d) | T_par (i, d) -> Some (d, i)
          | V d -> Some (d, A.A.vec_size)
          | _ -> None)
        scheme
    in
    let dim_end_sizes = dim_sizes_alphabetical sugar_params in
    List.map
      (fun (udim, end_size) ->
        let tile_sizes_udim =
          List.filter_map
            (fun (dim, tile_size) ->
              if dim = udim then Some tile_size else None)
            parsed
        in
        let begin_size =
          List.fold (fun acc size -> acc * size) 1 tile_sizes_udim
        in
        (udim, begin_size, end_size))
      dim_end_sizes

  (** `footprint tensor scheme dims` gives the number of floats used by the array of `tensor` in a given `scheme`, `dims just here to give infos`*)
  let footprint tensor sugar_params =
    let dim_b_e_sizes = dim_begin_end_sizes sugar_params in
    let sizes =
      List.filter
        (fun (dim, _, _) -> is_dim_in_tens tensor dim sugar_params)
        dim_b_e_sizes
    in
    List.fold (fun acc (_, size, _) -> acc * size) 1 sizes

  (** Indices are the index where we gonna put the packing and T(x,X) will be shifted to the right *)
  let indices_where_we_could_pack scheme tensor sugar_params =
    let tiles_indices = List.mapi (fun i tile -> (i, tile)) scheme in
    let index_end_mk =
      List.find_map_opt
        (function i, Hoist_vars _ -> Some i | _ -> None)
        tiles_indices
    in
    let index_end_mk =
      match index_end_mk with
      | Some i -> i
      | None ->
          failwith
            "Schemes to insert packing in should contains a Hoist_vars to \
             separate micro kernel from tiles where we could insert packing."
    in
    (* we don't pack into microkernel so remove those indices *)
    tiles_indices
    |> List.filter (fun (i, _) -> i > index_end_mk)
    |> List.filter_map (fun (i, tile) ->
           match tile with
           | (T (_, dim) | T_par (_, dim))
             when is_dim_in_tens tensor dim sugar_params ->
               Some i
           | _ -> None)

  (** Pack only where tensor footprint fits in min 20% of L1 and 100% of L2 at max or if not of them exists just go for all possible indices where packing is possible*)
  let packing_heuristic ({ scheme; _ } as sugar_params) tensor =
    let indices = indices_where_we_could_pack scheme tensor sugar_params in
    let footprint_info =
      indices
      |> List.map (fun i ->
             let sub_scheme = List.take i scheme in
             let fp =
               footprint tensor { sugar_params with scheme = sub_scheme }
             in
             (i, count_cache_occupation fp))
      |> List.filter (fun (_, (cache, occupied)) ->
             not (cache = Search.Arch_info.Dracula.l1_size && occupied < 0.2))
      |> List.filter (fun (_, (cache, _)) ->
             not (cache >= Search.Arch_info.Dracula.l3_size))
      |> List.sort (fun (_, (cache1, occupied1)) (_, (cache2, occupied2)) ->
             match compare cache1 cache2 with
             | 0 -> compare occupied2 occupied1
             | c -> c)
    in
    (* Printf.printf "fp info = ";
       print_endline @@ [%show: (int * (int * float)) list] footprint_info; *)
    let pack_indices =
      if List.is_empty footprint_info then indices
      else List.map (fun (i, _) -> i) footprint_info
    in
    (tensor, pack_indices)

  (** Filter all possible packing insertion spots to only the L1 or L2 caches with better occupation *)
  let insert_packing ({ scheme; _ } as sugar_params) pack_info =
    let possible_indices =
      pack_info
      |> List.map (fun (tensor, opt_transposition) ->
             let _, indices = packing_heuristic sugar_params tensor in
             (tensor, indices, opt_transposition))
    in

    (* randomly choose L1 or L2 spots to insert *)
    let to_insert =
      possible_indices
      |> List.map (fun (tensor, indices, opt_transposition) ->
             (* Random.int can raise an exception when indices list is empty, it occurs only with extrem sizes *)
             let n = List.length indices in
             let choosen_index = List.at indices (Random.int n) in
             let packing =
               match opt_transposition with
               | Some permut_dims -> Pack_trans (tensor, permut_dims)
               | None -> Pack_tens tensor
             in
             (choosen_index, packing))
    in

    (* Sort is mandatory else we can't use the hack to insert easily multiple elements in the scheme *)
    let to_insert =
      List.sort (fun (i, _) (j, _) -> Int.compare i j) to_insert
    in
    (* Insert all packing items in the scheme *)
    let new_packed_scheme =
      List.fold_lefti
        (fun new_scheme i (index, packing) ->
          (* little hack: inserting items create an offset so + i  *)
          insert_at packing (index + i) new_scheme)
        scheme to_insert
    in
    { sugar_params with scheme = new_packed_scheme }

  (*================== HERE's code not related with packing ====================== *)

  (** Generate one random scheme using the sample of tile sizes first then sample the permutation of all tiles *)
  let gen_1_random_scheme tile_sizes_per_dim =
    (* 1- sample the numbers to put inside T(num, dim) *)
    let selected_sizes =
      tile_sizes_per_dim
      |> List.filter (fun (_, tile_sizes) -> tile_sizes <> [ [] ])
      |> List.map (fun (dim, tile_sizes) ->
             let n =
               float_of_int (List.sum (List.map List.length tile_sizes))
             in
             (* We can't pick uniformly in tile_sizes because there are more permutations involving candidates with more tiles
                so we choose ponderation to make the one with more tiles have a greater chance of getting selected *)
             let weights =
               List.map
                 (fun s ->
                   let i = float_of_int (List.length s) in
                   i /. n)
                 tile_sizes
             in
             let sizes = choice_pond tile_sizes weights in
             (dim, sizes))
    in

    (* 2- map those numbers and dim to real tile type T(num, dim) *)
    let selected_tiles =
      selected_sizes
      |> List.map (fun (dim, tile_size) ->
             List.map (fun num -> T (num, dim)) tile_size)
      |> List.flatten
    in

    (* 3- select one permutation by shuffling *)
    shuffle selected_tiles

  (**`random_non_packed 500 4 sugar_params` generates 500 non packed schemes with 4 levels of tiling at max (out of microkernel= scheme in sugar_params) on each dimension*)
  let random_non_packed n_sample max_terms ({ scheme; _ } as sugar_params) =
    let _ =
      match scheme with
      | [] ->
          failwith
            "Microkernel expected inside sugar_params, scheme shouldn't be \
             empty"
      | _ -> ()
    in
    (* Compute min sizes for each dimensions *)
    let dim_b_e_sizes = dim_begin_end_sizes sugar_params in
    (* precompute all possible tile_sizes for all dim knowing their min and max sizes *)
    let all_tile_sizes =
      List.map
        (fun (dim, start_size, final_size) ->
          (dim, products_giving (final_size / start_size) max_terms))
        dim_b_e_sizes
    in

    List.init n_sample (fun _ ->
        let random_tiles = gen_1_random_scheme all_tile_sizes in
        let new_scheme = scheme @ random_tiles in
        { sugar_params with scheme = new_scheme })
end

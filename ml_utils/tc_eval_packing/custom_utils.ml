open Batteries
open Execution

let get_divisors_of n =
  if n <= 1 then []
  else
    let max_candidat = int_of_float (sqrt (float_of_int n)) in
    let tmp =
      List.filter (fun i -> n mod i = 0) (List.of_enum (2 -- max_candidat))
      |> List.map (fun i -> if n / i <> i then [ i; n / i ] else [ i ])
      |> List.flatten
    in
    n :: tmp

let copy_except_element i new_value old_list =
  List.mapi
    (fun index old_value -> if i = index then new_value else old_value)
    old_list

(** `insert_at x n l` gives a new list with `t` at index `n` and evrything next is shifted right*)
let rec insert_at x n = function
  | [] -> [ x ]
  | h :: t as l -> if n = 0 then x :: l else h :: insert_at x (n - 1) t

let rec products_giving n n_max_terms =
  match (n, n_max_terms) with
  | x, y when x <= 1 || y <= 0 -> [ [] ]
  | _, 1 -> [ [ n ] ]
  | _, _ ->
      get_divisors_of n
      |> List.map (fun d ->
             products_giving (n / d) (n_max_terms - 1)
             |> List.map (fun sub_list ->
                    List.fast_sort Int.compare (d :: sub_list)))
      |> List.flatten |> List.unique

let rec cumulative_sumf acc l =
  match l with
  | [] -> []
  | h :: tl ->
      let new_acc = acc +. h in
      new_acc :: cumulative_sumf new_acc tl

(**`shuffle l` returns a new list with the same elements than `l` but randomly permutated *)
let shuffle l =
  let rand_l = List.map (fun it -> (Random.bits (), it)) l in
  let sorted = List.sort compare rand_l in
  List.map snd sorted

(** `choice_pond list weights` choose randomly an element from list given a ponderation *)
let choice_pond list weights =
  let sum = List.kahan_sum weights in
  if List.length list <> List.length weights then
    raise
      (Invalid_argument
         "choice_pond: list and weights should have the same length")
  else if sum <> 1.0 then
    raise (Invalid_argument "choice:pond weights sum should be 1.0")
  else
    let cumulative = cumulative_sumf 0.0 weights in
    let pairs = List.combine list cumulative in
    let rand = Random.float 1.0 in
    List.find_map
      (fun (element, threshold) ->
        if rand < threshold then Some element else None)
      pairs

(**  `spread_factors numbers factors` return a list of list of int containing all distinct possible ways to multiply n factors on k numbers *)
let rec spread_factors numbers factors =
  match factors with
  | [] -> [ numbers ]
  | h :: t ->
      numbers
      |> List.mapi (fun i size ->
             let new_value = h * size in
             copy_except_element i new_value numbers)
      |> List.map (fun new_sizes -> spread_factors new_sizes t)
      |> List.flatten |> List.unique

(** greatest common divisor*)
let rec gcd a b = if b = 0 then a else gcd b (a mod b)

(** gives list of new sugar_args with dim_sizes increased by factor factors*)
let sugar_args_with_increased_sizes sugar_args factors =
  let dim_sizes = Tc_sugar.dim_sizes_alphabetical sugar_args in
  let dims = List.map fst dim_sizes in
  let begin_sizes = List.map snd dim_sizes in
  let possible_sizes = spread_factors begin_sizes factors in
  let possible_dim_sizes = List.map (List.combine dims) possible_sizes in
  List.map
    (fun new_dim_sizes ->
      Tc_sugar.update_dim_sizes_in_args new_dim_sizes sugar_args)
    possible_dim_sizes

(** Utils function to manage the progress bar*)
let update_progress_bar i n oc =
  let bar = List.init 100 (fun x -> if x <= 100 * i / n then "#" else ".") in
  let bar = String.concat "" bar in
  Printf.fprintf oc "[%s] (%d / %d)\n" bar i n;
  flush oc

(** `do_with_progress_bar f f_args` wrappers the execution of function `f` with parameters `f_args*)
let do_with_progress_bar f f_args =
  let oc = open_out "progress_pack_xp.txt" in
  let n = List.length f_args in

  List.iteri
    (fun i arg ->
      update_progress_bar i n oc;
      f arg)
    f_args;
  update_progress_bar n n oc;
  close_out oc

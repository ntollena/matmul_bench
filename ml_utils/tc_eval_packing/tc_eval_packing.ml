open Batteries
open Common
open Execution
open Bench_types
open Gen_schemes

module Exec (A : Arch.Arch_t) = struct
  include Bench (A)
  module Pred = Arch.Pred_tc (A)

  (* Execution functions *)
  let exec_scheme_size_stride ?(wrap_code = ("", "")) ?(exec_args_bench = None)
      ?(regen_code = true) ?(nthreads = 1) benches_w_lay counters_list
      (lsizes_left, lsizes_red, lsizes_right) scheme shape =
    let conf_gen =
      Config.build_tc_config_with_layout ~error:Config.NO_CHECK ~num_rep:15
        (Some (regen_code, true, Gcc, O2))
        (* FIRST bool to false means that code in gen_tc.c is not re gen on evry call to this function. *)
        benches_w_lay counters_list
    in
    let args = Config.tc_args lsizes_left lsizes_red lsizes_right in
    let exec_args_conf =
      match exec_args_bench with
      | None -> None
      | Some ((ll, lr, lrr), bench) ->
          let old_conf =
            Config.build_tc_config_with_layout ~error:(Config.EPSILON 0.001)
              ~num_rep:5
              (Some (regen_code, true, Gcc, O2))
              (* FIRST bool to false means that code in gen_tc.c is not re gen on evry call to this function. *)
              bench counters_list
          in
          Some (Config.tc_args ll lr lrr, old_conf)
    in
    exec ~wrap_code ~exec_args_conf ~nthreads conf_gen args (TC_tile scheme)
      (TC_shape shape)

  let exec_scheme_size_tc_only ?(wrap_code = ("", "")) ?(exec_args_conf = None)
      ?(regen_code = true) ?(nthreads = 1) counters_list llsizes
      (l_layout_A, l_layout_B, l_layout_C) scheme shape =
    let layout = Config.TC_layout (l_layout_A, l_layout_B, l_layout_C) in
    let benches_w_lay = [ (Config.TcGen, layout) ] in

    let exec_args_bench =
      match exec_args_conf with
      | None -> None
      | Some (arg, (lA, lB, lC)) ->
          let old_layout = Config.TC_layout (lA, lB, lC) in
          Some (arg, [ (Config.TcGen, old_layout) ])
    in
    let res =
      exec_scheme_size_stride ~wrap_code ~exec_args_bench ~regen_code ~nthreads
        benches_w_lay counters_list llsizes scheme shape
    in

    List.map
      (fun counter -> (counter, Config.select_bench_counter TcGen counter res))
      counters_list

  let write_results_in_file (lsizes_left, lsizes_right, lsizes_red) scheme_str
      results sep oc =
    (* Iterate through columns of the same line *)
    List.iter
      (fun (counter, n) ->
        match counter with
        | Counter.CYCLES ->
            let p = Pred.peak_percent lsizes_left lsizes_right lsizes_red n in
            Printf.fprintf oc "%d%s%.2f%%%s" n sep p sep
        | _ -> Printf.fprintf oc "%d%s" n sep)
      results;
    Printf.fprintf oc "%s\n" scheme_str;
    flush oc

  (** Gen, compile an run schemes. Keep track of counters and write results in filename. *)
  let run_schemes_to_csv ?(wrap_params = None) ?(nthreads = 1) sugar_params
      counters_list filename =
    (* Print first line with columns names in a file *)
    let oc = open_out filename in
    let sep = "\t" in

    List.iter
      (fun counter ->
        match counter with
        | Counter.CYCLES -> Printf.fprintf oc "CYCLES%sPEAK_PERF%s" sep sep
        | _ -> Printf.fprintf oc "%s%s" (Counter.show counter) sep)
      counters_list;
    Printf.fprintf oc "SCHEMES\n";
    let n = List.length sugar_params in
    let wrap_params =
      match wrap_params with
      | None -> List.map (fun _ -> None) sugar_params
      | Some x -> List.map (fun t -> Some t) x
    in
    let to_iter = List.combine sugar_params wrap_params in
    let open Inject_code.WrapperChangingLayout (A) in
    (* Compile and execute all the given schemes -> results = list of (counter, value) tuples *)
    List.iteri
      (fun i (sugar_param, args_to_wrap) ->
        let ( (scheme, lldimnames, llsizes_tc, ll_layout),
              wrap_code,
              exec_args_conf ) =
          match args_to_wrap with
          | None ->
              let usual_params_gen = Tc_sugar.build_basic_tc_args sugar_param in
              (usual_params_gen, ("", ""), None)
          | Some new_param ->
              let new_gen = Tc_sugar.build_basic_tc_args new_param in
              let wrapper = gen_packing_wrapper_code new_param sugar_param in
              let _, _, old_args, old_layout =
                Tc_sugar.build_basic_tc_args sugar_param
              in
              (new_gen, wrapper, Some (old_args, old_layout))
        in
        let scheme_str =
          [%show: Tc_sugar.sugar_tile_type list] sugar_param.scheme
        in
        let scheme_str =
          Str.global_replace (Str.regexp "[\t\n]") "" scheme_str
        in
        print_endline @@ [%show: Tc_sugar.tc_sugar_args] sugar_param;
        Printf.printf "(%d / %d)\n" i n;
        let results =
          exec_scheme_size_tc_only ~wrap_code ~exec_args_conf ~nthreads
            counters_list llsizes_tc ll_layout scheme lldimnames
        in
        write_results_in_file llsizes_tc scheme_str results sep oc)
      to_iter;
    close_out oc
end

(* ====================================== *)

let main () =
  let counters = Counter.[ CYCLES; CACHE_MISS_L1; CACHE_MISS_L2; BR_MISPRED ] in
  let n_sample = 50 in
  let nthreads = 4 in
  let max_tiles_dim = 4 in
  let module TestGen = GenSchemes (Search.Arch_info.Dracula) in
  let module TestExec = Exec (Arch.Dracula) in
  let open Execution.Tc_sugar in
  let template_arg =
    {
      dim_sizes_left = [ ("a", 4); ("c", 24) ];
      dim_sizes_red = [ ("e", 2); ("d", 2) ];
      dim_sizes_right = [ ("b", 4) ];
      layout_out = [ "a"; "b"; "c" ];
      layout_left = [ "a"; "d"; "e"; "c" ];
      layout_right = [ "e"; "b"; "d" ];
      scheme = [];
    }
  in
  let various_sizes = Xp5_cost.generated_sizes in
  let n = List.length various_sizes in
  Printf.printf "total possible sizes: %d \n" n;
  flush stdout;

  let parametrized_mk { dim_sizes_red; _ } =
    let size_e =
      List.find_map (function "e", x -> Some x | _ -> None) dim_sizes_red
    in
    let size_d =
      List.find_map (function "d", x -> Some x | _ -> None) dim_sizes_red
    in
    let gcde = Custom_utils.gcd size_e 8 in
    let gcdd = Custom_utils.gcd size_d 72 in
    [
      V "c";
      U (3, "c");
      U (4, "b");
      T (gcdd, "d");
      T (gcde, "e");
      Hoist_vars [ "d"; "e" ];
    ]
  in

  let args_basic_np =
    List.map
      (fun dim_sizes ->
        let basic_args = update_dim_sizes_in_args dim_sizes template_arg in
        let basic_args =
          { basic_args with scheme = parametrized_mk basic_args }
        in
        let basic_args =
          TestGen.random_non_packed n_sample max_tiles_dim basic_args
        in
        List.map
          (fun ({ dim_sizes_left; scheme; _ } as arg) ->
            {
              arg with
              dim_sizes_left =
                List.map
                  (function d, s when d = "c" -> (d, s * 4) | d, s -> (d, s))
                  dim_sizes_left;
              scheme = scheme @ [ T_par (4, "c") ];
            })
          basic_args)
      various_sizes
  in
  (* insert Pack_tens Out before T_par inside previously generated configs*)
  let args_basic_p =
    List.map
      (fun list_arg ->
        List.map
          (fun ({ scheme; _ } as p) ->
            let scheme =
              List.flatten
                (List.map
                   (function
                     | T_par (x, d) -> [ Pack_tens Tc.Out; T_par (x, d) ]
                     | tmp -> [ tmp ])
                   scheme)
            in
            { p with scheme })
          list_arg)
      args_basic_np
  in

  (* replace dim by dim2 in V, T and U, also dim is replace by dim1 in T_par*)
  let replace_dim scheme dim (dim1, dim2) =
    List.map
      (function
        | V d when d = dim -> V dim2
        | T (x, d) when d = dim -> T (x, dim2)
        | U (x, d) when d = dim -> U (x, dim2)
        | T_par (x, d) when d = dim -> T_par (x, dim1)
        | tmp -> tmp)
      scheme
  in

  let split_dim sugar_param dim (dim1, dim2) value =
    let size = size_of_dim dim sugar_param in
    let _ =
      if size mod value <> 0 then
        let error_msg =
          Printf.sprintf
            "Split dim %s should have size %d mltiple of value %d \n" dim size
            value
        in
        failwith error_msg
      else ()
    in
    let split_dim_in_layout dim layout =
      List.flatten
        (List.map (fun d -> if d = dim then [ dim1; dim2 ] else [ d ]) layout)
    in

    let split_layout_out dim =
      let dim2_replaced =
        List.map (fun d -> if d = dim then dim2 else d) sugar_param.layout_out
      in
      dim1 :: dim2_replaced
    in

    let split_dim_in_dim_sizes dim dim_sizes value =
      List.flatten
        (List.map
           (fun (d, s) ->
             if d = dim then [ (dim ^ "1", value); (dim ^ "2", s / value) ]
             else [ (d, s) ])
           dim_sizes)
    in
    (* update layout and sizes *)
    let is_left, is_red, is_right = is_dim_left_red_right dim sugar_param in
    let is_tc_out, is_tc_left, is_tc_right =
      (is_left || is_right, is_left || is_red, is_red || is_right)
    in
    {
      sugar_param with
      dim_sizes_left =
        (if is_left then
         split_dim_in_dim_sizes dim sugar_param.dim_sizes_left value
        else sugar_param.dim_sizes_left);
      dim_sizes_red =
        (if is_red then
         split_dim_in_dim_sizes dim sugar_param.dim_sizes_red value
        else sugar_param.dim_sizes_red);
      dim_sizes_right =
        (if is_right then
         split_dim_in_dim_sizes dim sugar_param.dim_sizes_right value
        else sugar_param.dim_sizes_right);
      layout_out =
        (if is_tc_out then split_layout_out dim else sugar_param.layout_out);
      layout_left =
        (if is_tc_left then split_dim_in_layout dim sugar_param.layout_left
        else sugar_param.layout_left);
      layout_right =
        (if is_tc_right then split_dim_in_layout dim sugar_param.layout_right
        else sugar_param.layout_right);
    }
  in

  let args_splitted =
    List.map
      (fun list_arg ->
        List.map
          (fun ({ scheme; _ } as p) ->
            let scheme = replace_dim scheme "c" ("c1", "c2") in
            let splitted_p = split_dim p "c" ("c1", "c2") 4 in
            { splitted_p with scheme })
          list_arg)
      args_basic_p
  in

  let args_splitted_p =
    List.map
      (fun (a, b) -> (a, Some b))
      (List.combine args_basic_np args_splitted)
  in

  let args_l2_p =
    List.map
      (fun list_arg ->
        List.map (fun p -> TestGen.insert_packing p [ (Tc.Out, None) ]) list_arg)
      args_basic_np
  in

  let args_basic_np = List.map (fun a -> (a, None)) args_basic_np in
  let args_basic_p = List.map (fun a -> (a, None)) args_basic_p in
  let args_splitted = List.map (fun a -> (a, None)) args_splitted in
  let args_l2_p = List.map (fun a -> (a, None)) args_l2_p in
  let all_params =
    [
      (args_l2_p, Printf.sprintf "%s_local_pack_C.csv");
      (args_splitted_p, Printf.sprintf "%s_split_pack_C.csv");
      (args_splitted, Printf.sprintf "%s_split_non_pack.csv");
      (args_basic_np, Printf.sprintf "%s_non_pack.csv");
      (args_basic_p, Printf.sprintf "%s_pack_C.csv");
    ]
  in
  let exec (exec_wrap_args, gen_file_name) =
    List.iter
      (fun (dim_sizes, (exec_param, opt_gen)) ->
        let sizes = List.map (fun (_, s) -> string_of_int s) dim_sizes in
        let file = gen_file_name (String.join "_" sizes) in
        TestExec.run_schemes_to_csv ~wrap_params:opt_gen ~nthreads exec_param
          counters file)
      (List.combine various_sizes exec_wrap_args)
  in

  flush stdout;
  Custom_utils.do_with_progress_bar exec all_params

(* Don't forget to launch ! *)
let _ = main ()

(* Note - Compilation command (to write as a single line):
   gcc -O2 -g -Wall -fopenmp -Wno-unused-function -march=native -fno-align-loops
    buffer.c timing.c main.c mem_utils.c gen/handle_counters.c tc_utils.c matrix_utils.c gen/gen_tc.c
    -L/usr/lib -lm -lpthread -lpapi -o mm_bench.x
*)

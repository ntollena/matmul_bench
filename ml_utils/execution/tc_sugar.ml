open Batteries
open Common

type sugar_tile_type =
  | V of string
  | T of (int * string)
  | T_par of (int * string)
  | Fused_T_pars of (int * string) list
  | U of (int * string)
  | R of string
  | External_call of Tc.TcTypes.ext_call_arg
  | Tile_exact of (int * string)
  | Tile_gexact of (int * string)
  | Tile_partial of (int * string)
  | ULambda of string
  | TLambda of string
  | Hoist_vars of string list
  | Pack_tens of Tc.tensor
  | Pack_trans of Tc.tensor * string list
  | Lambda_apply of string * (Tensor_loops.iter * Tensor_loops.arg) list
[@@deriving show { with_path = false }]

type tc_sugar_args = {
  dim_sizes_left : (string * int) list;
  dim_sizes_red : (string * int) list;
  dim_sizes_right : (string * int) list;
  layout_out : string list;
  layout_left : string list;
  layout_right : string list;
  scheme : sugar_tile_type list;
}
[@@deriving show { with_path = false }]

(** `update_layout arg l` returns a new tc_sugar_args same as `arg` but with the layout of the specified tensor modified *)
let update_layout arg l =
  match l with
  | Tc.Left, layout_left -> { arg with layout_left }
  | Tc.Right, layout_right -> { arg with layout_right }
  | Tc.Out, layout_out -> { arg with layout_out }

let update_dim_sizes_in_args new_dim_sizes
    ({ dim_sizes_left; dim_sizes_red; dim_sizes_right; _ } as params) =
  let new_left =
    List.map
      (fun (dim, _) -> List.find (fun (dim2, _) -> dim2 = dim) new_dim_sizes)
      dim_sizes_left
  in
  let new_red =
    List.map
      (fun (dim, _) -> List.find (fun (dim2, _) -> dim2 = dim) new_dim_sizes)
      dim_sizes_red
  in
  let new_right =
    List.map
      (fun (dim, _) -> List.find (fun (dim2, _) -> dim2 = dim) new_dim_sizes)
      dim_sizes_right
  in
  {
    params with
    dim_sizes_left = new_left;
    dim_sizes_red = new_red;
    dim_sizes_right = new_right;
  }

let dim_sizes_alphabetical { dim_sizes_left; dim_sizes_red; dim_sizes_right; _ }
    =
  let dim_size_list =
    List.flatten [ dim_sizes_left; dim_sizes_red; dim_sizes_right ]
  in
  List.sort (fun (dim1, _) (dim2, _) -> String.compare dim1 dim2) dim_size_list

(** Give 2 string representations of layout and dim sizes 
      * example: abc_adec_ebd, 2_72_24_8_4 sizes order by alphabetical order of dim names *)
let strings_from_params ({ layout_out; layout_left; layout_right; _ } as arg) =
  let sizes =
    List.map string_of_int (List.map snd (dim_sizes_alphabetical arg))
  in
  let layout =
    List.map (String.concat "") [ layout_out; layout_left; layout_right ]
  in
  (String.concat "_" layout, String.concat "_" sizes)

(** Return a list ordered from outter to inner of dimension*)
let dims_from_tens { layout_left; layout_right; layout_out; _ } = function
  | Tc.Left -> layout_left
  | Tc.Right -> layout_right
  | Tc.Out -> layout_out

(** Return a list ordered from outter to inner of dimension and their size*)
let dim_and_sizes_from_tens tens param =
  let dim_sizes = dim_sizes_alphabetical param in
  let layout = dims_from_tens param tens in
  List.map
    (fun dim ->
      List.find_map
        (function d, size when d = dim -> Some (dim, size) | _ -> None)
        dim_sizes)
    layout

let is_dim_in_tens tensor dim param =
  let dims = dims_from_tens param tensor in
  List.mem dim dims

let is_dim_left_red_right dim { dim_sizes_left; dim_sizes_red; _ } =
  let aux dim_sizes =
    List.exists (function d, _ when d = dim -> true | _ -> false) dim_sizes
  in
  let is_left = aux dim_sizes_left in
  let is_red = aux dim_sizes_red in
  let is_right = not (is_left && is_red) in
  (is_left, is_red, is_right)

let size_of_tens tens param =
  let dim_sizes = dim_sizes_alphabetical param in
  let sizes =
    List.filter_map
      (function d, s when is_dim_in_tens tens d param -> Some s | _ -> None)
      dim_sizes
  in
  List.fold (fun a b -> a * b) 1 sizes

let size_of_dim dim param =
  let dim_sizes = dim_sizes_alphabetical param in
  List.find_map (function d, s when d = dim -> Some s | _ -> None) dim_sizes

let dim_from_name name dim_names =
  let left_names, red_names, right_names = dim_names in
  let left_dim x = Tc.LeftDim x in
  let red_dim x = Tc.RedDim x in
  let right_dim x = Tc.RightDim x in

  let opt_dim =
    List.find_map_opt
      (fun (names, dim) ->
        match List.index_of name names with Some i -> Some (dim i) | _ -> None)
      [ (left_names, left_dim); (red_names, red_dim); (right_names, right_dim) ]
  in

  match opt_dim with
  | None ->
      let s = [%show: string list * string list * string list] dim_names in
      let error_msg =
        Printf.sprintf "Can't find dim %s in any dimension names %s" name s
      in
      failwith error_msg
  | Some dim -> dim

(**`build_basic_tc_args sugar_args` return scheme with dim type set automatically and 3 tuple of len 3 cotaining info about dimensions names, sizes and tensors layouts *)
let build_basic_tc_args
    {
      dim_sizes_left;
      dim_sizes_red;
      dim_sizes_right;
      layout_out;
      layout_left;
      layout_right;
      scheme;
    } =
  let left = List.map fst dim_sizes_left in
  let red = List.map fst dim_sizes_red in
  let right = List.map fst dim_sizes_right in
  Utils.check_duplicates left "dim_names_left";
  Utils.check_duplicates red "dim_names_red";
  Utils.check_duplicates right "dim_names_right";

  let dimnames = (left, red, right) in
  let sizes =
    ( List.map snd dim_sizes_left,
      List.map snd dim_sizes_red,
      List.map snd dim_sizes_right )
  in

  Utils.check_duplicates layout_out "layout_out";
  Utils.check_duplicates layout_left "layout_left";
  Utils.check_duplicates layout_right "layout_right";
  let l_layout_Out = Config.Tc_lay_dims_str (layout_out, left, right) in
  let l_layout_Left = Config.Tc_lay_dims_str (layout_left, left, red) in
  let l_layout_Right = Config.Tc_lay_dims_str (layout_right, red, right) in
  let ll_layout = (l_layout_Left, l_layout_Right, l_layout_Out) in

  let scheme_dim =
    List.map
      (fun tile_with_str ->
        match tile_with_str with
        | V s -> Tc.TcTypes.V (dim_from_name s dimnames)
        | T (i, s) -> Tc.TcTypes.T (i, dim_from_name s dimnames)
        | T_par (i, s) -> Tc.TcTypes.T_par (i, dim_from_name s dimnames)
        | Fused_T_pars list ->
            Tc.TcTypes.Fused_T_pars
              (List.map (fun (i, s) -> (i, dim_from_name s dimnames)) list)
        | U (i, s) -> Tc.TcTypes.U (i, dim_from_name s dimnames)
        | R s -> Tc.TcTypes.R (dim_from_name s dimnames)
        | External_call arg -> Tc.TcTypes.External_call arg
        | Tile_exact (i, s) ->
            Tc.TcTypes.Tile_exact (i, dim_from_name s dimnames)
        | Tile_gexact (i, s) ->
            Tc.TcTypes.Tile_gexact (i, dim_from_name s dimnames)
        | Tile_partial (i, s) ->
            Tc.TcTypes.Tile_partial (i, dim_from_name s dimnames)
        | ULambda s -> Tc.TcTypes.ULambda (dim_from_name s dimnames)
        | TLambda s -> Tc.TcTypes.TLambda (dim_from_name s dimnames)
        | Hoist_vars list_s ->
            Tc.TcTypes.Hoist_vars
              (List.map (fun s -> dim_from_name s dimnames) list_s)
        | Pack_tens t -> Tc.TcTypes.Pack_tens t
        | Pack_trans (t, list_s) ->
            Utils.check_duplicates list_s "Pack_trans permutation";
            Tc.TcTypes.Pack_trans
              (t, List.map (fun s -> dim_from_name s dimnames) list_s)
        | Lambda_apply (s, arg) ->
            Tc.TcTypes.Lambda_apply (dim_from_name s dimnames, arg))
      scheme
  in
  (scheme_dim, dimnames, sizes, ll_layout)

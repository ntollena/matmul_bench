open Common

include module type of Modules_decl

module Bench (A : Arch.Arch_t) : sig
  val gen_code :
    'a Config.layout ->
    'a Config.args ->
    'a Codegen.scheme ->
    'a Config.shape ->
    string

  val gen_file :
    'a Config.layout ->
    'a Config.args ->
    'a Codegen.scheme ->
    'a Config.shape ->
    string ->
    unit

  val exec :
    ?wrap_code:string * string ->
    ?exec_args_conf:('a Config.args * 'a Config.t) option ->
    ?nthreads:int ->
    'a Config.t ->
    'a Config.args ->
    'a Codegen.scheme ->
    'a Config.shape ->
    'a Config.results
end

module TL = Tensor_loops
open Config
open Common
open Batteries

(* Note: similar to mm.ml and conv.ml *)

(* Definition of the dimensions *)
let dim_gen = TL.Ids.Dim.fresh_gen

(* Declared as a module and then immediatly included because we need to pass
 * that as a parameter later *)

(* Signature in order to pass the dimension names*)
module type TC_dims_args_t = sig
  val lname_left : string list (* Dims shared by Output and Left tensor *)
  val lname_right : string list (* Dims shared by Output and Right tensor *)
  val lname_red : string list (* Dims shared by both left and right tensor *)
end

(* Associating these dimensions with "easy to use" constructors names *)
type dim = LeftDim of int | RightDim of int | RedDim of int
[@@deriving eq, show]

(* Tensors of a tensor contraction *)
type tensor = Out | Left | Right [@@deriving show]

module Dim_tens_t = struct
  type nonrec dim = dim

  let pp_dim = pp_dim
  let show_dim = show_dim

  type nonrec tensor = tensor

  let pp_tensor = pp_tensor
  let show_tensor = show_tensor
  (* TODO fix *)

  let all_dims = []
  let all_tensors = [ Out; Left; Right ]
end

module TcTypes = Tile_type.F (Dim_tens_t)

type tile = TcTypes.tile [@@deriving show]

let tile_to_string = [%show: tile list]

(* Tensor does not make sense if we do not know how much *)
module TC_dims (TC_dims_args : TC_dims_args_t) = struct
  (* Building the dimensions *)
  let left_dim_gen_reset =
    List.map (fun name -> dim_gen name) TC_dims_args.lname_left

  let right_dim_gen_reset =
    List.map (fun name -> dim_gen name) TC_dims_args.lname_right

  let red_dim_gen_reset =
    List.map (fun name -> dim_gen name) TC_dims_args.lname_red

  let left_dim = List.map (fun (f, _) -> f ()) left_dim_gen_reset
  let right_dim = List.map (fun (f, _) -> f ()) right_dim_gen_reset
  let red_dim = List.map (fun (f, _) -> f ()) red_dim_gen_reset

  let reset () =
    List.iter
      (fun (_, r) -> r ())
      (left_dim_gen_reset @ right_dim_gen_reset @ red_dim_gen_reset)

  let from_dim = function
    | LeftDim i -> List.nth left_dim i
    | RightDim i -> List.nth right_dim i
    | RedDim i -> List.nth red_dim i

  (* Building the default layout arrays (logical layout, no permutation) *)
  let array_sizes_deflay_A, array_sizes_deflay_B, array_sizes_deflay_C =
    (* Default logical layout : C[left, right] += A[left, red] * B[red, right] *)
    let num_left = List.length TC_dims_args.lname_left in
    let num_right = List.length TC_dims_args.lname_right in
    let num_red = List.length TC_dims_args.lname_red in

    let array_sizes_A = Array.make (num_left + num_red) (List.hd left_dim) in
    let array_sizes_B = Array.make (num_red + num_right) (List.hd red_dim) in
    let array_sizes_C = Array.make (num_left + num_right) (List.hd left_dim) in

    (* Warning - layout is reversed : first dims are the innermost *)
    let () =
      List.iteri
        (fun i dim ->
          array_sizes_B.(i) <- dim;
          array_sizes_C.(i) <- dim)
        (List.rev right_dim)
    in

    let () =
      List.iteri
        (fun i dim ->
          array_sizes_A.(i) <- dim;
          array_sizes_B.(num_right + i) <- dim)
        (List.rev red_dim)
    in

    let () =
      List.iteri
        (fun i dim ->
          array_sizes_A.(num_red + i) <- dim;
          array_sizes_C.(num_right + i) <- dim)
        (List.rev left_dim)
    in
    (array_sizes_A, array_sizes_B, array_sizes_C)

  (* Note: no layout conversion function here (!= convolution) *)

  (* TODO: note - possible to add another argument to the functor here, with TC sizes *)
  (* This would be a permutation between A and TC_dims_args as the parameters of the module "Gen" *)
  module Gen (A : Arch.Arch_t) = struct
    open TL
    open Tensor_tile (A.M)

    module type Tc_sign = sig
      include Sign.TC_args_t
      (* Note: inside tensor-mapping, file "kernels_sign.ml" *)

      val gen_code : ?dim_sizes:(Ids.Dim.t * int) list -> tile_scheme -> string
    end

    (* Building the tensor-loops TC module, with the correct layout, tensor and dimensions *)

    (* Default layout : we derive it from the order "Tc_dims_args" *)
    (* Note: TC_build comes from tensor-mapping :: tensor_transform.mli *)
    module Tc_default_layout : Tc_sign = TC_build (struct
      (* Building objects of the signature *)
      let left = left_dim
      let right = right_dim
      let red = red_dim

      let a =
        Tensor.make ~name:"leftIn"
          array_sizes_deflay_A (* Note: must match gen_tc.c.template *)

      and b = Tensor.make ~name:"rightIn" array_sizes_deflay_B
      and c = Tensor.make ~name:"output" array_sizes_deflay_C

      let reset = reset
    end)

    let get_tc_default_layout _ : (module Tc_sign) = (module Tc_default_layout)

    (* Auxilliary function - permutate an array usinga layout *)
    (* Layout = list of integers from 0 to n, permuted *)
    let permutate_dim lay arr =
      let rec permutate_dim_aux narr arr permut_arr i lay =
        match lay with
        | [] -> permut_arr
        | h :: t ->
            permut_arr.(narr - 1 - i) <- arr.(narr - 1 - h);
            (* Need to reverse the permut here *)
            (* Indeed perm is outer to inner, layout is the opposite *)
            permutate_dim_aux narr arr permut_arr (i + 1) t
      in
      let narr = Array.length arr in
      assert (narr == List.length lay);
      let permut_arr = Array.make narr arr.(0) in
      permutate_dim_aux narr arr permut_arr 0 lay

    (* To build a TC module with a given layout *)
    let build_tc_module_layout left_perm right_perm out_perm : (module Tc_sign)
        =
      (* Use the permutation of the layout to get array_sizes_A *)
      let array_sizes_A = permutate_dim left_perm array_sizes_deflay_A in
      let array_sizes_B = permutate_dim right_perm array_sizes_deflay_B in
      let array_sizes_C = permutate_dim out_perm array_sizes_deflay_C in

      (* DEBUG
         let print_array arr_name arr =
           Printf.printf "%s = [ " arr_name;
           Array.iter (fun e -> Printf.printf "%s, " (Ids.Dim.show_id_of_t e) ) arr;
           Printf.printf ("]\n")
         in
         let _ = print_array "array_sizes_deflay_A" array_sizes_deflay_A in
         let _ = print_array "array_sizes_deflay_B" array_sizes_deflay_B in
         let _ = print_array "array_sizes_deflay_C" array_sizes_deflay_C in

         let _ = print_array "array_sizes_A" array_sizes_A in
         let _ = print_array "array_sizes_B" array_sizes_B in
         let _ = print_array "array_sizes_C" array_sizes_C in
      *)
      (module TC_build (struct
        let left = left_dim
        let right = right_dim
        let red = red_dim

        let a =
          Tensor.make ~name:"leftIn"
            array_sizes_A (* Note: must match gen_tc.c.template *)

        and b = Tensor.make ~name:"rightIn" array_sizes_B
        and c = Tensor.make ~name:"output" array_sizes_C

        let reset = reset
      end))

    let build_tc_module_layout_explicit_stride left_perm right_perm out_perm
        englob_sizes_A englob_sizes_B englob_sizes_C : (module Tc_sign) =
      (* Use the permutation of the layout to get array_sizes_A *)
      let array_sizes_A = permutate_dim left_perm array_sizes_deflay_A in
      let array_sizes_B = permutate_dim right_perm array_sizes_deflay_B in
      let array_sizes_C = permutate_dim out_perm array_sizes_deflay_C in

      (* Permutate englobing sizes, according to the layout *)
      let englob_sizes_A_perm = permutate_dim left_perm englob_sizes_A in
      let englob_sizes_B_perm = permutate_dim right_perm englob_sizes_B in
      let englob_sizes_C_perm = permutate_dim out_perm englob_sizes_C in

      (* Adding the Dim.t at the start *)
      let engl_array_to_l_engl_sizes array_size (acc, i) elem =
        ((array_size.(i), elem) :: acc, i + 1)
      in

      let (l_englob_sizes_A_perm, _) : (Ids.Dim.t * Exprs.Expr.t) list * int =
        Array.fold_left
          (engl_array_to_l_engl_sizes array_sizes_A)
          ([], 0) englob_sizes_A_perm
      in
      let (l_englob_sizes_B_perm, _) : (Ids.Dim.t * Exprs.Expr.t) list * int =
        Array.fold_left
          (engl_array_to_l_engl_sizes array_sizes_B)
          ([], 0) englob_sizes_B_perm
      in
      let (l_englob_sizes_C_perm, _) : (Ids.Dim.t * Exprs.Expr.t) list * int =
        Array.fold_left
          (engl_array_to_l_engl_sizes array_sizes_C)
          ([], 0) englob_sizes_C_perm
      in

      (module TC_build (struct
        let left = left_dim
        let right = right_dim
        let red = red_dim
        let reset = reset

        let a =
          Tensor.make ~name:"leftIn" ~strides:l_englob_sizes_A_perm
            array_sizes_A (* Note: must match gen_tc.c.template *)

        and b =
          Tensor.make ~name:"rightIn" ~strides:l_englob_sizes_B_perm
            array_sizes_B

        and c =
          Tensor.make ~name:"output" ~strides:l_englob_sizes_C_perm
            array_sizes_C
      end))

    (* Functor to plug the previous module to get the code gen for the loops *)
    module To_ttile (C : Tc_sign) : sig
      type nonrec tile = TcTypes.tile

      val to_ttile : tile -> loop_type
      val to_scheme : tile list -> loop_type list
      val dim_tile_size : tile list -> Dim_tens_t.dim -> int
    end = struct
      let from_tensor = function Out -> C.c | Left -> C.a | Right -> C.b

      include
        Tile_type.To_ttile (A) (Dim_tens_t)
          (struct
            let from_dim = from_dim
            let from_tensor = from_tensor
          end)

      let dim_tile_size scheme dim =
        dim_tile_size (to_scheme scheme) (from_dim dim)
    end

    module Default_tile = To_ttile (Tc_default_layout)

    (* Workaround : we don't really need any information on tensors 
     * for getting tile sizes, but we do need to translate the tile into
     * loop_tile list, so we use the default tensor definition as a placeholder *)
    let dim_tile_size = Default_tile.dim_tile_size

    (* Associate the sizes with the dimensions *)
    let build_dim_sizes lsizes_left lsizes_right lsizes_red =
      (* Aux function*)
      let rec associate_size_with_dim lsizes ldims lres =
        match (lsizes, ldims) with
        | [], [] -> lres
        | s :: rs, d :: rd -> associate_size_with_dim rs rd ((d, s) :: lres)
        | _, _ ->
            Format.eprintf
              "tc::build_dim_sizes: lsizes and ldims does not have the same \
               length\n\
               @?";
            assert false
      in

      let lres = [] in
      let lres = associate_size_with_dim lsizes_left left_dim lres in
      let lres = associate_size_with_dim lsizes_right right_dim lres in
      let lres = associate_size_with_dim lsizes_red red_dim lres in
      lres
  end
end

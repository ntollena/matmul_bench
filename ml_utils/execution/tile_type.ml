open Tensor_loops
open Common
open Arch
open Ids

module type Dim_tens_types = sig
  type dim [@@deriving show]
  type tensor [@@deriving show]

  val all_tensors : tensor list
  val all_dims : dim list
end

module F (S : Dim_tens_types) = struct
  include S

  type ext_call_arg = {
    name : string;
    fixed_size : (dim * int) list;
    var_dim : dim;
    dynamic_sizes : (dim * int) list;
    max_range : int;
  }
  [@@deriving show]

  type tile =
    | V of dim
    | T of (int * dim)
    | T_par of (int * dim)
    | Fused_T_pars of (int * dim) list
    | U of (int * dim)
    | R of dim
    | External_call of ext_call_arg
    | Tile_exact of (int * dim)
    | Tile_gexact of (int * dim)
    | Tile_partial of (int * dim)
    | ULambda of dim
    | TLambda of dim
    | Hoist_vars of dim list
    | Pack_tens of tensor
    | Pack_trans of tensor * dim list
    | Lambda_apply of dim * (iter * arg) list
  [@@deriving show { with_path = false }]
end

module To_ttile
    (A : Arch_t)
    (Sign : Dim_tens_types) (M : sig
      val from_dim : Sign.dim -> Dim.t
      val from_tensor : Sign.tensor -> Tensor.t
    end) : sig
  include
    module type of F (Sign)
    with type dim := Sign.dim
     and type tensor := Sign.tensor

  type tile = F(Sign).tile
  type loop_type = Tensor_tile(A.M).loop_type

  val to_ttile : tile -> loop_type
  val to_scheme : tile list -> loop_type list
  end =
  struct
  open M
  open Tensor_tile (A.M)

  type nonrec loop_type = loop_type

  include F (Sign)

  let to_ttile : tile -> loop_type = function
    | V d -> V (from_dim d)
    | T (i, d) -> T (i, from_dim d)
    | T_par (i, d) -> T_par (i, from_dim d)
    | Fused_T_pars list ->
        Fused_T_pars (List.map (fun (i, d) -> (i, from_dim d)) list)
    | U (i, d) -> U (i, from_dim d)
    | R d -> R (from_dim d)
    | External_call { name; fixed_size; var_dim; dynamic_sizes; max_range } ->
        External_call
          {
            name;
            fixed_size = List.map (Utils.map_fst from_dim) fixed_size;
            var_dim = from_dim var_dim;
            tensors_list = List.map from_tensor Sign.all_tensors;
            dynamic_sizes = List.map (Utils.map_fst from_dim) dynamic_sizes;
            max_range;
          }
    | Tile_exact (i, d) -> Tile_exact (i, from_dim d)
    | Tile_gexact (i, d) -> Tile_gexact (i, from_dim d)
    | Tile_partial (i, d) -> Tile_partial (i, from_dim d)
    | ULambda d -> ULambda (from_dim d)
    | TLambda d -> TLambda (from_dim d)
    | Hoist_vars dlst -> Hoist_vars (List.map from_dim dlst)
    | Pack_tens tensor -> Pack_tens (from_tensor tensor)
    | Pack_trans (tensor, dims) ->
        Pack_trans (from_tensor tensor, List.map from_dim dims)
    | Lambda_apply (d, iter_args) -> Lambda_apply (from_dim d, iter_args)

  let to_scheme = List.map to_ttile
end

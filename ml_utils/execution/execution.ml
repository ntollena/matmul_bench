open Shexp_process
open Shexp_process.Infix

(* Doc: https://ocaml.janestreet.com/ocaml-core/v0.13/doc/shexp/Shexp_process__/Process/index.html *)
open Inject_code
open Batteries
open Common
module BC = Build_c
include Modules_decl

module Bench (A : Arch.Arch_t) = struct
  open Codegen
  module Gen = Gen (A)

  (* Generation of the generated files, from the .template *)

  let string_from_template lines_to_insert mark template =
    let mark_begin = Printf.sprintf ">>INSERT %s>>" mark
    and mark_end = Printf.sprintf "<<INSERT %s<<" mark in
    run "cat" [ template ]
    |- (* pipe between process *)
    fold_replace_shxp mark_begin mark_end [ lines_to_insert ]

  let file_from_template lines_to_insert mark template dstfile =
    string_from_template lines_to_insert mark template
    >>= Shexp_process.print |> stdout_to dstfile

  (* File: "gen/gen_XXX.c" *)
  let code_file_from_template ?(wrap_code = ("", "")) layout arg tile shape =
    let main_code = Gen.gen_code layout arg tile shape in
    let before_code, after_code = wrap_code in
    let all_code = String.concat "\n" [ before_code; main_code; after_code ] in
    file_from_template all_code "CODE"

  (* File "gen/upkeep_cache.h" *)
  let cache_upkeep_from_template layout arg shape cache_upkeep =
    let str_content = Gen.gen_cache_upkeep layout arg shape cache_upkeep in
    file_from_template str_content "CODE"

  let params_from_template conf =
    let src = from_config conf |> gen_params (module A) in
    file_from_template src "PARAMS"

  let layout_params_from_template conf =
    let src = from_config conf |> gen_layout_params (module A) in
    file_from_template src "LAYOUT_PARAMS"

  let handle_counters_from_template counters =
    let src = gen_counter_print counters in
    file_from_template src "COUNTERS_PRINT"

  let event_names_from_template counters =
    let src = gen_events_name counters in
    file_from_template src "EVENT_NAMES"

  (* ================================================ *)

  let discard_stdout command =
    stdout_to "/dev/null" command |> stderr_to "/dev/null"

  let home = get_env_exn "HOME"

  (* if generation is None, files are not recompiled *)
  let dry_run ?(discard_out = true) conf args =
    let proceed_command = if discard_out then discard_stdout else Fun.id in
    let open Config in
    let { generation; blis_lib; benches; _ } = to_conf conf in
    let exec_args : type a. a args -> unit Shexp_process.t =
      let soi = string_of_int in
      function
      | MM_args (i, j, k) -> run "./mm_bench.x" [ soi i; soi j; soi k ]
      | Conv_args
          {
            x;
            w;
            y;
            h;
            c;
            f;
            strides = None;
            englobing_input;
            englobing_output;
            englobing_params;
            _;
          } ->
          (* DEBUG
             (match englobing_input with
             | None -> Printf.printf "englobing_input = None\n"
             | Some (a,b,c,d,e,f) -> Printf.printf "englobing_input = %d, %d, %d, %d, %d, %d\n" a b c d e f
             );
             (match englobing_output with
             | None -> Printf.printf "englobing_output = None\n"
             | Some (a,b,c,d,e,f) -> Printf.printf "englobing_output = %d, %d, %d, %d, %d, %d\n" a b c d e f
             );
          *)

          (* Assert englobing_XXX are all equal (else, we need more arguments to send to the main.c ) *)
          assert (englobing_input = englobing_output);
          assert (englobing_input = englobing_params);

          let l_commandline_inputs = [ x; w; y; h; c; f ] in
          let l_commandline_inputs =
            match englobing_input with
            | None -> l_commandline_inputs
            | Some (fe, ce, ye, xe, he, we) ->
                l_commandline_inputs @ [ xe; we; ye; he; ce; fe ]
          in
          let args = List.map soi l_commandline_inputs in
          run "./mm_bench.x" args
      | Conv_args
          {
            x;
            w;
            y;
            h;
            c;
            f;
            strides = Some (str_x, str_y);
            englobing_input;
            englobing_output;
            englobing_params;
            _;
          } ->
          (* Assert englobing_XXX are all equal (else, we need more arguments to send to the main.c ) *)
          assert (englobing_input == englobing_output);
          assert (englobing_input == englobing_params);

          let l_commandline_inputs = [ x; w; y; h; c; f; str_x; str_y ] in
          let l_commandline_inputs =
            match englobing_input with
            | None -> l_commandline_inputs
            | Some (fe, ce, ye, xe, he, we) ->
                l_commandline_inputs @ [ xe; we; ye; he; ce; fe ]
          in
          let args = List.map soi l_commandline_inputs in
          run "./mm_bench.x" args
      | TC_args
          {
            sizes_left = lsize_left;
            sizes_red = lsize_red;
            sizes_right = lsize_right;
          } ->
          let args =
            List.map soi
              ([
                 List.length lsize_left;
                 List.length lsize_red;
                 List.length lsize_right;
               ]
              @ lsize_left @ lsize_red @ lsize_right)
          in
          run "./mm_bench.x" args
    in
    let open Build_c in
    home >>= fun _ ->
    Option.map_default
      (fun (_, _, compiler, opt_level) ->
        compile ~opt_level compiler A.include_dirs A.ld_libraries blis_lib
          A.arch_name (List.map fst benches)
        |> proceed_command)
      (return ()) generation
    >> exec_args args

  (* Run process only if cond is true *)
  let may cond process = if cond then Lazy.force process else return ()

  let may_opt cond process =
    match cond with Some x -> Lazy.force process x | None -> return ()

  (** Some important notes on optionnal args
      - wrap_code allows to insert code before and after the generated code
      - exec_args_conf allows to use a different conf and args for exec than those used with code gen
      *)
  let glue_all (type a) ?(wrap_code = ("", "")) ?(exec_args_conf = None)
      (conf : a Config.t) (args : a Config.args) tile shape =
    let exec_args, exec_conf =
      match exec_args_conf with None -> (args, conf) | Some a -> a
    in
    let open Config in
    let name_kernel : type a. a t -> string = function
      | MM_conf _ -> "matmul"
      | Conv_conf _ -> "conv"
      | TC_conf _ -> "tc"
    in
    let gen_name : type a. a t -> string =
     fun c -> "gen_" ^ name_kernel c ^ ".c"
    in
    let gen_name = gen_name conf in

    let { generation; benches; counters; _ } = Config.to_conf conf in
    let contains_gen_or_conv : type a. a bench * a layout -> a layout option =
      function
      | Gen, MM_layout -> Some MM_layout
      | Conv, conv_layout -> Some conv_layout
      | TcGen, tc_layout -> Some tc_layout
      | _ -> None
    in
    let layout_opt = List.find_map_opt contains_gen_or_conv benches in
    let regen_kern : a layout option =
      Option.bind generation (fun (cond, _, _, _) ->
          if cond then layout_opt else None)
    in
    let regen_framework =
      match generation with Some (_, true, _, _) -> true | _ -> false
    in
    discard_stdout A.init
    (* regenerate code only if regen = true - now that we have added layout this feels unecessary *)
    >> may_opt regen_kern
         (lazy
           (fun layout ->
             code_file_from_template ~wrap_code layout args tile shape
               (Printf.sprintf "%s.template" gen_name)
               (Printf.sprintf "gen/%s" gen_name)
             >> cache_upkeep_from_template layout args shape
                  Codegen.Hot_cache (* TODO !!! *)
                  (Printf.sprintf "cache_upkeep_%s.c.template"
                     (name_kernel conf))
                  "gen/cache_upkeep.c"))
    >> may regen_framework
         (lazy
           (params_from_template exec_conf "params.h.template" "gen/params.h"
           >> layout_params_from_template exec_conf "layout_params.h.template"
                "gen/layout_params.h"
           >> event_names_from_template counters "event_names.h.template"
                "gen/event_names.h"
           >> handle_counters_from_template counters
                "handle_counters.c.template" "gen/handle_counters.c"))
    >> dry_run ~discard_out:false conf exec_args
    (* Note: discard_out commented if silent *)
    |- read_all
    >>| Config.results conf

  let set_ld_library prog =
    match A.ld_libraries with
    | [] -> prog
    | l -> String.concat ":" l |> fun s -> set_env "LD_LIBRARY_PATH" s prog

  let set_runtime_vars prog =
    List.fold_left
      (fun prog (name, value) -> set_env name value prog)
      prog A.runtime_vars

  let gen_code_file layout args tile shape filename postfix =
    code_file_from_template layout args tile shape
      (Printf.sprintf "%s.c.template" filename)
      (Printf.sprintf "gen/%s.c" (filename ^ postfix))

  let filename : type a. a Config.args -> string = function
    | Config.MM_args _ -> "gen_mm"
    | Config.Conv_args _ -> "gen_conv"
    | Config.TC_args _ -> "gen_tc"

  let c_bench_path () = Common.Env.find_root () ^ "/c_bench"

  let from_template =
    Printf.sprintf
      "#include <x86intrin.h>\n\
      \    #include <assert.h>\n\
      \    void gen_conv(float * const  __restrict__ output,\n\
      \    float const * const __restrict__ input, float const * const \
       __restrict__ params,\n\
      \        int X, int W,\n\
      \        int Y, int H,\n\
      \    int C, int F) {\n\
      \        %s\n\
      \        }\n\
      \    "

  let gen_code layout args tile shape =
    from_template (Gen.gen_code layout args tile shape)

  let gen_file layout args tile shape postfix =
    let context = Context.(create ~cwd:(Path (c_bench_path ())) ()) in
    eval ~context
    @@ gen_code_file layout args tile shape (filename args) postfix

  let exec (type t) ?(wrap_code = ("", "")) ?(exec_args_conf = None)
      ?(nthreads = 1) (conf : t Config.t) (args : t Config.args) tile shape :
      t Config.results =
    let context = Context.(create ~cwd:(Path (c_bench_path ())) ()) in
    (* If dispose is not called at some point, we'll have a resource leak (too
     * many files opened) *)
    (*Gc.finalise Context.dispose context;*)
    let glue =
      glue_all ~wrap_code ~exec_args_conf conf args tile shape
      |> set_ld_library |> set_runtime_vars
      |> set_env "OMP_NUM_THREADS" (string_of_int nthreads)
    in
    let res = eval ~context glue in
    Context.dispose context;
    res
end

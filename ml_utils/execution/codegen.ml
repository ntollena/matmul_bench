open Batteries
open Common
open Arch
module TL = Tensor_loops

(* Generic type for the scheme / loop strategies (MM, Conv or TC) *)
type _ scheme =
  | MM_tile : Mm.tile list -> Bench_types.mm scheme
  | Conv_tile : Conv.tile list -> Bench_types.conv scheme
  | TC_tile : Tc.tile list -> Bench_types.tc scheme

(* Which cache upkeep to do between repetitions *)
type 'a cache_upkeep =
  | Hot_cache (* Nothing done to the cache between 2 repets *)
  | Cold_cache (* Cache flushed between 2 repets *)
  | Scheme_cache of 'a scheme
(* An implementation is run between 2 repets of a cache *)
(* Warning: this scheme must be a subset of the embedding (else, segfault) *)

let check_layout_sizes layout left_dims right_dims =
  let n = List.length layout in
  let n1 = List.length left_dims in
  let n2 = List.length right_dims in
  if n <> n1 + n2 then
    let s =
      Printf.sprintf "layout %s of size %d " ([%show: string list] layout) n
    in
    let s2 =
      Printf.sprintf "indices %s and %s of size %d"
        ([%show: string list] left_dims)
        ([%show: string list] right_dims)
        (n1 + n2)
    in
    let error_msg = Printf.sprintf "%s doesn't match permutation of %s" s s2 in
    failwith error_msg

(* Auxilliary function on permutations *)
let extract_permutation tc_lay =
  match tc_lay with
  | Config.Tc_default_lay ->
      failwith "Codegen.ml : Default TC layout should not happen here"
  | Config.Tc_lay_dims l_permut -> l_permut
  | Config.Tc_lay_dims_str (names, left_dims, right_dims) ->
      let logical_order = left_dims @ right_dims in
      check_layout_sizes names left_dims right_dims;
      List.map
        (fun dim_name ->
          let option_index = List.index_of dim_name logical_order in
          match option_index with
          | None ->
              let s1 = [%show: string list] names in
              let s2 = [%show: string list] logical_order in
              let error_msg =
                Printf.sprintf
                  "Codegen.ml : Can't extract permutation with dimensions %s, \
                   they don't match logical layout %s."
                  s1 s2
              in
              failwith error_msg
          | Some i -> i)
        names

type 'a params = {
  error : Config.error;
  benches : ('a Config.bench * 'a Config.layout) list;
  num_rep : int;
  counters : Counter.t list;
}

let build_params num_rep benches error counters =
  {
    benches;
    num_rep;
    counters = List.sort_uniq Counter.compare counters;
    error;
  }

(* Generation of the template content of the "params.h" file *)
let gen_params (module Arch : Arch_t) { num_rep; benches; error; counters } =
  let error_str =
    match error with
    | EXACT_EPS eps ->
        Printf.sprintf
          "#define ERROR_EXACT\n#define ERROR_EPS\n#define EPSILON %f" eps
    | EPSILON eps -> Printf.sprintf "#define ERROR_EPS\n#define EPSILON %f" eps
    | EXACT -> "#define ERROR_EXACT"
    | NO_CHECK -> ""
  in
  let config_str (type a) (b : a Config.bench) =
    let open Config in
    match b with
    | Blis -> "#define USE_MM\n#define BLIS"
    | Mkl -> "#define USE_MM\n#define MKL"
    | XSMM -> "#define USE_MM\n#define XSMM"
    | Gen -> "#define USE_MM\n#define GEN"
    | Conv -> "#define USE_CONV\n#define GEN"
    | Rui -> "#define USE_CONV\n#define RUI"
    | OneDNN -> "#define USE_CONV\n#define ONEDNN"
    | TcGen -> "#define USE_TC\n#define GEN"
  in
  let bench_str = String.concat "\n" (List.map (config_str % fst) benches) in

  let layout_vars (type a) ((b, l) : a Config.bench * a Config.layout) =
    let open Config in
    let def_vars prefix lay_out lay_in lay_par =
      let lout = show_conv_layout_out lay_out
      and lin = show_conv_layout_in lay_in
      and lpar = show_conv_layout_par lay_par in
      Printf.sprintf
        "#define %s_LAYOUT_OUT %s\n\
         #define %s_LAYOUT_IN %s\n\
         #define %s_LAYOUT_PAR %s\n"
        prefix lout prefix lin prefix lpar
    in
    match (b, l) with
    | _, MM_layout -> None
    | Conv, Conv_layout (lay_out, lay_in, lay_par) ->
        Some (def_vars "GEN" lay_out lay_in lay_par)
    | Rui, Conv_layout (lay_out, lay_in, lay_par) ->
        Some (def_vars "RUI" lay_out lay_in lay_par)
    | OneDNN, Conv_layout (lay_out, lay_in, lay_par) ->
        Some (def_vars "DNN" lay_out lay_in lay_par)
    | _, TC_layout _ -> None
    (* We do not generate the TC layout in param.h, but in layout_params.h *)
  in
  let layout_str = String.concat "\n" (List.filter_map layout_vars benches) in
  let enumerate =
    let rec enum i = function h :: t -> (i, h) :: enum (i + 1) t | [] -> [] in
    enum 0
  in
  let counter_str =
    enumerate counters
    |> List.map (fun (i, ctr) ->
           Printf.sprintf "#define %s %d" (Counter.show ctr) i)
    |> String.concat "\n"
  in
  let num_counter_str =
    let n_ctr = List.length counters in
    Printf.sprintf "#define N_CTR %d" n_ctr
  in
  let rep_str = Printf.sprintf "\n#define NUM_REP (%d)\n" num_rep
  and print_res = "#undef PRINT_RESULTS"
  and allocator = "#undef USE_CUSTOM_ALLOC"
  and architecture = "#define " ^ Arch.arch_type in
  String.concat "\n"
    [
      architecture;
      bench_str;
      layout_str;
      error_str;
      counter_str;
      num_counter_str;
      rep_str;
      allocator;
      print_res;
    ]

(* Generation of the template content of the "layout_params.h" file *)
let gen_layout_params (module Arch : Arch_t) conf =
  let benches = conf.benches in

  (* Permutation for the TC layout *)
  let get_str_permut_tc lpermut suffix =
    let str_permut_tc =
      "const IND_TYPE ndim_tc_" ^ suffix ^ " = "
      ^ string_of_int (List.length lpermut)
      ^ ";\n"
    in
    let str_permut_tc =
      str_permut_tc ^ "const IND_TYPE permut_tc_" ^ suffix ^ "["
      ^ string_of_int (List.length lpermut)
      ^ "] = { "
    in
    let str_permut_tc, _ =
      List.fold_left
        (fun (acc, isfirst) elem ->
          let res = if not isfirst then acc ^ ", " else acc in
          let res = res ^ string_of_int elem in
          (res, false))
        (str_permut_tc, true) lpermut
    in
    let str_permut_tc = str_permut_tc ^ " };\n" in
    str_permut_tc
  in

  (* Generated code for the layout array/dim size *)
  let layout_tc (type a) ((b, l) : a Config.bench * a Config.layout) =
    let open Config in
    match (b, l) with
    | _, TC_layout (tc_lay_left, tc_lay_right, tc_lay_out) ->
        let l_permut_left = extract_permutation tc_lay_left in
        let l_permut_right = extract_permutation tc_lay_right in
        let l_permut_out = extract_permutation tc_lay_out in

        (* Example of str_permut_tc:
            IND_TYPE const * const permut_tc_left = 3;
            const IND_TYPE ndim_tc_left = { 0, 1, 2 };
        *)
        let str_permut_tc_left = get_str_permut_tc l_permut_left "left" in
        let str_permut_tc_right = get_str_permut_tc l_permut_right "right" in
        let str_permut_tc_out = get_str_permut_tc l_permut_out "out" in

        let str_layout_tc =
          "\n" ^ str_permut_tc_left ^ str_permut_tc_right ^ str_permut_tc_out
        in

        Some str_layout_tc
    | _ -> None (* Rest of bench: do nothing here *)
  in
  let layout_str = String.concat "\n" (List.filter_map layout_tc benches) in
  layout_str

let from_config conf =
  let open Config in
  let { benches; num_rep; counters; error; _ } = Config.to_conf conf in
  build_params num_rep benches error counters

let from_config_with_layout conf benches =
  let open Config in
  let { num_rep; counters; error; _ } = Config.to_conf conf in
  build_params num_rep benches error counters

let gen_events_name counters =
  let counters = List.sort_uniq Counter.compare counters in
  let str_from_counter c =
    Printf.sprintf "#ifdef %s\n\t\"%s\",\n#endif" (Counter.show c)
      (Counter.counter_name c)
  in
  String.concat "\n" (List.map str_from_counter counters)

let gen_counter_print counters =
  let counters = List.sort_uniq Counter.compare counters in
  let str_from_counter c =
    let ctr_s = Counter.show c in
    Printf.sprintf "#ifdef %s\n\tprintf(\"%%lld\\n\", ctrs[%s]);\n#endif" ctr_s
      ctr_s
  in
  String.concat "\n" (List.map str_from_counter counters)

(* Generation of the content of the main file *)
module Gen (A : Arch_t) = struct
  module Conv = Conv.Gen (A)
  module MM = Mm.Gen (A)

  module TC (S : Tc.TC_dims_args_t) = struct
    module Tc_specialized = Tc.TC_dims (S)
    include Tc_specialized.Gen (A) (* Berk! *)

    let build_dim_sizes = build_dim_sizes

    (* module Tc_default_layout = Tc_default_layout *)
    let get_tc_default_layout = get_tc_default_layout
    let build_tc_module_layout = build_tc_module_layout

    (* module Ttile = To_ttile(Tc_default_layout)  *)
  end

  (* Seems unused
     type _ codegen_mod =
     | MM_mod:  Bench_types.mm codegen_mod
     | Conv_mod: (module Conv.Conv_sign) -> Bench_types.conv codegen_mod
     | Tc_mod : (module Tc.TC_dims_args_t as TC_Arg) -> (module TC(TC_Arg).Tc_sign) -> Bench_types.tc codegen_mod

     let build_conv_module out in_ params =
     Conv_mod (Conv.build_conv_module out in_ params)
  *)

  let gen_code :
      type a.
      a Config.layout -> a Config.args -> a scheme -> a Config.shape -> string =
    let open Config in
    fun layout arg scheme shape ->
      match (layout, arg, scheme, shape) with
      | _, MM_args (i, j, k), MM_tile t, _ ->
          let dim_sizes = MM.build_dim_sizes i j k in
          MM.gen_code ~dim_sizes (MM.to_scheme t)
      | ( Conv_layout (out, in_, params),
          Conv_args
            {
              x;
              w;
              y;
              h;
              c;
              f;
              englobing_input;
              englobing_output;
              englobing_params;
              strides;
            },
          Conv_tile t,
          _ ) -> (
          match strides with
          | Some (str_x, str_y) ->
              let module Conv_l =
                (val Conv.build_conv_module_explicit_strides englobing_output
                       englobing_input englobing_params out in_ params str_x
                       str_y)
              in
              let dim_sizes = Conv.build_dim_sizes x w y h c f in
              let module Ttile = Conv.To_ttile (Conv_l) in
              Conv_l.gen_code ~dim_sizes (Ttile.to_scheme t)
          | None ->
              let module Conv_l =
                (val Conv.build_conv_module_explicit_strides englobing_output
                       englobing_input englobing_params out in_ params 1 1)
              in
              let dim_sizes = Conv.build_dim_sizes x w y h c f in
              let module Ttile = Conv.To_ttile (Conv_l) in
              Conv_l.gen_code ~dim_sizes @@ Ttile.to_scheme t)
      (* Tensor contraction layout / Note: all defined in config.ml *)
      | ( TC_layout (tc_lay_left, tc_lay_right, tc_lay_out),
          TC_args
            {
              sizes_left = left_sizes;
              sizes_red = red_sizes;
              sizes_right = right_sizes;
            },
          TC_tile t,
          TC_shape (lname_left, lname_red, lname_right) ) ->
          let module Tc_Gen_dim_args : Tc.TC_dims_args_t = struct
            let lname_left = lname_left
            let lname_red = lname_red
            let lname_right = lname_right
          end in
          let open TC (Tc_Gen_dim_args) in
          (* We adapt the start of the generated code, in order to get the sizes from the array "SIZES" *)
          let get_parameters_adaptation_code offset lname_dims lsizes =
            assert (List.length lname_dims == List.length lsizes);
            let str, _ =
              List.fold_left
                (fun (acc, i) _ ->
                  let dim_name_i = List.nth lname_dims i in
                  (* Note: same convention than in tensor-mapping package, Ids.ml::Dim::fresh_gen *)
                  let param_name_i = String.capitalize dim_name_i in
                  let newline =
                    "\tIND_TYPE " ^ param_name_i ^ " = SIZES["
                    ^ string_of_int (offset + i)
                    ^ "];\n"
                  in
                  (acc ^ newline, i + 1))
                ("", 0) lsizes
            in
            str
          in
          let num_left_dim = List.length lname_left in
          let num_red_dim = List.length lname_red in
          let str_adapt_code_param_left =
            get_parameters_adaptation_code 0 lname_left left_sizes
          in
          let str_adapt_code_param_red =
            get_parameters_adaptation_code num_left_dim lname_red red_sizes
          in
          let str_adapt_code_param_right =
            get_parameters_adaptation_code
              (num_left_dim + num_red_dim)
              lname_right right_sizes
          in
          let str_adapt_params =
            str_adapt_code_param_left ^ str_adapt_code_param_red
            ^ str_adapt_code_param_right
          in

          (* For the rest of the generated code, we need to call Ttile to get the main loops *)
          (* First, we build the Tc module, with the correct layout *)
          let module Tc_l =
            (val match (tc_lay_left, tc_lay_right, tc_lay_out) with
                 | Tc_default_lay, Tc_default_lay, Tc_default_lay ->
                     get_tc_default_layout ()
                     (* Note Gui: did not find a way to give back directly Tc_default_layout,
                         without OCaml confusing it with a constructor *)
                 | _ ->
                     (* We impose that if the layout of one is specified, all layout must be specified *)
                     (* => No mix of default layout and general layout *)
                     let left_perm = extract_permutation tc_lay_left in
                     let right_perm = extract_permutation tc_lay_right in
                     let out_perm = extract_permutation tc_lay_out in
                     build_tc_module_layout left_perm right_perm out_perm)
          in
          let module Ttile = To_ttile (Tc_l) in
          (* Main code (from Ttile) *)
          let dim_sizes = build_dim_sizes left_sizes right_sizes red_sizes in
          let str_maincode = Tc_l.gen_code ~dim_sizes (Ttile.to_scheme t) in

          (* From TC(Tc_dim_args) *)

          (* Append the results *)
          str_adapt_params ^ str_maincode

  (* Generation of the cache upkeep content of the "cache_upkeep.h" file *)
  let gen_cache_upkeep :
      type a.
      a Config.layout ->
      a Config.args ->
      a Config.shape ->
      a cache_upkeep ->
      string =
   fun layout arg shape cache_up ->
    let str_body =
      match cache_up with
      | Hot_cache -> "" (* Nothing done *)
      | Cold_cache -> "flush_cache();\n" (* Flush the cache *)
      | Scheme_cache scheme_cache -> gen_code layout arg scheme_cache shape
    in
    str_body
end

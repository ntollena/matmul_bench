module TL = Tensor_loops
open Common

let dim_gen = TL.Ids.Dim.fresh_gen
let map_fst f (x, y) = f x, y
let app_fst p = map_fst (fun f -> f ()) p

module MM_dims = struct
  let i_dim, reset_i = dim_gen "i" |> app_fst
  let j_dim, reset_j = dim_gen "j" |> app_fst
  let k_dim, reset_k = dim_gen "k" |> app_fst
  let reset () = reset_i (); reset_j (); reset_k ()
end


(* This is mostly modules and types witchery, but the purpose is to export
 * a definition of dim and tensor without having to specify Arch - or
 * tensor - yet *)
type dim = I | J | K [@@deriving eq, show {with_path = false}]
type tensor = A | B | C [@@deriving eq, show {with_path = false}]
let all_dims = [I; J; K]
let all_tensors= [C; A; B]

module MM_Types = struct
  type nonrec dim = dim
  let pp_dim = pp_dim
  let show_dim = show_dim
  type nonrec tensor = tensor
  let pp_tensor = pp_tensor
  let show_tensor = show_tensor
  let all_dims = all_dims
  let all_tensors = all_tensors
end

module Tile_T = Tile_type.F(MM_Types)
type tile = Tile_T.tile [@@deriving show]

(* here we are architecture dependant *)
module Gen(A: Arch.Arch_t) = struct

  open TL.Tensor_tile(A.M)
  include MM_build(struct
      include MM_dims

      let a = TL.Tensor.make ~name:"A" [|k_dim; i_dim |]
      let b = TL.Tensor.make ~name:"B" [|j_dim; k_dim |]
      let c = TL.Tensor.make ~name:"C" [|j_dim; i_dim |]
    end)

  let from_dim = function
    | I -> i_dim
    | J -> j_dim
    | K -> k_dim

  let from_tensor = function
    | A -> a
    | B -> b
    | C -> c

  include Tile_type.To_ttile(A)(MM_Types)
      (struct
        let from_dim = from_dim
        let from_tensor = from_tensor
      end)

  let dim_tile_size scheme dim =
    dim_tile_size (to_scheme scheme) (from_dim dim)

  module type MM_sign = sig
    include TL.Sign.MM_args_t
    val gen_code: ?dim_sizes:((TL.Ids.Dim.t * int) list) -> tile_scheme -> string
  end

  let build_dim_sizes i j k =
    [i_dim, i; j_dim, j; k_dim, k]
end

module Prc = Shexp_process
open Shexp_process.Infix
open Batteries
open Tc_sugar

let build_marker mark s =
  let reg = Str.regexp @@ ".*" ^ mark ^ ".*" in
  match Str.search_forward reg s 0 with exception _ -> false | _ -> true

type replacing = Before | During | After

exception MarkNotFound

let fold_replace begin_marker end_marker lines_to_insert
    (lines, replacing_state) line =
  let is_begin = build_marker begin_marker
  and is_end = build_marker end_marker in
  match replacing_state with
  | Before -> if is_begin line then (lines, During) else (line :: lines, Before)
  | During ->
      if is_end line then (List.rev_append lines_to_insert lines, After)
      else (lines, During)
  | After -> (line :: lines, After)

let fold_replace_shxp begin_marker end_marker lines_to_insert =
  let fold state line =
    Prc.return
    @@ fold_replace begin_marker end_marker lines_to_insert state line
  in
  Prc.fold_lines ~init:([], Before) ~f:fold >>= function
  | lines, After -> Prc.return @@ String.concat "\n" @@ List.rev lines
  | _, _ -> raise MarkNotFound

(** 
This has been written in the context of TENSOR CONTRACTION to remap memory layout of output tensor and another to avoid FALSE SHARING for parallel execution. 
   
   gen_packing_wrapper_code returns 2 codes to put in gen_tc.c file:
   - first one: to insert at the beginning of the function gen_tc
   - second one: to insert at the end of the function gen_tc


   *)
module WrapperChangingLayout (A : Common.Arch.Arch_t) : sig
  val gen_packing_wrapper_code :
    tc_sugar_args -> tc_sugar_args -> string * string
end = struct
  let get_tab i = String.join "" (List.init i (fun _ -> "\t"))

  let find_size dim dim_sizes =
    List.find_map (function d, s when d = dim -> Some s | _ -> None) dim_sizes

  let access_expr tens param =
    let dim_sizes = List.rev (dim_and_sizes_from_tens tens param) in
    let sizes = List.map snd dim_sizes in
    let exprs =
      List.mapi
        (fun i (index, _) ->
          let sizes_inner, _ = List.split_at i sizes in
          let size_inner = List.fold (fun a b -> a * b) 1 sizes_inner in
          if size_inner = 1 then index
          else Printf.sprintf "%s * %d" index size_inner)
        dim_sizes
    in
    String.join " + " exprs

  (** `gen_unpack_loop Tc.Out false` will gen the packing loop copying packed_output to output array*)
  let gen_unpack_loop new_param old_param out_str =
    let old_inner_dim = List.last old_param.layout_out in
    let new_inner_dim = List.last new_param.layout_out in
    (* be carefuls with names of old_inner_dim and others, should be different *)
    let new_outter_dim = List.first new_param.layout_out in
    let _ =
      if old_inner_dim = new_inner_dim || old_inner_dim = new_outter_dim then
        let error_msg =
          Printf.sprintf
            "old dimension %s should have different names than splitted \
             dimensions %s, %s"
            old_inner_dim new_inner_dim new_outter_dim
        in
        failwith error_msg
      else if new_inner_dim = new_outter_dim then
        let error_msg =
          Printf.sprintf
            "splitted dimensions %s, %s should have different names"
            new_inner_dim new_outter_dim
        in
        failwith error_msg
    in
    let dims_to_loop = dim_and_sizes_from_tens Tc.Out new_param in

    (* OPEN LOOPS NEST *)
    let open_loop =
      List.mapi
        (fun i (dim, size) ->
          let indent = get_tab i in
          let incr =
            if dim = new_inner_dim then Printf.sprintf "%s += %d" dim A.vec_size
            else Printf.sprintf "%s++" dim
          in
          Printf.sprintf "%sfor (int %s = 0; %s < %d; %s) {" indent dim dim size
            incr)
        dims_to_loop
    in

    (* CONTENT OF THE NESTED LOOPS *)
    let avx = A.vec_size * 4 * 8 in
    (* 4 is size of float in bytes times 8 to convert in bits *)
    let dim_sizes = dim_sizes_alphabetical new_param in
    let indent = get_tab (List.length dims_to_loop) in
    let size_split_dim_in = find_size new_inner_dim dim_sizes in

    let src, dst = (out_str, "output") in
    let access_src, access_dst =
      (access_expr Tc.Out new_param, access_expr Tc.Out old_param)
    in

    let content_loop =
      [
        Printf.sprintf "%sint %s = %s + %s * %d;" indent old_inner_dim
          new_inner_dim new_outter_dim size_split_dim_in;
        Printf.sprintf "%s__m%d tmp_vec= _mm%d_loadu_ps(&%s[%s]);" indent avx
          avx src access_src;
        Printf.sprintf "%s_mm%d_storeu_ps(&%s[%s], tmp_vec);" indent avx dst
          access_dst;
      ]
    in

    (* CLOSE LOOPS NEST*)
    let n = List.length dims_to_loop in
    let close_loop =
      List.mapi
        (fun i _ -> Printf.sprintf "%s}" (get_tab (n - 1 - i)))
        dims_to_loop
    in
    open_loop @ content_loop @ close_loop

  (** `gen_packing_wrapper_code new_param old_param` will generate code to copy/recopy old_param tens to new layouts tens in new_param and call computation*)
  let gen_packing_wrapper_code new_param old_param =
    let out_str = "packed_output" in
    let size_out = size_of_tens Tc.Out old_param in
    let logical_order =
      new_param.dim_sizes_left @ new_param.dim_sizes_red
      @ new_param.dim_sizes_right
    in
    let logical_order =
      List.map (fun (_, size) -> string_of_int size) logical_order
    in
    let logical_order = String.join ", " logical_order in

    let n_left, n_red, n_right =
      ( List.length new_param.dim_sizes_left,
        List.length new_param.dim_sizes_red,
        List.length new_param.dim_sizes_right )
    in
    let before_code =
      [
        Printf.sprintf
          "M_TYPE *%s = (M_TYPE *)ALLOC(1024, sizeof(M_TYPE) * %d);" out_str
          size_out;
        "";
        "// 1) fill packed_output with zeros from output";
        Printf.sprintf "memcpy(packed_output, output, sizeof(M_TYPE) * %d);"
          size_out;
        "";
        "// 3) Compute using packed arrays instead of those  received in args";
        Printf.sprintf "IND_TYPE packed_SIZES[] = {%s};" logical_order;
        "//  hack to pass packed_SIZES instead of SIZES to compute code";
        "void compute( M_TYPE const * const __restrict__ leftIn, M_TYPE const \
         * const __restrict__ rightIn,\n\
        \            M_TYPE * const  __restrict__ output,\n\
        \            IND_TYPE const * const __restrict__ SIZES,\n\
        \            IND_TYPE nleft, IND_TYPE nred, IND_TYPE nright) {";
      ]
    in

    let after_code =
      [
        "}";
        Printf.sprintf "compute(leftIn, rightIn, %s, packed_SIZES, %d, %d, %d);"
          out_str n_left n_red n_right;
        "";
        Printf.sprintf "// 4) Unpack %s to output with old layout: %s" out_str
          ([%show: string list] old_param.layout_out);
        "#pragma omp parallel for";
      ]
      @ gen_unpack_loop new_param old_param out_str
      @ [ Printf.sprintf "free(%s);" out_str ]
    in
    (String.join "\n" before_code, String.join "\n" after_code)
end

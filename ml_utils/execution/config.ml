module TL = Tensor_loops
open Batteries
open Bench_types

type error = EXACT_EPS of float | EXACT | EPSILON of float | NO_CHECK

type _ bench =
  | Blis : mm bench
  | Mkl : mm bench
  | XSMM : mm bench
  | Gen : mm bench
  | Conv : conv bench
  | Rui : conv bench
  | OneDNN : conv bench
  | TcGen : tc bench
(*             | TCCG : tc bench *)

type conv_layout_in = XYC | YXC | XCY | CYX
[@@deriving show { with_path = false }]

type conv_layout_par = HWCF | WHCF | FCHW
[@@deriving show { with_path = false }]

type conv_layout_out = YXF | XYF | FYX [@@deriving show { with_path = false }]

type tc_layout =
  | Tc_lay_dims of int list
  | Tc_lay_dims_str of (string list * string list * string list)
    (* permutations but with letters instead of numbers *)
  | Tc_default_lay

type _ layout =
  | MM_layout : mm layout
  | Conv_layout :
      conv_layout_out * conv_layout_in * conv_layout_par
      -> conv layout
  | TC_layout :
      tc_layout * tc_layout * tc_layout
      -> tc layout (* Order: Left, Right, Out *)

let default_layout : type a. a bench -> a layout = function
  | Gen -> MM_layout
  | Mkl -> MM_layout
  | XSMM -> MM_layout
  | Blis -> MM_layout
  | Conv -> Conv_layout (XYF, XYC, WHCF)
  | Rui -> Conv_layout (XYF, YXC, WHCF)
  | OneDNN -> Conv_layout (XYF, CYX, WHCF)
  | TcGen -> TC_layout (Tc_default_lay, Tc_default_lay, Tc_default_lay)

type _ shape =
  | MM_shape : mm shape
  | Conv_shape : conv shape
  | TC_shape : (string list * string list * string list) -> tc shape

let show_bench (type a) (b : a bench) =
  match b with
  | Blis -> "Blis"
  | Mkl -> "Mkl"
  | XSMM -> "XSMM"
  | Gen -> "Gen"
  | Conv -> "Conv"
  | Rui -> "Rui"
  | OneDNN -> "OneDNN"
  | TcGen -> "TcGen"
(*  | TCCG -> "TCCG" *)

(* Arbitrary comparison *)
let compare_bench (type a) (b1 : a bench) (b2 : a bench) =
  match (b1, b2) with
  | Blis, Blis | Mkl, Mkl | XSMM, XSMM | Gen, Gen -> 0
  | Blis, _ -> -1
  | _, Blis -> 1
  | Mkl, (XSMM | Gen) -> -1
  | (Gen | XSMM), Mkl -> 1
  | XSMM, Gen -> -1
  | Gen, XSMM -> 1
  | Conv, Conv | Rui, Rui | OneDNN, OneDNN -> 0
  | Conv, _ -> -1
  | _, Conv -> 1
  | Rui, OneDNN -> -1
  | OneDNN, Rui -> 1
  | TcGen, TcGen -> 0
(* | TCCG, TCCG -> 0
   | TCCG, TcGen -> -1
   | TcGen, TCCG -> 1 *)

type _ args =
  | MM_args : (int * int * int) -> mm args
  (*| Conv_args: (int * int * int * int * int * int * (int * int) option) -> conv args*)
  | Conv_args : {
      x : int;
      w : int;
      y : int;
      h : int;
      c : int;
      f : int;
      englobing_input : (int * int * int * int * int * int) option;
      englobing_params : (int * int * int * int * int * int) option;
      englobing_output : (int * int * int * int * int * int) option;
      strides : (int * int) option;
    }
      -> conv args
  | TC_args : {
      sizes_left : int list;
      sizes_red : int list;
      sizes_right : int list;
    }
      -> tc args

let mm_args i j k = MM_args (i, j, k)

let conv_args ?englobing_output ?englobing_input ?englobing_params x w y h c f =
  Conv_args
    {
      x;
      w;
      y;
      h;
      c;
      f;
      englobing_input;
      englobing_output;
      englobing_params;
      strides = None;
    }

let conv_args_stride ?englobing_output ?englobing_input ?englobing_params x w y
    h c f str_x str_y =
  Conv_args
    {
      x;
      w;
      y;
      h;
      c;
      f;
      englobing_input;
      englobing_output;
      englobing_params;
      strides = Some (str_x, str_y);
    }

let tc_args lsizes_left lsizes_red lsizes_right =
  TC_args
    {
      sizes_left = lsizes_left;
      sizes_red = lsizes_red;
      sizes_right = lsizes_right;
    }

type 'a conf = {
  generation : (bool * bool * compiler * opt_level) option;
  blis_lib : string;
  error : error;
  benches : ('a bench * 'a layout) list;
  num_rep : int;
  counters : Counter.t list;
}

type 'a t =
  | MM_conf : mm conf -> mm t
  | Conv_conf : conv conf -> conv t
  | TC_conf : tc conf -> tc t

let is_mkl : type a. a bench -> bool = function Mkl -> true | _ -> false
let is_blis : type a. a bench -> bool = function Blis -> true | _ -> false

(*
let is_tccg: type a. a bench -> bool = function
    TCCG -> true
  | _ -> false
*)

let is_gen : type a. a bench -> bool = function Gen -> true | _ -> false
let is_conv : type a. a bench -> bool = function Conv -> true | _ -> false

(* let is_tc: type a. a bench -> bool = function
   | TC -> true
   | _ -> false *)

let cmp_by cmp f a1 a2 = cmp (f a1) (f a2)

let build_config_with_layout ?(num_rep = 500) ?(error = NO_CHECK)
    ?(blis_lib = "../blis") generation benches_layouts counters =
  assert (
    (match generation with Some (_, _, Icc, _) | None -> true | _ -> false)
    || not (List.exists (is_mkl % fst) benches_layouts));
  (* assert (List.length counters < 5); *)
  {
    generation;
    num_rep;
    blis_lib;
    benches = benches_layouts;
    error;
    counters = List.sort Counter.compare counters;
  }

let build_config ?(num_rep = 500) ?(error = NO_CHECK) ?(blis_lib = "../blis")
    generation benches counters =
  let benches =
    List.map
      (fun x -> (x, default_layout x))
      benches (* Note: default layout here *)
    |> List.sort (cmp_by compare_bench fst)
  in
  build_config_with_layout ~num_rep ~error ~blis_lib generation benches counters

let build_mm_config ?(num_rep = 500) ?(error = NO_CHECK) ?(blis_lib = "../blis")
    generation benches counters =
  MM_conf (build_config ~num_rep ~error ~blis_lib generation benches counters)

let build_conv_config ?(num_rep = 500) ?(error = NO_CHECK)
    ?(blis_lib = "../blis") generation benches counters =
  Conv_conf (build_config ~num_rep ~error ~blis_lib generation benches counters)

let build_conv_config_with_layout ?(num_rep = 500) ?(error = NO_CHECK)
    ?(blis_lib = "../blis") generation benches counters =
  Conv_conf
    (build_config_with_layout ~num_rep ~error ~blis_lib generation benches
       counters)

let build_tc_config ?(num_rep = 500) ?(error = NO_CHECK) ?(blis_lib = "../blis")
    generation benches counters =
  TC_conf (build_config ~num_rep ~error ~blis_lib generation benches counters)

let build_tc_config_with_layout ?(num_rep = 500) ?(error = NO_CHECK)
    ?(blis_lib = "../blis") generation benches_w_layout counters =
  TC_conf
    (build_config_with_layout ~num_rep ~error ~blis_lib generation
       benches_w_layout counters)

let to_conf : type a. a t -> a conf = function
  | MM_conf conf -> conf
  | Conv_conf conf -> conf
  | TC_conf conf -> conf

module Results (T : sig
  type bench
end) =
struct
  include Map.Make (struct
    type t = T.bench bench * Counter.t

    let compare (b1, c1) (b2, c2) =
      match compare_bench b1 b2 with 0 -> Counter.compare c1 c2 | r -> r
  end)
end

type 'a results = (('a bench * Counter.t) * int) list

let dummy_from_bench_and_counters :
    type a. a bench -> Counter.t list -> a results =
 fun bench counters -> List.map (fun counter -> ((bench, counter), 42)) counters

let dummy_from_conf : type a. a t -> a results = function
  | TC_conf { benches; counters; _ } ->
      List.concat_map (fun b -> dummy_from_bench_and_counters b counters)
      @@ List.map fst benches
  | MM_conf { benches; counters; _ } ->
      List.concat_map (fun b -> dummy_from_bench_and_counters b counters)
      @@ List.map fst benches
  | Conv_conf { benches; counters; _ } ->
      List.concat_map (fun b -> dummy_from_bench_and_counters b counters)
      @@ List.map fst benches

let build_parser s =
  let parse_long = String.strip %> int_of_string in
  let match_empty = Re.(rep space |> whole_string |> compile |> execp) in
  String.split_on_char '\n' s
  (* TODO: Why do I even need to do this ? *)
  |> List.filter (Bool.not % match_empty)
  |> List.map @@ parse_long

let results (type t) config bench_str_output =
  let module Results = Results (struct
    type bench = t
  end) in
  let { benches; counters; _ } = to_conf config in
  let bind l f = List.concat (List.map f l) in
  let bench_count =
    bind benches (fun (b, _) -> List.map (fun c -> (b, c)) counters)
  in
  let values = build_parser bench_str_output in
  List.fold_left2
    (fun results (b, c) value -> Results.add (b, c) value results)
    Results.empty bench_count values
  |> Results.bindings

let select_bench bench res =
  List.filter_map
    (function (b, c), value when b = bench -> Some (c, value) | _ -> None)
    res

let show_results res =
  List.map
    (fun ((b, c), count) ->
      Printf.sprintf "%s, %s, %d" (show_bench b) (Counter.show c) count)
    res
  |> String.concat "\n"

let mkl_ctrs res = select_bench Mkl res
let blis_ctrs res = select_bench Blis res
let gen_ctrs res = select_bench Gen res

let select_bench_counter (type a) (bench : a bench) counter res =
  match List.assoc (bench, counter) res with
  | exception Not_found ->
      failwith (Printf.sprintf "Bench %s is missing" (show_bench bench))
  | res -> res

open Counter

let mkl_cycles res = select_bench_counter Mkl CYCLES res
let blis_cycles res = select_bench_counter Blis CYCLES res
let gen_cycles res = select_bench_counter Gen CYCLES res
let conv_cycles res = select_bench_counter Conv CYCLES res
let rui_cycles res = select_bench_counter Rui CYCLES res
(* let tccg_cycles res = select_bench_counter TCCG CYCLES res *)

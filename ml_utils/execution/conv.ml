module TL = Tensor_loops
open Config
open Common
open Batteries
open TL.Tensor

let dim_gen = TL.Ids.Dim.fresh_gen
let map_fst f (x, y) = f x, y
let app_fst p = map_fst (fun f -> f ()) p

(* Declared as a module and then immediatly included because we need to pass
 * that as a parameter later *)
module Conv_dims = struct
  let x_dim, reset_x = dim_gen "x" |> app_fst
  let w_dim, reset_w = dim_gen "w" |> app_fst
  let y_dim, reset_y = dim_gen "y" |> app_fst
  let h_dim, reset_h = dim_gen "h" |> app_fst
  let c_dim, reset_c = dim_gen "c" |> app_fst
  let f_dim, reset_f = dim_gen "f" |> app_fst

  let reset () = reset_x (); reset_y (); reset_c (); reset_f (); reset_w (); reset_h () 
end

include Conv_dims

type dim = F | C | X | Y | W | H [@@deriving eq, show {with_path = false}]

let from_dim = function
  | F -> f_dim
  | C -> c_dim
  | Y -> y_dim
  | X -> x_dim
  | W -> w_dim
  | H -> h_dim


type tensor = Input | Output | Params [@@deriving show]

let all_tensors = [Output; Input; Params]
let all_dims = [F; C; X; Y; W; H]

module Dim_tens_t = struct
  type nonrec dim = dim
  let pp_dim = pp_dim
  let show_dim = show_dim
  type nonrec tensor = tensor
  let pp_tensor = pp_tensor
  let show_tensor = show_tensor
  let all_dims = all_dims
  let all_tensors = all_tensors
end

module ConvTypes = Tile_type.F(Dim_tens_t)
type tile = ConvTypes.tile [@@deriving show]
let tile_to_string = [%show: tile list]

let input_layout =
  let c = single c_dim
  and yh = join_dims y_dim h_dim
  and xw =join_dims x_dim w_dim in
  function XYC -> [| c; yh; xw|]
         | YXC -> [| c; xw; yh|]
         | XCY -> [| yh; c; xw|]
         | CYX -> [| xw; yh; c|]

let input_layout_stride strx stry =
  let c = single c_dim
  and yh = join_dims_stride y_dim h_dim stry
  and xw = join_dims_stride x_dim w_dim strx in
  function XYC -> [| c; yh; xw|]
         | YXC -> [| c; xw; yh|]
         | XCY -> [| yh; c; xw|]
         | CYX -> [| xw; yh; c|]

let input_strides_from_layout stry strx (_,c,y,x,h,w)  =
  let c_s = single c_dim
  and yh = join_dims_stride y_dim h_dim stry
  and xw = join_dims_stride x_dim w_dim strx in
  function
    XYC -> [c_s,  1; yh, c; xw, c * (stry * y + h - 1)]
  | YXC -> [c_s, 1; xw, c; yh, c * (strx * x + w - 1)]
  | XCY -> [yh, 1; c_s, (stry * y + h - 1); xw, c * (stry * y + h - 1)]
  | CYX -> [xw, 1; yh, (strx * x + w - 1); c_s, (strx * x + w - 1) * (stry * y + h - 1)]

let params_layout = function
  | WHCF -> [|f_dim; c_dim; h_dim; w_dim;|]
  | HWCF -> [|f_dim; c_dim; w_dim; h_dim;|]
  | FCHW -> [|w_dim; h_dim; c_dim; f_dim;|]

let params_strides_from_layout (f,c,_,_,h,w)  = function
  | WHCF -> [f_dim, 1; c_dim, f; h_dim, c * f; w_dim, h * c * f;]
  | HWCF -> [f_dim, 1; c_dim, f; w_dim, c * f; h_dim, w * c * f;]
  | FCHW -> [w_dim, 1; h_dim, w; c_dim, w * h; f_dim, w * h * c;]

let output_layout = function
    YXF -> [|f_dim; x_dim; y_dim; |]
  | XYF -> [|f_dim; y_dim; x_dim; |]
  | FYX -> [|x_dim; y_dim; f_dim; |]

let output_strides_from_layout (f,_,y,x,_,_) = function
    YXF -> [f_dim, 1; x_dim, f; y_dim, f * x; ]
  | XYF -> [f_dim, 1; y_dim, f; x_dim, f * y; ]
  | FYX -> [x_dim, 1; y_dim, x; f_dim, x * y; ]


module Gen(A:Arch.Arch_t) = struct
  open TL
  open Tensor_tile(A.M)
  module type Conv_sign = sig
    include Sign.Conv_args_t
    val gen_code: ?dim_sizes:((Ids.Dim.t * int) list) -> tile_scheme -> string
  end

  module Conv_default_layout: Conv_sign = Conv_build(struct
      include Conv_dims
      let input = Tensor.make_join ~name:"input" @@ input_layout YXC
      and params = Tensor.make ~name:"params" @@ params_layout HWCF
      and output = Tensor.make ~name:"output" @@ output_layout YXF
    end)

  let build_conv_module out in_ params: (module Conv_sign) = 
    let input = Tensor.make_join ~name:"input" @@ input_layout in_
    and params = Tensor.make ~name:"params" @@ params_layout params
    and output = Tensor.make ~name:"output" @@ output_layout out in
    (module Conv_build( struct
         include Conv_dims
         let input = input let params = params let output = output
       end))


  let build_conv_module_stride out in_ params strx stry: (module Conv_sign) = 
    let input = Tensor.make_join ~name:"input" @@ input_layout_stride strx stry in_
    and params = Tensor.make ~name:"params" @@ params_layout params
    and output = Tensor.make ~name:"output" @@ output_layout out in
    (module Conv_build( struct
         include Conv_dims
         let input = input let params = params let output = output
       end))

  let build_conv_module_explicit_strides
      output_sizes input_sizes param_sizes out in_ params strx stry: (module Conv_sign) = 
    let open Exprs in
    let strides_opt f layout sizes = 
      sizes
      |> Option.map @@ fun sizes ->
      f sizes  layout
      |> List.map (Utils.map_snd (fun c -> Expr.const c)) in
    let dims_in_strides = strides_opt (input_strides_from_layout strx stry) in_ input_sizes
    and dims_par_strides = strides_opt params_strides_from_layout params param_sizes
    and dims_out_strides = strides_opt output_strides_from_layout out output_sizes in
    let from_stride_opt name layout =
      Option.map_default
        (fun strides -> Tensor.make ~strides ~name layout) (Tensor.make ~name layout) in
    let input = Option.map_default
        (fun strides ->
           Tensor.make_join ~strides:strides  ~name:"input"
           @@ input_layout_stride strx stry in_)
        (Tensor.make_join ~name:"input" @@ input_layout_stride strx stry in_)
        dims_in_strides
    and params = from_stride_opt "params" (params_layout params) dims_par_strides
    and output = from_stride_opt "output" (output_layout out) dims_out_strides in
    (module Conv_build( struct
         include Conv_dims
         let input = input let params = params let output = output
       end))


  module To_ttile(C: Conv_sign): sig
    type nonrec tile = ConvTypes.tile
  [@@deriving show {with_path = false}]
    val to_ttile : tile -> loop_type
    val to_scheme : tile list -> loop_type list
    val dim_tile_size: tile list -> Dim_tens_t.dim -> int
  end = struct
    let from_tensor =  function
        Input -> C.input
      | Output -> C.output 
      | Params -> C.params 

    include Tile_type.To_ttile(A)(Dim_tens_t)
        (struct
          let from_dim = from_dim
          let from_tensor = from_tensor
        end)

    let dim_tile_size scheme dim =
      dim_tile_size (to_scheme scheme) (from_dim dim)
  end

  module Default_tile = To_ttile(Conv_default_layout)

  (* Workaround : we don't really need any information on tensors 
   * for getting tile sizes, but we do need to translate the tile into
   * loop_tile list, so we use the default tensor definition as a placeholder *)
  let dim_tile_size = Default_tile.dim_tile_size

  let build_dim_sizes x w y h c f =
    [ x_dim, x; w_dim, w; y_dim, y; h_dim, h;  c_dim, c; f_dim, f]
end

from distutils.core import setup

NAME = "ttilepy"

setup(
    name=NAME,
    version="0.0.1",
    author="Nicolas Tollenaere",
    author_email="nicolas.tollenaere@inria.fr",
    description="Search and code generation of optimized convolutions",
    packages=[NAME],
    package_data={NAME: ['python_frontend.so']})

open Batteries
open Common
open Execution
open Bench_types

(* Microkernel exploration (ICS 2021 experiments) *)
(* Authors: Guillaume & Hugo *)

(* This module is parametrized over Architecture. It contains a whole bunch of
 * experiments. This is not stable and is not intended to be. Any function
 * considered worth being stabilized should be moved to another file - maybe
 * data_process. *)
module Expe (A : Arch.Arch_t) = struct
  open Execution
  include Bench (A)
  include Config
  module Pred_mm = Arch.Pred_mm (A)
  module Pred_conv = Arch.Pred_conv (A)
  module Pred_tc = Arch.Pred_tc (A)

  (*
  let sizes_from_tile_mm tile = let open MM in
    let map3 f x y z = f x, f y, f z in
    map3 (dim_tile_size tile) I J K

  let sizes_from_tile_conv tile = let open Conv in
    let map6 f x y z a b c = f x, f y, f z, f a, f b, f c in
    map6 (dim_tile_size tile)  X W Y H C F
*)
end

(* ========================================== *)

(* First experiments with microkernel measurement. Try different sizes of microkernel,
    surronded by a loop on a reuse dimension (to repeat them), and run them to see their perf *)
(* This is an ideal case where the data of a microkernel forms a matrix: this is like having
    a packing before doing the microkernel *)

(* Auxilliary function: generate all (x,y) such that rmin <= x,y < rmax
        and  pmin <= x * y <= pmax *)
let generate_configs (rmin, rmax) (pmin, pmax) =
  (* Sorry but this code is much much clearer in sequential *)
  let lconfig = ref [] in
  for x = rmin to rmax do
    for y = rmin to rmax do
      if pmin <= x * y && x * y <= pmax then lconfig := (x, y) :: !lconfig
    done
  done;
  !lconfig

let generate_configs3 (rmin, rmax) (pmin, pmax) =
  (* Sorry but this code is much much clearer in sequential *)
  let lconfig = ref [] in
  for x = rmin to rmax do
    for y = rmin to rmax do
      for z = rmin to rmax do
        if pmin <= x * y * z && x * y * z <= pmax then
          lconfig := (x, y, z) :: !lconfig
      done
    done
  done;
  !lconfig

(* Try one µkernel for tc =  abc adec edb *)
let test_microkernels_tc (module A : Arch.Arch_t) size_a size_b size_c size_d
    size_e =
  let open Expe (A) in
  let open Tc in
  let strat =
    List.concat
      TcTypes.
        [
          [ V (LeftDim 1) ];
          [
            U (size_c, LeftDim 1); U (size_b, RightDim 0); U (size_a, LeftDim 0);
          ];
          [
            T (size_e, RedDim 1);
            T (size_d, RedDim 0);
            Hoist_vars [ RedDim 1; RedDim 0 ];
          ];
        ]
  in
  let lsizes_left = [ size_a; size_c * A.vec_size ] in
  let lsizes_red = [ size_d; size_e ] in
  let lsizes_right = [ size_b ] in
  let ldim_name_left = [ "a"; "c" ] in
  let ldim_name_red = [ "d"; "e" ] in
  let ldim_name_right = [ "b" ] in
  let shape = (ldim_name_left, ldim_name_red, ldim_name_right) in
  (* C = A.B *)
  let l_layout_C = Config.Tc_lay_dims [ 0; 2; 1 ] in
  let l_layout_A = Config.Tc_lay_dims [ 0; 3; 2; 1 ] in
  let l_layout_B = Config.Tc_lay_dims [ 0; 2; 1 ] in
  let layout = Config.TC_layout (l_layout_A, l_layout_B, l_layout_C) in
  let args = Config.tc_args lsizes_left lsizes_red lsizes_right in
  let conf =
    Config.build_tc_config_with_layout ~error:(Config.EPSILON 0.01) ~num_rep:50
      (Some (true, true, Gcc, O2))
      [ (Config.TcGen, layout) ]
      Counter.[ CYCLES ]
  in
  let res = exec conf args (TC_tile strat) (TC_shape shape) in
  let res = Config.select_bench_counter Config.TcGen Counter.CYCLES res in
  let gen_score =
    Pred_tc.peak_percent lsizes_left lsizes_right lsizes_red res
  in

  (gen_score, size_a, size_b, size_c, size_d, size_e)

(* Try all µkernels for tc =  abc adec edb *)
let test_all_microkernels_tc fun_test size_e size_d =
  (* range_min <= X,Y <= range_max *)
  let range_min = 1 in
  let range_max = 15 in

  (* prod_min <= X*Y <= prod_max *)
  let prod_min = 4 in
  let prod_max = 30 in

  let config_abc =
    generate_configs3 (range_min, range_max) (prod_min, prod_max)
  in
  let config_abc = List.rev config_abc in

  (* More confort in reading the results *)
  let lres_sizes =
    List.map (fun (a, b, c) -> fun_test a b c size_d size_e) config_abc
  in

  let comp_sorting (res1, _, _, _, _, _) (res2, _, _, _, _, _) =
    if res1 == res2 then 0 else if res1 < res2 then -1 else 1
  in
  List.sort comp_sorting lres_sizes
  |> List.map (fun (res, a, b, c, d, e) -> (a, b, c, d, e, res))

(* Printing result out as a csv file *)
let print_csv_tc l =
  Printf.printf "a,b,c,d,e,res\n";
  List.iter
    (fun (a, b, c, d, e, res) ->
      Printf.printf "%d,%d,%d,%d,%d,%.2f\n" a b c d e res)
    l

(* Try all µkernels for matmult *)
let test_microkernels_mm (module A : Arch.Arch_t) size_i size_j size_k =
  let open Expe (A) in
  let open MM in
  let size_j_ = size_j * A.vec_size in
  let unroll = false in
  let strat =
    List.concat
      Tile_T.
        [
          [ V J; U (size_j, J); U (size_i, I) ];
          (if unroll then [ U (size_k, K); T (1, K) ] else [ T (size_k, K) ]);
          [ Hoist_vars [ K ] ];
        ]
  in
  let args = mm_args size_i size_j_ size_k in
  let conf =
    build_mm_config ~error:(EPSILON 0.01) ~num_rep:50
      (Some (true, true, Gcc, O3))
      [ Gen ]
      Counter.[ CYCLES ]
  in
  let gen_score =
    exec conf args (MM_tile strat) MM_shape
    |> Config.gen_cycles
    |> Pred_mm.peak_percent size_i size_j_ size_k
  in
  (gen_score, size_i, size_j, size_k)

(* Try all µkernels for convolution *)
let test_all_microkernels_mm fun_test k =
  (* range_min <= X,Y <= range_max *)
  let range_min = 1 in
  let range_max = 15 in

  (* prod_min <= X*Y <= prod_max *)
  let prod_min = 1 (* 4 *) in
  let prod_max = 30 in

  let config_i_j =
    generate_configs (range_min, range_max) (prod_min, prod_max)
  in
  let config_i_j = List.rev config_i_j in

  (* More confort in reading the results *)
  let lres_sizes = List.map (fun (i, j) -> fun_test i j k) config_i_j in

  let comp_sorting (res1, _, _, _) (res2, _, _, _) =
    if res1 == res2 then 0 else if res1 < res2 then -1 else 1
  in
  List.sort comp_sorting lres_sizes
  |> List.map (fun (res, i, j, k) -> (i, j, k, res))

(* Printing result out as a csv file *)
let print_csv_mm l =
  Printf.printf "i,j,k,res\n";
  List.iter (fun (i, j, k, res) -> Printf.printf "%d,%d,%d,%.2f\n" i j k res) l

(* ============== *)

(* Microkernel for convolution *)
let test_microkernels_conv (module A : Arch.Arch_t) size_h size_w size_f size_y
    =
  let open Expe (A) in
  let open Conv in
  (* let open Gen in *)
  let size_f_ = size_f * A.vec_size in
  let size_c = 512 in

  (* 256 128 *)
  (* Streaming dimension*)
  let size_x = 1 in

  let unroll = false in
  let strat =
    List.concat
      ConvTypes.
        [
          [ V F; U (size_f, F); U (size_y, Y); U (size_h, H); U (size_w, W) ];
          (if unroll then [ U (size_c, C); T (1, C) ] else [ T (size_c, C) ]);
          [ Hoist_vars [ C ] ];
        ]
  in
  (* b=1 *)
  let args = conv_args size_x size_w size_y size_h size_c size_f_ in

  (* x w y h c f *)

  (* Build the scheme *)
  let conf_gen =
    build_conv_config_with_layout ~error:(EPSILON 0.01) ~num_rep:20
      (Some (true, true, Gcc, O3))
      [ (Conv, Conv_layout (XYF, XYC, WHCF)) ]
      [ CYCLES ]
  in

  (*let args = conv_args_stride x w y h c f sx sy in *)
  let gen_score =
    exec conf_gen args (Conv_tile strat) Conv_shape
    |> select_bench_counter Conv CYCLES
    |> Pred_conv.peak_percent size_x size_y size_w size_h size_c size_f_
  in
  (gen_score, size_f, size_y, size_h, size_w)

(* Try all µkernels for convolution *)
let test_all_microkernels_conv fun_test =
  (* range_min <= X,Y <= range_max *)
  let range_min = 1 in
  let range_max = 15 in

  (* prod_min <= X*Y <= prod_max *)
  let prod_min = 1 (* 4 *) in
  let prod_max = 30 in

  let config_x_y =
    generate_configs (range_min, range_max) (prod_min, prod_max)
  in
  let config_x_y = List.rev config_x_y in

  (* More confort in reading the results *)
  let lres_sizes =
    List.concat_map
      (fun (h, w) -> List.map (fun (f, y) -> fun_test h w f y) config_x_y)
      [ (1, 1); (3, 1); (1, 3); (3, 3) ]
  in

  let comp_sorting (res1, _, _, _, _) (res2, _, _, _, _) =
    if res1 == res2 then 0 else if res1 < res2 then -1 else 1
  in
  List.sort comp_sorting lres_sizes
  |> List.map (fun (res, f, y, h, w) -> (1, y, w, h, 1, f, res))

(* Printing result out as a csv file *)
let print_csv_conv l =
  Printf.printf "x,y,w,h,c,f,res\n";
  List.iter
    (fun (x, y, w, h, c, f, res) ->
      Printf.printf "%d,%d,%d,%d,%d,%d,%.8f\n" x y w h c f res)
    l

(* ============== *)

(* Guillaume experiment - March 2022 - Larger class of µkernels *)

(* Microkernel for convolution *)
let test_microkernels_conv_extended (module A : Arch.Arch_t) size_x size_y
    size_w size_h size_c size_f =
  let open Expe (A) in
  let open Conv in
  (* let open Gen in *)
  let size_f_ = size_f * A.vec_size in
  let size_c_stream = 512 in
  let size_c_ = size_c * size_c_stream in

  (* Streaming dimension*)
  let strat =
    ConvTypes.
      [
        V F;
        U (size_f, F);
        U (size_x, X);
        U (size_y, Y);
        U (size_c, C);
        U (size_w, W);
        U (size_h, H);
        T (size_c_stream, C);
        Hoist_vars [ C ];
      ]
  in
  (* b=1 *)
  let args = conv_args size_x size_w size_y size_h size_c_ size_f_ in

  (* x w y h c f *)

  (* Build the scheme *)
  let conf_gen =
    build_conv_config_with_layout ~error:(EPSILON 0.01) ~num_rep:20
      (Some (true, true, Gcc, O3))
      [ (Conv, Conv_layout (XYF, XYC, WHCF)) ]
      [ CYCLES ]
  in

  (*let args = conv_args_stride x w y h c f sx sy in *)
  let gen_score =
    exec conf_gen args (Conv_tile strat) Conv_shape
    |> select_bench_counter Conv CYCLES
    |> Pred_conv.peak_percent size_x size_y size_w size_h size_c_ size_f_
  in
  (gen_score, size_f, size_c, size_h, size_w, size_x, size_y)

(* Try all µkernels for convolution *)
let test_all_microkernels_conv_extended fun_test =
  (* Constraints of the space *)

  (* 16 <= X*Y*F + H*W*C*F <= 36 (contains 31 which is theoretical max without spill) *)
  (* 14 <= X*Y*F <= 28 (priority on the first term) *)
  (* 1 <= X,Y,F,C <= 16  and  H,W in [1, 3, 5, 7] *)
  let range_main_min = 16 in
  let range_main_max = 36 in

  let range_out_min = 14 in
  let range_out_max = 28 in

  let range_min = 1 in
  let range_max = 16 in

  let lval_hw = [ 1; 3; 5; 7 ] in

  let ref_lconfig_xywhcf = ref [] in
  for f = range_min to range_max do
    for x = range_min to range_max do
      for y = range_min to range_max do
        for c = range_min to range_max do
          for hind = 0 to List.length lval_hw - 1 do
            for wind = 0 to List.length lval_hw - 1 do
              (* Skip if h!=w when both are strictly greater than 1 *)
              if hind > 0 && wind > 0 && hind != wind then ()
              else
                let h = List.nth lval_hw hind in
                let w = List.nth lval_hw wind in

                let nvecout = x * y * f in
                let nvec = nvecout + (h * w * c * f) in

                if
                  range_out_min <= nvecout && nvecout <= range_out_max
                  && range_main_min <= nvec && nvec <= range_main_max
                then
                  ref_lconfig_xywhcf :=
                    (x, y, w, h, c, f) :: !ref_lconfig_xywhcf
            done
          done
        done
      done
    done
  done;

  let lconfig_xywhcf = List.rev !ref_lconfig_xywhcf in
  (* More confort in reading the results *)
  let lres_sizes =
    List.map (fun (x, y, w, h, c, f) -> fun_test x y w h c f) lconfig_xywhcf
  in

  let comp_sorting (res1, _, _, _, _, _, _) (res2, _, _, _, _, _, _) =
    if res1 == res2 then 0 else if res1 < res2 then -1 else 1
  in

  List.sort comp_sorting lres_sizes
  |> List.map (fun (res, f, c, h, w, x, y) -> (x, y, w, h, c, f, res))

(* Main function - Printing microkernel in stdout *)
let print_csv_microkernels_conv_extended arch =
  test_all_microkernels_conv_extended (test_microkernels_conv_extended arch)
  |> print_csv_conv

(* ========================================== *)

(* Building class of the best candidate *)

open Search
open Conv_search_type

(* Given a csv file of microkernel results,
 * returns the specification of all kernel class built with
 * every kernel that goes above the given threshold
 * *)
let search_class_from_perf (module A : Arch.Arch_t) threshold perf_list =
  let open Arch_info in
  search_class A.vec_size threshold perf_list
  |> print_endline % [%show: kernel_spec list]

let search_class_from_csv arch threshold csvname =
  let perf_list = Parse_csv.parse_remove_header csvname in
  search_class_from_perf arch threshold @@ List.map Arch_info.to_perfs perf_list

(* Main function - Printing microkernel in stdout *)
let print_csv_microkernels_conv arch =
  test_all_microkernels_conv (test_microkernels_conv arch) |> print_csv_conv

(*  Searching microkernels and building classes on the fly *)
let build_class_from_scratch arch threshold =
  test_all_microkernels_conv (test_microkernels_conv arch)
  |> List.map Arch_info.to_perfs_from_ints
  |> search_class_from_perf arch threshold

(* Main function -  Printing microkernel in stdout *)
let print_csv_microkernels_mm arch k =
  test_all_microkernels_mm (test_microkernels_mm arch) k |> print_csv_mm

let print_csv_microkernels_tc arch d e =
  test_all_microkernels_tc (test_microkernels_tc arch) e d |> print_csv_tc

(* Extended search_class from csv *)
let search_class_from_csv_extended (module A : Arch.Arch_t) threshold csvname =
  let open Arch_info in
  let csv_info_list = Parse_csv.parse_remove_header csvname in
  let perf_list = List.map Arch_info.to_perfs csv_info_list in

  let l_ukern_class = search_class_extended A.vec_size threshold perf_list in
  l_ukern_class |> print_endline % [%show: kernel_spec list]

(* ========================================== *)

(* Microkernel experiments with strides on the layout *)

(* Microkernel for convolution *)
let test_microkernels_conv_with_no_contig_layout (module A : Arch.Arch_t)
    conv_spec (* Enbedded sizes *) size_h size_w size_f size_y =
  (* Microkernel size*)
  let open Expe (A) in
  let open Conv in
  (* let open Gen in *)
  let size_f_ = size_f * A.vec_size in
  let size_c = 512 in

  (* 256 128 *)
  (* Streaming dimension*)
  let size_x = 1 in

  let unroll = false in
  let strat =
    List.concat
      ConvTypes.
        [
          [ V F; U (size_f, F); U (size_y, Y); U (size_h, H); U (size_w, W) ];
          (if unroll then [ U (size_c, C); T (1, C) ] else [ T (size_c, C) ]);
          [ Hoist_vars [ C ] ];
        ]
  in

  (* b=1 *)

  (* Note: adapted size = number of iteration. Original size = size of the images,
      which are different if stride!=1 *)
  let englobing_problem_sizes = conv_spec.Conv_spec.adapted_size in

  let args =
    conv_args ~englobing_output:englobing_problem_sizes
      ~englobing_input:englobing_problem_sizes
      ~englobing_params:englobing_problem_sizes size_x size_w size_y size_h
      size_c size_f_
  in

  (* x w y h c f *)

  (* Build the scheme *)
  let conf_gen =
    build_conv_config_with_layout ~error:(EPSILON 0.01) ~num_rep:20
      (Some (true, true, Gcc, O3))
      [ (Conv, default_layout Conv) ]
      [ CYCLES ]
    (* default_layout = Conv_layout (XYF, XYC, WHCF) *)
  in

  (*let args = conv_args_stride x w y h c f sx sy in *)
  let gen_score =
    exec conf_gen args (Conv_tile strat) Conv_shape
    |> select_bench_counter Conv CYCLES
    |> Pred_conv.peak_percent size_x size_y size_w size_h size_c size_f_
  in
  (gen_score, size_f, size_y, size_h, size_w)

(* Testing function for different microkernel sizes *)
let test_all_microkernels_conv_with_no_contig_layout fun_test_ncontig =
  (* range_min <= X,Y <= range_max *)
  let range_min = 1 in
  let range_max = 15 in

  (* prod_min <= X*Y <= prod_max *)
  let prod_min = 1 (* 4 *) in
  let prod_max = 30 in

  let config_x_y =
    generate_configs (range_min, range_max) (prod_min, prod_max)
  in
  let config_x_y = List.rev config_x_y in

  (* More confort in reading the results *)

  (* DEBUG
     let config_x_y = [(2,6)] in *)

  (* Iterate over all configurations of microkernel *)
  let lres_sizes =
    List.concat_map
      (fun (h, w) ->
        List.map (fun (f, y) -> fun_test_ncontig h w f y) config_x_y)
      [ (1, 1); (3, 1); (1, 3); (3, 3) ]
  in

  (* Sort the results according to their performance *)
  let comp_sorting (res1, _, _, _, _) (res2, _, _, _, _) =
    if res1 == res2 then 0 else if res1 < res2 then -1 else 1
  in
  List.sort comp_sorting lres_sizes
  (* And print it out*)
  |> List.map (fun (res, f, y, h, w) -> (1, y, w, h, 1, f, res))

(* Main function *)
let print_csv_microkernels_conv_no_contig_layout arch conv_spec =
  test_all_microkernels_conv_with_no_contig_layout
    (test_microkernels_conv_with_no_contig_layout arch conv_spec)
  |> print_csv_conv

(* ========================================== *)
(* Run it! *)

(* Example testing all microkernels from Haswell *)
(* let () = print_csv_microkernels_conv (module Arch.Haswell) *)
(* let () = print_csv_microkernels_mm (module Arch.Haswell) 128 *)
(* let () = print_csv_microkernels_mm (module Arch.Haswell) 256 *)
(* let () = print_csv_microkernels_mm (module Arch.Haswell) 512 *)
(* let () = print_csv_microkernels_mm (module Arch.Haswell) 1024 *)
let () = print_csv_microkernels_tc (module Arch.Dracula) 72 72

(* Example building classes from Haswell with a threshold at 60% of peak perf*)
(* let flute () = build_class_from_scratch (module Arch.Dracula) 60. *)

(* Example testing all microkernels, but with the strides of mobilNet9 *)
(* let () = print_csv_microkernels_conv_no_contig_layout (module Arch.Haswell)
   Conv_sizes.mobilNet_9_spec *)

let () = print_csv_microkernels_conv_extended (module Arch.Haswell)

(*let () = search_class_from_csv_extended (module Arch.Pinocchio) 85.0 "best_microkernel_extended_search.csv"
*)

open Batteries
open Common
open Execution
open Bench_types

let conv_yolo_basic_perm output_name =
  let module A = Arch.Sky_lake in
  let module Tile = Codegen.Tile(A) in
  let open Tile.Conv in
  let open Config in
  let module Conv_l =
    (val build_conv_module XYF XYC HWCF) in
  let module Gen = Schemes.Gen(A)(Conv_l) in
  let module Pred = Arch.Pred_conv(A) in
  let open Streaming in
  let open Config in
  let file_write output_name =
    let init () = let f = File.open_out output_name in
      IO.write_line f "name, onednn, gen, best tile"; (f, 0)
    and push (f, n) (name, onednn, gen, best_tile) =
      Printf.fprintf f "%s, %f, %f\nBest tile : %s\n" name onednn gen best_tile;
      IO.flush f;
      (f, n + 1)
    and stop (f, _) = IO.close_out f in
    Sink.make ~init ~push ~stop () in
  let _print_advance _nb_of_test = () in
  let conf_gen = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:5
      (Some (true, true, Gcc,  O3))
      [Conv; ] [CYCLES] in
  let conf_dnn = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (false, true, Gcc,  O3))
      [OneDNN; ] [CYCLES] in
  let open Execution.Bench(A) in
  let open Gen in
  Stream.of_list Gen.(yolo_all_tuned_schemes)
  |> Stream.map (fun {name; size ={f;c;y;x;h;w}; original;  _} ->
      let args = conv_args x w y h c f in
      let tiles = Gen.gen_scheme (build_dim_sizes x w y h c f) original in
      let best_tile, gen_score = List.map (fun t -> t, exec conf_gen args (Tile.Conv_tile t)
                                         |> Config.select_bench_counter Conv CYCLES
                                         |> Pred.peak_percent x y w h c f) tiles
                      |> List.min_max ~cmp:(fun (_, f1) (_,f2) -> Float.compare
                                               f1 f2)
                      |> snd in
      let onednn_score = exec conf_dnn args (Tile.Conv_tile [])
                         |> Config.select_bench_counter OneDNN CYCLES
                         |> Pred.peak_percent x y w h c f in
      (name, onednn_score, gen_score, Gen.tile_to_string best_tile)
    )
  |> Stream.into (file_write output_name)

let conv_auguste_vs_dnn output_name =
  let module A = Arch.Sky_lake in
  let module Tile = Codegen.Tile(A) in
  let open Tile.Conv in
  let module Conv_default =
    (val build_conv_module XYF XYC HWCF) in
  let module Gen = Schemes.Gen(A)(Conv_default) in
  let module Pred = Arch.Pred_conv(A) in
  let open Streaming in
  let open Config in
  let file_write output_name =
    let init () = let f = File.open_out output_name in
      IO.write_line f "name, onednn, gen, best tile"; (f, 0)
    and push (f, n) (name, onednn, gen, best_tile) =
      Printf.fprintf f "%s, %f, %f\nBest tile : %s\n" name onednn gen best_tile;
      IO.flush f;
      (f, n + 1)
    and stop (f, _) = IO.close_out f in
    Sink.make ~init ~push ~stop () in
  let conf_gen = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:5
      (Some (true, true, Gcc,  O3))
      [Conv; ] [CYCLES] in
  let conf_dnn = build_conv_config ~error:(Config.EPSILON 0.01) ~num_rep:100
      (Some (false, true, Gcc,  O3))
      [OneDNN; ] [CYCLES] in
  let open Execution.Bench(A) in
  Stream.of_list Gen.(mb_rsnet_tactics_original)
  |> Stream.map (fun (name, (f,c,y,x,h,w) , t) ->
      let args = conv_args x w y h c f in
      let tiles = Gen.gen_scheme (build_dim_sizes x w y h c f) t in
      let best_tile, gen_score = List.map (fun t -> t, exec conf_gen args (Tile.Conv_tile t)
                                         |> Config.select_bench_counter Conv CYCLES
                                         |> Pred.peak_percent x y w h c f) tiles
                      |> List.min_max ~cmp:(fun (_, f1) (_,f2) -> Float.compare
                                               f1 f2)
                      |> snd in
      let onednn_score = exec conf_dnn args (Tile.Conv_tile [])
                         |> Config.select_bench_counter OneDNN CYCLES
                         |> Pred.peak_percent x y w h c f in
      (name, onednn_score, gen_score, Gen.tile_to_string best_tile)
    )
  |> Stream.into (file_write output_name)

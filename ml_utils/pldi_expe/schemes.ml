open Batteries
open Common
open Arithmetic

module S(A: Arch.Arch_t)(Conv: Tensor_loops.Sign.Conv_args_t) = struct
  include Tensor_loops.Tensor_tile(A.M)
  include Conv
  (*K*C*H*W*R*S:*)

  type size = {x: int; y: int; w: int; h:int; f:int; c:int}

  type conv_scheme = {name: string; size: size; original : loop_type list;
                      best: loop_type list; strides : (int * int) option}


  let scheme name (f,c,y,x,w,h) ?strides original best =
    match strides with
    None -> { name; size = {f;c;y;x;w;h}; original; strides= None; best}
            (* HEEEERE : divides size *)
    | Some (sx, sy) ->{ name; size = {f;c;y = y/sy;x =x/sx;w;h}; original; strides; best}

  let yolo9000_0_size =
    32,3,544,544,3,3

  let yolo9000_0 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    R c_dim;
    T (8, x_dim);
    R w_dim;
    R h_dim;
    (* End L1 *)
    T (431, y_dim);
    (* End L2 *)
    T (289, x_dim);
    (* End L3 *)
    R x_dim;
    R y_dim;
  ]

  let yolo9000_0_nomic = [
    V f_dim;
    T (2, f_dim);
    T (3, w_dim);
    T (3, h_dim);
    R c_dim;
    T (14, x_dim);
    T (14, y_dim);
    (* End L1 *)
    T (324, y_dim);
    (* End L2 *)
    T (440, y_dim);
    R x_dim;
    (* End L3 *)
    R y_dim;
  ]

  let yolo9000_0_nomic_best = 
    [V f_dim; U (2, f_dim); U (3, w_dim); U (3, h_dim); T (3, c_dim); Hoist_vars
       [c_dim]; T (544, x_dim); T (544, y_dim)]

  let yolo9000_0_best =
    [V f_dim; U (2, f_dim); U (8, y_dim); T (3, c_dim);
     T (16, x_dim);
     T (3, w_dim);
     T (3, h_dim);
     T (68, y_dim);
     T (34, x_dim)]

  let yolo0 = scheme "Yolo9000_0" yolo9000_0_size yolo9000_0 yolo9000_0_best
  let yolo0_nomic = scheme "Yolo9000_0" yolo9000_0_size yolo9000_0_nomic
      yolo9000_0_nomic_best

  let yolo9000_2_size =
    64,32,272,272,3,3

  let yolo9000_2 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (11, c_dim);
    T (4, x_dim);
    R w_dim;
    R h_dim;
    (* End L1 *)
    T (271, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    T (191, x_dim);
    (* End L3 *)
    R x_dim;
    R y_dim;
  ]

  let yolo9000_2_nomic = [
    V f_dim;
    T (2, f_dim);
    T (3, w_dim);
    T (3, h_dim);
    T (11, c_dim);
    T (10, x_dim);
    T (10, y_dim);
    (* End L1 *)
    T (138, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]
  let yolo9000_2_best =
    [V f_dim; U (2, f_dim); U (8, y_dim); T (32, c_dim);
     T (8, x_dim);
     T (3, w_dim);
     T (3, h_dim);
     T (34, y_dim);
     T (2, f_dim);
     T (34, x_dim)]

  let yolo9000_2_par =
    [V f_dim; U (2, f_dim); U (8, y_dim); T (32, c_dim);
     T (8, x_dim);
     T (3, w_dim);
     T (3, h_dim);
     T (34, y_dim);
     T (2, f_dim);
     T (34, x_dim)]

  let yolo9000_2_nomic_best =
    [V f_dim; U (3, w_dim); U (3, h_dim); U (4, f_dim);
     T (32, c_dim); Hoist_vars [c_dim]; T (272, x_dim); T (272, y_dim)]

  let yolo2 = scheme "Yolo9000_2" yolo9000_2_size yolo9000_2 yolo9000_2_best
  let yolo2_nomic = scheme "Yolo9000_2" yolo9000_2_size yolo9000_2_nomic
      yolo9000_2_nomic_best

  let yolo9000_4_size =
    128,64,136,136,3,3

  let yolo9000_4_best =
    [V f_dim; U (2, f_dim); U (8, y_dim); T (64, c_dim); T (3, w_dim);
     T (3, h_dim); T (4, f_dim); T (136, x_dim); T (17, y_dim)]

  let yolo9000_4 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (11, c_dim);
    T (4, x_dim);
    R w_dim;
    R h_dim;
    (* End L1 *)
    T (114, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]

  let yolo9000_4_nomic = [
    V f_dim;
    T (2, f_dim);
    T (3, w_dim);
    T (3, h_dim);
    T (11, c_dim);
    Hoist_vars [c_dim];
    T (10, x_dim);
    T (10, y_dim);
    (* End L1 *)
    R f_dim;
    T (32, x_dim);
    T (32, y_dim);
    (* End L2 *)
    T (90, x_dim);
    R y_dim;
    R c_dim;
    (* End L3 *)
    R x_dim;
  ]

  let yolo9000_4_nomic_best =
    [V f_dim; U (3, w_dim); U (3, h_dim); T (32, c_dim); Hoist_vars [c_dim];
     T (8, f_dim); T (136, y_dim); T (2, c_dim); T (136, x_dim)]

  let yolo4 = scheme "Yolo9000_4" yolo9000_4_size yolo9000_4 yolo9000_4_best
  let yolo4_nomic = scheme "Yolo9000_4" yolo9000_4_size yolo9000_4_nomic
      yolo9000_4_nomic_best

  let yolo9000_5_size =
    64,128,136,136,1,1

  let yolo9000_5_best =
    [V f_dim; U (4, f_dim); U (8, y_dim); T (128, c_dim); T (136, x_dim); T (17, y_dim)]

  let yolo9000_5 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (79, c_dim);
    T (3, x_dim);
    (* End L1 *)
    T (22, x_dim);
    T (33, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]

  let yolo9000_5_nomic_best =
    [V f_dim; T (4, f_dim); T (128, c_dim); T (136, x_dim); T (136, y_dim)]

  let yolo9000_5_nomic = [
    V f_dim;
    U (2, f_dim);
    T (64, c_dim);
    T (8, x_dim);
    T (8, y_dim);
    (* End L1 *)
    T (25, x_dim);
    T (25, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]

  let yolo5 = scheme "Yolo9000_5" yolo9000_5_size yolo9000_5 yolo9000_5_best

  let yolo5_nomic = scheme "Yolo9000_5" yolo9000_5_size yolo9000_5_nomic
      yolo9000_5_nomic_best

  let yolo9000_8_size =
    256,128,68,68,3,3

  let yolo9000_8_best =
[V f_dim; U (4, f_dim); U (4, y_dim); T (128, c_dim); T (68, x_dim); T (3, w_dim); T (3, h_dim);
  T (4, f_dim); T (17, y_dim)]

  let yolo9000_8_nomic_best =
    [V f_dim; U (2, f_dim); T (3, w_dim); T (3, h_dim); T (8, f_dim); T (68, x_dim); T (68, y_dim);
     T (128, c_dim)]

  let yolo9000_8_nomic = [
    V f_dim;
    T (2, f_dim);
    T (3, w_dim);
    T (3, h_dim);
    T (11, c_dim);
    Hoist_vars [c_dim];
    T (9, x_dim);
    T (11, y_dim);
    (* End L1 *)
    T (190, f_dim);
    T (76, c_dim);
    (* End L2 *)
    R x_dim;
    R y_dim;
    R f_dim;
    R c_dim;
    (* End L3 *)
  ]


  let yolo9000_8 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (12, c_dim);
    T (4, x_dim);
    R w_dim;
    R h_dim;
    (* End L1 *)
    T (196, f_dim);
    R c_dim;
    (* End L2 *)
    T (226, f_dim);
    R x_dim;
    R y_dim;
    (* End L3 *)
    R f_dim;
  ]

  let yolo8 = scheme "Yolo9000_8" yolo9000_8_size yolo9000_8 yolo9000_8_best

  let yolo8_nomic = scheme "Yolo9000_8" yolo9000_8_size yolo9000_8_nomic
      yolo9000_8_nomic_best


  let yolo9000_9_size =
    128,256,64,68,1,1

  let yolo9000_9_best =
    [V f_dim; U (4, f_dim); U (8, y_dim); T (256, c_dim); T (68, x_dim);
     T (8, y_dim); T (2, f_dim)]

  let yolo9000_9_nomic_best =
    [V f_dim; T (8, f_dim); T (256, c_dim); T (68, x_dim); T (64, y_dim)]

  let yolo9000_9_nomic = [
    V f_dim;
    U (2, f_dim);
    T (63, c_dim);
    T (4, x_dim);
    T (16, y_dim);
    (* End L1 *)
    R f_dim;
    T (22, x_dim);
    T (35, y_dim);
    (* End L2 *)
    R c_dim;
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]

  let yolo9000_9 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (79, c_dim);
    T (3, x_dim);
    (* End L1 *)
    T (14, x_dim);
    T (24, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]

  let yolo9 = scheme "Yolo9000_9" yolo9000_9_size yolo9000_9 yolo9000_9_best
  let yolo9_nomic = scheme "Yolo9000_9" yolo9000_9_size yolo9000_9_nomic
      yolo9000_9_nomic_best

  let yolo9000_12_size =
    512,256,34,34,3,3

  let yolo9000_12_handcraft =
    [V f_dim; U (4, f_dim); U (2, y_dim); U (2, x_dim); U (3, h_dim);U (3, w_dim);
     T (16, c_dim); Hoist_vars [c_dim]; T (17, x_dim);
      Pack_tens params; T (8, f_dim); T (17, y_dim);  R c_dim]

  let yolo9000_12_tuned =
[V f_dim; U (2, f_dim); U (17, y_dim); T (256, c_dim); T (34, x_dim); T (3, w_dim); T (3, h_dim);
  T (16, f_dim); T (2, y_dim)]

  let yolo9000_12_nomic_best =
    [V f_dim; U (2, f_dim); T (3, w_dim); T (3, h_dim); T (16, f_dim); T (34, y_dim); T (256, c_dim);
  T (34, x_dim)]

  let yolo9000_12_nomic = [
    V f_dim;
    U (2, f_dim);
    T (3, w_dim);
    T (3, h_dim);
    T (11, c_dim);
    Hoist_vars [c_dim];
    T (10, x_dim);
    T (10, y_dim);
    (* End L1 *)
    T (186, f_dim);
    T (26, x_dim);
    T (26, y_dim);
    (* End L2 *)
    T (30, x_dim);
    R y_dim;
    R f_dim;
    R c_dim;
    (* End L3 *)
    R x_dim;
  ]

  let yolo9000_12 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (11, c_dim);
    T (4, x_dim);
    R w_dim;
    R h_dim;
    (* End L1 *)
    T (178, f_dim);
    T (19, x_dim);
    R y_dim;
    (* End L2 *)
    T (26, x_dim);
    T (511, f_dim);
    R c_dim;
    (* End L3 *)
    R f_dim;
    R x_dim;
  ]

  let yolo12_nomic = scheme "Yolo9000_12" yolo9000_12_size yolo9000_12_nomic
      yolo9000_12_tuned

  let yolo12_tuned = scheme "Yolo9000_12" yolo9000_12_size yolo9000_12
      yolo9000_12_tuned

  let yolo12_handcraft = scheme "Yolo9000_12" yolo9000_12_size yolo9000_12
      yolo9000_12_handcraft

  let yolo9000_13_size =
    256,512,34,34,1,1

  let yolo9000_13_handcraft =
    [V f_dim; U (4, f_dim); U (2, x_dim); U (2, y_dim); T (256, c_dim);
     T (17, y_dim);  T (4, f_dim); T (2, c_dim); T (17, x_dim)]

  let yolo9000_13_tuned =
    [V f_dim; U (2, f_dim); U (17, y_dim); T (256, c_dim); T (2, x_dim); T (2, y_dim); T (8, f_dim);
     T (2, c_dim); T (17, x_dim)]

  let yolo9000_13_nomic = [
    V f_dim;
    U (2, f_dim);
    T (62, c_dim);
    T (8, x_dim);
    T (8, y_dim);
    (* End L1 *)
    R f_dim;
    T (21, x_dim);
    T (21, y_dim);
    (* End L2 *)
    T (28, x_dim);
    T (28, y_dim);
    R c_dim;
    (* End L3 *)
    R x_dim;
    R y_dim;
  ]

  let yolo9000_13 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (79, c_dim);
    T (3, x_dim);
    (* End L1 *)
    T (6, x_dim);
    T (16, y_dim);
    R f_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    R y_dim;
    (* End L3 *)
  ]

  let yolo_13_nomic = [
    V f_dim;
    U (2, f_dim);
    T (62, c_dim);
    T (8, x_dim);
    T (8, y_dim);
    (* End L1 *)
    R f_dim;
    T (21, x_dim);
    T (21, y_dim);
    (* End L2 *)
    T (28, x_dim);
    T (28, y_dim);
    R c_dim;
    (* End L3 *)
    R x_dim;
    R y_dim;
  ]

  let yolo13_tuned = scheme "Yolo9000_13" yolo9000_13_size yolo9000_13
      yolo9000_13_tuned

  let yolo13_nomic = scheme "Yolo9000_13" yolo9000_13_size yolo9000_13_nomic yolo9000_0_nomic

  let yolo13_handcraft = scheme "Yolo9000_13" yolo9000_13_size yolo9000_13
      yolo9000_13_handcraft

  let yolo9000_18_size =
    1024,512,17,17,3,3

  let yolo9000_18 = [
    V f_dim; 
    U (2, f_dim); 
    U (12, y_dim); 
    T (11, c_dim); 
    T (4, x_dim); 
    R w_dim; 
    R h_dim; 
    (* End L1 *) 
    T (376, f_dim); 
    R x_dim; 
    R y_dim; 
    (* End L2 *) 
    T (708, f_dim); 
    R c_dim; 
    (* End L3 *) 
    R f_dim; 
  ]

  let yolo9000_18_nomic = [
    V f_dim;
    T (2, f_dim);
    T (3, w_dim);
    T (3, h_dim);
    T (11, c_dim);
    T (10, x_dim);
    T (10, y_dim);
    (* End L1 *)
    T (406, f_dim);
    R x_dim;
    R y_dim;
    (* End L2 *)
    R f_dim;
    R c_dim;
    (* End L3 *)
  ]


  let yolo18_nomic = scheme "Yolo9000_18" yolo9000_18_size yolo9000_18_nomic yolo9000_0_nomic

  let yolo9000_19_size =
    512,1024,17,17,1,1

  let yolo9000_19_nomic = [
    V f_dim;
    U (2, f_dim);
    T (61, c_dim);
    T (8, x_dim);
    T (8, y_dim);
    (* End L1 *)
    T (463, f_dim);
    T (15, x_dim);
    T (15, y_dim);
    (* End L2 *)
    T (16, x_dim);
    T (16, y_dim);
    R f_dim;
    R c_dim;
    (* End L3 *)
    R x_dim;
    R y_dim;
  ]



  let yolo9000_19 = [
    V f_dim; 
    U (2, f_dim); 
    U (12, y_dim); 
    T (78, c_dim); 
    T (3, x_dim); 
    (* End L1 *) 
    T (442, f_dim); 
    T (15, x_dim); 
    T (15, y_dim); 
    (* End L2 *) 
    T (16, x_dim); 
    T (16, y_dim); 
    R f_dim; 
    R c_dim; 
    (* End L3 *) 
    R y_dim; 
    R x_dim; 
  ]

  let yolo19_nomic = scheme "Yolo9000_19" yolo9000_19_size yolo9000_19_nomic yolo9000_0_nomic

  let yolo9000_23_size_real =
    28269, 1024, 17, 17, 1,1
  let yolo9000_23_size =
    28272, 1024, 17, 17, 1,1

  let yolo9000_23_nomic = [
    V f_dim;
    U (2, f_dim);
    T (61, c_dim);
    T (8, x_dim);
    T (8, y_dim);
    (* End L1 *)
    T (463, f_dim);
    T (15, x_dim);
    T (16, y_dim);
    (* End L2 *)
    T (6360, f_dim);
    R c_dim;
    (* End L3 *)
    R f_dim;
    R x_dim;
    (* End L3 *) 
    R y_dim; 
    R x_dim; 
  ]


  let yolo9000_23 = [
    V f_dim; 
    U (2, f_dim); 
    U (12, y_dim); 
    T (78, c_dim); 
    T (3, x_dim); 
    (* End L1 *) 
    T (442, f_dim); 
    T (15, x_dim); 
    T (15, y_dim); 
    (* End L2 *) 
    T (3717, f_dim); 
    R c_dim; 
    R x_dim; 
    R y_dim; 
    (* End L3 *) 
    R f_dim;
  ]

  let yolo23_nomic = scheme "Yolo9000_23" yolo9000_23_size yolo9000_23_nomic yolo9000_0_nomic

  let yolo_nomic = [
    yolo0_nomic;
    yolo2_nomic;
    yolo4_nomic;
    yolo5_nomic;
    yolo8_nomic;
    yolo9_nomic;
    yolo12_nomic;
    yolo13_nomic;
    yolo18_nomic;
    yolo19_nomic;
    (*yolo23_nomic;*)
  ]

  let mobilNet_1_size =
    32,32,112,112,3,3
  let mobilNet_1 = [
    V f_dim;
    U (2, f_dim);
    U (12, y_dim);
    T (11, c_dim);
    T (4, x_dim);
    R w_dim;
    R h_dim;
    (* End L1 *)
    T (17, x_dim);
    R y_dim;
    R c_dim;
    (* End L2 *)
    R x_dim;
    (* End L3 *)
  ]


  let mobilNet_1_best = 
[V f_dim; U (2, f_dim); U (14, y_dim); T (32, c_dim); T (3, w_dim);
T (3, h_dim); T (112, x_dim); T (8, y_dim)]

  let mobilNet_2_size =
    64,64,112,112,3,3
(*   let  stride_mobilenet_2 = 2 *)

  let mobilNet_2_best =
[V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim); T (3, w_dim);
 T (3, h_dim); T (112, x_dim); T (8, y_dim); T (2, f_dim)]

  let mobilNet_2_best_stride =
[V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim); T (3, w_dim);
 T (3, h_dim); T (56, x_dim); T (4, y_dim); T (2, f_dim)]

  let mobilNet_2=
    [V   f_dim;    
     U (2, f_dim);    
     U (12, y_dim);    
     T (10, c_dim);    
     T (4, x_dim);    
     R w_dim;    
     R h_dim;    
     (* End L1 *)    
     T (7, x_dim);    
     R y_dim;    
     R f_dim;    
     R c_dim;    
     (* End L2 *)    
     R x_dim;    
     (* End L3 *)
    ]

  let mobilNet_3_size =
    128,128,56,56,3,3

  let mobilNet_3 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (54, y_dim);
     R f_dim;
     R c_dim;
     (* End L2 *)
     R x_dim;
     R y_dim;
     (* End L3 *)
    ]
  let mobilNet_3_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); T (4, x_dim); T (3, w_dim); T (3, h_dim);
  T (4, f_dim); T (14, x_dim); T (4, y_dim)]

  let mb3_scheme = scheme "mobilNet_3" mobilNet_3_size mobilNet_3 mobilNet_3_best

  let mobilNet_4_size =
    128,128,56,56,3,3

(*   let _stride_m4 = 2 *)

  let mobilNet_4_best =
[V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); T (3, w_dim); T (3, h_dim); T (2, y_dim);
  T (4, f_dim); T (56, x_dim); T (2, y_dim)]

  let mobilNet_4_best_stride =
[V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); T (3, w_dim); T (3, h_dim); T (2, y_dim);
 T (4, f_dim); T (28, x_dim); ]

  let mobilNet_4 =
    [
      V f_dim;
      U (2, f_dim);
      U (12, y_dim);
      T (10, c_dim);
      T (4, x_dim);
      R w_dim;
      R h_dim;
      (* End L1 *)
      T (48, y_dim);
      R f_dim;
      R c_dim;
      (* End L2 *)
      R x_dim;
      R y_dim;
      (* End L3 *)
    ]

  let mb4_scheme_stride = scheme "mobilNet_4"  mobilNet_4_size ~strides:(2,2)
      mobilNet_4 mobilNet_4_best_stride

  let mb4_scheme = scheme "mobilNet_4"  mobilNet_4_size ~strides:(2,2) mobilNet_4 mobilNet_4_best

  let mobilNet_5_size =
    256,256,28,28,3,3

  let mobilNet_5_best = 
[V f_dim; U (2, f_dim); U (14, y_dim); T (32, c_dim); T (3, w_dim); T (3, h_dim); T (8, f_dim);
  T (28, x_dim); T (2, y_dim); T (8, c_dim)]

  let mobilNet_5 = 
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (179, f_dim);
     T (23, x_dim);
     R y_dim;
     (* End L2 *)
     R x_dim;
     R f_dim;
     R c_dim;
     (* End L3 *)
    ]

  let mb5_scheme = scheme "mobilNet_5"  mobilNet_5_size mobilNet_5 mobilNet_5_best

  let mobilNet_6_size =
    256,256,28,28,3,3
(*   let stride_m6 = 2 *)

  let mobilNet_6_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (32, c_dim); T (3, w_dim);
     T (3, h_dim); T (8, f_dim); T (4, x_dim);
     T (2, y_dim); T (7, x_dim); T (8, c_dim)]

  let mobilNet_6_best_stride =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (32, c_dim); T (3, w_dim);
     T (3, h_dim); T (8, f_dim); T (2, x_dim);
      T (7, x_dim); T (8, c_dim)]

  let mobilNet_6 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (10, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (200, f_dim);
     T (20, x_dim);
     R y_dim;
     (* End L2 *)
     T (24, x_dim);
     R f_dim;
     R c_dim;
     (* End L3 *)
     R x_dim;
    ]

  let mb6_scheme_stride = scheme "mobilNet_6"  ~strides:(2, 2) mobilNet_6_size
      mobilNet_6 mobilNet_6_best_stride
  let mb6_scheme = scheme "mobilNet_6"   mobilNet_6_size mobilNet_6 mobilNet_6_best

  let mobilNet_7_size =
    512,512,14,14,3,3

  let mobilNet_7_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); T (14, x_dim);
     T (3, w_dim); T (3, h_dim);
     T (16, f_dim); T (4, c_dim)]

  let mobilNet_7 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (12, c_dim);
     R f_dim;
     R x_dim;
     R y_dim;
     (* End L2 *)
     R c_dim;
     (* End L3 *)
    ]

  let mb7_scheme = scheme "mobilNet_7"  mobilNet_7_size mobilNet_7 mobilNet_7_best

  let mobilNet_8_size =
    512,512,14,14,3,3
(*   let stride_m8 = 2 *)

  let mobilNet_8_best = 
    [V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim);
     T (14, x_dim); T (3, w_dim); T (3, h_dim);
     T (16, f_dim); T (8, c_dim)]

  let mobilNet_8_best_stride = 
    [V f_dim; U (4, f_dim); U (7, y_dim); T (64, c_dim);
     T (7, x_dim); T (3, w_dim); T (3, h_dim);
     T (8, f_dim); T (8, c_dim)]


  let mobilNet_8 = 
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     R f_dim;
     R x_dim;
     R y_dim;
     (* End L2 *)
     R c_dim;
     (* End L3 *)
    ]

  let mb8_scheme = scheme "mobilNet_8"  ~strides:(2, 2) mobilNet_8_size mobilNet_8 mobilNet_8_best
  let mb8_scheme_stride = scheme "mobilNet_8"  ~strides:(2, 2) mobilNet_8_size
      mobilNet_8 mobilNet_8_best_stride

  let mobilNet_9_size =
    1024,1024,7,7,3,3

  let mobilNet_9_best =
    [V f_dim; U (2, f_dim); T (7, y_dim); T (8, c_dim); T (4, f_dim);
     T (8, f_dim); T (128, c_dim); T (7, x_dim);
     T (3, w_dim); T (3, h_dim)]

  let mobilNet_9 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (168, c_dim);
     (* End L1 *)
     R y_dim;
     T (883, f_dim);
     T (280, c_dim);
     (* End L2 *)
     T (5, x_dim);
     T (939, f_dim);
     T (927, c_dim);
     T (2, w_dim);
     T (2, h_dim);
     (* End L3 *)
     R f_dim;
     R c_dim;
     R x_dim;
     R w_dim;
     R h_dim;
    ]

  let mb9_scheme = scheme "mobilNet_9" mobilNet_9_size mobilNet_9 mobilNet_9_best

  let resNet18_1_size =
    64,3,224,224,7,7

(*   let stride_r1 = 2 *)

  let resNet18_1_best =
    [V f_dim; U (4, f_dim); U (8, y_dim); T (7, w_dim);
     T (7, h_dim); T (112, x_dim); T (28, y_dim);
     T (3, c_dim); T (2, x_dim)]

  let resNet18_1_best_stride =
    [V f_dim; U (4, f_dim); U (8, y_dim); T (7, w_dim);
     T (7, h_dim); T (112, x_dim); T (14, y_dim);
     T (3, c_dim); ]

  let resNet18_1 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (2, c_dim);
     T (5, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (7, x_dim);
     R y_dim;
     R f_dim;
     R c_dim;
     (* End L2 *)
     R x_dim;
     (* End L3 *)
    ]

  let res1_scheme = scheme "resNet18_1"   resNet18_1_size resNet18_1 resNet18_1_best

  let res1_scheme_stride =
    scheme "resNet18_1"  ~strides:(2, 2) resNet18_1_size resNet18_1
      resNet18_1_best_stride

  let resNet18_2_size =
    64,64,56,56,3,3

  let resNet18_2_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim); T (3, w_dim);
     T (3, h_dim); T (4, x_dim); T (4, y_dim);
     T (2, f_dim); T (14, x_dim)]

  let resNet18_2 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (10, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (7, x_dim);
     R y_dim;
     R f_dim;
     R c_dim;
     (* End L2 *)
     R x_dim;
     (* End L3 *)
    ]

  let res2_scheme = scheme "resNet18_2" resNet18_2_size resNet18_2 resNet18_2_best

  let resNet18_3_size =
    64,64,56,56,1,1

  let resNet18_3_best =
    [V f_dim; U (2, f_dim); U (8, y_dim); T (64, c_dim);
     T (2, x_dim); T (2, f_dim); T (28, x_dim); T (7, y_dim)]

  let resNet18_3 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     R c_dim;
     T (4, x_dim);
     (* End L1 *)
     T (26, x_dim);
     T (30, y_dim);
     R f_dim;
     (* End L2 *)
     R x_dim;
     R y_dim;
     (* End L3 *)
    ]

  let res3_scheme = scheme  "resNet18_3" resNet18_3_size resNet18_3 resNet18_3_best

  let resNet18_4_size =
    128,64,56,56,3,3
(*   let stride_r4 = 2 *)

  let resNet18_4_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim); T (3, w_dim);
     T (3, h_dim); T (4, x_dim); T (4, y_dim);
     T (4, f_dim); T (14, x_dim)]

  let resNet18_4_best_stride =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim); T (3, w_dim);
     T (3, h_dim); T (4, x_dim); T (2, y_dim);
     T (4, f_dim); T (7, x_dim)]

  let resNet18_4 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (10, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (7, x_dim);
     R y_dim;
     R f_dim;
     R c_dim;
     (* End L2 *)
     R x_dim;
     (* End L3 *)
    ]

  let res4_scheme = scheme "resNet18_4" resNet18_4_size resNet18_4 resNet18_4_best

  let res4_scheme_stride = scheme "resNet18_4" ~strides:(2, 2) resNet18_4_size
      resNet18_4 resNet18_4_best_stride

  let resNet18_5_size =
    128,64,56,56,1,1
(*   let stride_r5 = 2 *)

  let resNet18_5_best =
    [V f_dim; U (2, f_dim); U (8, y_dim); T (64, c_dim); T (2, x_dim);
     T (4, f_dim); T (28, x_dim); T (7, y_dim)]

  let resNet18_5_best_stride =
    [V f_dim; U (2, f_dim); U (7, y_dim); U (2, x_dim); T (64, c_dim);
     T (4, f_dim); T (14, x_dim); T (4, y_dim)]


  let resNet18_5 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     R c_dim;
     T (4, x_dim);
     (* End L1 *)
     T (21, x_dim);
     T (25, y_dim);
     R f_dim;
     (* End L2 *)
     R x_dim;
     R y_dim;
     (* End L3 *)
    ]

  let res5_scheme_stride = scheme "resNet18_5"  ~strides:(2, 2) resNet18_5_size
      resNet18_5 resNet18_5_best_stride

  let res5_scheme = scheme "resNet18_5"  resNet18_5_size resNet18_5 resNet18_5_best

  let resNet18_6_size =
    128,128,28,28,3,3

  let resNet18_6_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); T (3, w_dim);
     T (3, h_dim); T (2, y_dim);
     T (4, f_dim); T (28, x_dim)]

  let resNet18_6 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (7, x_dim);
     R y_dim;
     R f_dim;
     R c_dim;
     (* End L2 *)
     R x_dim;
     (* End L3 *)
    ]

  let res6_scheme = scheme "resNet18_6"  resNet18_6_size resNet18_6 resNet18_6_best

  let resNet18_7_size =
    256,128,28,28,3,3
(*   let stride_r7 = 2 *)

  let resNet18_7_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (16, c_dim); T (3, w_dim); T (3, h_dim);
     T (2, y_dim); T (2, f_dim); T (28, x_dim); T (4, f_dim); T (8, c_dim)]

  let resNet18_7_best_stride =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (16, c_dim); T (3, w_dim); T (3, h_dim);
      T (2, f_dim); T (14, x_dim); T (4, f_dim); T (8, c_dim)]

  let resNet18_7 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (16, y_dim);
     T (204, f_dim);
     T (114, c_dim);
     (* End L2 *)
     R x_dim;
     R y_dim;
     R f_dim;
     R c_dim;
     (* End L3 *)
    ]

  let res7_scheme_stride = scheme "resNet18_7" ~strides:(2, 2) resNet18_7_size
      resNet18_7 resNet18_7_best_stride
  let res7_scheme = scheme "resNet18_7"  resNet18_7_size resNet18_7 resNet18_7_best

  let resNet18_8_size =
    256,128,28,28,3,3

  let resNet18_8_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim);
     T (28, x_dim); T (3, w_dim); T (3, h_dim);
     T (4, f_dim); T (2, f_dim); T (2, y_dim)]

  let resNet18_8 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (12, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (196, f_dim);
     R c_dim;
     (* End L2 *)
     T (227, f_dim);
     R x_dim;
     R y_dim;
     (* End L3 *)
     R f_dim;
    ]

  let res8_scheme = scheme "resNet18_8" resNet18_8_size resNet18_8 resNet18_8_best

  let resNet18_9_size =
    256,256,14,14,3,3

  let resNet18_9_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (32, c_dim); T (3, w_dim);
     T (3, h_dim); T (8, f_dim); T (14, x_dim); T (8, c_dim)]

  let resNet18_9 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     T (48, c_dim);
     R f_dim;
     R x_dim;
     R y_dim;
     (* End L2 *)
     R c_dim;
     (* End L3 *)
    ]

  let res9_scheme = scheme "resNet18_9" resNet18_9_size resNet18_9 resNet18_9_best

  let resNet18_10_size =
    512,512,14,14,3,3
(*   let stride_r10 = 2 *)

  let resNet18_10_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (128, c_dim); T (14, x_dim); T (3, w_dim); T (3, h_dim);
     T (16, f_dim); T (4, c_dim)]

  let resNet18_10_best_stride =
    [V f_dim; U (2, f_dim); U (7, y_dim); T (128, c_dim); T (7, x_dim); T (3, w_dim); T (3, h_dim);
     T (16, f_dim); T (4, c_dim)]

  let resNet18_10 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (11, c_dim);
     T (4, x_dim);
     R w_dim;
     R h_dim;
     (* End L1 *)
     R f_dim;
     R x_dim;
     R y_dim;
     (* End L2 *)
     R c_dim;
     (* End L3 *)
    ]

  let res10_scheme = scheme "resNet18_10"  resNet18_10_size resNet18_10 resNet18_10_best
  let res10_scheme_stride = scheme "resNet18_10" ~strides:(2, 2)
      resNet18_10_size resNet18_10 resNet18_10_best_stride

  let resNet18_11_size =
    512,256,14,14,1,1
(*   let stride_r11 = 2 *)

  let resNet18_11_best =
    [V f_dim; U (2, f_dim); U (14, y_dim); T (64, c_dim); T (16, f_dim);
     T (4, c_dim); T (14, x_dim)]

  let resNet18_11_best_stride =
    [V f_dim; U (2, f_dim); U (7, y_dim); T (64, c_dim); T (16, f_dim);
     T (4, c_dim); T (7, x_dim)]

  let resNet18_11 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (79, c_dim);
     T (3, x_dim);
     (* End L1 *)
     T (5, x_dim);
     R f_dim;
     R c_dim;
     (* End L2 *)
     R x_dim;
     R y_dim;
     (* End L3 *)
    ]

  let res11_scheme = scheme "resNet18_11"  resNet18_11_size resNet18_11 resNet18_11_best

  let res11_scheme_stride = scheme "resNet18_11" ~strides:(2, 2)
      resNet18_11_size resNet18_11 resNet18_11_best_stride

  let resNet18_12_size =
    512,512,7,7,3,3

  let resNet18_12_best =
[V f_dim; T (7, y_dim); T (2, f_dim); T (8, c_dim); T (2, f_dim);
 T (8, f_dim); T (64, c_dim); T (7, x_dim); T (3, w_dim); T (3, h_dim)]

  let resNet18_12 =
    [V f_dim;
     U (2, f_dim);
     U (12, y_dim);
     T (168, c_dim);
     (* End L1 *)
     R y_dim;
     T (499, f_dim);
     T (477, c_dim);
     (* End L2 *)
     T (5, x_dim);
     T (503, f_dim);
     T (495, c_dim);
     T (2, w_dim);
     T (2, h_dim);
     (* End L3 *)
     R f_dim;
     R c_dim;
     R x_dim;
     R w_dim;
     R h_dim;
    ]

  let res12_scheme = scheme "resNet18_12" resNet18_12_size resNet18_12 resNet18_12_best

  let yolo_all_tuned_schemes = [
    yolo0;
    yolo2;
    yolo4;
    yolo5;
    yolo8;
    yolo9;
    yolo12_tuned;
    yolo13_tuned;
    yolo18_nomic;
    yolo19_nomic;
    yolo23_nomic;
  ]

  let all_tuned_schemes = [
    yolo0;
    yolo2;
    yolo4;
    yolo5;
    yolo8;
    yolo9;
    yolo12_tuned;
    yolo13_tuned;
    mb3_scheme;
    mb4_scheme_stride;
    mb5_scheme;
    mb6_scheme_stride;
    mb7_scheme;
    mb8_scheme_stride;
    mb9_scheme;
    res1_scheme_stride;
    res2_scheme;
    res3_scheme;
    res4_scheme_stride;
    res5_scheme_stride;
    res6_scheme;
    res7_scheme_stride;
    res8_scheme;
    res9_scheme;
    res10_scheme_stride;
    res11_scheme_stride;
    res12_scheme;
  ]

  let tune_and_handcraft_schemes = [
    yolo0;
    yolo2;
    yolo4;
    yolo5;
    yolo8;
    yolo9;
    yolo12_handcraft;
    yolo13_handcraft;
    mb3_scheme;
    mb4_scheme_stride;
    mb5_scheme;
    mb6_scheme_stride;
    mb7_scheme;
    mb8_scheme_stride;
    mb9_scheme;
    res1_scheme_stride;
    res2_scheme;
    res3_scheme;
    res4_scheme_stride;
    res5_scheme_stride;
    res6_scheme;
    res7_scheme_stride;
    res8_scheme;
    res9_scheme;
    res10_scheme_stride;
    res11_scheme_stride;
    res12_scheme;
  ]

  let no_stride_schemes = [
    yolo0;
    yolo2;
    yolo4;
    yolo5;
    yolo8;
    yolo9;
    yolo12_handcraft;
    yolo13_handcraft;
    mb3_scheme;
    mb4_scheme;
    mb5_scheme;
    mb6_scheme;
    mb7_scheme;
    mb8_scheme;
    mb9_scheme;
    res1_scheme;
    res2_scheme;
    res3_scheme;
    res4_scheme;
    res5_scheme;
    res6_scheme;
    res7_scheme;
    res8_scheme;
    res9_scheme;
    res10_scheme;
    res11_scheme;
    res12_scheme;
  ]

  let sizes_tactics_original =
      ["yolo9000_0",  yolo9000_0_size, yolo9000_0;
      "yolo9000_2",  yolo9000_2_size, yolo9000_2;
      "yolo9000_4",  yolo9000_4_size, yolo9000_4;
      "yolo9000_5",  yolo9000_5_size, yolo9000_5;
      "yolo9000_8",  yolo9000_8_size, yolo9000_8;
      "yolo9000_9",  yolo9000_9_size, yolo9000_9;
      "yolo9000_12", yolo9000_12_size, yolo9000_12;
      "yolo9000_13", yolo9000_13_size, yolo9000_13;
      "yolo9000_18", yolo9000_18_size, yolo9000_18;
      "yolo9000_19", yolo9000_19_size, yolo9000_19;
      "yolo9000_23", yolo9000_23_size, yolo9000_23;
    ]

  let new_sizes_tactics_original =
    [ "yolo9000_18", yolo9000_18_size, yolo9000_18;
      "yolo9000_19", yolo9000_19_size, yolo9000_19;
      "yolo9000_23", yolo9000_23_size, yolo9000_23;
    ]
  let mb_rsnet_tactics_original =
    [
      "mobilNet_3", mobilNet_3_size, mobilNet_3;
      "mobilNet_4", mobilNet_4_size, mobilNet_4;
      "mobilNet_5", mobilNet_5_size, mobilNet_5;
      "mobilNet_6", mobilNet_6_size, mobilNet_6;
      "mobilNet_7", mobilNet_7_size, mobilNet_7;
      "mobilNet_8", mobilNet_8_size, mobilNet_8;
      "mobilNet_9", mobilNet_9_size, mobilNet_9;
      "resNet18_1", resNet18_1_size, resNet18_1;
      "resNet18_2", resNet18_2_size, resNet18_2;
      "resNet18_3", resNet18_3_size, resNet18_3;
      "resNet18_4", resNet18_4_size, resNet18_4;
      "resNet18_5", resNet18_5_size, resNet18_5;
      "resNet18_6", resNet18_6_size, resNet18_6;
      "resNet18_7", resNet18_7_size, resNet18_7;
      "resNet18_8", resNet18_8_size, resNet18_8;
      "resNet18_9", resNet18_9_size, resNet18_9;
      "resNet18_10", resNet18_10_size, resNet18_10;
      "resNet18_11", resNet18_11_size, resNet18_11;
      "resNet18_12", resNet18_12_size, resNet18_12;
    ]

  let last_batch_schemes = [
    mb3_scheme;
    mb4_scheme;
    mb5_scheme;
    mb6_scheme;
    mb7_scheme;
    mb8_scheme;
    mb9_scheme;
    res1_scheme;
    res2_scheme;
    res3_scheme;
    res4_scheme;
    res5_scheme;
    res6_scheme;
    res7_scheme;
    res8_scheme;
    res9_scheme;
    res10_scheme;
    res11_scheme;
    res12_scheme;
  ]

  let strides_schemes = [
    mb4_scheme_stride;
    mb6_scheme_stride;
    mb8_scheme_stride;
    res1_scheme_stride;
    res4_scheme_stride;
    res5_scheme_stride;
    res7_scheme_stride;
    res10_scheme_stride;
    res11_scheme_stride;
  ]

  let print_sizes (f,c,y,x,h,w) tactic =
    let dim_size = dim_tile_size tactic in
    let print d s = let ds =dim_size d in
      Printf.printf "Dim %s, size %d, tile %d\n" (Tensor_loops.Ids.Dim.show_id_of_t d) s ds in
    print f_dim f;
    print c_dim c;
    print y_dim y;
    print x_dim x;
    print h_dim h;
    print w_dim w

  let check_sizes ((f,c,y,x,h,w), tactic) =
    let dim_size = dim_tile_size tactic in
    f mod dim_size f_dim = 0
    && c mod dim_size c_dim = 0
    && y mod dim_size y_dim = 0
    && x mod dim_size x_dim = 0
    && h mod dim_size h_dim = 0
    && w mod dim_size w_dim = 0

end

module Gen(A: Arch.Arch_t)(Conv: Tensor_loops.Sign.Conv_args_t) = struct
  include S(A)(Conv)
  open Tensor_loops.Ids

  let tee f x = f x; x

  let rec scanl f acc = function
      [] -> []
    | h::t -> let acc' = f acc h in acc'::(scanl f acc' t)

  type lt = V_s | T_s | U_s [@@deriving show {with_path = false}]

  (* take size of dimension and a "tiling" *)
  let gen_scheme_dim size perm =
    let perm_only = List.map snd perm in
    let valid decmp =
      List.fold_left2
        (fun o (m, _) y ->
           match m, y with
           | V_s, c when c <> A.vec_size -> None
           | _ -> Option.map  (fun l -> y::l) o)
        (Some []) perm decmp
      |> Option.map List.rev in
    let distance =
      List.fold_left2 (fun d x y ->
          d + Int.abs (x - y)
        ) 0 in
    let len_perm = List.length perm_only in
    let decmp = if size = 1 then [perm_only]
      else decompose size len_perm
           |> List.filter_map valid in
    List.map (fun l -> distance perm_only l, l) decmp
    |> List.sort (fun (i1, _) (i2, _) -> Int.compare i1 i2)


  let forget_perm dim_sizes = function
      V d -> Some (V_s, d, A.vec_size)
    | T (s, d) -> Some (T_s, d, s)
    | U (s, d) -> Some (U_s, d, s)
    | R d -> let (d, s) = List.find (fun (d', _) -> Dim.equal d d') dim_sizes
      in
      Some (T_s, d, s)
    | _ -> None

  let cartesian_list f ll =
    List.fold_left
      (fun l_current l -> List.concat_map
          (fun e -> List.map (fun l' ->e::l') l_current) l)
      [[]] ll
    |> List.map List.rev
    |> List.map f

  let map_snd f (x, y) = x, f y

  let (let*) = Option.bind

  type ('a, 'b) either = Left of 'a | Right of 'b
  let partition f l =
    let l, r = List.fold_left (fun (l_list, r_list) e ->
        match f e with Left x -> x::l_list, r_list
                     | Right x -> l_list, x::r_list)
        ([], []) l in
    List.rev l, List.rev r


  let gen_scheme (dim_sizes:(Dim.t * int) list) (perm :loop_type list) =
    let handle_one_dim d size =
      let keep_unroll_on_y l =
        let filtered = List.filter
            (if Dim.equal d y_dim
                (* Make sure we have some unroll on y *)
             then (List.exists
                     (function _, U (s, _) -> s >= 8 && s <= 20
                             | _ -> false))
                  % snd
             else Fun.const true
            ) l in
        if List.is_empty filtered then l else filtered in
      let pos_s = List.filteri_map (fun i lt ->
          let* (mp, dp, sp) = forget_perm dim_sizes lt in
          if Dim.equal d dp
          then Some (i, sp, mp) else None) perm in
      if List.is_empty pos_s then None
      else Some (
          gen_scheme_dim size (List.map (fun (_, sp, mp) -> mp, sp ) pos_s)
          |>  List.map (map_snd @@ List.map2 (fun (i, _, mp)  s ->
              match mp with
                V_s -> i, V d
              | U_s -> i, U (s, d)
              | T_s -> i, T (s, d)
            ) pos_s)
          |> keep_unroll_on_y
        )
      (*|> List.take 40*) in
    dim_sizes
    |> List.filter_map (uncurry handle_one_dim)
    |> cartesian_list
      (fun ll ->
         List.fold_right
           (fun (s, l) (tot_score, ll) ->
              tot_score + s, List.append l ll)
           ll (0, [])
         |> map_snd @@
         List.sort (fun (i1, _) (i2, _) -> Int.compare i1 i2)
         %> List.filter_map (fun (_, lt) -> match lt with
               T (1, _) | U (1, _) -> None
             | other -> Some other
           )
      )
    |> List.sort (fun (i1, _) (i2, _) -> Int.compare i1 i2)
    |> List.map snd

  let tune_open_mp_one_level best_tile =
    let parallel_dim = [f_dim; y_dim; x_dim] in
    let tile_indexes = List.mapi (fun i tl -> i, tl) best_tile in
    let parallel = function
      | i, T (s, d) when List.exists (Dim.equal d) parallel_dim && i >= 7 ->
        let best_divisor =  List.init 9 ((+) 4)
                            |> List.filter (fun i -> s mod i = 0)
                            |> function [] -> None
                                      | l -> Some (List.max l) in
        Option.map (fun div -> div, T (s, d)(* previously T_par *)) best_divisor 
      | _ -> None in
    let rec gen_all = function
        [] -> []
      | ((_, t) as h)::tail -> match parallel h with
          Some (d, pt) ->
          let g = gen_all tail in
          let append_all e =  match g with [] -> [1, [e]]
                                         | _ -> List.map (fun (d, l) -> d, e::l) g in
          (d, pt::List.map snd tail)::(append_all t)
        | None ->
          let g = gen_all tail in
          let append_all e =  match g with [] -> [1, [e]]
                                         | _ -> List.map (fun (d, l) -> d, e::l) g in
          append_all t in
    gen_all tile_indexes

  let tune_open_mp best_tile =
    let parallel_dim = [f_dim; y_dim; x_dim] in
    let tile_indexes = List.mapi (fun i tl -> i, tl) best_tile in
    let parallel = function
      | i, T (s, d) when List.exists (Dim.equal d) parallel_dim && i >= 5 ->
        Some (T (s, d)) (* previously T_par *)
      | _ -> None in
    let rec gen_all = function
        [] -> []
      | ((_, t) as h)::tail -> match parallel h with
          Some pt ->
          let g = gen_all tail in
          let append_all e =  match g with [] -> [[e]]
                                         | _ -> List.map (fun l -> e::l) g in
          List.append (append_all pt) (append_all t)
        | None ->
          let g = gen_all tail in
          let append_all e =  match g with [] -> [[e]]
                                         | _ -> List.map (fun l -> e::l) g in
          append_all t in
    gen_all tile_indexes

end

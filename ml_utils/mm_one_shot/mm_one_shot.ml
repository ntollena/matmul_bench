open Batteries
open Common
open Execution
open Bench_types 


module Exec(A: Arch.Arch_t) = struct
  include Bench(A)
  module Pred = Arch.Pred_mm(A)
  open Codegen

  let dim_sizes tile =
    let open MM in
    let open Gen(A) in
    let ds = dim_tile_size tile in
    ds I, ds J, ds K

  let exec_scheme_size_stride benches counters_list (i, j, k) scheme  =
    let conf_gen = Config.build_mm_config ~error:(Config.EPSILON 0.001) ~num_rep:30
        (Some (true, true, Gcc,  O2))
        benches counters_list in
    let args = Config.mm_args i j k in
    exec conf_gen args (MM_tile scheme)

  let exec_scheme_size counters_list size scheme  =
    let res = exec_scheme_size_stride [Gen] counters_list size scheme MM_shape in
    List.map (fun counter -> counter, Config.select_bench_counter Gen counter res)
      counters_list

  let exec_print_counters counters_list ((i, j, k) as size) scheme =
    let open Counter in
    List.iter (fun (counter, n) -> match counter with
        | CYCLES ->
          let p = Pred.peak_percent i j k n in
          Printf.printf "%d cycles, peak perf : %.2f%%\n" n p
        | _ -> Printf.printf "%s : %d\n" (show counter) n)
      (exec_scheme_size counters_list size scheme)


  let gen_file_size postfix (i,j,k) scheme =
    let args = Config.mm_args i j k in
    let open Config in
    gen_file MM_layout args (MM_tile scheme) postfix

end

(* Example with MobileNet1 *)
let unroll_k () =
  let open Exec(Arch.Dracula) in
  let unr_scheme = MM.Tile_T.[ V J;  U (2, J); U (12, I); U (128, K); T (1, K);
                               Hoist_vars [K];  ] in
  exec_print_counters Counter.[CYCLES] (dim_sizes unr_scheme) unr_scheme

let vec_size = 8


let build_microk i j k =
  let open Exec(Arch.Dracula) in
  let pp = Pred.peak_perf i j k  in
  Printf.printf "peak perf : %d cyc\n" pp;
  let open Exec(Arch.Dracula) in
  MM.Tile_T.[V J; U (j, J); U (i, I);
              T (k, K);
             (*Hoist_vars [c_dim]*)]

(* Example with our best microkernel *)
let microk () =
  let open Exec(Arch.Dracula) in
  let microkern = build_microk 2 128 6 in
  exec_print_counters Counter.[CYCLES]
    (dim_sizes microkern) microkern

let exec_scheme_with_counters scheme = 
  let open Exec(Arch.Dracula) in
  exec_scheme_size_stride [Gen] Counter.[CYCLES; TOT_INS; LST_INS; RES_STL] (dim_sizes scheme)
    scheme MM_shape
  |>  Config.show_results
  |> print_endline

let () = unroll_k ()
(*

let () =
  print_endline "\ny = 5";
  exec_scheme_with_counters @@ build_microk 2 256 ~h_size:1 5;
  print_endline "\ny = 6";
  exec_scheme_with_counters @@ build_microk 2 256 ~h_size:1 6
*)

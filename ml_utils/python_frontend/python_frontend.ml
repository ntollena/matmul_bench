open Python_lib
open Python_lib.Let_syntax
open Common
open Search
module CST = Conv_search_type


(* Structure of the return_type:
  Outer "list" : candidates
  Inner "list" : 1 or 2 elements, depending on single or combination of µkernel
  Then:
  - microkernel unroll sizes (ex: "(Y,1), (F,4), (X,2)")
  - sizes of the tensorized code (fragment of code, included in the whole problem), same format
  - "height", aka length of the lambda dimension (Y), times the number of repetitions
  - C tensorized code (for TVM)
*)
type return_type =
 ((CST.dim * int) list * (CST.dim * int) list * int * string) list list [@@deriving python]

module S(Ainfo : CST.Arch_info_t) = struct
  open Search.Schemes(Ainfo)
  open Execution
  module A = Ainfo.A
  module B = Execution.Bench(Ainfo.A)
  (*module Tile = Tile *)
  module Pred_conv = Arch.Pred_conv(Ainfo.A)

  open Streaming
  type select_mode = Rand of int
                   | Sorted of int
                   | All

  let gen_rand_seed () =
    Random.self_init ();
    Random.bits ()

  let gen_schemes_tvm_style perm size num_candidates =
    gen_all perm size
    |> (match num_candidates with
        | Sorted num_candidates  -> select_mixed 50 num_candidates 40
        | Rand num_candidates -> random_select (gen_rand_seed ()) num_candidates
        | All -> Fun.id)
    |> Stream.of_list
    |> Stream.map to_tvm

  let gen_code englobing_output englobing_input englobing_params (f',c',y',x',h',w') stride scheme =
    let args = match stride with None -> Config.conv_args ~englobing_output ~englobing_input ~englobing_params
                                           x' w' y' h' c' f'
                               |Some (sx, sy) ->
                                 Config.conv_args_stride ~englobing_output ~englobing_input ~englobing_params
                                   x' w' y' h' c' f' sx sy in
    let layout = Config.Conv_layout (XYF, XYC, WHCF) in
    B.gen_code layout args (Conv_tile scheme)

  let get_mode num_candidates =
    function "random" -> Rand num_candidates
           | "metric" -> Sorted num_candidates
           | _ -> failwith "Invalid search mode. accepted values : random|metric"

  let gen_tvm_schemes search_mode num_candidates ((f,c,_,x,h,w) as adapted_size) stride =
    gen_schemes_tvm_style None adapted_size (get_mode num_candidates search_mode)
    |> Stream.map (
      List.map (fun (l_out, l_tens, size_y, scheme) ->
          let size d = List.assoc d l_tens in
          let sizes = Conv_search_type.(size F, size C, size Y, size X, size H, size W) in
          let code_str = gen_code (f,c,size_y,x,h,w) adapted_size adapted_size sizes stride scheme Conv_shape in
          l_out, l_tens, size_y, code_str)
    )
    |> Stream.into Sink.list
    |> python_of_return_type

  (* =============== *)

  let gen_random_from_tree_tvm_style num_cands o_nthread adapted_size =
    let l_rand_schemes = gen_random_from_tree adapted_size num_cands o_nthread in
    
    (* Add the scheme separation between l1, l2, l3 and ram to be compatible with to_tvm *)
    let l_rand_schemes_dummyl1l2l3ramlist = List.map (fun (uk_class, choice_uk, scheme) ->
      (uk_class, choice_uk, (scheme, [], [], []))
    ) l_rand_schemes in
    l_rand_schemes_dummyl1l2l3ramlist
      |> Stream.of_list
      |> Stream.map to_tvm


  (* num_candidate: number of candidate asked
     o_nthread: int option. If "None", then normal random algo. If "Some nthread", then try to
      book some parallel iteration at the outermost level (exactly nthread,
        or if div does not work at least 4*nthread, or all parallel iteration if that fails)
     adapted_size/stride: problem sizes
  *)
  let gen_tvm_random_tree_schemes num_candidates o_nthread ((f,c,_,x,h,w) as adapted_size) stride =
    gen_random_from_tree_tvm_style num_candidates o_nthread adapted_size
    |> Stream.map (
      List.map (fun (l_out, l_tens, size_y, scheme) ->
          let size d = List.assoc d l_tens in
          let sizes = Conv_search_type.(size F, size C, size Y, size X, size H, size W) in
          let code_str = gen_code (f,c,size_y,x,h,w) adapted_size adapted_size sizes stride scheme Conv_shape in
          (l_out, l_tens, size_y, code_str)
      )
    )
    |> Stream.into Sink.list
    |> python_of_return_type

end

(* =============================== *)

let arch_of_string : string -> (module CST.Arch_info_t) =
  function
  | "skylake" -> (module Arch_info.Skylake_info)
  | "xeon" -> (module Arch_info.Xeon)
  | "broadwell" -> (module Arch_info.Broadwell)
  | "E52630" -> (module Arch_info.E52630)
  | "silver" -> (module Arch_info.Silver_info)
  | "chifflet" -> (module Arch_info.Chifflet)
  | "pinocchio" -> (module Arch_info.Pinocchio)
  | "pinocchio_extended" -> (module Arch_info.Pinocchio_extended)
  | _-> failwith "Python frontend : Unknown architecture. accepted values :skylake|xeon|broadwell|E52630|silver|chifflet|pinocchio|pinocchio_extended"

let gen_tvm_schemes (module A:CST.Arch_info_t) num_cand size stride =
  let open S(A) in
  gen_tvm_schemes num_cand size stride

(* Random algo above the microkernel *)
let gen_tvm_random_tree_schemes (module A:CST.Arch_info_t) num_cand size stride =
  let open S(A) in
  gen_tvm_random_tree_schemes num_cand None size stride

(* Random algo, with parallel iteration reservation *)
let gen_tvm_random_par_tree_schemes (module A:CST.Arch_info_t) num_cand nthread size stride =
  let open S(A) in
  gen_tvm_random_tree_schemes num_cand (Some nthread) size stride


(* =============================== *)
(* === Python interfacing === *)

let gen_py_scheme () =
  let%map_open arch = positional ~docstring:"" "architecture" string
  and search_mode = positional ~docstring:"" "search_mode" string
  and num_cand = positional ~docstring:"" "num_candidate" int
  and f = positional ~docstring:"" "f" int
  and c = positional ~docstring:"" "c" int
  and y = positional ~docstring:"" "y" int
  and x = positional ~docstring:"" "x" int
  and h = positional ~docstring:"" "h" int
  and w = positional ~docstring:"" "w" int
  and stride = positional ~docstring:"" "stride" int in
  gen_tvm_schemes (arch_of_string arch) search_mode num_cand (f,c,y,x,h,w) (Some (stride, stride))


let gen_py_random_tree_scheme () =
  let%map_open arch = positional ~docstring:"" "architecture" string
  and num_cand = positional ~docstring:"" "num_candidate" int
  and f = positional ~docstring:"" "f" int
  and c = positional ~docstring:"" "c" int
  and y = positional ~docstring:"" "y" int
  and x = positional ~docstring:"" "x" int
  and h = positional ~docstring:"" "h" int
  and w = positional ~docstring:"" "w" int
  and stride = positional ~docstring:"" "stride" int in
  gen_tvm_random_tree_schemes (arch_of_string arch) num_cand
        (f,c,y,x,h,w) (Some (stride, stride))

let gen_py_random_par_tree_scheme () =
  let%map_open arch = positional ~docstring:"" "architecture" string
  and num_cand = positional ~docstring:"" "num_candidate" int
  and nthread = positional ~docstring:"" "nthread" int
  and f = positional ~docstring:"" "f" int
  and c = positional ~docstring:"" "c" int
  and y = positional ~docstring:"" "y" int
  and x = positional ~docstring:"" "x" int
  and h = positional ~docstring:"" "h" int
  and w = positional ~docstring:"" "w" int
  and stride = positional ~docstring:"" "stride" int in
  gen_tvm_random_par_tree_schemes (arch_of_string arch) num_cand nthread
        (f,c,y,x,h,w) (Some (stride, stride))


let () =
  if not (Py.is_initialized ()) then Py.initialize ();
  let mod_ = Py_module.create "ttile" in
  Py_module.set mod_ "gen_schemes"
    ~docstring:"Generate schemes from arch and convolution description" (gen_py_scheme ());
  Py_module.set mod_ "gen_random_tree_scheme"
    ~docstring:"Randomly create a scheme from from arch and convolution description.\nNo permutation used from Ioopt"
    (gen_py_random_tree_scheme ());
  Py_module.set mod_ "gen_random_par_tree_scheme"
    ~docstring:"Randomly create a scheme from from arch and convolution description.\nNo permutation used from Ioopt, parallel reservation"
    (gen_py_random_par_tree_scheme ())


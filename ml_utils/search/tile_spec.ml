open Batteries
open Common
open Conv_search_type

module type Dim_Tile_t = sig
  type tile_choice [@@deriving show, eq, ord]
  type tile_state [@@deriving show, eq, ord]
  val choose: tile_state -> (dim * tile_choice) list option
  val choose_and_update: tile_state -> (dim * tile_choice * tile_state) list option
  val choice_size: tile_choice -> int
  val from_sizes : (dim * int) list -> tile_state
  val footprint: tile_state -> int
  val to_fp_tile: (dim * tile_choice) list -> ((dim * Footprint.fp) list, (dim * Footprint.fp) list)  Either.t
  val data_volume: tile_state -> (dim * tile_choice) -> int
  val update_state: tile_state -> dim -> tile_choice -> tile_state
end

type simple_choice = int [@@deriving show, eq, ord]

type simple_state = {original_size: conv_size; current_tile_size: conv_size} 
  [@@deriving show, eq, ord]

module type Single_tile_t = sig
  include Dim_Tile_t with type tile_choice = int
end

module Single_tile(UKSize: sig val base_kern : (dim * int) list end): Single_tile_t
= struct
  open Conv_search_type
  type tile_choice = simple_choice [@@deriving show, eq, ord]

  type tile_state = simple_state [@@deriving show, eq, ord]

  let from_sizes original =
    let from_list l = {f = List.assoc F l; c = List.assoc C l;
                       x = List.assoc X l; y = List.assoc Y l;
                       w =List.assoc W l; h = List.assoc H l;} in
    {original_size = from_list original; current_tile_size = from_list UKSize.base_kern}

  let update_tile ({current_tile_size;_ } as size) d s =
    {size with current_tile_size = update_size_op ( * ) current_tile_size d s}

  let update_state size d c =
    update_tile size d c

  let choice_size = Fun.id

  let remaining {original_size; current_tile_size;} = 
      List.map (fun d -> d, assoc d original_size / assoc d current_tile_size) all_of_dim

  let choose_and_update state =
    let remaining = remaining state in
    if List.for_all ((=) 1 % snd) remaining then None
    else Some (List.concat_map (fun (dim, r) -> 
        let multiples = Common.Arithmetic.all_divisor r
                        |> List.filter ((<>) 1) in
        List.map (fun m -> dim, m, update_tile state dim m) multiples) remaining)

  let choose state =
    choose_and_update state
    |> Option.map @@ List.map (fun (a,b,_) -> a, b)

  let footprint {current_tile_size;_} = 
    Footprint.global @@ Footprint.fp_compute @@ dict_from_struct current_tile_size

  let to_fp_tile l = Either.Left (List.map (Utils.map_snd (fun x -> Footprint.Tile x)) l) 

  let data_volume ({ current_tile_size;_} as state) (dim, s) =
    let remaining = remaining state in
    Footprint.compute_volume (dict_from_struct current_tile_size) (Some (dim, s, remaining))
end

type double_choice = Merge | T of int [@@deriving show, eq, ord]

type double_state' = Merged
                   | Split [@@deriving show, eq, ord]

type double_state = {original_size: conv_size; current_state: double_state'
                    ;current_tile_size: conv_size}
  [@@deriving show, eq, ord]

module type Lambda_t = sig
  include Dim_Tile_t with type tile_choice = double_choice
  val to_fp : (dim * double_choice) list -> (dim * Footprint.fp) list
  val complete_candidate: (dim * int) list -> (dim * double_choice) list
    -> (dim * double_choice) list
end 

module Lambda_tile(UKsize: sig
    val var_dim : dim
    val u1: int
    val i1: int
    val u2: int
    val i2: int
    val base_kern : (dim * int) list
  end) : Lambda_t = struct
  open Conv_search_type

  let to_fp_tile l =
    let open UKsize in
    Either.Right (List.map (Common.Utils.map_snd @@
                 function Merge ->  Footprint.Merge (i1, i2)
                        | T s -> Footprint.Tile s) l)
  let to_fp l =
    let open UKsize in
    (var_dim, Footprint.Split (u1, u2))
    :: List.map (Common.Utils.map_snd @@
                 function Merge ->  Footprint.Merge (i1, i2)
                        | T s -> Footprint.Tile s) l


  type tile_state = double_state [@@deriving show, eq, ord]
  type tile_choice = double_choice [@@deriving show, eq, ord]

  let choice_size = function T s -> s | Merge -> UKsize.(i1 + i2)

  let from_sizes original =
    let from_list l = {f = List.assoc F l; c = List.assoc C l;
                       x = List.assoc X l; y =  List.assoc Y l;
                       w =List.assoc W l; h = List.assoc H l;} in
    let original_size =(let csize = from_list original in
                     update_size_op (fun s () -> s / UKsize.(i1 * u1 + i2 * u2))
                                                 csize UKsize.var_dim ()) in
    {original_size = original_size ;
     current_state = Split;
     current_tile_size =
        (from_list @@ UKsize.(var_dim, 1) :: UKsize.base_kern)}

  let update_tile ({current_tile_size; current_state; _ } as size) d tile_choice =
    let update_csize csize d s = update_size_op ( * ) csize d s in
    let updated_state, updated_tile = match current_state, current_tile_size, tile_choice with
        Merged, conv_size, T s -> Merged, update_csize conv_size d s
      | Split, conv_size, T s ->
        Split, (update_csize conv_size d s)
      | Split, csize, Merge ->
        Merged, csize 
      | Merged, _, Merge -> failwith "Tile was Merged twice"
    in
    {size with current_state = updated_state; current_tile_size = updated_tile }

  let update_state d =
    update_tile d

  let choose_and_update ({original_size; current_state; current_tile_size;} as
                         state)  =
    let remaining tile =
      List.map (fun d -> d, assoc d original_size / assoc d tile)
      @@ all_of_dim in
    let gen_multiple_choices csize =
      List.concat_map (fun (dim, r) -> 
          let multiples = Common.Arithmetic.all_divisor r
                          |> List.filter ((<>) 1) in
          List.map (fun m -> dim, T m, update_state state dim (T m)) multiples) @@ remaining csize in
    match current_state with
    | Merged when List.for_all ((=) 1 % snd) @@ remaining current_tile_size ->
      None
    | Merged ->
      Some (gen_multiple_choices current_tile_size)
    | Split ->
      Some ((UKsize.var_dim, Merge, update_state state UKsize.var_dim Merge) :: gen_multiple_choices current_tile_size)

  let choose state = Option.map (List.map (fun (a,b,_) -> a, b)) @@ choose_and_update state 

  let update_merged cs = update_size (fun y -> y * UKsize.(i1 * u1 + i2 * u2)) cs UKsize.var_dim 

  let footprint {current_tile_size; current_state;_} =
    let fp csize = Footprint.global @@ Footprint.fp_compute @@ dict_from_struct csize in
    match current_state, current_tile_size with
    Merged, csize ->
    fp @@
    (List.fold_left (fun csize (d, s) -> update_size (( * ) s) csize d) csize UKsize.base_kern
    |> update_merged)
    | Split, csize ->
      let update_fp_micro u =
        update_size (( * ) u) csize UKsize.var_dim in
      max (fp @@ update_fp_micro UKsize.u1) (fp @@ update_fp_micro UKsize.u2)

  let complete_candidate original_size cand =
    let remaining_tiles original conv_size = List.filter_map (fun d ->
           let size = List.assoc d original / assoc d conv_size  in
           if size = 1 || size = 0 then None
           else Some (d, T size)) all_of_dim in
     List.fold_left (fun s (d, c) -> update_state s d c) (from_sizes original_size) cand
     |> function {current_state=Merged; current_tile_size= conv_size;_} ->
       remaining_tiles original_size conv_size
               | {current_state=Split; current_tile_size=  conv_size;_} ->
                 let remaining = remaining_tiles original_size conv_size in
                 remaining @ [UKsize.var_dim, Merge]

  let remaining original_size current_tile_size = 
      List.map (fun d -> d, assoc d original_size / assoc d current_tile_size) all_of_dim

  let data_volume {current_tile_size; current_state; original_size;_} (dim, s) =
    match current_state, current_tile_size, s with
    | Merged, current_tile_size, T s ->
      let updated_size =  update_size (( * ) s) (update_merged current_tile_size)  dim in
      let remaining = remaining updated_size original_size in
    Footprint.compute_volume (dict_from_struct current_tile_size) (Some (dim, s, remaining))
    | Split, current_tile_size, T s ->
      let update u =  update_size (( * ) u) current_tile_size UKsize.var_dim in
      let updated_size1 =  update UKsize.u1
      and updated_size2 =  update UKsize.u2 in
      let r1 = remaining updated_size1 original_size
      and r2 =  remaining updated_size2 original_size in
      let fp1 =
        Footprint.compute_volume
          (dict_from_struct updated_size1) (Some (dim, s, r1)) 
      and fp2 =
        Footprint.compute_volume
          (dict_from_struct updated_size2) (Some (dim, s, r2))  in
      fp1 + fp2
    | Split, current_tile_size, Merge ->
      let update u =  update_size (( * ) u) current_tile_size UKsize.var_dim in
      let updated_size1 =  update UKsize.u1
      and updated_size2 =  update UKsize.u2 in
      let complete = update_size (( * ) UKsize.(i1 * u1 + i2 * u2) )
          current_tile_size UKsize.var_dim in
      let remaining = remaining complete original_size in
      let fp1 = Footprint.compute_volume
          (dict_from_struct updated_size1) (Some (dim, UKsize.i1, remaining)) 
      and fp2 = Footprint.compute_volume
          (dict_from_struct updated_size2) (Some (dim, UKsize.i2, remaining))  in
      fp1 + fp2
    | Merged, _, Merge -> assert false

end

module Partial_tile(UK: sig
    val var_dim: dim
    val name : string
    val fixed_size : (dim * int) list (* list of fixed sizes *)
    val max_range : int
  end) = struct
  type tile_choice = int [@@deriving eq, ord, show]
  type tile_state = {size: conv_size; remaining: (dim * int list) list}
  [@@deriving eq, ord, show]

  let gen_rand_choice seed size =
    if size < 5 then [size]
    else let () = Random.init seed in
      let num_tiles = Random.int 4 in
      let tiles = List.init num_tiles (fun _ -> Random.int size)
                  |> List.unique
                  |> List.filter ( fun x -> x <> 1 && x <> 0 )
                  |> List.sort Int.compare in
      tiles @ [size]

  let compute_remaining seed sizes =
    List.map (Utils.map_snd @@ gen_rand_choice seed)
         @@ List.map (fun (d,s) -> match d, List.assoc_opt d UK.fixed_size with
        | C, _ -> C, if s <= 256 then 1 else s / 256
        | _, None -> d, s / 12 + 1
        | _, Some fixed -> d, (if s mod fixed <> 0 then Printf.printf
                                   "%s, %d; fixed %d\n"
                                   (show_dim d) s fixed; assert (s mod fixed = 0); s / fixed)
         ) sizes 

  let from_sizes_rand seed sizes =
    {size = struct_from_dict sizes;
     remaining = compute_remaining seed sizes}

  let from_sizes size =
    from_sizes_rand 0 size

  let choice_size  = Fun.id

  let choose_and_update {remaining; size} =
    List.filter_map (function (d, c :: t) ->
        Some (d, c, {size; remaining = List.modify d (fun _ -> t) remaining})
                            | (_, []) -> None) remaining
    |> function [] -> None | l -> Some l

  let choose {remaining;_} =
    List.filter_map (fun (d, ltile) ->
        Option.map (fun c -> d, c) @@ List.Exceptionless.hd ltile) remaining
    |> function [] -> None | l -> Some l

  let update_state ({remaining; _} as state) dim _ =
    {state with remaining = List.modify dim (List.tl) remaining}

  let data_volume _ = assert false
  let footprint _ = assert false
  let to_fp_tile _ = assert false

  let to_ttile size choices = let open Execution.Conv.ConvTypes in
    let mark_first p update l = 
    List.fold_left_map (fun seen elem ->
        if  p seen elem
        then  seen, (elem, false)
        else update elem seen, (elem, true)
      ) [] l |> snd in
    let mark_first_and_last_on_dim =
      mark_first (fun seen (d, _) -> List.mem d seen)
        (List.cons % fst) choices
      |> List.rev
      |> mark_first (fun seen ((d, _), _) -> List.mem d seen)
        (fun ((d, _), _) seen -> d ::seen)
      |> List.rev_map (fun (((d, c), b1), b2) -> d, c, b1, b2) in

    let open UK in
    let adapt_size d s = match List.assoc_opt d UK.fixed_size with
      | Some fixed -> s * fixed
      | None -> s * max_range in
    let c_size = min 256 (List.assoc C size) in
    External_call {name; fixed_size; var_dim; max_range; dynamic_sizes = [C, c_size]}
    :: List.map (fun (d, c, b1, b2) -> match b1, b2 with
          true, true -> if equal_dim d UK.var_dim then
             Tile_exact (List.assoc var_dim size, d)
            else T (c, d)
        | true, false ->
           if equal_dim d UK.var_dim then Tile_gexact (List.assoc var_dim size, d)
          else Tile_partial (adapt_size d c, d)
        | false, true -> Tile_exact (List.assoc d size, d)
        | false, false -> Tile_gexact (adapt_size d c, d)
      ) mark_first_and_last_on_dim
end

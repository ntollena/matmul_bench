open Common
open Batteries
(* Bidule pour calculer footprint and co *)
type dim = Execution.Conv.dim = F | C | X | Y | W | H  [@@deriving eq, ord, enumerate, yojson, show {with_path = false}]

let l_parallel_dim = [F; X; Y]
let l_reduction_dim = [C; W; H]

let python_of_dim = function
  | F -> Py.String.of_string "F"
  | C -> Py.String.of_string "C"
  | Y -> Py.String.of_string "Y"
  | X -> Py.String.of_string "X"
  | H -> Py.String.of_string "H"
  | W -> Py.String.of_string "W"

let dim_from_string = function
  "F" -> F | "C" -> C | "Y" -> Y | "X" -> X | "H" -> H | "W" -> W
  | _ -> failwith "Not a dimension"

let string_from_dim = function
  F -> "F" | C -> "C" | Y -> "Y" | X -> "X" | H -> "H" | W -> "W"

let dim_of_python  =
   dim_from_string % Py.String.to_string

let kernel_dim_order = [F; Y; X; H; W; C] 

type conv_size = {f: int; c : int; x : int; y : int; w : int; h : int}
[@@deriving show {with_path =false}, eq, ord]

let struct_from_dict l =
  let assoc d = List.assoc d l in
  {f = assoc F; c = assoc C; x = assoc X; y = assoc Y; w = assoc W; h = assoc H}

let dict_from_struct {f;c;x;y;w;h} =
  [F, f; C, c; X, x; Y, y; W, w; H, h]

let struct_from_tuple (f,c,x,y,w,h) = {f;c;x;y;w;h}

let tuple_from_struct  {f;c;x;y;w;h} = (f,c,x,y,w,h)

let dict_from_tuple = dict_from_struct % struct_from_tuple

let tuple_from_dict = tuple_from_struct % struct_from_dict

let show_tuple  = show_conv_size % struct_from_tuple
let show_dict  = show_conv_size % struct_from_dict

let update_size fn ({f; c; x; y; w; h} as size) d = match d with
  | F -> {size with f = fn f }
  | C -> {size with c = fn c }
  | X -> {size with x = fn x }
  | Y -> {size with y = fn y }
  | W -> {size with w = fn w }
  | H -> {size with h = fn h }

let update_size_op op size d s =
  update_size (fun m -> op m s) size d

let assoc d {f;c;x;y;w;h} = match d with
  | F -> f
  | C -> c
  | X -> x
  | Y -> y
  | W -> w
  | H -> h

(* Type for a class of microkernel *)
type kernel_spec =
  {varying_dim : dim;               (* Dimension that changes in the class of microkernel (usually X) *)
   range : int * int;               (* Range of the dimension (min, max) *)
   base_kern : (dim * int) list;    (* Sizes of the dimensions which are not the varying dims *)
   kern_order : dim list;           (* Permutation of the unrolls inside the microkernel. Should not be used *)
  } [@@deriving show {with_path = false}, eq, yojson]

(* Get a "representative" ukernel from a class *)
let kernel_repr sizes {varying_dim; range; base_kern;_} =
  let all_not_one = List.filter (fun (_, size) -> size <> 1) base_kern
  and median = let (min, max) = range in
    Stdlib.min (List.assoc varying_dim sizes) ((max + min) / 2) in
  (varying_dim, median) :: all_not_one


module type Arch_info_t = sig
  val l1_size: int
  val l2_size: int
  val l3_size: int
  val l1_bw: float
  val l2_bw: float
  val l3_bw: float
  val ukernels : kernel_spec list
  module A : Arch.Arch_t
end

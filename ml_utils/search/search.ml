open Batteries
open Common
open Utils

open Conv_search_type
open Footprint
include Modules_decl_search

(* Warning ! Activate only for reproductibility. For experiments, leave that to False *)
let deterministic_random_debug_mode = false


(* Warning: all search functions are specialized for convolution *)

module S(A_info : Arch_info_t) = struct

  let split_footprint =
    scanl

  let gen_perm dim_split (perm : dim list) (f,c,y,x,h,w)  =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    let size d = List.assoc d pair_sizes in
    let scheme_dim d = match dim_split with
      | Some (dim, a, b) when equal_dim dim d ->
        let count_d = List.length (List.filter (equal_dim d) perm) - 1 in
        Arithmetic.decompose (size d) count_d
        |> List.concat_map (insert_anywhere (Merge (a, b)) % (List.map (fun x -> Tile x)))
        |> List.map (fun l -> d, l)
      | Some _
      | None ->
        let count_d = List.length @@ List.filter (equal_dim d) perm in
        List.map (fun l -> d, List.map (fun x -> Tile x) l) @@ Arithmetic.decompose (size d) count_d
    in
    let build_scheme perm decomp_list =
      let rec build_scheme acc perm decomp_list = match perm with
        | d::t ->
          let (next_f, other_f), dcmp = remove_map
              (fun (d', l) -> if equal_dim d d'
                then match l with
                    h::t -> Some (h, t)
                  | [] -> failwith "Decomposition is invalid"
                else  None
              ) decomp_list in
          build_scheme ((d, next_f)::acc) t ((d, other_f)::dcmp)
        | [] -> List.rev acc in
      build_scheme [] perm decomp_list in
    List.map (build_scheme perm) (cartesian @@ List.map scheme_dim all_of_dim)

  let filter_scheme
      perm_l1 perm_l2 perm_l3 perm_mem
      {varying_dim; base_kern;_} parameters (f,c,y,x,h,w) =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    let map_size d = match List.assoc_opt d base_kern with
      | Some kern_s -> let size = List.assoc d pair_sizes in
        if (size mod kern_s <> 0)
        then None
        else Some (size / kern_s)
      | None ->
        assert (equal_dim d varying_dim);
        let size = List.assoc d pair_sizes in
        let open Either in
        begin match parameters with
          | Left f ->
            if (size mod f <> 0)  then
              None else Some (size / f)
          | Right (a, u, b, v) ->
            let f = a * u + b * v in
            if (size mod f <> 0)  then
              None
            else  Some (size / f)
        end in
    let sizes = List.fold_right (fun d acc ->
        Option.bind acc (fun l -> Option.map (fun x -> (d, x)::l) (map_size d)))
        all_of_dim (Some []) in
    match sizes with
      None -> []
    | Some sizes ->
      let _tvarying, dim_split = match parameters with
          Right (iter1, arg1, iter2, arg2) ->
          Split(arg1, arg2), Some (varying_dim, iter1, iter2)
        | Left f -> Tile f, None in
      let resplit_scheme scheme =
        let l1_perm, remain = List.takedrop (List.length perm_l1) scheme in
        let l2_perm, remain = List.takedrop (List.length perm_l2) remain in
        let l3_perm, mem = List.takedrop (List.length perm_l3) remain in
        l1_perm, l2_perm, l3_perm, mem in
      let get d = List.assoc d sizes in
      gen_perm  dim_split (perm_l1 @ perm_l2 @ perm_l3 @ perm_mem)
        (get F, get C, get Y, get X, get H, get W)
      |> List.map resplit_scheme


  let check_ukernel pair_sizes
      {varying_dim; base_kern; range = rmin, _;  _}  =
    let check_size (dim, size) =
      if equal_dim dim varying_dim then
        size >= rmin
      else
        size >= List.assoc dim base_kern  in
    List.for_all check_size pair_sizes

  let gen_perm_opt cache_opt sizes ukern =
    let open Ioopt in
    Option.map_default_delayed
      (fun perm_cache -> ioopt_permutation_cached perm_cache (module A_info) sizes ukern)
      (fun () -> ioopt_permutation (module A_info) sizes ukern)
      cache_opt

  let (let*) o f = Option.bind o f
  let (let+) o f = Option.map f o

  (* Find all permutations *)
  let gen_all_perm_sizes specs =
    let open Ioopt in
    let open Conv_spec in
    List.cartesian_product specs A_info.ukernels
    |> Parmap.(fun l -> L l)
    |> Parmap.parmap (fun ({ adapted_size;_}, ukern) ->
        let* params = build_ioopt_params (module A_info) adapted_size ukern in
        let* perm = ioopt_permutation (module A_info) adapted_size ukern in
        Some (params, perm )
      )
    |> List.filter_map Fun.id

  let read_perm_from_file filename =
    let open Ioopt in
    read_from_file filename
    |> print_endline % [%show: (ioopt_params * perm) list]

  let gen_all_perms ukernels ((f,c,y,x,h,w) as sizes) =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    ukernels
    |> List.filter (check_ukernel pair_sizes)
    |> List.map (fun ukernel -> ukernel, gen_perm_opt (Ioopt.cache_file_path ()) sizes ukernel)


  (* Given:
    * cache_opt = None or Some (path to the Ioopt cache)
    * ukernels = list of good microkernel classes
    * sizes = convolution sizes
    * perms = None or Some (fixed permutation, decomposed into (l1,l2,l3,mem) )
    Returns the info on a scheme that uses a single microkernel (same output than "Schemes.gen_all")
  *)
  let gen_schemes cache_opt ukernels sizes perms =
    ukernels
    (* Get the permutation of indexes on top of the µkernel *)
    |> List.filter_map (*(check_ukernel pair_sizes)*)
      (fun ukern ->
         match perms with
           Some perms -> Some (ukern, perms)
         | None ->
           let+ perms = gen_perm_opt cache_opt sizes ukern in
           ukern, perms
      )
    (* Get the sizes of the tiles above the microkernel*)
    |> List.concat_map
      ( fun ({range = rmin, rmax; _} as ukern,
             (perm_l1, perm_l2, perm_l3, perm_mem)) ->
        range rmin rmax
        |> List.concat_map
          (fun ds ->
             let parameters = Either.Left ds in
             filter_scheme
               perm_l1 perm_l2 perm_l3 perm_mem
               ukern parameters sizes
             |> List.map (fun l -> ukern, parameters, l)
          )
      )

  let intersect eq l1 l2 = List.filter (fun x -> List.exists (eq x) l2) l1


  (* List all the microkernel in a class that divides the problem size *)
  let gen_ukern_single {varying_dim; base_kern; range = rmin, rmax;_} (f,c,y,x,h,w) =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    let var_dim_size = List.assoc varying_dim pair_sizes in
    if List.exists (fun (d, s) -> List.assoc d pair_sizes mod s <> 0) base_kern then []
    else List.filter (fun s -> var_dim_size mod s = 0) @@ range rmin rmax

  (* List all the combination of microkernel in a class that divides the problem size *)
  let gen_ukern_pair {varying_dim; range = rmin, rmax;base_kern;_} (f,c,y,x,h,w) =
    let pair_sizes = [F, f; C, c; Y, y; X, x; H, h; W, w] in
    if List.exists (fun (d, s) -> List.assoc d pair_sizes mod s <> 0) base_kern then []
        else let var_divisors var_dim = Arithmetic.all_divisor @@ List.assoc var_dim pair_sizes in
    range rmin rmax
    |> ordered_pair
    |>  List.filter_map
      (fun (arg1, arg2) ->
         List.find_map_opt (Arithmetic.pair_dec arg1 arg2)
           (var_divisors varying_dim)
         |> Option.map @@ fun (iter1, iter2) ->
         (* iter1 * arg1 + iter2 * arg2 | y *)
         (iter1, arg1, iter2, arg2)
      )

  let gen_pair cache_opt ukernels ((f,c,y,x,h,w) as sizes) perms  =
    ukernels
    |> List.filter_map (*(check_ukernel pair_sizes)*)
      (fun ukern ->
         match perms with
           Some perms -> Some (ukern, perms)
         | None ->
           let+ perms = gen_perm_opt cache_opt sizes ukern in
           ukern, perms
      )
    |> List.concat_map (
      fun ( ukern, (perm_l1, perm_l2, perm_l3, perm_mem)) ->
        gen_ukern_pair ukern sizes
        |> List.concat_map (fun (iter1, arg1, iter2, arg2) ->
            let parameters = Either.Right (iter1, arg1, iter2, arg2) in
            filter_scheme
              perm_l1 perm_l2 perm_l3 perm_mem
              ukern parameters (f, c, y, x, h, w)
            |> List.map (fun l -> ukern, parameters, l)
          )
    )

  let data_volumes_l1_l2_l3 ({varying_dim; kern_order;  base_kern;_}, parameters,
                             (l1, l2, l3, mem)) =
    let build_ukernel d =
      if equal_dim varying_dim d then
        match parameters with
          Right (_iter1, arg1, _iter2, arg2) ->
          d, Split (arg1, arg2)
        | Left f -> (d, Tile f)
      else (d,  Tile (List.assoc d base_kern)) in
    let ukernel = List.map build_ukernel kern_order in
    let scheme =
      let scheme = ukernel @ l1 @ l2 @ l3 @ mem in
      match parameters with
        Left _ -> Left scheme
      | Right _ -> Right scheme in
    let cache_volume lsize =
      footprint_split scheme |> data_volume_cache_level lsize  in
    cache_volume (floats_to_cache_line A_info.l1_size),
    cache_volume (floats_to_cache_line A_info.l2_size),
    cache_volume (floats_to_cache_line A_info.l3_size)

  let data_volume_weighted implem =
    let l1, l2, l3 = data_volumes_l1_l2_l3 implem in
    let foi = float_of_int in
    Float.(foi l1 / A_info.l1_bw + foi l2 / A_info.l2_bw + foi l3 / A_info.l3_bw)



  (* Get kernel occupation *)
  let cache_occupation ({varying_dim;  base_kern; _}, parameters,
                        (l1, l2, l3, _mem)) =
    let build_ukernel d =
      if equal_dim varying_dim d then
        match parameters with
          Right (_iter1, arg1, _iter2, arg2) ->
          Some (d, Split (arg1, arg2))
        | Left f -> Some (d, Tile f)
      else match List.assoc d base_kern with
          1 -> None
        | n -> Some (d, Tile n) in
    let ukernel = List.filter_map build_ukernel all_of_dim in
    let to_l1 = ukernel @ l1 in
    let to_l2 = to_l1 @ l2 in
    let to_l3 = to_l2 @ l3 in
    let cache_occupation cache_size footprints =
      let foi = float_of_int in
      List.min_max
      @@ List.map (fun fp -> 100. *. (foi (global fp)) /. foi cache_size)
        footprints in
    let min_max_occup cache_size tile_choices =
      cache_occupation cache_size (List.last @@ tensor_footprint tile_choices)
    in
    min_max_occup A_info.l1_size to_l1,
    min_max_occup A_info.l2_size to_l2,
    min_max_occup A_info.l3_size to_l3

  let red_size red_dim (_, _,
                        (l1, l2, l3, mem)) =
    let (d, size) = List.hd l1  in
    if not (equal_dim red_dim d)
    then failwith
        (let sp = [%show: (dim * fp) list] in
         Printf.sprintf
           "Faulty permutation : Dim after microkernel is %s and not %s\n%s %s %s %s\n"
           (show_dim d) (show_dim red_dim)
           (sp l1) (sp l2) (sp l3) (sp mem)) ;
    size |> function Tile s -> s
                   | _ -> assert false

  let metric_conv scheme_spec =
    let red_size = red_size C scheme_spec
    and ((l1_min, l1_max), _, _) = cache_occupation scheme_spec in
    red_size, l1_min, l1_max

  let compare_metrics_lexi _problem_red_size scheme_spec1 scheme_spec2 =
    let goal = 120. in
    let goal_metric l1min l1max =
      Float.abs (goal -. l1min) +. Float.abs (goal -. l1max) in
    let rs1, l1min1, l1max1 = metric_conv scheme_spec1
    and rs2, l1min2, l1max2 = metric_conv scheme_spec2 in
    [%ord: int * float] (Int.neg rs1, goal_metric l1min1 l1max1)
      (Int.neg rs2, goal_metric l1min2 l1max2)

  let compare_metrics problem_red_size scheme_spec1 scheme_spec2 =
    (* rule out small reduction if possible *)
    let p rs1 rs2 =
      let maxr, minr = max rs1 rs2, min rs1 rs2 in
      if (problem_red_size >= 16) && (maxr >= 16 && minr < 16)
      (* if one of them is
         inferior to 16,
         then higher is
         better - put lowest
         score to low red *)
      then Some (Int.compare rs2 rs1)
      (* Else we decide normally *)
      else None in
    let goal = 120. in
    let goal_metric l1min l1max =
      Float.abs (goal -. l1min) +. Float.abs (goal -. l1max) in
    let rs1, l1min1, l1max1 = metric_conv scheme_spec1
    and rs2, l1min2, l1max2 = metric_conv scheme_spec2 in
    let cachem1 = goal_metric l1min1 l1max1
    and  cachem2 = goal_metric l1min2 l1max2 in
    match p rs1 rs2 with
      Some cmp -> cmp
    | None ->
      if Float.abs (cachem1 -. cachem2) >= 100. then Float.compare cachem1 cachem2
      else [%ord: int * float] (Int.neg rs1, goal_metric l1min1 l1max1)
          (Int.neg rs2, goal_metric l1min2 l1max2)
end


(* ======================================================= *)

module Schemes(A_info : Arch_info_t) = struct

  open Execution
  open S(A_info)

  let gen_all_perms = gen_all_perms

  (* Output of gen_all : (ukernel : Conv_search_type.kernel_spec , parameters, (l1, l2, l3, mem) )
    where "parameters" tells us which microkernel or combination of µkernel we use in the class "ukernel"
        Left (size) ==> single microkernel with varying dimension of the specified size
        Right (iter1, arg1, iter2, arg2) ===> combination of microkernels
    and "(l1,l2,l3,mem)" are 4 list of (dim, spec) where "spec" is Tile/Split/Merge
      (some kind of specifier, contains sizes)

    Invariants:
      - l1 start with reuse dimension (usually c)
      - Hoist_vars [c] is automatically added above this reuse dimension
      - Lambda_apply added when we have "Merge" specifier.
  *)
  let gen_all perms sizes =
    let cache_opt = Ioopt.cache_file_path () in
    gen_schemes cache_opt A_info.ukernels sizes perms
    @ gen_pair cache_opt A_info.ukernels sizes perms

  (* Random selection function, get num_candidates from a list *)
  let random_select seed num_candidates l =
    if (deterministic_random_debug_mode) then
      (* For reproducibility, set seed to a known value or dump seed value *)
      Random.init seed
    else
      Random.self_init ();
    let num_candidates = min (List.length l) num_candidates in
    Random.multi_choice num_candidates (List.enum l) 
    |> List.of_enum

  let sort_and_select c num_candidates l =
    List.sort (compare_metrics_lexi c) l
    |> List.take num_candidates

  let sort_and_select_volume num_candidates l =
    List.sort (Utils.compare_by data_volume_weighted) l
    |> List.take num_candidates

  let select_mixed min_threshold max_threshold ratio implems =
    let n_candidates = List.length implems in
    let num_select = Int.min n_candidates
        (Int.max min_threshold (ratio * n_candidates / 100)) in
    let implemsi = List.mapi (fun i c -> i, c) implems in
    let best_red = Utils.sort_by (fun (_, c) -> Int.neg @@ red_size C c) implemsi
                   |> List.take num_select in
    let best_red_idxs = List.map fst best_red in
    List.map (List.at implems) best_red_idxs
    |> Utils.sort_by data_volume_weighted
    |> List.take max_threshold

  let cache_occupation = cache_occupation

  let assoc_first e  = List.map (fun x -> e, x)

  (* Generate all the microkernel that fits a size. Left = single / Right = lambda *)
  let gen_all_uk size =
    let open Tree_search in
    let singles = List.concat_map (fun uk ->
        gen_ukern_single uk size
        |> List.map @@ fun factor -> {base=uk; factor}
      ) A_info.ukernels
      |> List.map (Either.left)
    and doubles = List.concat_map (fun uk ->
        List.map (fun (i1, u1, i2, u2) -> {base=uk;  i1; u1; i2; u2})
        @@  gen_ukern_pair uk size
      ) A_info.ukernels
      |> List.map (Either.right)
    in
    singles @ doubles

  let gen_all_candidates size =
    let open Tree_search in
    let all_kernels = gen_all_uk size in
    Tree_search.all_candidates_sorted_lexi (dict_from_tuple size) all_kernels
    |> List.concat
    |> List.map (function
      | L (Simple, ({base; factor;_} as uk), lcands, _) ->
        let l1, l2,l3, mem =
          complete_candidate (struct_from_tuple size) (base_from_sing uk)  lcands in
        let to_tile = List.map (fun (d, s) -> d, Tile s) in
        base, Left factor,
        (to_tile @@ l1 @ l2 @ l3 @ mem)
      | L (Double, {base= {varying_dim; base_kern;_} as base;
                    u1; i1; u2 ;i2;}, c, _) ->
        let l1, l2,l3, mem =
          complete_candidate_pair (dict_from_tuple size) varying_dim i1 u1 i2 u2 base_kern c in
        let to_tile = List.map (map_snd @@ function  Tile_spec.T s   -> Tile s
                                                   | Tile_spec.Merge -> Merge (i1, i2)) in
        base, Right (i1, u1, i2, u2),
        (to_tile @@ l1 @ l2 @ l3 @ mem)
    )

  (* Pure random algorithm - level per level *)
  (* ukernopt : None = do the normal selection of µkernel (single + lambda)
      Some luker = restrict ourselves to these microkernels (they must divide the problem size!)
  *)
  let gen_random_from_tree_aux ukernopt size num_cands o_nthread =
    let open Tree_search in
    
    (* Microkernel selection, plus all level above *)
    let all_kernels = match ukernopt with
      | None -> gen_all_uk size
      | Some luker_couple ->
        let luker = List.map (fun (uk_class, y_val) ->
          Either.Left (Tree_search.sing_from_uk uk_class y_val)
        ) luker_couple in
        luker
    in

    Tree_search.random_candidates_from_uk (dict_from_tuple size) all_kernels num_cands o_nthread

    (* Separate the levels above the microkernel into the l1/l2/l3 portions *)
    (* Note: in this function, this is useless (concatenation right after), but
        it might be useful later *)
    |> List.map (function
        | Left ({ base; factor;_} as sk, simple_choices) ->
          let l2, l3 = Tree_search.take_until_level_single (base_from_sing sk) l2_size simple_choices in 
          let l1, l2 = Tree_search.take_until_level_single (base_from_sing sk) l1_size l2 in
          let to_tile = List.map (fun (d, s) -> d, Tile s) in
          base, Left factor, (to_tile (l1 @ l2 @ l3))
        | Right ({base= {varying_dim; base_kern;_} as base; u1; i1; u2 ;i2;},
                 double_choices) ->
          let mlambda = module_from_arg varying_dim i1 u1 i2 u2 base_kern in
          let l2, l3 = take_until_level_pair mlambda l2_size double_choices in 
          let l1, l2 = take_until_level_pair mlambda l1_size l2 in
          let to_tile = List.map (map_snd @@ function  Tile_spec.T s   -> Tile s
                                                   | Tile_spec.Merge -> Merge (i1, i2)) in
          base, Right (i1, u1, i2, u2), (to_tile (l1 @ l2 @ l3))
      )

  (* Main entry point for gen_random_from_tree function *)
  let gen_random_from_tree size num_cands o_nthread =
    gen_random_from_tree_aux None size num_cands o_nthread

  let gen_random_partial_tile size num_cands stride =
    Tree_search.random_over_partial_microkernel Y
      [X, 1; (*C, 256;*) F, 32; H, 1; W, 1]     (* Note: impose AVX512 vectorization here ! *)
      12 num_cands size stride

  let show_random_partial_tile size num_cands =
    gen_random_partial_tile size num_cands false
    |> List.iter (print_endline % [%show: Conv.ConvTypes.tile list])


  (* === Conversion functions back to a classical Conv scheme === *)

  open Conv
  open Conv.ConvTypes

  (* Auxilliary function that build the µkernel part of the scheme from the choices made *)
  let to_scheme_build_ukernel {varying_dim; base_kern;_} parameters d =
    if equal_dim varying_dim d then
      match parameters with
        Left f | Right (Left (f, _, _, _, _, _)) ->
        Some (U (f, d))
      | Right (Right _) ->
        Some (ULambda d)
    else match List.assoc d base_kern with
        1 -> None
      | n -> if equal_dim F d then Some (U (n / A_info.A.vec_size,  d))
        else Some (U (n,  d))

  (* Insert Hoist_vars at the first occurence of the reuse dim (C) *)
  let to_scheme_insert_hoist l = match l with
    | T (f, d)::tail when Conv.equal_dim d C ->
      T (f, d) :: Hoist_vars [C] :: tail
    | Tile_exact (f, d)::tail when Conv.equal_dim d C ->
      Tile_exact (f, d) :: Hoist_vars [C] :: tail
    | _ -> failwith "C dim not here ? "

  (* parameters : Left f => single microkern, no split
   * Right Left (f, dim, i1, a1, i2, a2)  => single microkern, split at a
   * higher level on dimension dim
   * Right (Right (i1, a1, i2, a2) => double microkerne *)
  let to_scheme_aux ({kern_order; _} as uk) parameters tiling =
    let ukernel = (V F)::(List.filter_map (to_scheme_build_ukernel uk parameters) kern_order) in

    (* Levels above the microkernel - only produces T/TLambda/Lambda_apply (div) *)
    let to_spec (dim, f) = match f with
      | Tile f -> T (f,  dim)
      | Split (_, _) -> TLambda dim
      | Merge (_, _) ->
        begin match parameters with
          | Right (Left (_, split_dim, iter1, arg1, iter2, arg2)) ->
            Lambda_apply (split_dim, [Iter iter1, Arg arg1; Iter iter2, Arg arg2;])
          | Right (Right (iter1, arg1, iter2, arg2)) ->
            Lambda_apply (dim, [Iter iter1, Arg arg1; Iter iter2, Arg arg2;])
          | Left _ -> assert false
        end
    in
    ukernel @ (List.map to_spec tiling |> to_scheme_insert_hoist)

  (* Create the strategy from the choices made, i.e.:
    the µkernel class (uk),
    the single/combination of µselected from it (parameters)
    and tiling (list of strategy above it)
    Note /!\ tiling assume a single list (not a quadruplet of list for caches+RAM) *)
  let to_scheme (uk, parameters, tiling) = match parameters with
    | Left f -> to_scheme_aux uk (Left f) tiling
    | Right (iter1, arg1, iter2, arg2) -> 
      to_scheme_aux uk (Right (Right (iter1, arg1, iter2, arg2))) tiling
  

  (* === TVM interface things === *)

  (* TVM style output, to be passed to Stephane *)
  let to_scheme_from_list parameters l =
    let to_spec_ukernel (dim, f) = match f with
        Tile f when equal_dim dim F -> [V F; U (f / A_info.A.vec_size, F)]
      | Tile f  -> [U (f,  dim)]
      | Split (_, _) -> [ULambda ( dim)]
      | Merge (_, _) -> let i1, a1, i2, a2 = Either.find_right parameters
      |> function None -> assert false | Some (i1, a1, i2, a2) -> i1, a1, i2, a2 in
        [Lambda_apply ( dim, [Iter i1, Arg a1; Iter i2, Arg a2])] in
    let to_spec (dim, f) = match f with
        Tile f -> T (f,  dim)
      | Split (_, _) -> ULambda ( dim)
      | Merge (_, _) ->
        begin match parameters with
            Right (iter1, arg1, iter2, arg2) ->
            Lambda_apply ( dim, [Iter iter1, Arg arg1; Iter iter2, Arg arg2;])
          | Left _ -> assert false
        end in
        let before_c, c_and_after =
          let rec loop acc = function
            | (C, s) :: t -> List.rev acc, (C, s) ::t 
            | h :: t -> loop (h::acc) t 
            | [] -> assert false in
          loop [] l in
    let rec insert_hoist l = match l with
      | T (f, d)::tail when Conv.equal_dim d C ->
        T (f, d) :: Hoist_vars [C] :: tail
      | h :: tail -> h :: (insert_hoist tail)
      | [] -> failwith "C is absent" in
        ((List.concat_map to_spec_ukernel before_c) @ List.map to_spec c_and_after)
        |> insert_hoist

  let take_until p init =
    let rec loop l acc = function [] -> List.rev l, []
                                | h::t -> match p acc h with
                                  | Some acc' -> loop (h::l) acc' t
                                  (* Should h go with l or t ? *)
                                  | None -> List.rev (h::l), t in
    loop [] init

  (* Tactic used by stephane *)
  let take_all_reductions red_list l  =
    take_until
      (fun ldims (d,_) ->
         match (List.remove ldims d) with
           [] -> None | l -> Some l)
      red_list l

  let tensorize_ukernel_only l =
    take_all_reductions [C] l

  let choose_level_with_nohw l =
    if List.for_all (fun (d,_) -> ((not @@ equal_dim H d) && (not @@ equal_dim W d))) l
    then        take_all_reductions [C] l
      (*
      then let l_in, l_out =  take_all_reductions [C] l in
        match l_out with h::t -> l_in @ [h], t
                       | [] -> l_in, []
      *)
    else take_all_reductions [C; H; W] l

  let to_tvm_predicate choose_tile_level ({varying_dim; kern_order; base_kern;_},
              parameters,
              (l1, l2, l3, mem)) =
    let build_ukernel d =
      if equal_dim varying_dim d then
        match parameters with
          Right (_iter1, arg1, _iter2, arg2) ->
          d, Split (arg1, arg2)
        | Left f -> (d, Tile f)
      else (d,  Tile (List.assoc d base_kern)) in
    let ukernel = List.map build_ukernel kern_order in
    let complete_scheme = ukernel @ l1 @ l2 @ l3 @ mem in
    let update_d d m = List.modify d (( * ) m) in
    let split_scheme l =
      List.map (function (d, Merge (a1, a2))
                       | d, Split (a1, a2) -> (d, a1), (d, a2)
                       | d, Tile t -> (d, t), (d, t)) l
      |> List.split in
    let split_scheme_either l =
      List.fold_left (fun acc (d, factor) -> match acc, factor with
          | Left l, Tile t -> Left (update_d d t l)
          | Right (l1, l2), Tile t -> Right (update_d d t l1, update_d d t l2)
          | Left l, (Split (a1, a2) | Merge (a1, a2)) -> Right (update_d d a1 l, update_d d a2 l)
          | Right (l1, l2), Merge (a1, a2) ->
            let t1 = List.assoc d l1
            and t2 = List.assoc d l2 in
            Left (List.modify d (fun _ -> a1 * t1 + a2 * t2) l1)
          |  Right _, Split _ -> assert false
        ) (Left init_dim_ones) l in
    let l_tensorize, l_out =
      choose_tile_level complete_scheme in
    let size_on_dim d =
      List.fold_left (fun s (d', s') -> if equal_dim d d' then s * s' else s) 1 in
    match split_scheme_either l_tensorize with
      Left ltens ->
      (* Shouldn't be any merge out of ltens in this case *)
      let l_out = List.map (function (_, (Merge _ | Split _)) -> assert false
                                   | d, Tile t -> d, t) l_out in
      [l_out, ltens, size_on_dim Y (l_out @ ltens), to_scheme_from_list parameters l_tensorize]
    | Right (l1, l2) ->
      let l1_tens, l2_tens = split_scheme l_tensorize
      and l1_out, l2_out = split_scheme l_out in
      let into_fp = List.map (map_snd (fun t -> Tile t)) in
      [l1_out, l1, size_on_dim Y (l1_out @ l1), to_scheme_from_list parameters @@ into_fp l1_tens;
       l2_out, l2, size_on_dim Y (l2_out @ l2), to_scheme_from_list parameters @@ into_fp l2_tens]

  let to_tvm = to_tvm_predicate choose_level_with_nohw
end


(* ================================================== *)


(* Function for random uniform strategy selection in the non-divisibility case *)
(* Author: Guillaume *)
module NonDivRandomSelection(A_info : Arch_info_t) = struct
  open Execution
  open Schemes(A_info)
  open Conv
  open Conv.ConvTypes

  (* Considered strategy space for non-divisible:
    - Microkernel (exact)
    - Sizes of the tiles above must be multiple of the microkernel, but not of the problem size
      => We can have partial tiles (using Tile_exact), but nothing must cut a microkernel
  *)
  (* Random algorithm selection (non divisible):
    - Pick randomly the microkernel (as usual).
        (If combination, put lambda as early as possible, ie right above the C)
    - For each dimension d, select a number of level l_d between 1 and 4
    - For each dimension/level (d,l) (except the last level),
        pick a number between [2, problem size] and sort them
        note: even if the ratio is exact, we do not "T" to improve perf
            (it would have an effect only if we have 2 consecutives T)
      => Build a "bag" of possibilities (dim, random_size) for every dimension
    - For the permutation: draw the possibility uniformly over all the remaining dimensions
      => When selected, take the new size, and iterate until the bag is empty
  *)
  (* Random algorithm selection (divisible):
    Note: space is Ttile, with µkernel and full divisibility, but no imposed permutation
    - Same procedure than the non divisibile space, except for the tile size selection for a dimension
    - For each dimension/level (d,l) (except the last level),
        Get all decomposition of "problem size" in d factor
          (factors must be different than 1, if no possible, reduce l, l=1 is always possible)
        Then pick uniformly randomly one of these decomposition
      => Build a "bag" of possibilities (dim, ratios) for every dimension
  *)


  (* Uniform random selection of µkernel (don't forget Random.self_init) *)
  let select_randomly_ukernel sizes =
    let all_ukernel = gen_all_uk sizes in
    (* Note: function comes from Batteries:
      List.enum : https://ocaml-batteries-team.github.io/batteries-included/hdoc2/BatList.html
      Random.choice : https://ocaml-batteries-team.github.io/batteries-included/hdoc2/BatRandom.html *)
    let rand_ukernel = Random.choice (List.enum all_ukernel) in
    rand_ukernel

  (* Get the size of a selected µkernel, to be applied to the output of gen_all_uk *)
  let get_ukernel_sizes ukernel = match ukernel with
    | Left (suk : Tree_search.sing_uk) -> 
      let lassoc_sizes = suk.base.base_kern in
      (suk.base.varying_dim, suk.factor)::lassoc_sizes
    | Right (duk : Tree_search.double_uk) -> (* Combination of microkernel *)
      let lassoc_sizes = duk.base.base_kern in
      let f = duk.i1 * duk.u1 + duk.i2 * duk.u2 in
      (duk.base.varying_dim, f)::lassoc_sizes

  (* Build the strategy corresponding to the µkernel (without the reuse loop above it) *)
  let build_uk_scheme ukernel = 
    let (uk, parameters) = match ukernel with
      | Left (suk : Tree_search.sing_uk)  ->
        (suk.base, Left suk.factor)
      | Right (duk : Tree_search.double_uk) ->
        (duk.base, Right (Right (duk.i1, duk.u1, duk.i2, duk.u2)))
    in
    (V F) :: (List.filter_map (to_scheme_build_ukernel uk parameters) uk.kern_order)


  (* Function of tile size selection - non divisible *)
  (* l_tile_sizes: (dim, [list of tile sizes above microkernel, including prbl size, increasing order]) *)
  let pick_tile_sizes_non_div remaining_sizes l_numlevel_dim =
    let l_tile_sizes : (dim * int list) list = List.fold_left (fun acc (d,nlvl) ->
      let rem_size = List.assoc d remaining_sizes in

      (* Draw nlvl-1 random numbers in [1; rem_size-1] *)
      let l_tile_sizes_dim = ref [] in
      for _ = 1 to nlvl-1 do
        if (rem_size==1) then
          l_tile_sizes_dim := 1 :: !l_tile_sizes_dim
        else
          l_tile_sizes_dim := ((Random.int (rem_size-1))+1) :: !l_tile_sizes_dim;
      done;
      
      (* Adding the last one (problem size) *)
      l_tile_sizes_dim := rem_size :: !l_tile_sizes_dim;

      (* Sort them + remove equality *)
      let l_sorted_ts_dim = List.sort_uniq compare !l_tile_sizes_dim in

      (* Store them*)
      (d, l_sorted_ts_dim)::acc
    ) [] l_numlevel_dim in
    l_tile_sizes


  (* Function of tile size selection - non divisible *)
  (* l_tile_sizes: (dim, [list of tile sizes above microkernel, including prbl size, increasing order]) *)
  let pick_tile_sizes_div remaining_sizes l_numlevel_dim =
    let l_tile_sizes : (dim * int list) list = List.fold_left (fun acc (d,nlvl) ->
      let rem_size = List.assoc d remaining_sizes in

      (* Get all possibilities of decomposition in nlvl parts *)
      let ll_decomp = Arithmetic.decompose rem_size nlvl in

      let i_rand = Random.int (List.length ll_decomp) in
      let l_tfactor_dim = List.nth ll_decomp i_rand in

      (d, l_tfactor_dim)::acc
    ) [] l_numlevel_dim in
    l_tile_sizes


  (* Debug function - pretty-printing association list of (dim, sizes): dim*int *)
  let print_assoc_sizes ppf lassoc_sizes =
    List.iter (fun (dim, size) ->
      Printf.fprintf ppf "(%s, %d) " (string_from_dim dim) size
    ) lassoc_sizes;
    ()

  (* Debug function - pretty-printing association list of (dim, lsizes): dim*(int list) *)
  let print_tilesize_bag ppf l_tile_sizes =
    let rec print_int_list ppf lint = match lint with
      | [] -> ()
      | h::t ->
        Printf.fprintf ppf "%d, " h;
        print_int_list ppf t
    in
    List.iter (fun (dim, lsizes) ->
      Printf.fprintf ppf "(%s, [%a]) " (string_from_dim dim)
        print_int_list lsizes
    ) l_tile_sizes;
    ()


  (* Replace the first "Tile_exact" by a "Tile_gexact" for every dim (used for non-div space) *)
  (* Rule for the positioning of Tile_partial/Tile_gexact/Tile_exact given a dimension:
    [Maybe several U/T from the µkernel, divisible]
       THEN [one Tile_partial]
       THEN [several Tile_gexact]
       THEN [a Tile_exact at the end]
  *)
  let add_tilepartial scheme =
    (* Add a tile partial at the start, and a tile_exact at the end with Tile_gexact at the middle *)
    (* If start=end, add a "T" instead *)
    (* Returns (new_scheme, list of dim encountered after the current atom) *)
    let rec add_tilepartial_aux ldim_encountered scheme = match scheme with
      | [] -> ([], [])
      | (Tile_exact (i, d)) :: t ->
        if (List.mem d ldim_encountered) then
          (* Dimension d was already seen before (this is not the first occurence) *)
          let (nt, ldim_encountered_after) = add_tilepartial_aux ldim_encountered t in
          if (List.mem d ldim_encountered_after) then
            (* Dimension d is also after that => we are in the middle*)
            ((Tile_gexact (i, d)) :: nt, ldim_encountered_after)
          else
            (* This is the last occurence of d, but not the first one*)
            ((Tile_exact (i, d)) :: nt, d::ldim_encountered_after)
        else
          (* First occurence of dimension d *)
          let (nt, ldim_encountered_after) = add_tilepartial_aux (d::ldim_encountered) t in
          if (List.mem d ldim_encountered_after) then
            (* Dimension d is also after that => we are in the first occurrence *)
            ((Tile_partial (i, d)) :: nt, ldim_encountered_after)
          else
            (* Single occurence of d for this dimension => R is enough *)
            ((R d) :: nt, d::ldim_encountered_after)
      | h :: t ->
        let nt, ldim_encountered_after = add_tilepartial_aux ldim_encountered t in
        (h :: nt, ldim_encountered_after)
    in
    let (scheme, _) = add_tilepartial_aux [] scheme in
    scheme


  (* Decide and insert a lambda expression in the scheme, above the hoist_vars [C] *)
  let insert_lambda ukernel_info tile_scheme =
    (* Insert the scheme_elem (lambda) above the Hoist_vars [C] in the scheme *)
    let rec insert_above_hoist_vars scheme_elem tile_scheme = match tile_scheme with
      | [] -> failwith "No Hoist vars here"
      | Hoist_vars _ ::r -> (List.hd tile_scheme) :: scheme_elem :: r
      | e::r -> e::(insert_above_hoist_vars scheme_elem r)
    in
    match ukernel_info with
    | Left _  -> tile_scheme
    | Right (duk : Tree_search.double_uk) ->
      let uk = duk.base in
      let lambda_atom = Lambda_apply (uk.varying_dim,
            [(Iter duk.i1, Arg duk.u1); (Iter duk.i2, Arg duk.u2)])
      in
      (* Insert lambda_atom right above the hoist_vars *)
      insert_above_hoist_vars lambda_atom tile_scheme



  (* === Main functions === *)

  (* Auxilliary function of the main functions *)
  let random_select_non_div_div_aux is_non_div pick_tile_sizes sizes =
    let (size_f,size_c,size_y,size_x,size_h,size_w) = sizes in
    let lassoc_sizes = [ (F, size_f); (C, size_c); (Y, size_y);
        (X, size_x); (H, size_h); (W, size_w)]
    in

    (* Microkernel selection *)
    Random.self_init ();
    let rand_ukernel = select_randomly_ukernel sizes in
    let lassoc_ukernel_sizes = get_ukernel_sizes rand_ukernel in

    (* Pick a number of tile level => build an association list *)
    let l_numlevel_dim = List.map (fun dim ->
      let nlevel = (Random.int 4) + 1 in
      (dim, nlevel)
    ) Conv_search_type.kernel_dim_order in

    (* For each dimension/levels, pick the sizes of the tiles *)
    (* Must be a multiple of ukernel sizes *)
    let remaining_sizes = List.map (fun (d, prb_size) ->
      let uker_size = List.assoc d lassoc_ukernel_sizes in
      assert( (prb_size mod uker_size) == 0);
      (d, prb_size / uker_size)
    ) lassoc_sizes in

    (* DEBUG
    Printf.printf "lassoc_ukernel_sizes = %a\n" print_assoc_sizes lassoc_ukernel_sizes;
    Printf.printf "lassoc_sizes = %a\n" print_assoc_sizes lassoc_sizes;
    Printf.printf "remaining_sizes = %a\n" print_assoc_sizes remaining_sizes;
    Printf.printf "l_numlevel_dim = %a\n" print_assoc_sizes l_numlevel_dim;
    *)


    (* l_tile_sizes: (dim, [list of tile sizes above microkernel, including prbl size, increasing order]) *)
    let l_tile_sizes = pick_tile_sizes remaining_sizes l_numlevel_dim in

    (* DEBUG
    Printf.printf "l_tile_sizes = %a\n" print_tilesize_bag l_tile_sizes;
    *)


    (* Permutation - building the scheme above the µkernel from l_tile_sizes *)
    (* is_first => impose "C" as the first dimension above the µkernel *)
    let rec select_dim is_non_div scheme_out is_first l_tile_sizes =
      (* Termination*)
      if (l_tile_sizes==[]) then scheme_out else
      
      let (d, lsizes) = if (is_first) then
        let d = C in
        let lsizes = List.assoc d l_tile_sizes in
        (d, lsizes)
      else
        let num_dim_remaining = List.length l_tile_sizes in
        let rand_draw = Random.int num_dim_remaining in
        List.nth l_tile_sizes rand_draw
      in

      assert(List.length lsizes > 0);

      (* Note: Schemes avec div de microkernel force le lambda à être juste au dessus de la dim de reuse *)
      let new_atom = if is_non_div then
          (* lsizes contains a size divided by the µkernel size over that dimension *)
          let uker_size = List.assoc d lassoc_ukernel_sizes in (* Note: assume lambda_apply already done *)
          let size = (List.hd lsizes) * uker_size in
          [ Tile_exact (size, d) ]
        else
          (* We have a ratio directly in lsizes *)
          let ratio = List.hd lsizes in
          [ T (ratio, d)]
      in

      let nscheme_out = scheme_out @ new_atom  in

      let nlsizes = List.tl lsizes in
      let n_l_tile_sizes =
        if (nlsizes==[]) then (List.remove_assoc d l_tile_sizes)
        else (d, nlsizes)::(List.remove_assoc d l_tile_sizes)
      in

      select_dim is_non_div nscheme_out false n_l_tile_sizes
    in

    (* Building the full scheme: µkern + [dim_C + Hoist + Lambda + tile_scheme] *)
    let uk_scheme = build_uk_scheme rand_ukernel in

    let tile_scheme = select_dim is_non_div [] true l_tile_sizes in

    (* DEBUG
    Printf.printf "uk_scheme = %s\n" (tile_to_string uk_scheme);
    Printf.printf "tile_scheme = %s\n" (tile_to_string tile_scheme);
    *)

    (* Adding the Hoist_vars above the C dimension *)
    let tile_scheme = to_scheme_insert_hoist tile_scheme in
    let tile_scheme = insert_lambda rand_ukernel tile_scheme in

    let tile_scheme = if (is_non_div) then
        add_tilepartial tile_scheme
      else
        tile_scheme
    in

    (* DEBUG
    Printf.printf "tile_scheme (post-preprocessing) = %s\n\n" (tile_to_string tile_scheme);
    *)

    let scheme_all = uk_scheme @ tile_scheme in

    (* DEBUG
    Printf.printf "scheme_all = %s\n\n" (tile_to_string scheme_all);
    *)

    scheme_all

  (* Main function for non-divisibility random picking *)
  let random_select_non_div sizes =
    random_select_non_div_div_aux true pick_tile_sizes_non_div sizes

  (* Main function for divisibility random picking *)
  let random_select_div sizes =
    random_select_non_div_div_aux false pick_tile_sizes_div sizes


  (* Function that repeats the random picking a amount of time *)
  let repeat_random_draw fdraw num_draw arg =
    let l_drawed_cand = List.fold_left (fun acc _ ->
      (fdraw arg)::acc
    ) [] (Arithmetic.range_exc 0 num_draw) in
    l_drawed_cand

end



(* ================================================== *)


(* Function for the evaluation of Ioopt informations *)
(* Author: Guillaume *)
module IooptSelection(A_info : Arch_info_t) = struct
  open Execution
  open Schemes(A_info)
  open NonDivRandomSelection(A_info) (* For some auxilliary functions *)
  open Conv


  (* Main function - select randomly a good microkernel, then call Ioopt on top of it and get its sizes,
    then arrange a bit its sizes to the closest multiple of the microkernel size (partial tiles)
    Thus, this is technically one-shot, one a microkernel is chosen *)
  let random_select_ioopt_partial_mickerdiv sizes =
    let (size_f,size_c,size_y,size_x,size_h,size_w) = sizes in
    let _lassoc_sizes = [ (F, size_f); (C, size_c); (Y, size_y);
        (X, size_x); (H, size_h); (W, size_w)]
    in

    (* Microkernel selection (only random part) *)
    Random.self_init ();
    let rand_ukernel = select_randomly_ukernel sizes in
    let _lassoc_ukernel_sizes = get_ukernel_sizes rand_ukernel in

    (* TODO: use Ioopt, arrange it and build "tile_scheme" *)
    let tile_scheme = [] in
    (* TEMP  +  lassoc_sizes + lassoc_ukernel_sizes with "_"*)



    (* Building the whole scheme *)
    let uk_scheme = build_uk_scheme rand_ukernel in

    (* Note: dim C should normally be the first dim of the permutation (done in ioopt.ml) *)
    (* We still need to insert the Hoist_var above it, and the lambda *)
    let tile_scheme = to_scheme_insert_hoist tile_scheme in
    let tile_scheme = insert_lambda rand_ukernel tile_scheme in

    (* Wrapping up *)
    let scheme_all = uk_scheme @ tile_scheme in
    scheme_all



end
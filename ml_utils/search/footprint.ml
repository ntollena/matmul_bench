open Batteries
open Conv_search_type
open Common.Utils

let floats_to_cache_line fnum = fnum / 4
let cache_line_size = 16
let cache_line_sizef = float_of_int cache_line_size

let ceil_cache_line inner_size =
  int_of_float @@ Float.ceil (float_of_int inner_size /. cache_line_sizef)
  |> Int.max 1

let input_footprint _f c y x h w  =
  (x + w - 1) * (y + h - 1) * (ceil_cache_line c)

let param_footprint f c _y _x h w  =
  w * h * c * (ceil_cache_line f)

let output_footprint f _c y x _h _w  =
  x * y * (ceil_cache_line f)

type footprint = {input : int; params: int;
                  output : int; global: int;
                  volume : int;
                 } [@@deriving show {with_path = false}]

let tensor_compute f c y x h w =
  let apply fn = fn f c y x h w in
  let input = apply input_footprint
  and params = apply param_footprint
  and output = apply output_footprint in
  let global = input + params + output in
  let volume = input + params + 2 * output in
  {input; params; output; global; volume}

type fp = Tile of int
        | Split of int * int
        | Merge of  int * int [@@deriving show {with_path = false}]

type fact = Simpl of int | Pair of int * int

(* Multi-split *)
let get fp_list dim = List.assoc dim fp_list

let init_dim_ones = List.map (fun d -> d, 1) all_of_dim

let init_fp = List.map (fun d -> d, Simpl 1) all_of_dim

let footprint  =
  scanl (fun fp (d, f) -> match f with
      | Tile f ->
        List.modify d (function Pair (f1, f2) -> Pair (f * f1, f * f2)
                         | Simpl f' -> Simpl (f' * f)
          ) fp
      | Split (f1, f2) ->
        List.modify d (function Pair _ -> failwith "Already split"
                         | Simpl f -> Pair (f1 * f, f2 * f)
          ) fp
      | Merge (u1, u2) ->
        List.modify d (function Simpl _ -> failwith "Not split"
                         | Pair (f1, f2) -> Simpl (f1 * u1 + f2 * u2)
          ) fp
    ) init_fp

(* Auguste-like Data volume *)
let footprint_split  =
  let update_list d f l =
    List.modify d (( * ) f) l in
  let init_dim_split d f1 f2 =
    (update_list d f1 init_dim_ones, update_list d f2 init_dim_ones) in
  let update_right (l1, l2) (d, f) = match  f with
      Merge (f1, f2) | Split (f1, f2) ->
      update_list d f1 l1,
      update_list d f2 l2
    | Tile f ->
      update_list d f l1,
      update_list d f l2 in
  function
  | Left [] | Right [] -> failwith "Empty"
  | Right ((_, Merge _) :: _)
  | Left ((_, Merge _) :: _) -> failwith "Merge should not be first"
  | Left ((_, Split _) :: _) -> failwith "Found a split in left"
  | Left ((d, Tile f) :: remaining) ->
    scanl (fun fp (d, f) ->
        match  f with
          Merge _ | Split _ -> failwith "Error"
        | Tile f -> update_list d f fp
      ) (update_list d f init_dim_ones) remaining
    |> Either.left
  | Right ((d, Tile f) :: remaining) ->
    scanl update_right (init_dim_split d f f) remaining
    |> List.split |> Either.right
  | Right ((d, Split (f1, f2)) :: remaining) ->
    scanl update_right (init_dim_split d f1 f2) remaining
    |> List.split |> Either.right

let fp_compute fp_list =
  let get = get fp_list in
  let f, c, y, x, h, w = get F, get C, get Y, get X, get H, get H in
  tensor_compute f c y x h w

let level_dim_mul level1 level2 =
  List.fold_left2 (fun prod (d1, l1) (d2, l2) ->
      assert (equal_dim d1 d2);  prod * (l2 / l1))
    1 level1 level2

let all_footprint =  function
    Left l -> Left (List.map fp_compute l)
  | Right (l1, l2) -> Right (List.map fp_compute l1, List.map fp_compute l2)

let fp_outer_max =
  let compute_fp_outer = List.last %> fun {global;_} -> global in
  footprint_split
  %> all_footprint
  %> function Left fp -> fp |> compute_fp_outer
            | Right (fp1, fp2) ->
              max (compute_fp_outer fp1) (compute_fp_outer fp2)

let out_of_cache cache_size {global;_} =
  global >= cache_size

let compute_volume tile_in_cache out_of_cache  =
  let volume level = (fp_compute level).volume  in
  match out_of_cache with
  | Some (dim_out, t_out, remaining_sizes) ->
    let first_out = List.modify dim_out (( * ) t_out) tile_in_cache in
    let remaining_prod = List.fold_left (fun p (_, t) -> p * t) 1 remaining_sizes in
     remaining_prod * volume first_out
  | None ->
    volume tile_in_cache

let data_volume_cache_level cache_size levels =
  let volume level = (fp_compute level).volume  in
  let compute_list loops =
    let l_in_cache, l_out_cache =
      List.span (Bool.not % out_of_cache cache_size % fp_compute) loops in
    match l_out_cache with
    | [] ->  List.last l_in_cache |> volume
    | (first_out :: nexts) ->
      match List.Exceptionless.last nexts with
        Some last_level -> level_dim_mul first_out last_level * volume first_out
      | None  ->   volume first_out in
  match levels with
    Left loops -> compute_list loops
  | Right (loops_left, loops_right) ->
    compute_list loops_left + compute_list loops_right

let global {global;_} = global

let compute_footprint fp_list =
  let get = get fp_list %> function Pair (u1, u2) -> [u1; u2]
                                  | Simpl f -> [f] in
  let fl, cl, yl, xl, hl, wl = get F, get C, get Y, get X, get H, get H in
  let all_comb = cartesian [fl; cl; yl; xl; hl; wl] in
  let map_tensor_footprint fp = List.map (function [f; c; y; x; h; w] ->
      fp f c y x h w
                                                 | _ -> assert false)
      all_comb in
  let input = map_tensor_footprint input_footprint
  and params =  map_tensor_footprint param_footprint
  and output = map_tensor_footprint output_footprint  in
  let rec map3 f l1 l2 l3 = match l1, l2, l3 with
    | [], [], [] -> []
    | h1::t1, h2::t2, h3::t3 -> (f h1 h2 h3)::(map3 f t1 t2 t3)
    | _ -> failwith "Lists are not of equal lengths" in
  map3 (fun input params output ->
      let global = input + params + output in
      let volume = input + params + 2 * output in
      {input; params; output; global; volume}
    ) input params output

let tensor_footprint =
  footprint %> List.map compute_footprint

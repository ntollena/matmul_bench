open Batteries
open Common.Utils
open Conv_search_type
open Ppx_python_runtime
open Tree_search

let () = Random.self_init ()

let example () =
  let size = [F, 256; X, 68; Y, 68; C, 256; W, 3; H, 3]  in

  let microkernel = [F, 32; Y, 17; C, 1; X, 1; Y, 1; W, 1; H,1] in 
  let l1_a =
    extend_l1_l2
      ~microkernel 
      size
    |> Option.get
  in
(*
  print_endline
  @@ [%show: search_tree ]
  @@ l1_a;
*)
  let branches = collect_branches l1_a in
(*
  print_endline
  @@ [%show: (dim * int) list list] branches;
*)
  Printf.printf "num branches %d\n" @@ List.length @@ branches;
  let r_12 = Random.multi_choice 12 @@ List.enum branches in
  List.iter (print_endline % pretty_print ~microkernel ) @@ List.of_enum r_12


type cdims = Single of dim
           | Join of dim * dim

let show_cdims = function Single d -> show_dim d
                        | Join (d1, d2) -> show_dim d1 ^ " + " ^ show_dim d2

type array = {name : string; dims : string list; multiplier : int} [@@deriving show]

type program = {
  dims : dim list;
  arrays : array list;
  reuse : (array * dim list) list;
}

let convolution  =
  let output = {name = "output";
                dims = ["F"; "X"; "Y";];
                multiplier = 2;
               }
  and input = {name = "input";
               dims = [ "C"; "X + W"; "Y + H";];
               multiplier = 1;
              }
  and params = {name = "params";
                dims = [ "F"; "C"; "W"; "H"];
                multiplier = 1;
               } in
  {dims = [F; C; X; Y; W; H];
   arrays = [output; input; params];
   reuse = [output, [C; W; H]; input, [F;]; params, [X; Y]]
  }

(* Finding permutation by calling ioopt via python api *)
let py_of_program ({dims; arrays; reuse} : program) =
  let dims = [%python_of: string list] @@ List.map show_dim dims
  and arrays =
    arrays
    |> List.map
      (fun {name; dims; multiplier} ->
         name,
         (dims,
          multiplier)
         |> [%python_of: string list * int]
      )
    |> Py.Dict.of_bindings_string
  and reuse = List.map
      (fun ({name;_}, dim_list) -> name, [%python_of : string list] @@ List.map show_dim dim_list)
      reuse
              |> Py.Dict.of_bindings_string
  in
  dims, arrays, reuse

let check_ukernel_params (f,c,y,x,h,w)
    {varying_dim; base_kern; range = rmin, _;  _}  =
  let pair_sizes = [F, f; C, c; X, x; Y, y; W, w; H, h] in
  let check_size (dim, size) =
    if equal_dim dim varying_dim then
      size >= rmin
    else
      size >= List.assoc dim base_kern  in
  List.for_all check_size pair_sizes

let py_of_kern (f,c,x,y,w,h) kernel_class =
  let sizes = [F, f; C, c; X, x; Y, y; W, w; H, h] in
  if check_ukernel_params (f,c,y,x,h,w) kernel_class
  then
    Some (kernel_repr sizes kernel_class
          |> List.map (fun (d, s) -> show_dim d, Py.Int.of_int s)
          |> Py.Dict.of_bindings_string)
  else None

(*
type ioopt_params = {l1_size: int; l2_size : int; l3_size : int;
                     l1_bw: float; l2_bw: float; l3_bw: float;
                     size: int * int * int * int * int * int;
                     kernel_class : kernel_spec;
                     name :string;
                    } [@@deriving show {with_path = false}, eq, yojson]
*)

(* same struct without name *)
type ioopt_params = {l1_size: int; l2_size : int; l3_size : int;
                     l1_bw: float; l2_bw: float; l3_bw: float;
                     size: int * int * int * int * int * int;
                     kernel_class : kernel_spec;
                    } [@@deriving show {with_path = false}, eq, yojson]



let build_ioopt_params (module Ainfo : Arch_info_t) size kernel_class  =
  let open Ainfo in
  if check_ukernel_params size kernel_class 
  then Some {size; kernel_class; l1_size; l2_size; l3_size; l1_bw; l2_bw; l3_bw}
  else None

(*
let forget_name {size; kernel_class; name = _; l1_size; l2_size;
                 l3_size; l1_bw; l2_bw; l3_bw}: ioopt_params_no_name =
  {size; kernel_class; l1_size; l2_size; l3_size; l1_bw; l2_bw; l3_bw}
*)
  

type perm = dim list * dim list * dim list * dim list
[@@deriving show {with_path = false}, eq, yojson]

let cache_file_path () =
  let filepath = Common.Env.find_root () ^ "/ml_utils/perm_cache.json" in
  if Sys.file_exists filepath then Some filepath
  else None

let read_from_file filename =
  Yojson.Safe.from_file filename
  |> [%of_yojson: (ioopt_params * perm) list]
  |> Result.get_ok

let write_to_file dictname params_perm =
  [%to_yojson: (ioopt_params * perm) list ] params_perm
  |> Yojson.Safe.to_file dictname

let py_build_values_from_params 
    {l1_bw;l2_bw;l3_bw;l1_size;l2_size;l3_size;size=f,c,x,y,w,h;kernel_class} =
  let () = if not @@ Py.is_initialized () then Py.initialize ~version:3 () in
  let lsizes  = "cache", [%python_of: int list] [l1_size; l2_size; l3_size]
  and bw_list = "bw", [%python_of: float list] [l1_bw; l2_bw; l3_bw]
  and dims = "dims", [%python_of: int list] [f;c;x;y;w;h]
  and ukernel = py_of_kern  (f,c,x,y,w,h) kernel_class
  and n_levels = "n_levels", Py.Int.of_int 3 in
  Option.map (fun ukernel ->
      Py.Dict.of_bindings_string [lsizes; bw_list; dims; "microkernel", ukernel; n_levels]
    ) ukernel 

let py_build_values ainfo size kernel_class =
  Option.bind (build_ioopt_params ainfo size kernel_class) py_build_values_from_params

let py_build_values_program program params =
  let () = if not @@ Py.is_initialized () then Py.initialize ~version:3 () in
  let dims, arrays, reuse = py_of_program program
  and values = py_build_values_from_params params in
  Option.map (
    fun values -> Py.Dict.of_bindings_string
        ["dims", dims; "arrays", arrays; "reuse", reuse; "values", values]
  ) values

let py_build_params program ainfo sizes kernel_class =
  let () = if not @@ Py.is_initialized () then Py.initialize ~version:3 () in
  let dims, arrays, reuse = py_of_program program
  and values = py_build_values ainfo sizes kernel_class in
  Option.map (fun values ->
      Py.Dict.of_bindings_string
        ["dims", dims; "arrays", arrays; "reuse", reuse; "values", values])
    values


let tee f x = f x; x

let (let*) o f = Option.bind o f
let (let+) o f = Option.map f o


(* Extract the loop permutation for Ioopt (and only the permutation) *)
let ioopt_permutation_from_params ({kernel_class; size;_} as params) =
  let* params_py = py_build_values_program convolution params in
  let* ukernel = py_of_kern size kernel_class in
  let () = if not @@ Py.is_initialized () then Py.initialize ~version:3 () in
  let ioopt = Py.import "ioopt" in
  let open Pyops in
  let+ l1, l2, l3, mem =
    Some (ioopt.&("get_perm_from_microkernel_direct_yaml") [|params_py; ukernel|]
    |> [%of_python: string list list]
    |> List.map (List.map dim_from_string)
    |> (function [l1; l2; l3; mem] -> l1, l2, l3, mem
               | _ -> failwith "Invalid ioopt output")) in
  (* Make sure C is the first dimension around the ukernel *)
  let () = Py.finalize () in
  let add_c = function C::_ as l -> l | l -> C::l in
  add_c l1, l2, l3, mem

(* Extract the loop permutation and the tile sizes recommendation from Ioopt *)
let ioopt_perm_sizes_from_params ({kernel_class; size;_} as params) =
  let* params_py = py_build_values_program convolution params in
  let* ukernel = py_of_kern size kernel_class in
  let () = if not @@ Py.is_initialized () then Py.initialize ~version:3 () in
  let ioopt = Py.import "ioopt" in
  let open Pyops in


  let+ l1, l2, l3, mem = Some (
    let output_ioopt = ioopt.&("get_perm_sizes_from_microkernel_direct_yaml") [|params_py; ukernel|]
      |> [%of_python: (string * int) list list]
    in
    let dim_int_from_str (s, i) = (dim_from_string s, i) in
    let output_ioopt_with_dim = List.map (List.map dim_int_from_str) output_ioopt in
    (function
      | [l1; l2; l3; mem] -> l1, l2, l3, mem
      | _ -> failwith "Invalid ioopt output") output_ioopt_with_dim
  ) in
  let () = Py.finalize () in

  (* WARNING! The sizes are not the ratio between 2 levels of tiling *)
  (* C : if not in the first position of the permutation, we set it to "1", by default *)
  (*    => We might want to change that (through the microkernel sizes we send to Ioopt) *)

  (* Make sure C is the first dimension around the ukernel *)
  let add_c_1 = function (C,_)::_ as l -> l | l -> (C, 1)::l in
  add_c_1 l1, l2, l3, mem



let ioopt_permutation ainfo sizes kernel_class =
  let* params = build_ioopt_params ainfo sizes kernel_class in
  ioopt_permutation_from_params params

let ioopt_permutation_cached cachefile ainfo sizes kernel_class =
  let params_perm = read_from_file cachefile in
   build_ioopt_params ainfo sizes kernel_class
   |> Option.map (fun params -> match List.assoc_opt params params_perm with
       | Some perm -> perm
       | None -> Option.get @@ ioopt_permutation ainfo sizes kernel_class)

(* reimplementation of permutation search *)

(* Algo : Find the dimension that maximizes number of tensor reuse.
 * Use it, then remove tensor that do not have reuse along this dimension
 * This is actually not quite interesting as is, as reuse dimensions are actually very
 * symetrical
 * We should refine this *)
let perms reuse = 
  let count_dims reuse dim =
    List.count_matching
      (fun (_, reuse_dims) -> List.exists (equal_dim dim) reuse_dims) reuse in
  let find_best_dim reuse_list dims =
    let _, (best_dim, _) = List.map (fun d -> d, count_dims reuse_list d) dims
                           |> min_max_by (fun (_, c) -> c) in
    best_dim,
    List.filter (fun (_, reuse_dims) -> List.mem best_dim reuse_dims) reuse_list,
    List.remove dims best_dim in
  unfold (fun (reuse, dims) -> if List.is_empty dims then None
           else let best_dim, next_reuse, remaining = find_best_dim reuse dims in
             Some ((next_reuse, remaining), best_dim)) (reuse, all_of_dim)

open Batteries
open Common.Utils
open Conv_search_type
open Tile_spec
open Common.Arithmetic
(* open Footprint *)

let get fp_list dim = List.assoc dim fp_list

let update fp_list dim v = List.modify dim (( * ) v) fp_list

let init_dim_ones = List.map (fun d -> d, 1) all_of_dim

type sing = Sg

type double = D

type sing_uk = {base: kernel_spec; factor: int} [@@deriving show]
type double_uk =  {base: kernel_spec; u1: int; i1: int; u2: int; i2: int; }[@@deriving show]

let sing_from_uk base factor =
  {base; factor}

let base_from_sing {base={varying_dim; base_kern;_}; factor;_} =
      (varying_dim, factor) :: base_kern

let double_from_uk base i1 u1 i2 u2 =
  {base; u1;i1;u2;i2; }

type ('tile_choice, 'state) search_tree =
    Complete
  | Root of ('tile_choice, 'state) search_tree list
  | Expanded of dim * 'tile_choice * ('tile_choice, 'state) search_tree list
  | Pending of dim * 'tile_choice * 'state [@@deriving show {with_path=false}]

type (_, _) choices =
    Simple : (sing_uk, simple_choice) choices
  | Double : (double_uk, double_choice) choices

type 's tree = T: (('uk, 'tile_choice) choices * 'uk * ('tile_choice, 's) search_tree) -> 's tree

type 's choice_lists = L :(('uk, 'tile_choice) choices * 'uk *
                           (dim * 'tile_choice) list * 's) -> 's choice_lists

let rec fold_tree f r p c  = function
  | Complete -> c
  | Root l ->  r @@ List.map (fold_tree f r p c) l
  | Expanded (d, i, l) ->
    f d i @@ List.map (fold_tree f r p c) l
  | Pending (d, i, s) -> p d i s

let count t =
  fold_tree (fun _ _ -> List.sum) List.sum (fun _ _ _ -> 1) 1 t

let collect_branches t = fold_tree
    (fun d i l -> List.concat_map (List.map @@ List.cons (d, i)) l)
    List.concat (fun d i _ -> [[d, i]]) [[]] t

let l1_size =(* 32k: 2 ^^ 15 / 2 ^^ 6 *) 512
let l2_size = Int.(2 ** 14)

module Tree_expand(Choice: Dim_Tile_t) = struct

  type 'state tree = (Choice.tile_choice, 'state) search_tree

  (* Pick randomly dimensions/coefficients (d/c) over a list of choice *)
  let pure_random tile_state_init =
    let rec loop_inner current state =
      match Choice.choose_and_update state with
      | Some choices when not (List.is_empty choices) ->
        let (d, c, s') = Random.choice (List.enum choices) in
      (* print_endline @@ Choice.show_tile_state state; *)
        loop_inner ((d,c) :: current) s'
      | None -> List.rev current
      | Some _ -> failwith "Weird, isn't it ?" in
      loop_inner [] tile_state_init

  (* Also pick randomly dimensions/coefficients (d/c) over a list of choice,
      but impose that the first picked dimension is C (streaming dimension) *)
  let random tile_state_init =
    let rec loop_inner is_first current state =
      match Choice.choose_and_update state with
      | Some choices when not (List.is_empty choices) ->
        let choices = if is_first
          then List.filter (function (C, _, _) -> true | _ -> false) choices
          else choices
        in
        assert(choices!=[]); (* If assert failed, then no dim C remaining *)
        let (d, c, s') = Random.choice @@ List.enum choices in
        (* print_endline @@ Choice.show_tile_state state; *)
        loop_inner false ((d,c) :: current) s'
      | None -> List.rev current
      | Some _ -> failwith "Weird, isn't it ?" in
    loop_inner true [] tile_state_init

  (* filter returns a list of (dim * int) either where Left (dim, size) means
   * that this branch is meant to be extended further, while Right (dim, size)
   * means we should suspend the search for now *)
  let rec expand_and_filter tile_state search_state filter =
    match Choice.choose_and_update tile_state with
      None -> [Complete]
    | Some choices ->
      filter tile_state search_state choices
      |> List.map (function
            Left (search_state, dim, choice) ->
            Expanded (dim, choice,
                      expand_and_filter
                        (Choice.update_state tile_state dim choice)
                        search_state
                        filter)
          | Right (state, dim, choice) ->
            Pending (dim, choice, state))

  let rec remove_empty =
    let remove_and_filter_if_empty l =
      let l = List.map remove_empty l in
      (* This accounts for empty list *)
      if List.for_all Option.is_none l then None
      else Some (List.filter_map Fun.id l) in
    function
    | Root l ->
      Option.map (fun l -> Root l) (remove_and_filter_if_empty l) 
    | Expanded (d, s, l) ->
      Option.map (fun l -> Expanded (d,s,l)) (remove_and_filter_if_empty l) 
    | Pending (d, s, state) -> Some (Pending (d, s, state))
    | Complete -> Some Complete

  let extend_from_scratch select tile_state_init search_state_init =
    Root (expand_and_filter
            tile_state_init
            search_state_init
            (fun tile_state sstate ->
               List.filter_map (select tile_state sstate))
         )
    |> remove_empty

  let extend_from_scratch_gen select tile_state_init search_state_init =
    Root (expand_and_filter
            tile_state_init
            search_state_init
            (fun tile_state sstate ->
               select tile_state sstate)
         )
    |> remove_empty

  (*  state -> (dim * int) -> (state * dim * int, dim * int) either option *) 
  (* (dim * int) list -> (state -> dim -> int -> state)
   * -> restart:(state -> (dim * int) list -> (state * dim * int, dim * int) either option)
   * -> (dim * int) size -> state -> search_tree -> search_tree *)
  let restart_search init_tstate init_sstate build_state finalize_state restart =
    let rec loop tile_state sstate = function
        Complete ->  Complete
      | Root l -> Root (List.map (loop tile_state sstate) l)
      | Expanded (d, size, ll) ->
        Expanded (d, size,
                  let tstate = Choice.update_state tile_state d size in
                  List.map (loop tstate (build_state sstate d size)) ll)
      | Pending (d, choice, _) ->
        Expanded (d, choice,
                  let tstate = Choice.update_state tile_state d choice in
                  (* no need for finalize_state to return the same kind of state as build_state *)
                  let sstate' = finalize_state @@ build_state sstate d choice in
                  expand_and_filter tstate sstate' restart) in
    loop init_tstate init_sstate

  (* cache_size: int -> state -> (dim * int) -> (state * dim * int, dim * int) either option *) 
  (* (dim * int) list -> (state -> dim -> int -> state)
   * -> restart:(state -> (dim * int) list -> (state * dim * int, dim * int) either option)
   * -> (dim * int) size -> state -> search_tree -> search_tree *)
  let restart_search_from_state  init_tstate build_state restart =
    let rec loop tile_state = function
        Complete ->  Complete
      | Root l -> Root (List.map (loop tile_state) l)
      | Expanded (d, size, ll) ->
        Expanded (d, size,
                  let tstate = Choice.update_state tile_state d size in
                  List.map (loop tstate) ll)
      | Pending (d, choice, pending_state) ->
        Expanded (d, choice,
                  let tstate = Choice.update_state tile_state d choice in
                  (* no need for finalize_state to return the same kind of state as build_state *)
                  expand_and_filter tstate (build_state pending_state) restart) in
    loop init_tstate

  (*We take an ordered list of choices and a final state and rebuild the tree
   * from it *)
  let rebuild_tree: ((dim * Choice.tile_choice) list * 'state) list -> 'state tree =
    let rec take_drop_until p taken =  function
      | h :: t when p h -> take_drop_until p (h :: taken) t
      | l -> List.rev taken, l in
    let take_drop_until p = take_drop_until p [] in
    let rec loop state_lists state_clist =
      match state_clist with
      | (dim, choice) :: (h :: t), state ->
        let taken, remaining = take_drop_until ([%eq: dim * Choice.tile_choice]
                                                (dim, choice) %
                                               List.hd % fst) state_lists in
        List.cons (Expanded (dim, choice, loop (List.map (map_fst List.tl) taken)
                    (List.cons h t, state)))
        begin match remaining with h :: t -> loop t h | [] -> [] end
      | ([dim, choice], state) ->
        [Pending (dim, choice, state)]
      | _ -> failwith "Was not supposed to get empty list here"
         in
         fun choice_state_list ->
           match List.sort
                   (let cmp = [%ord: (dim * Choice.tile_choice) list] in
                    fun x y -> cmp (fst x) (fst y))
                   choice_state_list  with
           h :: t -> Root (loop t h)
           | [] -> assert false

  let collect_branches_and_rstatus t =
    fold_tree
      (fun d i l -> List.concat_map (List.map @@ Option.map @@ map_fst @@ List.cons (d, i)) l)
      List.concat (fun d i status -> [Some ([d, i], status)]) [None] t
    |> List.filter_map Fun.id

  let collect_branches_and_status init_tstate t =
    fold_tree
      (fun d i l -> List.concat_map (List.map @@ Option.map @@ map_snd @@ List.cons (d, i)) l)
      List.concat (fun d i status -> [Some (status, [d, i])]) [None] t
    |> List.filter_map
      (Option.map
         (fun (status, choice_list) ->
            status,
            choice_list,
            List.fold_left
              (fun state (d, c) ->
                 Choice.update_state state d c)
              init_tstate choice_list
         )
      )

  let select_volumes num_l1 init_tstate restart tree =
    collect_branches_and_status init_tstate tree
    |> Common.Utils.sort_by (fun (status, _, _) -> status) 
    |> List.take num_l1
    |> List.map (fun (rstate, clist, tstate) -> List.rev clist,
                                                expand_and_filter rstate tstate restart)
end

type conv_tensor = Execution.Conv.tensor = Input | Output | Params
[@@deriving show {with_path = false}, eq, enumerate]

let show_search_tree : (int, dim list * conv_tensor list * int) search_tree ->
  string = [%show: (int, dim list * conv_tensor list * int) search_tree]

let last_dim_and_tile_size tree =
  let open Common.Utils in
  fold_tree (fun d i ->
      List.concat_map (fun l -> List.map (map_snd @@ fun l -> update l d i) l))
    List.concat
    (fun d i _ -> [Some (d, i), init_dim_ones])
    ([None, init_dim_ones]) tree

(* Assuming tree is expanded until a given cache level *)
let compute_data_volumes microkernel original_size tree =
  let tile_c_d_t_list = last_dim_and_tile_size tree in
  List.map (fun (di_opt, tile_size) ->
      let updated_size =
        List.fold_left (fun tile (d, s) -> update tile d s) microkernel tile_size in 
      let remainings = List.map (fun (d, s) -> d, List.assoc d original_size / s ) updated_size in
      Footprint.compute_volume updated_size @@ Option.map (fun (d, i) -> d, i, remainings) di_opt)
    tile_c_d_t_list



let dim_from_tensor = function
  | Input -> [X; Y; W; H; C]
  | Output -> [X; Y; F]
  | Params -> [W; H; C; F]

let complement all l = List.filter (Bool.not % Fun.flip List.mem l) all
let reuses = complement all_of_dim % dim_from_tensor
let reuse_dims_from_tensor = function
  | Input -> [W; H; F]
  | Output -> [W; H; C]
  | Params -> [X; Y]

type grow_reuse = Grow | Reuse let is_grow = function Grow -> true | Reuse -> false

type search_state = { grow_or_reuse: grow_reuse; tensors_to_reuse : conv_tensor list option;
                      current_perm : dim list; first_branch: bool;
                    }


(* (module Dim_Tile_t) -> cache_size: int -> tile_state -> search_state ->
 * (dim * choice) -> (state * dim * choice, dim * choice) either option *) 
let filter_cache_level (type state choice)
    (module Tile: Dim_Tile_t with type tile_state = state and type tile_choice = choice)
    cache_size (tile_state: state)
    {grow_or_reuse; tensors_to_reuse; current_perm; first_branch}
    (dim, (choice: choice), tile_state_updated) = 
  let inter2 l1 l2 = List.filter (fun x -> List.mem x l2) l1 in
  let rec intersection l =  function hl :: tl -> intersection (inter2 l hl) tl
                                   | [] -> List.rev l   in
  let intersection = function [] -> []
                            | hl :: tl -> intersection hl tl |> List.unique in
  let dims_of_interest =
    if first_branch then [C]
    else Option.map_default (fun lt -> if is_grow grow_or_reuse
                              then List.concat_map dim_from_tensor lt
                              else intersection @@ List.map reuse_dims_from_tensor lt)
      all_of_dim tensors_to_reuse in
  let take_tensors_from_dims select_dim =
    List.filter (fun t -> List.mem dim @@ select_dim t) all_of_conv_tensor in
  let tensors_to_reuse = if is_grow grow_or_reuse
    then inter2
      (take_tensors_from_dims dim_from_tensor)
        (Option.default all_of_conv_tensor tensors_to_reuse)
    else  inter2
      (take_tensors_from_dims reuse_dims_from_tensor)
        (Option.default all_of_conv_tensor tensors_to_reuse) in
  let global_fp = Tile.footprint tile_state_updated in 
  (* Was dim used already at this level, does dim offer reuse, does fp overflow cache_size,
   * is d the same as the last dim used *)
  match (grow_or_reuse,
         List.mem dim current_perm,
         List.mem dim dims_of_interest,
         global_fp <= cache_size ) with
  (* if dim was already used at this level of cache,
   * or if dim is not a reuse dimension but the cache is not full yet
   * abort this branch *)
    Grow, false, true, false
(*
    -> 
    Some (true, Left ({grow_or_reuse = Reuse;
                 tensors_to_reuse = Some tensors_to_reuse;
                 current_perm = dim::current_perm;
                 first_branch = false
                },
                dim, choice))
*)
  | _, true, _,  _
    (* This one is weird
     * we require that the tile that overflows the cache is not a reuse dimension *)
  | Reuse, _, true, false
  | Reuse, _, false, true
  | Grow, _, false, _ -> None
  (* We continue only in this situation : 
   * the dimension was not used at this cache level, it is a reuse dimension,
   * and we did not overflow the cache yet *)
  | _, false, true, true ->
    Some (Left ({grow_or_reuse = if global_fp <= cache_size / 2 then Grow else Reuse;
                 tensors_to_reuse = Some tensors_to_reuse;
                 current_perm = dim::current_perm;
                 first_branch = false
                },
                dim, choice))
  (* Here we overflow the cache, which is a signal we should stop the branch
   * But at this precise point there is no need that dim is a reuse dimension 
   * - isn't it even better that it is not a reuse dimension ? *)
  | Reuse, _, false, false ->
    Some (Right
            ( (current_perm, tensors_to_reuse, Tile.data_volume tile_state (dim, choice)),
              dim, choice))

let filter_global (type choice state)
    (tile_mod: (module Dim_Tile_t with type tile_choice = choice and type tile_state = state))
    cache_size
    tile_state ({first_branch;_} as rstate) (dim_choices: (dim * choice * state) list) =
  let l = List.filter_map (filter_cache_level tile_mod cache_size tile_state rstate)
       dim_choices in
  if first_branch then
    let module M = (val tile_mod) in
    let size = function Left (_, _, c) | Right (_, _, c) -> M.choice_size c in
    (* TODO: find something less arbitrary *)
    let only_bigger_than32 = List.filter (fun choice ->size choice >= 32) l in
    if List.is_empty only_bigger_than32 then  l else only_bigger_than32
  else l


let extend_until_l1 ?(microkernel=init_dim_ones) size =
  let module S = Single_tile(struct let base_kern = microkernel end) in
  let open Tree_expand(S) in
  extend_from_scratch_gen
    (filter_global (module S) l1_size)
    (S.from_sizes size)
    {grow_or_reuse = Grow; tensors_to_reuse = Some [Params];
     current_perm=[]; first_branch =true}

let collect_l1_volume t =
  fold_tree (fun _ _ ->List.concat) List.concat
    (fun _ _ (_,_,vol) -> [vol]) [] t

let filter_and_build_back (type state choice)
    (module Tile: Dim_Tile_t with type tile_state = state and type tile_choice = choice)
    num_cand t =
  let open Tree_expand(Tile) in
  collect_branches_and_rstatus t
  |> List.sort (fun (_ ,(_,_,v1)) (_ ,(_,_,v2)) -> Int.compare v1 v2)
  |> List.take num_cand
  |> rebuild_tree

let extend_l1_l2 ?(microkernel=init_dim_ones) size  =
  let module S = Single_tile(struct let base_kern = microkernel end) in
  let open Tree_expand(S) in
  let (>>|) a f = Option.map f a in
  extend_until_l1 ~microkernel size
  >>| filter_and_build_back (module S) 3
  >>| 
  restart_search_from_state
    (S.from_sizes size)
    (fun (current_perm, tensors_to_reuse, _) ->
       let tensors = List.filter (fun t -> not @@ List.mem t tensors_to_reuse)
                                   all_of_conv_tensor in
       let perm = current_perm
                  |> List.take_while (fun d -> List.mem d @@ List.concat_map reuse_dims_from_tensor tensors_to_reuse)
                  in
       {grow_or_reuse = Grow; tensors_to_reuse = Some tensors;
        current_perm = perm; first_branch = false}
    )
    (fun fp state ->
      List.filter_map (filter_cache_level (module S) l2_size fp state)
    )
(*     (filter_global (module S) l2_size) *)
(*
  |> tee @@ Option.may
    (fun t -> let branches =collect_branches t in
      Printf.printf "%d branches at L2 level\n\n" @@ List.length branches)
*)

let module_from_arg var_dim i1 u1 i2 u2 base_kern: (module Lambda_t) =
  let module L = Lambda_tile( struct
      let var_dim = var_dim
      let u1 = u1
      let i1 = i1
      let u2 = u2
      let i2 = i2
      let base_kern = base_kern
  end) in
  (module L)

let extend_until_l1_pair var_dim i1 u1 i2 u2 base_kern size =
  let module L = (val module_from_arg var_dim i1 u1 i2 u2 base_kern) in
  let open Tree_expand(L) in
  extend_from_scratch_gen
(*     (fun ts ss -> filter_cache_level (module L) l1_size ts ss ) *)
    (filter_global (module L) l1_size)
    (L.from_sizes size)
    {grow_or_reuse = Grow; tensors_to_reuse = None;
     current_perm=[]; first_branch =true}

let extend_l1_l2_pair var_dim i1 u1 i2 u2 base_kern size  =
  let module L = (val module_from_arg var_dim i1 u1 i2 u2 base_kern) in
  let open Tree_expand(L) in
  let (>>|) a f = Option.map f a in
  extend_until_l1_pair var_dim i1 u1 i2 u2 base_kern size
  >>| 
  restart_search
    (L.from_sizes size)
    X (fun _ d _ -> d)
    (fun last_dim ->
       let tensors = List.find (fun t -> not (List.mem last_dim @@ dim_from_tensor t))
                                   all_of_conv_tensor in
       {grow_or_reuse = Grow; tensors_to_reuse = Some [tensors];
        current_perm=[last_dim]; first_branch = false}
    )
(*
    (fun fp state ->
       List.filter_map (filter_cache_level (module L) l2_size fp state )
    )
*)
    (filter_global (module L) l2_size)

let take_prefix p =
  let rec take_prefix p prev = function
    | h :: t  -> 
      let l = List.rev_append prev [h] in
      if p l then List.rev prev, h :: t else take_prefix p (h::prev) t
    | [] -> List.rev prev, [] in
  take_prefix p []

let compute_fp microkernel = List.fold_left (fun fp (d, s) -> update fp d s) microkernel

let take_until_level fp_compute lsize =
  take_prefix
    (fun l -> let gfp = fp_compute l in
      gfp >= lsize)

let take_until_level_single microkernel lsize =
  take_until_level (Footprint.global % Footprint.fp_compute % compute_fp microkernel) lsize

let take_until_level_pair (module T: Tile_spec.Lambda_t) lsize  =
  take_until_level (let open Footprint in
                    fp_outer_max % Either.right % T.to_fp)
    lsize

let pretty_print ?(microkernel=init_dim_ones) l =
  print_endline @@ [%show: (dim * int) list] l ;
  let l2, l3 = take_until_level_single microkernel l2_size l in 
  let l1, l2 = take_until_level_single microkernel l1_size l2 in
  [%show: (dim * int) list] l1
  ^ " | L1 | " ^ [%show: (dim * int) list] l2
  ^ " | L2 | " ^ [%show: (dim * int) list] l3
  ^ "\nFootprint : " 
  ^ string_of_int @@ Footprint.global @@ Footprint.fp_compute @@ compute_fp microkernel l

let complete_candidate original_size ukernel cand =
  let l2, l3 = take_until_level_single ukernel l2_size cand in 
  let l1, l2 = take_until_level_single ukernel l1_size l2 in
  let list_dict = List.fold_left (fun fp (d, s) -> update fp d s) ukernel cand in
  let remaining =
    List.map (fun d ->
        d, assoc d original_size / List.assoc d list_dict) all_of_dim in
  let remaining =
    List.filter_map (fun d -> 
      List.assoc d remaining
      |> function 1 -> None
                | s -> Some (d, s)
    ) [C; H; W; X; Y; F] in
  l1, l2, l3, remaining

let complete_candidate_pair original_size var_dim i1 u1 i2 u2 base_kern cand =
  let mlambda = module_from_arg var_dim i1 u1 i2 u2 base_kern in
  let l2, l3 = take_until_level_pair mlambda l2_size cand in 
  let l1, l2 = take_until_level_pair mlambda l1_size l2 in
  let module Mlambda = (val mlambda) in
  l1, l2, l3, Mlambda.complete_candidate original_size cand

let tree_from_uk_list : (dim * int) list -> (sing_uk, double_uk) Either.t list
  -> (dim list * conv_tensor list * int) tree list = 
  fun size uklist -> 
  List.filter_map (function Left ({base = {base_kern; varying_dim;_} ; factor;_} as
                           ukern) ->
      let microkernel =(varying_dim, factor) :: base_kern in 
      let (let+) o f = Option.map f o in
      let+ r = extend_until_l1 ~microkernel size in
      T (Simple, ukern, r)
                   | Right ({u1;i1;u2;i2;base = {varying_dim; base_kern;_} } as uk) ->
                     let (let+) o f = Option.map f o in
                     let+ t = extend_until_l1_pair varying_dim i1 u1 i2 u2 base_kern size in
                     T (Double, uk, t)
    )
                                         uklist

type rstate = (dim list * conv_tensor list * int)

let volume_of_l = function
  L (Simple, _, _, (_, _, v)) -> v 
  | L (Double, _, _, (_, _, v)) -> v 

(* More or less equivalent to a normal ADT for now... 
 * make sure it gets more coherent later *)
let collect_all_candidates: rstate tree -> rstate choice_lists list =
  function
    T (Simple, ({base={base_kern; varying_dim;_}; factor} as uk), t) ->
    (* TODO do a function that does this properly *)
    let module S = Single_tile(struct let base_kern = (varying_dim, factor) ::
                                                      base_kern end) in
    let open  Tree_expand(S) in
    collect_branches_and_rstatus t
    |> List.map (fun (dim_choice_list, rstate) ->
        L (Simple, uk, dim_choice_list, rstate)
      )
  | T (Double, ({base = {varying_dim; base_kern; _}; u1; i1; u2; i2; } as uk), t) ->
    let module Lambda_mod = (val module_from_arg varying_dim i1 u1 i2 u2
                                base_kern) in
    let open Tree_expand(Lambda_mod) in
    collect_branches_and_rstatus t
    |> List.map (fun (dim_choice_list, rstate) ->
        L (Double, uk, dim_choice_list, rstate)
      )

let select_trees num_cand lcands =
  Common.Utils.sort_by volume_of_l lcands
|> List.take num_cand

let rebuild_tree =
  function
        L (Simple, uk, dlist, rstate)  ->
        (* TODO do a function that does this properly *)
      let module S = Single_tile(struct let base_kern = base_from_sing uk end) in
      let open  Tree_expand(S) in
      T (Simple, uk, rebuild_tree [dlist, rstate] )
      | L (Double, ({base ={varying_dim;base_kern;_}; u1; i1; u2; i2; } as uk), dlist, rstate) ->
        let module Lambda_mod = (val module_from_arg varying_dim i1 u1 i2 u2
                                    base_kern) in
        let open Tree_expand(Lambda_mod) in
      T (Double, uk, rebuild_tree [dlist, rstate] )

let restart (type choice) (module DT: Dim_Tile_t with type tile_choice = choice) size =
  let open Tree_expand(DT) in
  restart_search_from_state (DT.from_sizes size)
           (fun (current_perm, tensors_to_reuse, _) ->
              let tensors = List.filter (fun t -> not @@ List.mem t tensors_to_reuse)
                  all_of_conv_tensor in
              let perm = current_perm
                         |> List.take_while (fun d -> List.mem d @@ List.concat_map reuse_dims_from_tensor tensors_to_reuse)
              in
              {grow_or_reuse = Grow; tensors_to_reuse = Some tensors;
               current_perm = perm; first_branch = false}
           )
           (fun fp state ->
              List.filter_map (filter_cache_level (module DT) l2_size fp state)
    )

let restart size =
  function
        T (Simple, uk, tree)  ->
      let module S = Single_tile(struct let base_kern = base_from_sing uk end) in
      T (Simple, uk, restart (module S) size tree)
      | T (Double, ({base={varying_dim; base_kern;_}; u1; i1; u2; i2; } as uk), tree) ->
        let module Lambda_mod = (val module_from_arg varying_dim i1 u1 i2 u2
                                    base_kern) in
        T (Double, uk, restart (module Lambda_mod) size tree)

let take_while_no_cands size n_l1 n_by_l1 n_total l1_cands =
  let sorted = Common.Utils.sort_by volume_of_l l1_cands in
  let rec take_until_enough n_l1_found n_t_found acc = function
      [] -> List.rev acc
    | l1_cand :: l1_tails -> 
      let tree = rebuild_tree l1_cand |> restart size in
      let cands = collect_all_candidates tree
                  |> Common.Utils.sort_by volume_of_l
                  |> List.take n_by_l1 in
      let n_taken = List.length cands in
      let n_t_found = n_t_found + n_taken
      and n_l1_found = n_l1_found + 1
      and acc = cands :: acc in
      if (n_l1_found >= n_l1  && n_t_found >= n_total) then
        List.rev acc
      else take_until_enough n_l1_found n_t_found acc l1_tails in
  take_until_enough 0 0 [] sorted


let all_candidates_sorted_lexi = fun size uklist -> 
  tree_from_uk_list size uklist
  |> List.concat_map collect_all_candidates
  |> take_while_no_cands size 1 1 1



(* Auxilliary debug function to pretty print list of (dim, size) *)
let print_list_dim_size ff lds =
  let rec print_list_dim_size_aux ff lds = match lds with
    | [] -> ()
    | (d,s) :: t ->
      Printf.fprintf ff "(%s, %d), " (string_from_dim d) s;
      print_list_dim_size_aux ff t
  in
  Printf.fprintf ff "[";
  print_list_dim_size_aux ff lds;
  Printf.fprintf ff "]"

(* Auxilliary debug function to pretty print list of (dim, list of prime factors) *)
let print_list_dim_lfactors ff ldlf =
  let rec print_list_factor ff lf = match lf with
    | [] -> ()
    | f :: t ->
      Printf.fprintf ff "%d, " f;
      print_list_factor ff t
  in
  let rec print_list_dim_lfactors_aux ff lds = match lds with
    | [] -> ()
    | (d,lf) :: t ->
      Printf.fprintf ff "(%s, [%a]), " (string_from_dim d) print_list_factor lf;
      print_list_dim_lfactors_aux ff t
  in
  Printf.fprintf ff "[";
  print_list_dim_lfactors_aux ff ldlf;
  Printf.fprintf ff "]"




(* Given "size", a list of problem size: "(dim, size)", if "o_nthread" is activated,
    then reserve randomly a certain amount of iteration from the parallel dimensions, according to
    2 algorithms (second one being a fallback of the first one)
  Returns the sizes of the parallel loops, and the size of the

*)
(* Added by Gui, to be used after µkern selection, and before filling the tiles above*)
let get_parallelization_reservation size o_nthread = match o_nthread with
  | None ->  []
  | Some nthread ->

    (* Algo: 1) reserve exactly nthread iteration of the parallel dimension for the outer levels *)
    (* More parallel dimensions can be taken later (during the normal random algo) *)
    (* 2) If a divisor is not present, then try to take at least "4*nthread" parallel iterations *)
    (* 3) If not possible, then take all of them *)


    (* 1) First try of distribution, if all divisors of nthread can be found *)

    (* l_parallel_dim : from "conv_search_type.ml" *)
    (* Getting the prime factors : arithmetic.ml :: list_prime_divisor *)
    let sizes_parallel = List.filter (fun (d,s) ->
      (List.mem d l_parallel_dim) && (s>1)
    ) size in
    let ldiv_par : (dim * (int list)) list =
      List.map (fun ((d, s) : dim * int) -> (d, list_prime_divisor s)) sizes_parallel in
    let ldiv_nthread = list_prime_divisor nthread in

    let (bworked, nsize_par, _) = List.fold_left (fun acc div_nthr ->
      (* Check if it is possible to get div_nthr from a parallel dimension *)

      (* Recursion invariants:
        - bworked : did we find the prime factors up to now? If false, the rest does not matter
        - nsize_par : size of the parallel loops that were selected
        - ldiv : list of divisor of nsize (kept in order to avoid recalculating them)
      *)
      let (bworked_acc, nsize_par_acc, ldiv_par_acc) = acc in
      if (not bworked_acc) then (false, nsize_par_acc, ldiv_par_acc) else

      (* We check the possibilities for div_nthr in ldiv_par_acc *)
      let lposs_dims = List.fold_left (fun lposs_acc (d, lpd) ->
        if List.mem div_nthr lpd then d::lposs_acc else lposs_acc
      ) [] ldiv_par_acc in

      (* If the divisor is not found => bworked = false (it did not work) *)
      if (List.is_empty lposs_dims) then (false, nsize_par_acc, ldiv_par_acc) else

      (* Selecting randomly a dimension (in lposs_dims), then updating everything *)
      let i_dim_selected = Random.int (List.length lposs_dims) in
      let dim_selected = List.nth lposs_dims i_dim_selected in

      (* Update nsize_par_acc by adding "div_nthr" to its corresponding entry *)
      let new_nsize_par_acc = match (List.assoc_opt dim_selected nsize_par_acc) with
        | None -> (dim_selected, div_nthr) :: nsize_par_acc
        | Some size ->
          (dim_selected, (div_nthr * size)) :: (List.remove_assoc dim_selected nsize_par_acc)
      in

      (* Update ldiv_par_acc by removing an element to its corresponding list of factor *)
      let new_ldiv_par_acc = match (List.assoc_opt dim_selected ldiv_par_acc) with
        | None -> failwith "get_parallelization_reservation - Method 1 : ldiv_par_acc should have an entry here"
        | Some lfactors -> (
          assert (List.mem div_nthr lfactors);
          let nlfactor = List.remove lfactors div_nthr in  (* Function is inside Batteries *)
          (dim_selected, nlfactor) :: (List.remove_assoc dim_selected ldiv_par_acc)
        )
      in
      (true, new_nsize_par_acc, new_ldiv_par_acc)
    ) (true, [], ldiv_par) ldiv_nthread in

    if bworked then
      let lscheme_par = nsize_par in  (* Data structure actually matches *)
      lscheme_par
    else begin

    (* DEBUG
    print_endline "get_parallelization_reservation : first method failed";
    Printf.printf "sizes_parallel = %a\n" print_list_dim_size sizes_parallel;
    Printf.printf "ldiv_par = %a\n" print_list_dim_lfactors ldiv_par;
    *)


    (* 2/3) Second try: grab randomly factors from ldiv_par until a threashold is reached
        or all are taken *)

    (* Arbitrary. Set up to have enough "full" iterations to compensate the non-full ones,
       without taking all of them for the parallel *)
    let threashold = nthread * 4 in

    (* While threasdhold not reached, select randomly a dim of ldiv_par, then one of its div *)
    (* Stoop if ldiv_par is empty *)
    let ref_cur_thr = ref 1 in
    let ref_nsize_par = ref [] in      (* Size of the selected parallel dim sizes *)
    let ref_ldiv_par = ref ldiv_par in (* List of prime factor *)

    while ( (not (List.is_empty !ref_ldiv_par)) && (!ref_cur_thr < threashold)) do

      (* DEBUG
      print_endline "get_par_resa - second method loop information:";
      Printf.printf " - ref_cur_thr = %d < %d\n" !ref_cur_thr threashold;
      Printf.printf " - ref_nsize_par = %a\n" print_list_dim_size !ref_nsize_par;
      Printf.printf " - ref_ldiv_par = %a\n" print_list_dim_lfactors !ref_ldiv_par;
      *)


      (* Select randomly a factor inside ref_ldiv_par *)
      (* Note: biais in the selection of the factor here: dim with a low number of factor
        have more chance being selected than uniform sampling. *)
      assert (not (List.is_empty !ref_ldiv_par));
      let i_dim_selected = Random.int (List.length !ref_ldiv_par) in
      let (dim_selected, lfactors) = List.nth !ref_ldiv_par i_dim_selected in

      assert (not (List.is_empty lfactors));
      let i_factor_selected = Random.int (List.length lfactors) in
      let fact_sel = List.nth lfactors i_factor_selected in

      (* Choice made: (dim_selected, fact_sel) *)
      ref_cur_thr := !ref_cur_thr * fact_sel;
      
      (* Update ref_nsize_par by adding "fact_sel" to its corresponding entry *)
      ref_nsize_par := (match (List.assoc_opt dim_selected !ref_nsize_par) with
        | None -> (dim_selected, fact_sel) :: !ref_nsize_par
        | Some size ->
          (dim_selected, (fact_sel * size)) :: (List.remove_assoc dim_selected !ref_nsize_par)
        );


      (* Update ref_ldiv_par by removing an element to its corresponding list of factor *)
      ref_ldiv_par := (match (List.assoc_opt dim_selected !ref_ldiv_par) with
        | None -> failwith "get_par_resa - Method 2 : ref_ldiv_par should have an entry here"
        | Some lfactors -> (
          assert (List.mem fact_sel lfactors);
          let nlfactor = List.remove lfactors fact_sel in  (* Function is inside Batteries *)
          if (List.is_empty nlfactor) then
            (List.remove_assoc dim_selected !ref_ldiv_par)
          else
            (dim_selected, nlfactor) :: (List.remove_assoc dim_selected !ref_ldiv_par)
        )
      );
      ()
    done;

    (* DEBUG
    print_endline "get_par_resa - second method done";
    *)

    let lscheme_par = !ref_nsize_par in
    lscheme_par
    end

(* Auxilliary function, remove the contribution of a tile from a full problem size *)
let remove_sizes_from_sizes ukersize fullsize =
  let nfullsize = List.fold_left (fun acc (d,s) ->
    match (List.assoc_opt d ukersize) with
    | None -> (d,s) :: acc
    | Some suk -> (
      assert (s mod suk == 0);
      (d, s/suk) :: acc
    )
  ) [] fullsize in
  nfullsize



(* Select randomly candidate microkernels + above dimensions ("random" function) *)
let random_candidates_from_uk : (dim * int) list -> (sing_uk, double_uk) Either.t list
    -> int
    -> int option
    -> (sing_uk * (dim * simple_choice) list, double_uk * (dim * double_choice) list) Either.t list =
  fun size ukerns num_cands o_nthread ->
  let from_uk = function
    | Left ({factor; base = {varying_dim; base_kern;_};_} as sk) ->
      let module S = Single_tile(struct
          let base_kern = (varying_dim, factor) :: base_kern
        end) in
      let open Tree_expand(S) in

      (* Remove the sizes of the microkernel before doing the par selection *)
      let base_kern = (varying_dim, factor) :: base_kern in
      let size_aboveuk = remove_sizes_from_sizes base_kern size in
      let l_par_level = get_parallelization_reservation size_aboveuk o_nthread in

      (* DEBUG
      Printf.printf "size (single) = %a\n" print_list_dim_size size;
      Printf.printf "base_kern = %a\n" print_list_dim_size base_kern;
      Printf.printf "size_aboveuk = %a\n" print_list_dim_size size_aboveuk;
      Printf.printf "l_par_level = %a\n" print_list_dim_size l_par_level;
      *)


      (* Remove the sizes of the full problem size, before randomly getting the rest *)
      let size_no_par = remove_sizes_from_sizes l_par_level size in

      (* DEBUG
      Printf.printf "size_no_par = %a\n\n" print_list_dim_size size_no_par;
      *)
      let above_uk = random (S.from_sizes size_no_par) in

      (* Don't forget to add the reserved parallel dim back at the end*)
      Left (sk, (above_uk @ l_par_level))

    | Right ({base ={varying_dim; base_kern;_}; u1; i1; u2; i2 ;_} as dk) ->
      let module L = Lambda_tile( struct
          let var_dim = varying_dim
          let u1 = u1
          let i1 = i1
          let u2 = u2
          let i2 = i2
          let base_kern = base_kern
        end) in
      let open Tree_expand(L) in

      (* Remove the sizes of the microkernel before doing the par selection *)
      let base_kern_lambda = List.remove_assoc varying_dim base_kern in
      let base_kern_lambda = (varying_dim, (u1*i1 + u2*i2)) :: base_kern_lambda in
      let size_aboveuk = remove_sizes_from_sizes base_kern_lambda size in

      let l_par_level = get_parallelization_reservation size_aboveuk o_nthread in

      (* DEBUG
      Printf.printf "size (double) = %a\n" print_list_dim_size size;
      Printf.printf "size_aboveuk = %a\n" print_list_dim_size size_aboveuk;
      Printf.printf "l_par_level = %a\n" print_list_dim_size l_par_level;
      Printf.printf "u1 = %d | i1 = %d | u2 = %d | i2 = %d\n\n" u1 i1 u2 i2;
      *)


      (* Remove the sizes of the full problem size, before randomly getting the rest *)
      let size_no_par = remove_sizes_from_sizes l_par_level size in
      let above_uk = random (L.from_sizes size_no_par) in

      let dbl_choice_l_par_level : (dim * double_choice) list
          = List.map (fun ((d, x) : dim * int) -> (d, Tile_spec.T x)) l_par_level in
      Right (dk, above_uk @ dbl_choice_l_par_level)
  in
  List.init num_cands @@ fun _ ->
  Random.self_init ();

  (* DEBUG
  print_int (List.length ukerns);
  let rec print_list_ukernel ff luks = match luks with
    | [] -> ()
    | Left h::t ->
      Printf.fprintf ff "%s,\n" ([%show: sing_uk] h);
      print_list_ukernel ff t
    | Right h::t ->
      Printf.fprintf ff "%s,\n" ([%show: double_uk] h);
      print_list_ukernel ff t
  in
  Printf.printf "[%a]" print_list_ukernel ukerns;
  print_newline ();
  *)

  (* Filter the ukerns that uses all the dim C already *)
  let ukerns = List.filter (fun uk -> match uk with
    | Left (sk : sing_uk) ->
      let size_c_uk = List.assoc C sk.base.base_kern in
      let size_c_prob = List.assoc C size in
      (size_c_uk < size_c_prob)
    | Right (dk : double_uk) ->
      let size_c_uk = List.assoc C dk.base.base_kern in
      let size_c_prob = List.assoc C size in
      (size_c_uk < size_c_prob)
  ) ukerns in

  (* DEBUG
  print_int (List.length ukerns);
  print_newline ();
  *)

  let uk = Random.choice @@ List.enum ukerns in
  from_uk uk

let random_over_partial_microkernel var_dim fixed_size max_range num_cands size
  stride =
  let f seed =
    let module P = Partial_tile (struct
                   let var_dim = var_dim
                   let fixed_size = fixed_size
                   let max_range = max_range
                   let name = if stride then
                       "microk_2_12_mask_store_cin_param_stride2"
                     else "microk_2_12_mask_store_cin_param"
                 end) in
    let open Tree_expand(P) in
    pure_random @@ P.from_sizes_rand seed size  (* Warning, seed deterministic ! *)
    |> P.to_ttile size in
  List.init num_cands f

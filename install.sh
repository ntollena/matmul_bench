ROOT=$(realpath .)
sudo apt install -y pkg-config opam
opam init
opam update
opam upgrade
opam switch create conv_bench 4.10.0
git submodule init && git submodule update
#cd $ROOT/blis && ./configure auto && make -j8
cd $ROOT/tensor-mapping/ml && opam install --yes tensor_loops .
cd $ROOT/ml_utils && opam install --yes mm_bench .
eval $(opam env)
